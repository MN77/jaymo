#!/bin/bash

for i in *.jmo; do
	echo
	echo "=== Test: $i"
	
	TEST="/tmp/jmo_manual_test.txt"
	#java -jar /home/mike/Prog/Java/Jar/jmo.jar "$i" "7" >$TEST 2>&1
	java -jar /home/mike/Dist/build/OUT/jaymo-cli-aio.jar "$i" "7" >$TEST 2>&1
	
	STORED="output/$i.txt"
	if( test -f $STORED; ) then
		diff -q -y $TEST $STORED || kdiff3 $STORED $TEST
	else
		cp $TEST $STORED
	fi
	
done
