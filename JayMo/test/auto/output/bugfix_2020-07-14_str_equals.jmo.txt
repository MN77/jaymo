true   |true   |true   |<Bool | Number>|<Bool | Number>|<Bool | Number>
false  |true   |true   |<Bool | Number>|<Bool | Number>|<Bool | Number>
false  |false  |true   |<Bool | Number>|<Bool | Number>|<Bool | Number>
<Chars>|<Chars>|<Chars>|true           |true           |true           
<Chars>|<Chars>|<Chars>|false          |true           |true           
<Chars>|<Chars>|<Chars>|false          |false          |true           

true   |false  |false  |<Bool | Number>|<Bool | Number>|<Bool | Number>
true   |true   |false  |<Bool | Number>|<Bool | Number>|<Bool | Number>
true   |true   |true   |<Bool | Number>|<Bool | Number>|<Bool | Number>
<Chars>|<Chars>|<Chars>|true           |false          |false          
<Chars>|<Chars>|<Chars>|true           |true           |false          
<Chars>|<Chars>|<Chars>|true           |true           |true           

true |false|false|false|false|false
false|true |false|false|false|false
false|false|true |false|false|false
false|false|false|true |false|false
false|false|false|false|true |false
false|false|false|false|false|true 

true |false|false|true |false|false
false|true |false|false|true |false
false|false|true |false|false|true 
true |false|false|true |false|false
false|true |false|false|true |false
false|false|true |false|false|true 

===== RESULT =====
4i
