-- DIRECT --
1234
-- FROM ROOT OUT --
1234
-- FROM LOOP OUT --
1234
1234
-- FROM FUNCTION OUT --
<Err>
Error   : Invalid source code
Message : Variable not initialized
Detail  : No value assigned to: bb
Call    : bb.print
Instance: Root
  @ var_access_err_2.jmo :6     bb.print
  @ var_access_err_2.jmo :30    this.incBB
  @ var_access_err_2.jmo :36    this.test1
</Err>
===== RESULT =====
null
