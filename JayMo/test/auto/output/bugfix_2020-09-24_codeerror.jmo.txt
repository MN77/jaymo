<Err>
Error   : Invalid source code
Message : Invalid amount of arguments
Detail  : Need 2, but got 1 for: <Str>.part
Call    : "fooooo".part(12.4)…
Instance: Root
  @ bugfix_2020-09-24_codeerror.jmo :4    "fooooo".part(12.4)…
</Err>
===== RESULT =====
null
