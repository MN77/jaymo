Error   : Invalid source code
Message : Strict! Open argument is forbidden!
Detail  : Please use brackets for arguments.
  @ open_arg_func_strict.jmo :3
===== RESULT =====
null
