Error   : Invalid source code
Message : Strict! Shortcut of variables and constants are not allowed!
Detail  : Please write 'a?' instead of 'a'!
  @ var_modifiers_1.jmo :6
===== RESULT =====
null
