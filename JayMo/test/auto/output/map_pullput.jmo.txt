a -> 4
b -> 3
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
---
r -> 5
s -> 6
t -> 7
[Map<3>,4]
r -> 5
s -> 6
t -> 7
[Map<3>,4]
---
r -> 5
s -> 6
t -> 7
[Map<3>,nil]
r -> 5
s -> 6
t -> 7
[Map<3>,nil]
---
a -> 4
b -> 3
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
a -> 4
b -> 0
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
r -> 5
s -> 1
t -> 7
a -> 4
b -> 0
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
a -> 4
b -> 1
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
---
a -> 4
b -> 1
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
a -> 4
b -> 2
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
a -> 4
b -> 2
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
z -> 0
a -> 4
b -> 2
c -> 2
d -> 1
e -> Map<4>
f -> Map<3>
z -> 0
a -> 4
b -> 3
c -> 2
d -> 1
e -> Map<4>
f -> Map<4>
z -> 0
y -> 2
===== RESULT =====
'a' -> 4i
'b' -> 3i
'c' -> 2i
'd' -> 1i
'e' -> 
  'z' -> 9i
  'y' -> 8i
  'x' -> 7i
  'w' -> 
    'r' -> 5i
    's' -> 2i
    't' -> 7i
    'a' -> 1i
'f' -> 
  'r' -> 5i
  's' -> 2i
  't' -> 7i
  'a' -> 1i
'z' -> 0i
'y' -> 2i
