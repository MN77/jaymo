1234567890123456789012345678901234567890.321z
--- 1 --- 
<BigDec>  123.0
<BigDec>  1.2345678901234567E39
--- 2 --- +
<BigDec>  124.0
<BigDec>  125.0
<BigDec>  1.2345678901234567E39
--- 3 --- -
<BigDec>  120.0
<BigDec>  119.0
<BigDec>  1.2345678901234567E39
--- 4 --- *
<BigDec>  615.0
<BigDec>  738.0
<BigDec>  2.4691357802469135E39
--- 5 --- /
<BigDec>  1.7571428571428571E1
<BigDec>  15.375
<BigDec>  6.1728394506172839E38
<BigDec>  2.4691357802469135E38
--- 6 --- %
<BigDec>  4.0
<BigDec>  3.0
<BigDec>  3.128
<BigDec>  0.322
<BigDec>  0.0
<BigDec>  4.1152263004115226E38
<BigDec>  0.322
<BigDec>  0.0
<BigDec>  2.4691357802469135E38
<BigDec>  3.322
<BigDec>  0.0
<BigDec>  1.7636684144620811E38
--- 7 --- ++
<BigDec>  124.0
<BigDec>  130.0
<BigDec>  131.0
<BigDec>  1.2345678901234567E39
<BigDec>  1.2345678901234567E39
<BigDec>  1.2345678901234567E39
--- 8 --- --
<BigDec>  122.0
<BigDec>  116.0
<BigDec>  115.0
<BigDec>  1.2345678901234567E39
<BigDec>  1.2345678901234567E39
<BigDec>  1.2345678901234567E39
--- 9 --- **
<BigDec>  15129.0
<BigDec>  425927596977747.0
<BigDec>  6.2820621517520215E41
<BigDec>  1.3641359902883583E3
<BigDec>  6.0893720256462994E130
<BigDec>  2.867971861733704E195
<BigDec>  +infinity
--- 10 --- //
<BigDec>  16.0
<BigDec>  2.0
<BigDec>  2.0
<BigDec>  1.4670096759437157E3
<BigDec>  1.4670096759437157E3
--- 11 --- neg/abs
<BigDec>  -256.0
<BigDec>  256.0
<BigDec>  256.0
<BigDec>  256.0
--- 12 --- log
<Dec>  8.0
<BigDec>  8.0
<BigDec>  8.0
<Dec>  12.0
<BigDec>  12.0
<BigDec>  12.0
--- 13 --- mod
<Int>  9
<BigDec>  9.0
<BigDec>  9.0
<BigInt>  10
<BigDec>  10.0
===== RESULT =====
nil
