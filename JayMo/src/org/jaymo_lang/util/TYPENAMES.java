/*******************************************************************************
 * Copyright (C): 2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

/**
 * @author Michael Nitsche
 * @created 04.01.2023
 */
public class TYPENAMES {

	public static final String[] ATOMIC = {"Nil", "Bool", "Byte", "Short", "Int", "Long", "Dec", "Float", "Double", "Char", "Str", "BigInt", "BigDec"};

	/**
	 * @apiNote Known internal types (not abstract)
	 */
	public static final String[] KNOWN = {
		// Manually
		"ConstLet",
		"FuncLet",
		"VarLet",
	};

	public static final String[] BLOCKED = {
		"Boolean", "String", "Character", "Integer", "Decimal", "Floating",
		"Function", "App", "Root",
		"Var", "Const", "VarLet", "ConstLet", "FuncLet", "MemLet",
		"True", "False", "Nil", "Null", "Infinity",
		"AssertError", "CodeError", "CustomError", "RuntimeError", "ExternalError", "JayMoError"
	};

	/**
	 * @apiNote Types that are abstract and can not be instantiated!
	 */
	//	TODO: Automatisieren?
	public static final String[] ABSTRACT = {
		// Manually
		"FileSys",
		"MagicConst",
		"AutoObject",
		"ObjectSimple",
		"ObjectToString",
		"MemLet",

		// Automatic cenerated
		// --> Without: "TreeNode", "Path", "Color"
		"AbstractFtpClient", "Atomic", "BaseCodec", "Chars", "Cipher", "DateTimeBase", "DecNumber", "Dialog", "Digest", "EventObject", "Immutable", "IntNumber", "Number", "Object",
		"Sequence", "SqlDatabase", "SWT_Component", "SWT_Control", "SWT_Layout", "SWT_Object", "SWT_Scrollable", "SWT_Widget", "Swing_Component", "Swing_Frame", "Swing_JComponent",
		"Swing_JTextComponent", "Swing_Object"
	};

}
