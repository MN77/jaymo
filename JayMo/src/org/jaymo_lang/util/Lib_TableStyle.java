/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.tablestyle.TableStyler;
import de.mn77.base.error.Err;


/*
 * Structure: table:columns:rows
 *
 * == DEFAULT ==
 * colAlign: _
 * colLines: .
 * rowLines: *
 *
 * == NO LINE ==
 * colLines: ' ', (0)
 * rowLines: ' ', (+)
 *
 * == VARIANTS ==
 * colAlign: LCR<^>
 * colLines: |!12
 * rowLines: -=
 * Table: buLCR<^>|!12-=
 */
/**
 * @author Michael Nitsche
 * @created 24.10.2022
 */
public class Lib_TableStyle {

	private enum CONV {
		OBJECT,
		IDENT,
		DESCRIBE
	}


	public static String compute(final CallRuntime cr, final ArrayTable<I_Object> tab, final String[] defs) {
		Err.ifNull(tab, defs);
		Err.ifNot(defs.length, 3);

		CONV conv = CONV.OBJECT;

		if(defs[0] != null) {

			if(defs[0].indexOf('I') >= 0) {
				conv = CONV.IDENT;
				defs[0] = FilterString.removeChar('I', defs[0]);
			}

			if(defs[0].indexOf('D') >= 0) {
				conv = CONV.DESCRIBE;
				defs[0] = FilterString.removeChar('D', defs[0]);
			}
		}
		final I_Table<Object> objectTable = new ArrayTable<>(tab.width(), tab.size());

		for(int y = 0; y < tab.size(); y++) {
			final Object[] row = new Object[tab.width()];
			for(int x = 0; x < tab.width(); x++)
				row[x] = conv == CONV.OBJECT
					? Lib_Java.jmoToJava(cr, tab.get(x, y), Object.class, false)
					: conv == CONV.IDENT
						? tab.get(x, y).toString(cr, STYPE.IDENT)
						: tab.get(x, y).toString(cr, STYPE.DESCRIBE);
			objectTable.add(row);
		}
		final TableStyler styler = new TableStyler();

		if(defs[0] != null)
			Lib_TableStyle.iSetTableStyles(cr, styler, defs[0]);
		if(defs[1] != null)
			Lib_TableStyle.iSetColumnStyles(cr, styler, defs[1]);
		if(defs[2] != null)
			Lib_TableStyle.iSetRowStyles(cr, styler, defs[2]);

		return styler.compute(objectTable);
	}

	private static void iSetColumnStyles(final CallRuntime cr, final TableStyler styler, final String def) {
		final Group2<String, String> split = Lib_TableStyle.iSplitColumnStyles(cr, def);
		int i = 0;

		for(final char c : split.o1.toCharArray()) {

			switch(c) {
				case '<':
				case 'L':
					styler.alignColumn(i, ALIGN.LEFT);
					break;
				case '^':
				case 'C':
					styler.alignColumn(i, ALIGN.CENTER);
					break;
				case '>':
				case 'R':
					styler.alignColumn(i, ALIGN.RIGHT);
					break;
				case '_':	// default
					break;

				default:
					Err.impossible(c);
			}
			i++;
		}
		styler.colLineStyles(split.o2);
	}

	private static void iSetRowStyles(final CallRuntime cr, final TableStyler styler, final String def) {
		for(final char c : def.toCharArray())
			switch(c) {
				case 'n':	// Whitespace line	(+/)
				case '-':	// Single line
				case '=':	// Double line

				case '*':	// Default

				case ' ':	// No line
					break;

				default:
					throw Lib_TableStyle.iThrowInvalidChar(cr, "row", c);
			}

		styler.rowLineStyles(def);
	}

	private static void iSetTableStyles(final CallRuntime cr, final TableStyler styler, final String def) {
		for(final char c : def.toCharArray())
			switch(c) {
				case 'b':
//				case 'B':
					styler.setBorder(true);
					break;
				case 'u':
//				case 'U':
					styler.setUnicode(true);
					break;
//				case 'l':
				case 'L':
				case '<':
					styler.alignDefault(ALIGN.LEFT);
					break;
//				case 'c':
				case 'C':
				case '^':
					styler.alignDefault(ALIGN.CENTER);
					break;
//				case 'r':
				case 'R':
				case '>':
					styler.alignDefault(ALIGN.RIGHT);
					break;
				case '|':
				case '!':
//				case '0':
				case '1':
				case '2':
					styler.colLineStyleDefault(c);
					break;
				case 'n': // +/
				case '-':
				case '=':
					styler.rowLineStyleDefault(c);
					break;

//				case '.':
//				case '…':
//					styler.rowDelimiter('·');
//					break;
				default:
					throw Lib_TableStyle.iThrowInvalidChar(cr, "table", c);
			}
	}

	private static Group2<String, String> iSplitColumnStyles(final CallRuntime cr, final String def) {
		final StringBuilder align = new StringBuilder(def.length());
		final StringBuilder lines = new StringBuilder(def.length());

		boolean onLine = false; // ALALALALALA.....

		for(final char c : def.toCharArray())
			switch(c) {
				case '<':
				case '^':
				case '>':
				case 'L':
				case 'C':
				case 'R':
				case '_':
					if(onLine)
						lines.append('.');
					align.append(c);
					onLine = true;
					break;
				case '|':
				case '!':
				case '1':
				case '2':

//				case '0':
				case ' ': // No line

				case '.': // Default
					if(!onLine)
						align.append('_');
					lines.append(c);
					onLine = false;
					break;
				default:
					throw Lib_TableStyle.iThrowInvalidChar(cr, "column", c);
			}

		return new Group2<>(align.toString(), lines.toString());
	}

	private static RuntimeError iThrowInvalidChar(final CallRuntime cr, final String part, final char c) {
		throw new RuntimeError(cr, "Invalid char for " + part + " definition", "Invalid: '" + c + "'");
	}

}
