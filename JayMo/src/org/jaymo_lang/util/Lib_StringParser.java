/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.util.List;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 16.09.2019
 * @apiNote
 *          Fast checks for parsing Strings
 */
public class Lib_StringParser {

	/**
	 * Schnelle Gültigkeitsprüfung für Chars
	 * Das erste Zeichen wird nicht mehr geprüft!
	 * Returns: < 0 on fail
	 * char.length (3|4) at success
	 **/
	public static final int checkChar(final String s, final boolean throwError, final DebugInfo debugInfo) {
		if(s.length() >= 8 && s.charAt(1) == '\\' && s.charAt(2) == 'u' && s.charAt(7) == '\'')
			return 8;
		if(s.length() >= 4 && s.charAt(1) == '\\' && s.charAt(3) == '\'')
			return 4;
		if(s.length() >= 3 && s.charAt(2) == '\'')
			return 3;

		if(throwError)
			throw new CodeError("Unclosed char", s, debugInfo);
		return -1;
	}

	/**
	 * @apiNote
	 *          Fast Validity-Check for []-Groups
	 * @return
	 *         Position at success
	 *         -1 on fail
	 **/
	public static final int checkGetOrList(final String s, final int offset, final boolean throwError, final DebugInfo debugInfo) {
		final char[] ca = s.toCharArray();
		int depth = 0;

		for(int idx = offset; idx < ca.length; idx++) {
			final char c = ca[idx];

			if(c == '\\') {
				idx++;
				continue;
			}

			if(c == '[') {
				depth++;
				continue;
			}

			if(c == ']') {
				depth--;
				if(depth == 0)
					return idx;
				continue;
			}

			// Filter Group
			if(c == '(') {
				idx = Lib_StringParser.checkGroup(s, idx, true, debugInfo); // +1 ?!?
				continue;
			}

			// Filter Strings
			if(c == '"') {
				idx += 1 + Lib_StringParser.checkString(s.substring(idx), true, debugInfo); // +1 ?!?
				continue;
			}

			// Filter Char
			if(c == '\'') {
				idx += Lib_StringParser.checkChar(s.substring(idx), true, debugInfo) - 1;
				continue;
			}
		}
		if(throwError)
			throw new CodeError("Unclosed get/list", s, debugInfo);
		return -1;
	}

	/**
	 * Schnelle Gültigkeitsprüfung für Get/Set
	 * Das erste Zeichen wird nicht mehr geprüft!
	 * Return: false on fail
	 * true at success
	 * !!! Achtung !!! Prüft keine Strings!
	 **/
	public static final boolean checkGetSet(final String s, final char open, final char close) {
		int level = 0;
		final char[] ca = s.toCharArray();

		for(int pos = 1; pos < ca.length; pos++) {
			final char c = ca[pos];

			if(c == open) {
				level++;
				continue;
			}

			if(c == close) {
				if(level == 0)
					// MOut.temp(s, pos, s.substring(pos+1),
					// s.substring(pos+1).matches("^\\s*=.*$"));
					return true;
				level--;
			}
		}
		return false;
	}

	/**
	 * Schnelle Gültigkeitsprüfung für Gruppen
	 *
	 * @return index from the last char of the group, -1 on fail
	 **/
	public static final int checkGroup(final String s, final int offset, final boolean throwError, final DebugInfo debugInfo) {
		final char[] ca = s.toCharArray();
		int depth = 0;

		for(int idx = offset; idx < ca.length; idx++) {
			final char c = ca[idx];

			if(c == '\\') {
				idx++;
				continue;
			}
			if(c == '(')
				depth++;
			if(c == ')')
				depth--;

			// Filter Strings
			if(c == '"') {
				idx += 1 + Lib_StringParser.checkString(s.substring(idx), true, debugInfo);
				continue;
			}

			// Filter Char
			if(c == '\'') {
				idx += Lib_StringParser.checkChar(s.substring(idx), true, debugInfo) - 1;
				continue;
			}
			if(depth == 0)
				return idx;
		}
		if(throwError)
			throw new CodeError("Unclosed group", s, debugInfo);

		return -1;
	}

	/**
	 * Schnelle Gültigkeitsprüfung für Strings
	 * Das erste Zeichen wird nicht mehr geprüft!
	 *
	 * @return: string.length at success, -1 on fail
	 *
	 *          TODO add Offset to start
	 **/
	public static final int checkString(final String s, final boolean throwError, final DebugInfo debugInfo) {
		final int sLen = s.length();

		for(int idx = 1; idx < sLen; idx++) {
			final char c = s.charAt(idx);

			if(c == '\\') {
				idx++;
				continue;
			}
			if(c == '"')
				return idx - 1;
		}
		if(throwError)
			throw new CodeError("Unclosed string", s, debugInfo);

		return -1;
	}

	/**
	 * @apiNote Fast validity check for a type name
	 * @return -1 on fail, next position at success
	 **/
	public static final int checkType(final String s, final int offset, final boolean throwError, final DebugInfo debugInfo) {
		final char[] ca = s.toCharArray();

		for(int idx = offset; idx < ca.length; idx++) {
			final char c = ca[idx];

			if(c == '<' || c >= 'A' && c <= 'Z')
				continue;
			else if(c >= 'a' && c <= 'z' || c >= '0' && c <= '9')
				continue;
			else if(c == '_')
				continue;
			else if(c == '>')
				return idx;

			else if(throwError)
				throw new CodeError("Illegal char in type", "Got: '" + c + "' in '" + s.substring(offset) + "'", debugInfo);
			else
				return -1;
		}
		if(throwError)
			throw new CodeError("Unclosed type", s, debugInfo);

		return -1;
	}

	public static Group2<String, String> getNextPar(final String s, final DebugInfo debug) {
		int offset = 0;

		final int offsetStart = s.charAt(0) == '?' ? 1 : 0;

		switch(s.charAt(offsetStart)) {
			case '[': // Liste
				offset = Lib_Parser.group('[', ']', s, debug).length() + 2;
				break;
			case '{': // For
				offset = Lib_Parser.group('{', '}', s, debug).length() + 2;
				break;
			// () will be parsed later
		}
//		int index=s.indexOf(','); // VERY BAD
		final int index = Lib_StringParser.search(s, offset, '(', ')', ',', debug);
		final String arg = index == -1 ? s : s.substring(0, index);
		final String rem = index == -1 ? "" : s.substring(index + 1);
		return new Group2<>(arg, rem);
	}

	/**
	 * @return Returns a string from the left with all chars, that could be a type, variable or constant.
	 */
	public static String parseIdentifier(final String s, final int offset) {

		for(int i = offset; i < s.length(); i++) {
			final char c = s.charAt(i);
			if(!(c == '_' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '?'))
				return s.substring(offset, i);
		}
		return offset == 0 ? s : s.substring(offset);
	}


//	/**
//	 * Sucht das nächste Vorkommen von einem Element aus 'search' auf der selben Ebene von open und close
//	 * Returns: -1 if nothing found
//	 **/
//	public static int search(final String s, final int offset, final char open, final char close, final String[] search, final DebugInfo debugInfo) {
//		int deep = 0;
//		final char[] ca = s.toCharArray();
//		for(int pos = offset; pos < s.length(); pos++) {
//			final char c = ca[pos];
//			if(c == open)
//				deep++;
//			if(c == close)
//				deep--;
//			if(deep == 0)
//				for(final String test : search)
//					//					if( c == test.charAt(0)) {
//					if(s.startsWith(test, pos))
//						return pos;
////					}
//
//			// Filter Strings
//			if(c == '"') {
//				pos += 1 + Lib_StringParser.checkString(s.substring(pos), true, debugInfo);
//				continue;
//			}
//
//			// Filter Char
//			if(c == '\'') {
//				pos += Lib_StringParser.checkChar(s.substring(pos), true, debugInfo) - 1;
//				continue;
//			}
//		}
//		return -1;
//	}

	/**
	 * Sucht das nächste Vorkommen von 'search' auf der selben Ebene von open und close
	 * Returns: -1 if nothing found
	 **/
	public static int search(final String s, final int offset, final char open, final char close, final char search, final DebugInfo debugInfo) {
		int deep = 0;
		final char[] ca = s.toCharArray();

		for(int pos = offset; pos < s.length(); pos++) {
			final char c = ca[pos];
			if(c == open)
				deep++;
			if(c == close)
				deep--;
			if(c == search && deep == 0)
				return pos;

			// Filter Strings
			if(c == '"') {
				pos += 1 + Lib_StringParser.checkString(s.substring(pos), true, debugInfo);
				continue;
			}

			// Filter Char
			if(c == '\'') {
				pos += Lib_StringParser.checkChar(s.substring(pos), true, debugInfo) - 1;
				continue;
			}

			// Filter List
			if(c == '[') {
				pos += Lib_Parser.group('[', ']', s.substring(pos), debugInfo).length();
				continue;
			}

			// Filter Deep
			if(c == '{') {
				pos += Lib_Parser.group('{', '}', s.substring(pos), debugInfo).length();
				continue;
			}
		}
		return -1;
	}

	/**
	 * Split a String by ',' but respect chars, strings, lists, ...
	 */
	public static List<String> splitArgs(final String s, final DebugInfo debugInfo) {
		final SimpleList<String> result = new SimpleList<>();
		final char[] ca = s.toCharArray();

		int start = 0;

		for(int pos = 0; pos < s.length(); pos++) {
			final char c = ca[pos];

			if(c == ',') {
				result.add(s.substring(start, pos));
				start = pos + 1;
				continue;
			}

			// Filter Strings
			if(c == '"') {
				pos += 1 + Lib_StringParser.checkString(s.substring(pos), true, debugInfo);
				continue;
			}

			// Filter Char
			if(c == '\'') {
				pos += Lib_StringParser.checkChar(s.substring(pos), true, debugInfo) - 1;
				continue;
			}

			// Filter Arguments
			if(c == '(') {
				pos += Lib_Parser.group('(', ')', s.substring(pos), debugInfo).length();
				continue;
			}

			// Filter List
			if(c == '[') {
				pos += Lib_Parser.group('[', ']', s.substring(pos), debugInfo).length();
				continue;
			}

			// Filter Deep
			if(c == '{') {
				pos += Lib_Parser.group('{', '}', s.substring(pos), debugInfo).length();
				continue;
			}
		}
		result.add(s.substring(start));

		return result;
	}

}
