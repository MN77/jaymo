/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.I_MagicGet;
import org.jaymo_lang.object.magic.I_MagicReplace;
import org.jaymo_lang.object.magic.var.A_MagicVar;
import org.jaymo_lang.object.magic.var.MV_CUR;
import org.jaymo_lang.object.magic.var.MV_EACH;
import org.jaymo_lang.object.magic.var.MV_SUPER;
import org.jaymo_lang.object.magic.var.MV_THIS;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 17.11.2020
 */
public class Lib_MagicVar {

	/**
	 * @apiNote All valid functions must be checked before!
	 */
	public static void checkForbiddenFuncs(final CallRuntime cr, final String method) {
		if(method != null && Lib_Function.isVarFunction(method) > -1)
			throw new CodeError(cr, "Invalid method for this MagicVar", "Can't execute: " + (cr.call.object != null ? cr.call.object.toString() + ' ' : "") + method);
	}

	public static String magicTypeToVarConstString(final I_MagicReplace m) {
		return m instanceof A_MagicVar ? "variable" : "constant";
	}

	public static I_Object replace(I_Object with, final CallRuntime cr, final I_Object streamIt, final I_Object eachIt) {
//		if(!(with instanceof I_ObjToReplace))
//			return with;

		if(with instanceof MV_THIS) {
			Lib_MagicVar.checkForbiddenFuncs(cr, cr.call.method);
			return cr.instance;
		}

		if(with instanceof MV_SUPER) {
			Lib_MagicVar.checkForbiddenFuncs(cr, cr.call.method);
			final I_Object parent = cr.instance.internalGetParent();
			if(parent == null)
				throw new CodeError(cr, "This type has no declared parent!", "Type: " + cr.instance.getTypeName());
			return parent;
		}

		if(with instanceof MV_CUR) {
			if(streamIt instanceof MV_CUR)
				Err.invalid(with, with, streamIt);
			with = streamIt;
			if(with instanceof MV_THIS)
				with = cr.instance;
			Err.ifNull(with);
//			if(with == null)
//				with = cr.instance; //Nil.NIL;	// If theres no stream object
			Lib_MagicVar.checkForbiddenFuncs(cr, cr.call.method);
			return with;
		}

		if(with instanceof MV_EACH) {
			if(eachIt == null)
				throw new CodeError(cr, "Invalid call!", "This type/function currently doesn't support \"" + MV_EACH.ID + "\".");
			Lib_MagicVar.checkForbiddenFuncs(cr, cr.call.method);
			return eachIt;
		}

		if(with instanceof I_MagicGet) {
			final I_Object with_copy = with;
			with = ((I_MagicGet)with).get(cr);

			if(with == null) {
				final String type = Lib_MagicVar.magicTypeToVarConstString((I_MagicReplace)with_copy);
				throw new CodeError(cr, "Invalid use of a magic " + type + "!", with_copy.toString());
			}
		}
		return with;
	}

}
