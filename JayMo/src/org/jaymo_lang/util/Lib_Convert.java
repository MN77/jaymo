/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.I_ObjectToString;
import org.jaymo_lang.object.atom.A_DecNumber;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.I_Decimal;
import org.jaymo_lang.object.atom.I_Integer;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.immute.datetime.JMo_Date;
import org.jaymo_lang.object.immute.datetime.JMo_Time;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.pseudo.MultiCall;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.data.type.datetime.MDate;
import de.mn77.base.data.type.datetime.MTime;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class Lib_Convert {

	private static final Class<?>[] INT_TO_OBJECT_ARRAY = {Bool.class, JMo_Byte.class, JMo_Short.class, Int.class, JMo_Long.class, Char.class};


	public static BigDecimal getBigDecimalValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof JMo_BigDec)
			return ((JMo_BigDec)o).getValue();
		if(o instanceof I_Atomic)
			return (BigDecimal)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.BIGDEC).getValue();
		throw Lib_Convert.iConvertError(cr, o, "BigInt");
	}

	public static BigInteger getBigIntegerValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof JMo_BigInt)
			return ((JMo_BigInt)o).getValue();
		if(o instanceof I_Atomic)
			return (BigInteger)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.BIGINT).getValue();
		throw Lib_Convert.iConvertError(cr, o, "BigInt");
	}

	public static boolean getBoolValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof Bool)
			return ((Bool)o).getValue();
		if(o instanceof I_Atomic)
			return (boolean)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.BOOL).getValue();
		throw Lib_Convert.iConvertError(cr, o, "Bool");
	}

	public static byte getByteValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof JMo_Byte)
			return ((JMo_Byte)o).getValue();
		if(o instanceof I_Atomic)
			return (byte)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.BYTE).getValue();
		throw Lib_Convert.iConvertError(cr, o, "Byte");
	}

	public static char getCharValue(final CallRuntime cr, final I_Object o) {
		return Lib_Convert.getCharValue(cr, o, false);
	}

	public static char getCharValue(final CallRuntime cr, final I_Object o, final boolean cut) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof Char)
			return ((Char)o).getValue();
		if(o instanceof Bool)
			return ((Bool)o).getValue() ? '1' : '0';
		if(o instanceof I_Atomic)
			if(cut)
				return ((I_Atomic)o).getValue().toString().charAt(0);
			else
				return (char)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.CHAR).getValue();
		throw Lib_Convert.iConvertError(cr, o, "Char");
	}

	public static Dec getDecValue(final CallRuntime cr, final I_Object o) {
		if(o instanceof JMo_Dec)
			return ((JMo_Dec)o).getValue();
		if(o instanceof JMo_Byte)
			return Dec.valueOf(((JMo_Byte)o).getValue());
		if(o instanceof JMo_Short)
			return Dec.valueOf(((JMo_Short)o).getValue());
		if(o instanceof Int)
			return Dec.valueOf(((Int)o).getValue());
		if(o instanceof JMo_Long)
			return Dec.valueOf(((JMo_Long)o).getValue());
		if(o instanceof JMo_Float)
			return Dec.valueOf(((JMo_Float)o).getValue());
		if(o instanceof JMo_Double)
			return Dec.valueOf(((JMo_Double)o).getValue());

		// TODO Big, Char, Str, ...
		throw Lib_Convert.iConvertError(cr, o, "Dec");
	}

	public static double getDoubleValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof JMo_Double)
			return ((JMo_Double)o).getValue();
		if(o instanceof I_Atomic)
			return (double)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.DOUBLE).getValue();
		throw Lib_Convert.iConvertError(cr, o, "Double");
	}

	public static File getFile(final CallRuntime cr, final I_Object arg) {
		if(arg instanceof JMo_File)
			return ((JMo_File)arg).getInternalFile();

		if(arg instanceof Str) {
			final String value = Lib_Convert.getStringValue(cr, arg);
			return new File(value);
		}
		throw Lib_Convert.iConvertError(cr, arg, "File or Str");
	}

	public static float getFloatValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof JMo_Float)
			return ((JMo_Float)o).getValue();
		if(o instanceof I_Atomic)
			return (float)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.FLOAT).getValue();
		throw Lib_Convert.iConvertError(cr, o, "Float");
	}

	public static int getIntValue(final CallRuntime cr, final I_Object o) {
		return Lib_Convert.getIntValue(cr, o, false);
	}

	public static int getIntValue(final CallRuntime cr, final I_Object o, final boolean ord) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof Int)
			return ((Int)o).getValue();
		if(ord && o instanceof Char)
			return ((Char)o).getValue();

		if(o instanceof I_Atomic) {
			final Object to = Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.INT).getValue();
			if(to == null || to == Nil.NIL)
				throw Lib_Convert.iConvertError(cr, o, "Int");
			return (int)to;
		}
		throw Lib_Convert.iConvertError(cr, o, "Int");
	}

	public static long getLongValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof JMo_Long)
			return ((JMo_Long)o).getValue();
		if(o instanceof I_Atomic)
			return (long)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.LONG).getValue();
		throw Lib_Convert.iConvertError(cr, o, "Long");
	}

	public static short getShortValue(final CallRuntime cr, final I_Object o) {
//		o = Lib_Convert.getValue(cr, o);
		if(o instanceof JMo_Short)
			return ((JMo_Short)o).getValue();
		if(o instanceof I_Atomic)
			return (short)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.SHORT).getValue();
		throw Lib_Convert.iConvertError(cr, o, "Short");
	}

	// ---

	public static String getStringValue(final CallRuntime cr, final I_Object o) {
		if(o instanceof Str)
			return ((Str)o).getValue();
		if(o instanceof I_Atomic)
			return ((Str)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.STR)).getValue();

		return o.toString(cr, STYPE.REGULAR);
	}

	public static I_Object getValue(final CallRuntime cr, final I_Object o) {
		Err.ifNull(o);
		o.initOnce(cr);

		if(o instanceof I_Mem)
			return ((I_Mem)o).get(cr);
		if(o instanceof MultiCall)
			return ((MultiCall)o).getCurrent();

		return o;
	}

	public static I_Object intToObject(final Class<?> start, final Class<?> end, final int i) {
		//, boolean varlet
//		if(args != null && args.length == 1 && !varlet)
//			return args[0];

		final int idxStart = Lib_Array.indexOf(start, Lib_Convert.INT_TO_OBJECT_ARRAY);
		final int idxEnd = Lib_Array.indexOf(end, Lib_Convert.INT_TO_OBJECT_ARRAY);
		final Class<?> type = Lib_Convert.INT_TO_OBJECT_ARRAY[Math.max(idxStart, idxEnd)];

		if(type == Bool.class)
			return Bool.getObject(i != 0);
		if(type == JMo_Byte.class)
			return new JMo_Byte((byte)i);
		if(type == JMo_Short.class)
			return new JMo_Short((short)i);
		if(type == Int.class)
			return new Int(i);
		if(type == JMo_Long.class)
			return new JMo_Long(i);
		if(type == Char.class)
			return new Char((char)i);

		throw Err.todo(type, i);
	}

	/**
	 * @apiNote Converts Integer to Decimal and Decimal to Integer.
	 *          This is used for 'convertLet'
	 */
	public static I_Object memConvert(final CallRuntime cr, final String definedType, final String objType, final I_Object obj) {
		final boolean objIsDec = obj instanceof I_Decimal;

		if(objIsDec)
			switch(definedType) {
				case "Byte":
					return new JMo_Byte(((Number)((A_DecNumber)obj).getValue()).byteValue());
				case "Short":
					return new JMo_Short(((Number)((A_DecNumber)obj).getValue()).shortValue());
				case "Int":
					return new Int(((Number)((A_DecNumber)obj).getValue()).intValue());
				case "Long":
					return new JMo_Long(((Number)((A_DecNumber)obj).getValue()).longValue());
				case "BigInt":
					return new JMo_BigInt(BigInteger.valueOf(((Number)((A_DecNumber)obj).getValue()).longValue()));

				case "Float":
					if(objType.equals("Double"))
						return new JMo_Float(Lib_ConvertExact.doubleToFloat(cr, ((JMo_Double)obj).getValue()));

//				TODO Alles prüfen und hier direkt zurück geben?
			}

		final boolean objIsInt = obj instanceof I_Integer;

		if(objIsInt)
			switch(definedType) {
				case "Float":
					return new JMo_Float(((Number)((A_IntNumber)obj).getValue()).floatValue());
				case "Dec":
					return new JMo_Double(((Number)((A_IntNumber)obj).getValue()).doubleValue());
				case "BigDec":
					return new JMo_BigDec(BigDecimal.valueOf(((Number)((A_IntNumber)obj).getValue()).doubleValue()));

				case "Byte":
					if(objType.equals("Int"))
						return new JMo_Byte(Lib_ConvertExact.intToByte(cr, ((Int)obj).getValue()));
				case "Short":
					if(objType.equals("Int"))
						return new JMo_Short(Lib_ConvertExact.intToShort(cr, ((Int)obj).getValue()));
//				case "Int":
//					if(objType.equals("Long"))
//						return new Int( Lib_MathConvert.longToIntExact(cr, ((JMo_Long)obj).getValue()));
			}

		return obj;
	}

	public static I_Object toJMo(final CallRuntime cr, final Object o, final boolean strict) {
		if(o == null)
			return Nil.NIL;

		final Class<?> cls = o.getClass();

		if(cls == String.class)
			return new Str((String)o);
		if(cls == Boolean.class)
			return (Boolean)o ? Bool.TRUE : Bool.FALSE;
		if(cls == Character.class)
			return new Char((Character)o);
		if(cls == Byte.class)
			return new JMo_Byte((Byte)o);
		if(cls == Short.class)
			return new JMo_Short((Short)o);
		if(cls == Integer.class)
			return new Int((Integer)o);
		if(cls == Long.class)
			return new JMo_Long((Long)o);
		// Dec ?!?
		if(cls == Float.class)
			return new JMo_Float((Float)o);
		if(cls == Double.class)
			return new JMo_Double((Double)o);

		if(cls == Date.class)
			return new JMo_Date(new MDate((Date)o));
		if(cls == Time.class)
			return new JMo_Time(new MTime((Time)o));

		if(!strict)
			try {
				if(cls == BigDecimal.class)
					return new JMo_Double(((BigDecimal)o).doubleValue());
				if(cls == BigInteger.class)
					return new Int(((BigInteger)o).intValueExact());
			}
			catch(final ArithmeticException e) {
				throw new RuntimeError(cr, "Value out of bounds", e.getMessage());
			}

		return new JMo_Java(o);
	}

	public static JMo_List toJMo(final Collection<String> list) {
		Err.ifNull(list);
		final SimpleList<I_Object> al = new SimpleList<>(list.size());
		for(final String s : list)
			if(s == null)
				al.add(Nil.NIL);
			else
				al.add(new Str(s));
		return new JMo_List(al);
	}

	public static JMo_Table toJMo(final I_Table<String> tab) {
		Err.ifNull(tab);
		final ArrayTable<I_Object> result = new ArrayTable<>(tab.width());
		for(final String[] row : tab)
			result.add(Lib_Convert.toJMoArray(row));
		return new JMo_Table(result);
	}

	public static JMo_List toJMo(final String[] list) {
		Err.ifNull((Object)list);
		final SimpleList<I_Object> al = new SimpleList<>(list.length);
		for(final String s : list)
			if(s == null)
				al.add(Nil.NIL);
			else
				al.add(new Str(s));
		return new JMo_List(al);
	}

	public static I_Object[] toJMoArray(final String[] list) {
		Err.ifNull((Object)list);
		final I_Object[] result = new I_Object[list.length];

		for(int i = 0; i < list.length; i++) {
			final String item = list[i];
			result[i] = item == null
				? Nil.NIL
				: new Str(item);
		}
		return result;
	}

	public static SimpleList<String> toListString(final Collection<I_Object> list) {
		Err.ifNull(list);
		final SimpleList<String> res = new SimpleList<>(list.size());
		for(final I_Object s : list)
			res.add(Lib_Convert.getStringValue(null, s));
		return res;
	}


	/*
	 * This is working fine, but leads to some Problems: true -> "true" // 5.0 -> "5.0" // 'a'.toByte -> Error
	 * public static I_Object toType(CallRuntime cr, I_Object arg, String typeName) {
	 * for(ATOMIC a : ATOMIC.values())
	 * if(a.name.equals(typeName)) {
	 * final String argTypeName = arg.getTypeName();
	 *
	 * for(ATOMIC a2 : ATOMIC.values())
	 * if(a2.name.equals(argTypeName)) {
	 * return a.ordinal() > a2.ordinal()
	 * ? ((I_Atomic)arg).convertTo(cr, a)
	 * : arg;
	 * }
	 * }
	 * return arg;
	 * }
	 */

	/**
	 * @apiNote Try to auto-convert an argument to match the given Type.
	 *          Converts only what is absolutly clear, unambiguously and errorsafe.
	 */
	public static I_Object toSafeType(final CallRuntime cr, final I_Atomic arg, final String typeName) {

		switch(arg.getEnum()) {
			case BYTE:
				switch(typeName) {
					case "Short":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.SHORT);
					case "Int":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.INT);
					case "Long":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.LONG);
					case "BigInt":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGINT);
					case "Dec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.DEC);
					case "Float":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.FLOAT);
					case "Double":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.DOUBLE);
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case SHORT:
				switch(typeName) {
					case "Int":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.INT);
					case "Long":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.LONG);
					case "BigInt":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGINT);
					case "Dec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.DEC);
					case "Float":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.FLOAT);
					case "Double":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.DOUBLE);
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case INT: // Float has not the needed range.
				switch(typeName) {
					case "Long":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.LONG);
					case "BigInt":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGINT);
					case "Dec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.DEC);
					case "Double":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.DOUBLE);
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case LONG:
				switch(typeName) {
					case "BigInt":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGINT);
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case BIGINT:
				switch(typeName) {
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case DEC:
				switch(typeName) {	// Double has not the needed range
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case FLOAT:
				switch(typeName) {
					case "Double":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.DOUBLE);
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case DOUBLE:
				switch(typeName) {
					case "BigDec":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.BIGDEC);
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case BIGDEC:
				switch(typeName) {
					case "Str":
						return Lib_AtomConv.convert(cr, arg, ATOMIC.STR);
					default:
						return arg;
				}
			case CHAR:
				return typeName.equals("Str") ? Lib_AtomConv.convert(cr, arg, ATOMIC.STR) : arg;

			default:
				return arg;
		}
	}

	public static String toString(final Call c) {
		if(c == null)
			return Nil.NIL.toString();
		return c.toString();
	}

	public static String toString(final CallRuntime cr, final boolean nested, final String delimiter, final I_ObjectToString[] arr) { //final boolean describe,
		final StringBuilder sb = new StringBuilder();

		for(int i = 0; i < arr.length; i++) {
			if(i > 0)
				sb.append(delimiter);

			sb.append(arr[i].toString(cr, nested ? STYPE.NESTED : STYPE.REGULAR));
		}
		return sb.toString();
	}

	public static String[] toStringArray(final I_Object[] titles) {
		Err.ifNull((Object)titles);
		final int len = titles.length;
		final String[] result = new String[len];

		for(int i = 0; i < len; i++)
			result[i] = Lib_Convert.getStringValue(null, titles[i]);

		return result;
	}

	public static String[] toStringArray(final JMo_List targetArgs) {
		Err.ifNull(targetArgs);
		final SimpleList<I_Object> internalList = targetArgs.getInternalCollection();
		final int len = internalList.size();
		final String[] result = new String[len];

		for(int i = 0; i < len; i++)
			result[i] = Lib_Convert.getStringValue(null, internalList.get(i));

		return result;
	}

	public static ArrayTable<String> toTableString(final JMo_Table tab) {
		final ArrayTable<String> res = new ArrayTable<>(tab.getInternalObject().width());
		for(final I_Object[] f : tab.getInternalObject())
			res.add(Lib_Convert.toStringArray(f));
		return res;
	}

	public static String typelistToText(final Class<?>[] types) {
		final StringBuilder sb = new StringBuilder();
		sb.append("(");

		for(int pos = 0; pos < types.length; pos++) {
			final Class<?> c = types[pos];
			if(pos != 0)
				sb.append(", ");
			sb.append(Lib_Type.getName(c, null));
		}
		sb.append(")");
		return sb.toString();
	}

	private static RuntimeError iConvertError(final CallRuntime cr, final I_Object o, final String neededType) {
		return new RuntimeError(cr, "Invalid type", "Object cannot be converted to " + neededType + ". Got: " + o.toString(cr, STYPE.IDENT));
	}

}
