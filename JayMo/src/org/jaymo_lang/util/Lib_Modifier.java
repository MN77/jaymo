/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

/**
 * @author Michael Nitsche
 * @created 17.06.2021
 */
public class Lib_Modifier {

	public static String[] sysCmd(final String rem, final String live, final String out, final String err, final String buffer) {

		if(rem.length() != 0) {
			final char c0 = rem.charAt(0);

			switch(c0) {
				case 'o':	// output
//				case 'p':	// pipe
					return new String[]{out, rem.substring(1)};
				case 'e':	// error
					return new String[]{err, rem.substring(1)};
				case 'b':	// buffer
					return new String[]{buffer, rem.substring(1)};
				case 'r':	// result
				case 'l':	// live
					return new String[]{live, rem.substring(1)};
			}
		}
		return new String[]{live, rem};
	}


//	/**
//	 * Replace modifier with a string
//	 */
//	@Deprecated
//	public static String replace(final String s, final String without, final String withModif1, final String withModif2) {
//		final int len = s.length();
//		if(len == 0)
//			return without + s;
//
//		final char c0 = s.charAt(0);
//
//		if(c0 == '?')
//			if(len >= 2 && s.charAt(1) == '?')
//				return withModif2 + s.substring(2);
//			else
//				return withModif1 + s.substring(1);
//		return without + s;
//	}

//	@Deprecated
//	public static Group2<Integer, String> remove(final String s) {
//		final int len = s.length();
//		if(len == 0)
//			return new Group2<>(0, s);
//
//		final char c0 = s.charAt(0);
//
//		if(c0 == '?')
//			if(len >= 2 && s.charAt(1) == '?')
//				return new Group2<>(2, s.substring(2));
//			else
//				return new Group2<>(1, s.substring(1));
//		return new Group2<>(0, s);
//	}

}
