/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.util.List;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.FunctionPar;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Group3;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class Lib_Parser {

//	public static final String COMBINE_CHARS = "\\+\\*/.\\],"; // not: [%&<>-
//	public static final String COMBINE_CHARS = "\\+*/.],"; // not: [%&<>-

	/**
	 * @apiNote For control ability two !! are needed.
	 *          This functions checks, if the second '!' is right there
	 */
	public static void checkControl(final String s, final int i, final Parser_Script parser) {
		if(i == s.length() - 1 || s.charAt(i + 1) != '!')
			throw new CodeError("Invalid function definition", "The second '!' is missing for control ability", parser.getDebugInfo());
	}

	/**
	 * @apiNote No length check will be done!
	 */
	public static int combineBegin(final String s) {

		switch(s.trim().charAt(0)) {
//			case '+': // Bad, because +5 is a valid Object
//			case '*': // This is irritating, as well as why if + and - are not possible.
//			case '/': // Same as above
			case '.':
				return 1; // Dot must be at the same level
			case ',':
			case ')':
			case ']':
				return 2; // These characters could be on a different level
//			case '\\':	// TODO ?????
//				return 3;
			default:
				return 0;
		}
	}

	/**
	 * @apiNote No length check will be done!
	 */
	public static boolean combineEnd(final String s) {
		final String lineTopTrim = s.trim();	// Necessary
		final char end = lineTopTrim.charAt(lineTopTrim.length() - 1);

		if(end == ',' || end == '[')
			return true;
		if(end == '(')
			return !s.startsWith("?");	// Only, when it is not whithin quickhelp context

		return false;
	}

	/**
	 * Switch between Type and enum
	 * No check for first char upper case
	 */
	public static boolean fastCheckIsType(final String name, final DebugInfo debug) {
		if(name == null || name.length() == 0)
			throw new CodeError("Invalid code", "Missing name", debug);

		if(name.charAt(0) == '_')
			return true;

		for(final char c : name.toCharArray()) {
			if(c >= 'a' && c <= 'z')
				return true;

			if(c < 'A' | c > 'Z' && c < '0' | c > '9' && c != '_')
				return false;
		}
		return false;
	}

	/**
	 * Returns: Regex-String up to next '"', Position of '"'
	 */
//	public static final Group2<String, Integer> regexGet(final String s, final DebugInfo debugInfo) {
//		final char[] ca = s.toCharArray();
//		for(int pos = 1; pos < ca.length; pos++) {
//			final char c = ca[pos];
//			switch(c) {
//				case '\\':
//					pos++;
//					break;
//				case '"':
//					return new Group2<>(s.substring(1, pos), pos);
//			}
//		}
//		return null;
//	}

	public static boolean fastCheckIsVar(final String name, final DebugInfo debug) {
		if(name == null || name.length() == 0)
			throw new CodeError("Invalid variable/constant", "Missing name", debug);
		final char c0 = name.charAt(0);
		return c0 >= 'a' && c0 <= 'z';
	}

	/**
	 * Fast check, if the given String matches type notation: <Type>
	 */
	public static boolean fastCheckStartWithTypeNotation(final String s) {
		final int len = s.length();
		if(s == null || len == 0 || s.charAt(0) != '<') //  || s.charAt(len-1) != '>'
			return false;

		char c = ' '; // Single

		for(int i = 1; i < len; i++) {
			c = s.charAt(i);
			if(c == '>')
				return true;

			if(c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z')
				continue;
			if(c >= '0' && c <= '9' || c == '_')
				continue;

			return false;
		}
		return false;
	}

	public static int getDepth(final String s) {
		int count = 0;
		for(int pos = 0; pos < s.length(); pos++)
			if(s.charAt(pos) == '\t')
				count++;
			else
				return count;
		return count;
	}

	public static Group3<FunctionPar[], Integer, Boolean> getFunctionPars(final Parser_Script parser, final Block current, final String s, final char open, final char close) {
		if(s == null)
			return new Group3<>(new FunctionPar[0], 0, false);

		final int begin = s.indexOf(open);
		if(begin == -1)
			return new Group3<>(new FunctionPar[0], 0, false);

		final String s2 = s.substring(begin);
		String ps = Lib_Parser.group(open, close, s2, parser.getDebugInfo());
		final int rem = begin + ps.length() + 2; // 2 because of ()
		ps = ps.trim();
		final List<String> psa2 = Lib_StringParser.splitArgs(ps, parser.getDebugInfo());
		final SimpleList<FunctionPar> args = new SimpleList<>();
		boolean varargs = false;

		for(final String element : psa2) {
			String sa = element.trim();

			if(sa.length() == 0)
				continue;

			if(sa.endsWith("...")) {
				varargs = true;
				sa = sa.substring(0, sa.length() - 3);
			}
			final String ident1 = Lib_StringParser.parseIdentifier(sa, 0);
			int left = ident1.length();

			while(left < sa.length() && sa.charAt(left) == ' ') // TODO also tab?!?
				left++;

			final int defIndex = sa.indexOf('=', left);
			final String pDef = defIndex == -1 ? null : sa.substring(defIndex + 1).trim(); // null when -1 ?!?
			final int right = defIndex == -1 ? sa.length() : defIndex;


			String pType = null;
			String pName = ident1;

			if(right - left > 0) {
				final String ident2 = Lib_StringParser.parseIdentifier(sa, left);
//				MOut.temp(right, offset, ident1, ident2);
				pType = ident1;
				pName = ident2;
			}

			if(pType != null && pType.length() > 0 && pType.charAt(0) == '_') {
				final String prefix = parser.getCurrentPrefix();
				if(prefix == null)
					throw new CodeError("Invalid type", "There is no prefix defined which could be used for Parameter '" + pName + "'", parser.getDebugInfo());
				pType = prefix + pType;
			}
//			MOut.temp(sa, pType, pName, pDef);
//			pType = Lib_Type.replaceAlias(pType);

			Call pDefC = null;
			if(pDef != null)
				//				final Group2<I_Object, String> g = Parser_Call.parseObject(parser, current, pDef, true); // new DebugInfo("New Function", pName)
//				if(g.o2 != null && g.o2.length() > 0)
//					Err.for bidden(g.o2);
//				pDefC = g.o1;
				pDefC = Parser_Call.parseCall(parser, current, pDef, false);

			final char pName0 = pName.charAt(0);
			final boolean fix = pName0 >= 'A' && pName0 <= 'Z'; // !pName.matches(REGEX.VAR);

			if(pType != null && !Lib_Comply.checkTypeName(pType))
				throw new CodeError("Invalid type", "Parameter '" + pName + "' has an invalid type name: " + pType, parser.getDebugInfo());

			parser.app.strict.checkFuncParType(pType, pName, parser.getDebugInfo());
			final FunctionPar fpar = new FunctionPar(pType, fix, pName, pDefC);

			// Check, if parname already exists
			final String pNameLower = pName.toLowerCase();
			for(final FunctionPar arg : args)
				if(arg.name.toLowerCase().equals(pNameLower))
					throw new CodeError("Duplicate function variable", "Variable is already used: " + pName, parser.getDebugInfo());

			// Add it
			args.add(fpar);
		}
		return new Group3<>(args.toArray(new FunctionPar[args.size()]), rem, varargs);
	}

	public static String getTypeName(final String s, final Parser_Script parser) {
		boolean openSpecial = false;

		for(int i = 0; i < s.length(); i++) {
			final char c = s.charAt(i);

			if(c == '{') {
				openSpecial = true;
				continue;
			}

			if(c == '}') {
				openSpecial = false;
				continue;
			}

			if(openSpecial) {
				final boolean hit = c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_' || c == '.' || c == '$';
				if(!hit)
					throw new CodeError("Invalid type", "Closing bracket '}' is missing: " + s.substring(0, i), parser.getDebugInfo());
			}
			else {
				final boolean hit = c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_';
				if(!hit)
					return s.substring(0, i);
			}
		}
		return s;
	}

	public static final String group(final char open, final char close, final String s, final DebugInfo debugInfo) {
		int deep = 0;
		int start = -1;

		for(int idx = 0; idx < s.length(); idx++) {
			final char c = s.charAt(idx);

			if(c == open) {
				deep++;
				if(start == -1)
					start = idx;
			}

			// Filter Strings
			if(c == '"') {
				idx += 1 + Lib_StringParser.checkString(s.substring(idx), true, debugInfo);
				continue;
			}

			// Filter Char
			if(c == '\'') {
				idx += Lib_StringParser.checkChar(s.substring(idx), true, debugInfo) - 1;
				continue;
			}

			if(c == close) {
				deep--;
				if(deep == 0)
					return s.substring(start + 1, idx);
			}
		}
		if(deep == 0)
			return s;
		else
			throw new CodeError("Uncomplete " + open + close + " group", "Missing: '" + close + "', got only: " + s, debugInfo);
	}

	public static String removeUnderlines(final String s, final DebugInfo debug) {
		if(s.charAt(0) == '_' || s.charAt(s.length() - 1) == '_')
			throw new CodeError("Invalid number", "Underline at beginning or end: " + s, debug);
		return s.replace("_", "");
	}

//	public static boolean fastCheckIsEnum(final String name, final DebugInfo debug) {
//		if(name == null || name.length() == 0)
//			throw new CodeError("Invalid enum", "Missing name", debug);
//
//		if(name.charAt(0) == '_')
//			return false;
//
//		int amount = 0;
//		for(char c : name.toCharArray()) {
//			if(c >= 'a' && c <= 'z')
//				return false;
//
//			if( (c < 'A' | c > 'Z') && c != '_' )	// 0..9 fehlen hier
//				return amount > 0;
//			else
//				amount++;
//		}
//		return true;
//	}

	/**
	 * Liefert zum zweiten Escape-Zeichen das richtige char
	 * n --> \n
	 */
	public static char replaceEscapeChar(final char c, final DebugInfo debugInfo) {

		switch(c) {
			case '\\':
				return c;
			case '\'':
				return c; // Müsste nicht unbedingt sein, aber ist ok
			case '"':
				return c;

			case 'n':
				return '\n';
			case 't':
				return '\t';

			case 'r':
				return '\r';
			case 'f':
				return '\f';
			case 'b':
				return '\b';
			case '0': // EOF
				return '\0';
		}
		throw new CodeError("Unknown char to escape", "\\" + c, debugInfo);
	}

	/**
	 * @apiNote
	 *          pos is pointing to index of 'u'
	 */
	public static char replaceUtfChar(final String s, final int pos, final DebugInfo debugInfo) {
		Err.ifTooSmall(pos + 5, s.length());
		final String hex = s.substring(pos + 1, pos + 5);
		if(pos + 5 > s.length())
			throw new CodeError("Uncomplete UTF-Character", "Got: " + hex, debugInfo);
		int value = 0;

		try {
			value = Integer.parseInt(hex, 16);
		}
		catch(final NumberFormatException nfe) {
			throw new CodeError("Invalid unicode statement", "Got: \\" + s.substring(pos), debugInfo);
		}
		return (char)value;
	}

	/**
	 * @apiNote First char will not be checked!
	 */
	public static final Group2<String, String> scanCommand(final String s, final DebugInfo debugInfo) {
		final int len = s.length();
		if(len < 3)
			throw new CodeError("Invalid command", "Got: " + s, debugInfo);
		final boolean isBuildt = s.charAt(1) == '(';

		for(int idx = 1; idx < len; idx++) {
			final char c = s.charAt(idx);

			switch(c) {
				case '"':
					// Filter Strings
					if(isBuildt)
						idx += 1 + Lib_StringParser.checkString(s.substring(idx), true, debugInfo);
					continue;
				case '\'':
					// Filter Char
					if(isBuildt)
						idx += Lib_StringParser.checkChar(s.substring(idx), true, debugInfo) - 1;
					continue;
				case '´':
				case '`':
					return new Group2<>(s.substring(1, idx), s.substring(idx + 1));
				default:
			}
		}
		throw new CodeError("Uncomplete command", "Missing '´' or '`' at the end: " + s, debugInfo); // Text anpassen
	}

	public static String setDepth(final int d, String s) {
		s = s.trim();
		return Lib_String.sequence('\t', d) + s;
	}

	public static final String space(final int level) {
		return level == 0
			? ""
			: Lib_String.sequence(' ', level * 2);
	}

	/**
	 * @apiNote
	 *          Parse a String up to the ending "
	 * @implNote
	 *           Replaces also Escape-Chars, so a rebuild char by char is necessary.
	 * @return
	 *         A Group2 with: The parsed String, index of the ending "
	 */
	public static final Group2<String, Integer> stringGet(final String s, final DebugInfo debugInfo) {
		final char[] ca = s.toCharArray();
		final StringBuilder sb = new StringBuilder(ca.length);

		for(int idx = 1; idx < ca.length; idx++) {
			final char c = ca[idx];

			switch(c) {
				case '\\':
					idx++;
					final char temp = ca[idx];
					if(temp == 'u') {
						final char utf = Lib_Parser.replaceUtfChar(s, idx, debugInfo);
						sb.append(utf);
						idx += 4;
					}
					else
						sb.append(Lib_Parser.replaceEscapeChar(temp, debugInfo));
					break;
				case '"':
					return new Group2<>(sb.toString(), idx);
				default:
					sb.append(c);
			}
		}
		return null;
	}

//	public static String shortcutVarLet(String x) {	//, final DebugInfo debugInfo
//		final String s = x.trim();
//		if(s.length() == 0)
//			return "";
//
//		if(s.charAt(0)>='a' && s.charAt(0)<='z') {
//			int len = 0;
//			for(int i=0; i< s.length(); i++) {
//				char c = s.charAt(i);
//				if((c >='a' && c <='z') || (c >='A' && c <='Z') ||(c >='0' && c <='9') || c == '_')
//					len++;
//				else
//					break;
//			}
//
//			final String v = s.substring(0, len);
////			final String v = s.replaceFirst("::(" + REGEX.VAR + ").*", "$1");	// Replace regex!
//			final String rem = s.substring(v.length());
//			return "(:" + v + ')' + rem;
//		}
////		if(s.charAt(0) == '(')
////			if(x.charAt(0) =='(')
////				return x;
////			else
////				throw new CodeError("Lonely argument brackets", "Brackets must be appended to the type or function without spaces.", debugInfo);
//		return s;
//	}

}
