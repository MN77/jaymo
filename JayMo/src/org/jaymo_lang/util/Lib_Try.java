/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 30.11.2020
 */
public class Lib_Try {

	public static ObjectCallResult mErrTry(final CallRuntime cr, final I_Object o) {
		return cr.argCount() > 0
			? Lib_Try.mErrTryArg(cr, o)
			: cr.getCallBlock() == null
				? Lib_Try.mErrTryStream(cr, o)
				: Lib_Try.mErrTryBlock(cr, o);
	}

	public static ObjectCallResult mErrTryArg(final CallRuntime cr, final I_Object o) {
		I_Object result = o;

		try {
			result = cr.args(o, I_Object.class)[0];
		}
		catch(final CodeError e) {
			if(cr.getApp().strict.getLazyErrors())
				result = new JMo_Error(e);
			else
				throw e;
		}
		catch(final ErrorBaseDebug t) { // Catches Parse- und Exec-Fehler, but no Java-Errors from JayMo
			result = new JMo_Error(t);
		}
		return new ObjectCallResult(result, false);
	}

	public static ObjectCallResult mErrTryBlock(final CallRuntime cr, final I_Object o) {
		cr.argsNone();
		cr.blockNecessary();

		I_Object result = o;

		try {
			result = Lib_Exec.execBlockOnly(cr, result);
		}
		catch(final JayMoError e) {
			throw e;
		}
		catch(final CodeError e) {
			if(cr.getApp().strict.getLazyErrors())
				result = new JMo_Error(e);
			else
				throw e;
		}
		catch(final ErrorBaseDebug t) { // Catches Parse- und Exec-Fehler, but no Java-Errors from JayMo
			result = new JMo_Error(t);
		}
		result = Lib_Exec.execStreamOnly(cr, result);

		return new ObjectCallResult(result, true);
	}

	public static ObjectCallResult mErrTryStream(final CallRuntime cr, final I_Object o) {
		cr.argsNone();
		cr.blockForbidden();

		I_Object result = o;

		try {
			result = Lib_Exec.execStreamOnly(cr, result);
		}
		catch(final JayMoError e) {
			throw e;
		}
		catch(final CodeError e) {
			if(cr.getApp().strict.getLazyErrors())
				result = new JMo_Error(e);
			else
				throw e;
		}
		catch(final ErrorBaseDebug t) { // Catches Parse- und Exec-Fehler, but no Java-Errors from JayMo
			result = new JMo_Error(t);
		}
		return new ObjectCallResult(result, true);
	}

	public static ObjectCallResult mErrTryUse(final CallRuntime cr, final I_Object o) {
		final boolean showMessage = cr.getApp().isDebug();
		Lib_Error.ifArgs(cr.argCount(), 1, 1, "Invalid amount of arguments", cr.getDebugInfo()); //TODO allow also 2 arguments?

		I_Object result = o;

		try {
			result = Lib_Exec.execBlockStream(cr, result);
		}
		catch(final JayMoError e) {
			throw e;
		}
		catch(final CodeError e) {

			if(!cr.getApp().strict.getLazyErrors())
				throw e;
			else {
				if(showMessage)
					new JMo_Error(e).mShow(cr.copyNull());
				result = cr.argEach(o, 0, new JMo_Error(e), I_Object.class);
			}
		}
		catch(final ErrorBaseDebug t) { // Catches Parse- und Exec-Fehler, but no Java-Errors from JayMo
			if(showMessage)
				new JMo_Error(t).mShow(cr.copyNull());
			result = cr.argEach(o, 0, new JMo_Error(t), I_Object.class);
		}
		return new ObjectCallResult(result, true);
	}

	public static ObjectCallResult mTryLazy(final CallRuntime cr, final I_Object obj) {
		cr.argsNone();
		final boolean showMessage = cr.getApp().isDebug();
		I_Object result = obj;

		try {
			result = Lib_Exec.execBlockStream(cr, result);
		}
		catch(final CodeError | JayMoError e) {
			throw e;
		}
		catch(final ErrorBaseDebug e) { // RuntimeError | CustomError | AssertError | ExternalError
			if(showMessage)
				new JMo_Error(e).mShow(cr.copyNull());
			result = Nil.NIL;
		}
		return new ObjectCallResult(result, true);
	}

}
