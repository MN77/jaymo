/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.parser.I_DebugInfoSource;

import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.util.Lib_Math;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 */
public class LineBuffer implements I_DebugInfoSource {

	private final String[] lines;
	private final int[]    line_nrs;
	private final int      len;
	private final String   filepath;
	private int            next        = 0;
	private int            depth       = 0;
	private int            downchanged = 0;
	private DebugInfo      debug       = null;


	public LineBuffer(final String filepath, final String[] lines, final int[] linenrs) {
		this.filepath = filepath;
		this.lines = lines;
		this.line_nrs = linenrs;
		this.len = lines.length;
	}

	public int getCurrentLevel() {
		return this.depth;
	}

	public DebugInfo getDebugInfo() {
		return this.debug;
	}

	public String getNext() {
		this.downchanged = 0;
		final String s = this.lines[this.next];
		this.depth = Lib_Parser.getDepth(s);
		final String result = this.depth == 0 ? s : s.substring(this.depth);

		// Create DebugInfo:
		final String fileInfo = this.filepath == null || this.filepath.length() == 0
			? null
			: this.filepath;

		final int lineNr = this.line_nrs.length == 0
			? 0
			: this.line_nrs[(int)Lib_Math.limit(0, this.line_nrs.length - 1, this.next)];

		this.debug = new DebugInfo(fileInfo, lineNr);
		this.next++;
		return result;
	}

	public boolean hasNext() {
		while(this.next < this.len && this.lines[this.next] == null)
			this.next++;
		return this.next < this.len;
	}

	public void levelDownchanged() {
		this.downchanged++;
	}

	public int nextLevelDiff() {
		while(this.next < this.len && this.lines[this.next] == null)
			this.next++;
		if(this.next == this.len) // last item
			return 0;

		final int depthOld = this.depth;
		final int depthNew = Lib_Parser.getDepth(this.lines[this.next]);
		if(depthNew == depthOld)
			return 0;
		final int result = depthNew - depthOld;
		return result + this.downchanged;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();

		for(int i = 0; i < this.len; i++) {
			String prefix = i == this.next + 1
				? "->"
				: "  ";
			final String depth = i == this.next + 1
				? FormNumber.right(2, this.depth)
				: "  ";
			prefix = depth + " " + prefix + " ";
			sb.append(prefix + this.lines[i]);
			sb.append("\n");
		}
		return sb.toString();
	}

}
