/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.util.List;

import org.jaymo_lang.model.App;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 10.04.2018
 */
public class Lib_Output {

	public static String appShowPart(final String title, final String s) {
		final StringBuilder sb = new StringBuilder();
		final String space = Lib_Parser.space(1);
		final List<String> lines = ConvertString.toLines(s);
		if(title != null)
			sb.append(title + '\n');

		for(final String s2 : lines)
			if(s2.length() > 0) {
				sb.append(space);
				sb.append(s2);
				sb.append('\n');
			}

		Lib_Output.removeEnd(sb, '\n');
		return sb.toString();
	}

	public static void err(final App app, final String s, final boolean newline) {
		Lib_Output.iOutput(app, s, true, newline);
	}

	public static void err(final CallRuntime cr, final I_Object o, final boolean newline) {
		Lib_Output.iOutput(cr.getApp(), Lib_Output.iObjToString(cr, o), true, newline);
	}

	public static void err(final CallRuntime cr, final String s, final boolean newline) {
		Lib_Output.iOutput(cr.getApp(), s, true, newline);
	}

	public static String indentLines(final String in, final boolean isEndOfLine) {
		final boolean multiline = in.indexOf('\n') >= 0;
		if(!multiline)
			return in;

		final List<String> lines = ConvertString.toLines(in);

		final String space = Lib_Parser.space(1);
		final StringBuilder sb = new StringBuilder();

		if(multiline)
			sb.append('\n');

		for(final String s : lines) {
			sb.append(space);
			sb.append(s);
			sb.append('\n');
		}
		if(isEndOfLine)
			Lib_Output.removeEnd(sb, '\n');

		return sb.toString();
	}

	public static void log(final CallRuntime cr, final I_Object obj) {
		final String s = Lib_Convert.getStringValue(cr, obj);
		MOut.log(s);
	}

	public static void out(final App app, final String s, final boolean newline) {
		Lib_Output.iOutput(app, s, false, newline);
	}

	public static void out(final CallRuntime cr, final I_Object arg, final boolean newline) {
		Lib_Output.iOutput(cr.getApp(), Lib_Output.iObjToString(cr, arg), false, newline);
	}

//	public static OutputStream streamOutput(final App app) {
//		return new OutputStream() {
//
//			@Override
//			public void write(final int i) throws IOException {
//				final String s = new String(new byte[]{(byte)i});
//				Lib_Output.out(app, s, false);
//			}
//		};
//	}

	public static void out(final CallRuntime cr, final String s, final boolean newline) {
		Lib_Output.iOutput(cr.getApp(), s, false, newline);
	}

	public static void removeEnd(final StringBuilder sb, final char c) {
		final int len = sb.length();
		if(len > 0 && sb.charAt(len - 1) == c)
//			sb.deleteCharAt(sb.length()-1);
			sb.setLength(len - 1);
	}

	public static Group2<String, Boolean> toObjectWithFunction(final CallRuntime cr, final I_Object obj, final String function, final boolean showType) {	//, final STYPE stype
		String o = "";

		if(obj != null)
			//			try {
			o = showType
				? Lib_Type.getTypeString(Lib_Convert.getValue(cr, obj))
				: obj.toString(cr, STYPE.IDENT);	// Any other leads to CycleOfDeath
//			}
//			catch(Throwable t) {
//				// Prevent cycle of death
//			}

		final boolean mathFunc = Lib_Function.isMathematicFunction(function);
		final String m = function == null
			? ""
			: (mathFunc ? " " : ".") + function;

		return new Group2<>(o + m, mathFunc);
	}

	//	public static String toString(final Object o, final boolean quote) {
	public static String toString(final Call[] calls) {
		if(calls == null)
			return ""; // null

//		if(o instanceof Str)
//			return quote
//				? '"' + o.toString() + '"'
//				: o.toString();
//
//		if(o instanceof Iterable) {
//			final StringBuilder sb = new StringBuilder();
//			for(final Object s : (Iterable<?>)o)
//				Lib_Output.iAdd_toString(sb, Lib_Output.toString(s, quote));
//			return sb.toString();
//		}
//
//		if(o instanceof I_List) // Hack
//			return Lib_Output.toString(((I_List<Call>)o).toArray(Call.class), quote);
//
//		if(o instanceof I_Object)
//			return ((I_Object)o).toString();
//
//		if(o instanceof I_Object[]) {
//			final StringBuilder sb = new StringBuilder();
//			for(final I_Object s : (I_Object[])o)
//				Lib_Output.iAdd_toString(sb, Lib_Output.toString(s, quote));
//			return sb.toString();
//		}
//
//		if(o instanceof Call)
//			return ((Call)o).toString();

//		if(o instanceof Call[]) {
		final StringBuilder sb = new StringBuilder();
		for(final Call s : calls)
			Lib_Output.iAdd_toString(sb, s);
		return sb.toString();
//		}

//		throw Err.todo(o.getClass());
	}

	private static void iAdd_toString(final StringBuilder sb, final Object o) {
		if(o == null)
			return;
		if(sb.length() != 0)
			sb.append(',');
		sb.append(o.toString());
	}

	private static String iObjToString(final CallRuntime cr, final I_Object arg) {

		if(arg instanceof Instance && ((Instance)arg).getType().getFunctions().knows("toStr")) {
			final Function f = ((Instance)arg).getType().getFunctions().get(cr, "toStr");
			final I_Object result = f.getBlock().execOverwrite(cr, (Instance)arg, f);
			return Lib_Convert.getStringValue(cr, result);
		}
		else
			return arg.toString(cr, STYPE.REGULAR);
	}

	/**
	 * Main-Output-Function
	 */
	private static void iOutput(final App app, String s, final boolean error, final boolean newline) {
//		final String st = app.terminalRawMode ? Lib_String.replace(s, '\n', "\n\r") : s;	// Executions now are always in cooked mode

		if(app.writeOutput(s, newline))
			return; // No other output (Security)

		final I_File outFile = app.getOutputFile();

		if(outFile != null) // Output only to file
			try {
				final StringBuilder sb = new StringBuilder();

				if(error)
					sb.append("<Err>\n");
				sb.append(s);
				if(newline)
					sb.append('\n');
				if(error)
					sb.append("</Err>\n");

				Lib_TextFile.append(outFile.getFile(), sb.toString());

				if(error)
					MOut.logErr(s);
			}
			catch(final Err_FileSys e) {
				Err.show(e);
			}
		else {
			if(newline)
				s += app.terminalRawMode ? "\r\n" : "\n";
			// TODO Use MOut.setLinebreak("\r\n") ?!?
			// TODO Windows ?!?

			if(error)
				MOut.echoErr(s); // Writes also to Log
			else
				MOut.echo(s);
		}
	}

}
