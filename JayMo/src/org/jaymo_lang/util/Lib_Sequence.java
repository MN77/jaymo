/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 05.05.2020
 */
public class Lib_Sequence {

	/**
	 * Resolves negative numbers and returns the correct position. If lazy and incorrect position, -1 will be returned.
	 * TODO Maybe don't return -1, return the invalid (positive) position?
	 */
	public static int realPosition(final CallRuntime cr, final int relPosition, final int size, final boolean lazy) {
		if(relPosition == 0)
			throw new RuntimeError(cr, "Invalid position", "Position '0' is not allowed!");
		if(Math.abs(relPosition) > size)
			if(lazy)
				return -1;
			else
				throw new RuntimeError(cr, "Position out of bounds", "Size is " + size + ", invalid position: " + relPosition);
		return relPosition < 1
			? size + relPosition + 1
			: relPosition;
	}

}
