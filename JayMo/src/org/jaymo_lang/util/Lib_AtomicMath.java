/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.NOP1;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.bigcalc.Lib_BigCalc;
import de.mn77.base.data.util.Lib_BigMath;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 23.01.2022
 */
public class Lib_AtomicMath {

	public static A_Number calcBigDec(final CallRuntime cr, final NOP1 op, final BigDecimal value, final BigDecimal arg) {

		switch(op) {
			case ADD:
				return new JMo_BigDec(arg.add(value));
			case SUB:
				return new JMo_BigDec(value.subtract(arg));
			case MUL:
				return new JMo_BigDec(value.multiply(arg));
			case DIV:
				if(arg.signum() == 0) {
//					throw new RuntimeError(cr, "Division by zero", "" + value + "/" + arg);
					Lib_AtomicMath.iDivisionByZero(cr, value, arg);
					return new JMo_BigDec(BigDecimal.ZERO);
				}
				else
					return new JMo_BigDec(value.divide(arg, JMo_BigDec.CONTEXT));
			case MOD:
				return new JMo_BigDec(Lib_BigMath.modulo(value, arg));
			case LOG:
				return new JMo_BigDec(Lib_BigMath.log(arg, value, JMo_BigDec.CONTEXT)); // Testen!
//					.log(null, null, null) Math.log(this.value) / Math.log(arg)); // benötigt die Basis und gibt die Potenz zurück
//			case INC:
//				return new JMo_BigDec(value.add(arg));
//			case DEC:
//				return new JMo_BigDec(value.subtract(arg));
			case POW:
				return new JMo_BigDec(Lib_BigCalc.pow(value, arg, JMo_BigDec.CONTEXT));
			case ROOT:
//					= exp(log(value) / arg))
				return new JMo_BigDec(Lib_BigCalc.exp(Lib_BigCalc.log(value, JMo_BigDec.CONTEXT).divide(arg, JMo_BigDec.CONTEXT), JMo_BigDec.CONTEXT));
		}
		throw Err.invalid(op);
	}

	public static A_Number calcBigInt(final CallRuntime cr, final NOP1 op, final BigInteger value, final BigInteger arg) {

		switch(op) {
			case ADD:
				return new JMo_BigInt(arg.add(value));
			case SUB:
				return new JMo_BigInt(value.subtract(arg));
			case MUL:
				return new JMo_BigInt(value.multiply(arg));
			case DIV:
				if(arg.signum() == 0) {
//					throw new RuntimeError(cr, "Division by zero", "" + value + "/" + arg);
					Lib_AtomicMath.iDivisionByZero(cr, value, arg);
					return new JMo_BigDec(BigDecimal.ZERO);
				}
				else
					return new JMo_BigDec(new BigDecimal(value).divide(new BigDecimal(arg), JMo_BigDec.CONTEXT));
			case MOD:
				return new JMo_BigInt(value.mod(arg));
			case LOG:
				return new JMo_BigDec(Lib_BigMath.log(new BigDecimal(arg), new BigDecimal(value), JMo_BigDec.CONTEXT)); // Testen!
//					.log(null, null, null) Math.log(this.value) / Math.log(arg)); // benötigt die Basis und gibt die Potenz zurück
//			case INC:
//				return new JMo_BigInt(value.add(arg));	// TODO argDec --> Dec	// auch bei inc/dec ?!? oder hier wirklich nur erhöhen?!? Byte,Short,Int,Long?!?
//			case DEC:
//				return new JMo_BigInt(value.subtract(arg));	// TODO argDec --> Dec
			case POW:
				return new JMo_BigInt(value.pow(arg.intValueExact())); // More than "Int" is not is a kind of utopia
			case ROOT:
//				= exp(log(value) / pard))
				return new JMo_BigDec(Lib_BigCalc.exp(Lib_BigCalc.log(new BigDecimal(value), JMo_BigDec.CONTEXT).divide(new BigDecimal(arg), JMo_BigDec.CONTEXT), JMo_BigDec.CONTEXT)); // TODO Test CONTEXT 1-3
		}
		throw Err.invalid(op);
	}

	public static A_Number calcDec(final CallRuntime cr, final NOP1 op, final Dec value, final Dec arg) {
		// TODO second version of this Function with (int value) oder (double value)?!?

		switch(op) {
			case ADD:
				return new JMo_Dec(value.add(arg));
			case SUB:
				return new JMo_Dec(value.sub(arg));
			case MUL:
				return new JMo_Dec(value.mul(arg));
			case DIV:
				if(arg.is0()) {
//					throw new RuntimeError(cr, "Division by zero", "" + value + "/" + arg);
					Lib_AtomicMath.iDivisionByZero(cr, value, arg);
					return new JMo_Dec(Dec.ZERO);
				}
				else
					return new JMo_Dec(value.div(arg));
			case MOD:
				return new JMo_Dec(value.mod(arg));
			case LOG:
//				return new Dec(Math.log(value) / Math.log(arg)); // benötigt die Basis und gibt die Potenz zurück
				return new JMo_Dec(value.log(arg));
//			case INC:
//				return new Dec(value + arg);
//			case DEC:
//				return new Dec(value - arg);
			case POW:
				return new JMo_Dec(value.pow(arg));
			case ROOT:
//				return JMo_Dec.fromDouble(Math.exp(Math.log(value) / arg));
				return new JMo_Dec(value.root(arg));
		}
		throw Err.invalid(op);
	}

	public static A_Number calcDouble(final CallRuntime cr, final NOP1 op, final double value, final double arg) {
		// TODO second version of this Function with (int value) oder (double value)?!?

		switch(op) {
			case ADD:
				return new JMo_Double(value + arg);
			case SUB:
				return new JMo_Double(value - arg);
			case MUL:
				return new JMo_Double(value * arg);
			case DIV:
				if(arg == 0d) {
//					throw new RuntimeError(cr, "Division by zero", "" + value + "/" + arg);
					Lib_AtomicMath.iDivisionByZero(cr, value, arg);
					return new JMo_Double(0d);
				}
				else
					return new JMo_Double(value / arg);
			case MOD:
				return new JMo_Double(value % arg);
			case LOG:
				return new JMo_Double(Math.log(value) / Math.log(arg)); // benötigt die Basis und gibt die Potenz zurück
//			case INC:
//				return new Dec(value + arg);
//			case DEC:
//				return new Dec(value - arg);
			case POW:
				return new JMo_Double(Math.pow(value, arg));
			case ROOT:
				return new JMo_Double(Math.exp(Math.log(value) / arg));
		}
		throw Err.invalid(op);
	}

	public static A_Number calcInt(final CallRuntime cr, final NOP1 op, final int value, final int arg) {

		switch(op) {
			case ADD:
				return new Int(value + arg);
			case SUB:
				return new Int(value - arg);
			case MUL:
				return new Int(value * arg);
			case DIV:
				if(arg == 0) {
//					throw new RuntimeError(cr, "Division by zero", "" + value + "/" + arg);
					Lib_AtomicMath.iDivisionByZero(cr, value, arg);
					return new JMo_Dec(Dec.ZERO);
				}
				else
					return JMo_Dec.valueOf(cr, value / (double)arg);
			case MOD:
				return new Int(value % arg);
			case LOG:
				final double logResult = Math.log(value) / Math.log(arg);
				return JMo_Dec.valueOf(cr, logResult); // benötigt die Basis und gibt die Potenz zurück

//			case INC:
//				return new Int(value + arg);
//			case DEC:
//				return new Int(value - arg);
			case POW:
				final double res = Math.pow(value, arg);
				final int i = Lib_ConvertExact.doubleToInt(cr, res);
				return new Int(i);
			case ROOT:
				return JMo_Dec.valueOf(cr, Math.exp(Math.log(value) / arg));
		}
		throw Err.invalid(op);
	}

	public static A_Number calcLong(final CallRuntime cr, final NOP1 op, final long value, final long arg) {

		switch(op) {
			case ADD:
				return new JMo_Long(value + arg);
			case SUB:
				return new JMo_Long(value - arg);
			case MUL:
				return new JMo_Long(value * arg);
			case DIV:
				if(arg == 0) {
//					throw new RuntimeError(cr, "Division by zero", "" + value + "/" + arg);
					Lib_AtomicMath.iDivisionByZero(cr, value, arg);
					return new JMo_Double(0d);
				}
				else
					return new JMo_Double(value / (double)arg);
			case MOD:
				return new JMo_Long(value % arg);
			case LOG:
				return new JMo_Double(Math.log(value) / Math.log(arg)); // benötigt die Basis und gibt die Potenz zurück
//			case INC:
//				return new JMo_Long(value + arg);
//			case DEC:
//				return new JMo_Long(value - arg);
			case POW:
				final double res = Math.pow(value, arg);
				final long l = Lib_ConvertExact.doubleToLong(cr, res);
				return new JMo_Long(l);
			case ROOT:
				return new JMo_Double(Math.exp(Math.log(value) / arg));
		}
		throw Err.invalid(op);
	}

	private static void iDivisionByZero(final CallRuntime cr, final Object value, final Object arg) {
		cr.warning("Division by zero", "" + value + " / " + arg);
	}

}
