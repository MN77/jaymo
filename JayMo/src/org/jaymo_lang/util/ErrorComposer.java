/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.I_ErrorDetails;


/**
 * @author Michael Nitsche
 * @created 28.04.2022
 */
public class ErrorComposer {

	private final String         message;
	private final String         group;
	private final ErrorBaseDebug ebd;
	private final Throwable      ex;
	ArrayTable<Object>           tab       = new ArrayTable<>(3);
	private int                  maxFile   = 0;
	private int                  maxLine   = 0;
	private int                  maxLineNr = 0;


	public ErrorComposer(final Throwable ex, final CallRuntime cr) {
		this(ex, cr, null);
	}

	public ErrorComposer(final Throwable ex, final CallRuntime cr, final String messageToUse) {
		this.ex = ex;
		this.message = messageToUse;

		this.ebd = ex instanceof ErrorBaseDebug ? (ErrorBaseDebug)ex : null;

//		String className = ex.getClass().getSimpleName();
//		if(className.startsWith("Err_"))
//			className = className.substring(4);

		this.group = Lib_Error.errorTypeString(ex); //+ " (" + className + ")";

		this.iComposeTable(cr, ex);
	}

	public String compose() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Error   : ");
		sb.append(this.group);
		sb.append("\nMessage : ");

		final String usedMessage = this.message != null
			? this.message
			: Lib_Error.collectMessages(this.ex);
		sb.append(usedMessage);

		sb.append('\n');
		if(this.ex instanceof I_ErrorDetails)
			for(final Object o : ((I_ErrorDetails)this.ex).getDetails()) {
				if(!(this.ex instanceof ErrorBaseDebug)) // ErrorBaseDebug-Details are already preformated
					sb.append("Detail  : ");
				sb.append(ConvertObject.toText(o));
				sb.append('\n');
			}

		// Add Table
//		int rem = this.tab.size();
		for(final Object[] row : this.tab) {
			sb.append("  ");
//			sb.append(rem == 1 ? '>' : '@');
			sb.append('@');
			sb.append(' ');
			sb.append(FormString.width(this.maxFile, (String)this.iEmpty(row[0]), false));
			sb.append(' ');

			if(row[1] != null) {
				sb.append(':');
				final String line = "" + row[1];
				sb.append(line);
				sb.append(Lib_String.sequence(' ', this.maxLine - line.length()));
			}

			if(row[2] != null) {
				sb.append("    ");
				sb.append(row[2]);
			}
			sb.append('\n');
//			rem--;
		}
		sb.setLength(sb.length() - 1); // Remove last linebreak

		// Output
		return sb.toString();
	}

	private void iAdd(String file, final int rawLine, final String s) {
		file = (String)this.iEmpty(file);

		final int lenFile = file.length();
		if(lenFile > this.maxFile)
			this.maxFile = lenFile;

		if(rawLine > this.maxLineNr)
			this.maxLineNr = rawLine;

		this.tab.addRow(file, rawLine, s);
	}

	private void iComposeTable(final CallRuntime cr, final Throwable t) {
		Call last = null;
		Call first = null;

		if(this.ebd != null) {
			CallRuntime crv = this.ebd.getCallRuntime();

			while(crv != null) {

				if(crv.call != null && crv.call != last) {
					final Call c = crv.call;

					if(first == null)
						first = c;

					this.iAdd(c.debugInfo.getRawFile(), c.debugInfo.getRawLine(), c.toString(cr, STYPE.IDENT));
					last = c;
				}
				crv = crv.parent;
			}
		}

		if(cr != null && cr.call != null) {
			final Call c = cr.call;

			if((last == null || c != last) && (first == null || c != first)) {
				final String s = c.toString(cr, STYPE.IDENT);
				if(s.length() > 0) // This happens at CallRuntime.copyNull
					this.iAdd(c.debugInfo.getRawFile(), c.debugInfo.getRawLine(), s);
			}
		}
		else if(this.ebd != null) {
			final String file = this.ebd.getFile();
			final Integer line = this.ebd.getLine();

			if(file != null || line != null)
				this.iAdd(file, line, null);
		}
		else
			//			Possible reasons are: Java-Errors, StackOverflow
//			if(this.tab.isEmpty())
//				this.tab.addRow("", 0, "No runtime information available");
			for(final StackTraceElement ste : t.getStackTrace())
				this.iAdd(ste.getFileName(), ste.getLineNumber(), ste.getMethodName());
		this.maxLine = ("" + this.maxLineNr).length();
	}

	private Object iEmpty(final Object o) {
		return o == null ? "" : o;
	}

//	private static String iLimit(final String s) {
//		final int max = 40;
//		return s.length() < max ? s : s.substring(0, max) + "...";
//	}

}
