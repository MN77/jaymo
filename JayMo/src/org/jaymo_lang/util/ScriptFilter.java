/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.atomic.IntList;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 18.02.2020
 */
public class ScriptFilter {

	private StringBuilder            currentLine = null;
	private final SimpleList<String> lines       = new SimpleList<>();
	private final IntList            lineNr      = new IntList();


	/**
	 * String[] lines
	 * int[] linenr
	 */
	public Group2<String[], int[]> filter(final String filepath, final String s) {
		if(!this.lines.isEmpty())
			throw Err.invalid("Filter is already done!");

		boolean openText = false;
		boolean openString = false;
		boolean openChar = false;
		boolean openCmd = false;
		boolean openTree = false;
		boolean openTable = false;
		boolean ignoreChar = false;
		boolean commentLine = false;
		boolean commentBlock = false;
		boolean abort = false;
		boolean trimLeftSpace = false;
		boolean treeKeyCombine = false;
		byte lineIndent = 0;
		byte doubleQuotes = 0;
		byte abortPart = 0;
		byte countGreater = 0;
		byte countLesser = 0;
		byte countPipe = 0;
		char last1 = '\n';
		char last2 = '\n';

		final char[] ca = s.toCharArray();
		final int len = ca.length;

		//-----
		this.currentLine = null;
		int currentLineNr = 1;

		int idx = -1;

		for(final char c : ca) {
			idx++;

			if(ignoreChar) {
				this.append(c);
				ignoreChar = false;
				last2 = last1;
				last1 = c;
				continue;
			}

			if((commentLine || commentBlock) && c != '\n' && c != '*' && c != '#' && c != '/') { // Because of: #**#, */, \n
				abortPart = 0;
				last2 = last1;
				last1 = c;
				continue;
			}

			if(openText) {

				if(c == '"') {
					doubleQuotes++;

					if(doubleQuotes == 4) {
						openText = false;
						doubleQuotes = 0;
						this.currentLine = new StringBuilder(this.currentLine.substring(0, this.currentLine.length() - 6));
						this.currentLine.append('"');
						continue;
					}
					this.append("\\\"");
				}
				else {
					doubleQuotes = 0;

					if(c == '\n') {
						currentLineNr++;
						this.append("\\n");
					}
					else
						this.append(c);
				}
				continue;
			}

			if(openTree) {

				if(c == '<') {
					countLesser++;
					this.append(c);

					if(countLesser == 4) {
						openTree = false;
						treeKeyCombine = false;
						countLesser = 0;
					}
				}
				else {
					countLesser = 0;

					if(c == '\n') {
						currentLineNr++;
						if(!treeKeyCombine)
							this.append("\\n");
						treeKeyCombine = false;
					}
					else if(c == ',') {
						this.append("\\t"); // This is a mark for "combine key lines"
						treeKeyCombine = true;
					}
					else {
						this.append(c);
						if(c != ' ')
							treeKeyCombine = false;
					}
				}
				continue;
			}
			//---------------------

			boolean add = true;

			switch(c) {
				case '\n':
					doubleQuotes = 0;
					commentLine = false;
					openString = false;
					openChar = false;
					openCmd = false;
					ignoreChar = false;
					trimLeftSpace = false;
					lineIndent = 0;

					if(openTable)
						this.append('\u0001'); // "\\n" may also match table content.
					else {
						this.add(currentLineNr, filepath);
						this.currentLine = null;
					}
					currentLineNr++;
					add = false;
					break;

				case ';':
					if(!openString && !openChar && !openCmd) {
						this.add(currentLineNr, filepath);
						this.currentLine = new StringBuilder(Lib_String.sequence('\t', lineIndent));

						trimLeftSpace = true;
						add = false;
					}
					break;

				case '\\':
					if(openString || openChar || openCmd)
						ignoreChar = true;
					break;

				case '\'':
					if(!openString && !openCmd)
						openChar = !openChar;
					break;

				case '"':
					if(!openChar && !openCmd) {
						openString = !openString;

						doubleQuotes++;

						if(!openString && doubleQuotes == 4) {
							openText = true;
							doubleQuotes = 0;
							this.currentLine = new StringBuilder(this.currentLine.substring(0, this.currentLine.length() - 3));
						}
					}
					break;

				case '>':
					if(!openChar && !openCmd && !openString && !commentLine && !commentBlock) {
						countGreater++;

						if(countGreater == 4) {
							openTree = true;
							countGreater = 0;
						}
					}
					break;

				case '|':
					if(!openChar && !openCmd && !openString && !commentLine && !commentBlock) {
						countPipe++;

						if(countPipe == 4) {
							openTable = !openTable;
							countPipe = 0;
						}
					}
					break;

				case '´':
				case '`':
					if(!openCmd && last1 == ')')
						break;
					if(!openChar && !openString)
						openCmd = !openCmd;
					break;

				case '#':
					if(!commentBlock && last1 == '*' && abortPart == 3) {
						abort = true;
						break;
					}

					if(!openChar && !openString && !openCmd) {
						abortPart = commentLine ? (byte)0 : (byte)1;

						if(!commentBlock) {
							commentLine = true;
							add = false;
						}
					}
					break;

				case '*':
					if(last1 == '#' && abortPart == 1) { // #**#
						abortPart = 2;
						break;
					}
					if(last1 == '*' && abortPart == 2) { // #**#
						abortPart = 3;
						break;
					}
					if(last1 == '/' && last2 != '/' && !commentLine && !openString && !openChar && !openCmd) {
//					if(last1 == '/' && !comment_line && !open_string && !open_char && !open_cmd && (idx < 2 || ca[idx-2] != '/')) {
						commentBlock = true;
//						comment_line = false;
						add = false;
						if(this.currentLine != null && this.currentLine.charAt(this.currentLine.length() - 1) == '/')
							this.currentLine = new StringBuilder(this.currentLine.substring(0, this.currentLine.length() - 1)); // Remove already added '/'
					}
					break;

				case '/':
					if(last1 == '*' && (idx + 1 == len || ca[idx + 1] != '/')) //   Must be possible: /**/
						if(commentBlock) {
							commentBlock = false;
							add = false;
							trimLeftSpace = true;
						}
						else if(!commentLine && !openString && !openText)
							throw new CodeError("Invalid end of block comment!", "Missing: /*", new DebugInfo(filepath, currentLineNr));
					break;

				case ' ':
					if(trimLeftSpace) {
						add = false;
						break;
					}
					break;

				case '\t':
//					if(trim_left_tab) {
//						add = false;
//						break;
//					}
//					else
					if(last1 == '\n' || last1 == '\t') {
						lineIndent++;
						if(lineIndent == Byte.MAX_VALUE)
							throw new CodeError("Too much indents!", "Line with " + lineIndent + " tabs.", new DebugInfo(filepath, currentLineNr));
					}
					break;

				default:
					if(c == ',' && openTable) {
						this.append('\u0002'); // "\\t" may also match table content.
						add = false;
					}

					if(c == '(' && openCmd && (last1 == '`' || last1 == '´'))
						openCmd = false;

					abortPart = 0;
					doubleQuotes = 0;
					countGreater = 0;
					countLesser = 0;
					countPipe = 0;
			}
			if(abort)
				break;

			if(add && !commentLine && !commentBlock) {
				trimLeftSpace = false;
				this.append(c);
			}
			last2 = last1;
			last1 = c;
		}
		// Clear buffer:
		this.add(currentLineNr, filepath);

		// DEBUG:
//		for(int i = 0; i < lines.size(); i++)
//			MOut.print(FormNumber.right(3, linenr.get(i)) + " |" + lines.get(i));

		return new Group2<>(this.lines.toArray(new String[this.lines.size()]), this.lineNr.toArray());
	}

	private void add(final int curLineNr, final String filepath) {
		if(this.currentLine == null)
			return;

		final String line = this.currentLine.toString();

		if(line.trim().length() > 0) {
			if(line.charAt(0) == ' ')
				throw new CodeError("Line starts with Space-Char(s)", "Line: " + curLineNr, new DebugInfo(filepath, curLineNr));

			this.lines.add(line);
			this.lineNr.add(curLineNr);
		}
	}

	private void append(final char c) {
		if(this.currentLine == null)
			this.currentLine = new StringBuilder(32); // 32 performs better than 16
		this.currentLine.append(c);
	}

	private void append(final String s) {
		if(this.currentLine == null)
			this.currentLine = new StringBuilder(32); // 32 performs better than 16
		this.currentLine.append(s);
	}

}
