/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 18.07.2020
 *
 *          https://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B
 */
public class Lib_Function {

	public static boolean isMathematicFunction(final String m) {
		if(m == null || m.length() == 0)
			return false;
		final char c = m.charAt(0);
		return !(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_' || c == '@');
	}

	/**
	 * @return
	 *         0 = no arguments
	 *         1 = one argument needed
	 *         2 = no or one argument needed
	 *         -1 = this method is not a VarFunction
	 */
	public static byte isVarFunction(final String method) {

		switch(method) {
			case "=":
			case "=~":
				return 1;

//			case "++":	// Not completely coherent, but common practice.	(And 5++ is senseless)
//			case "--":	// Not completely coherent, but common practice.	(And 5-- is senseless)
//				return 0;

			case "=!":
			case "=++":
			case "=--":
			case "=**":
			case "=//":
			case "=<<":
			case "=>>":
				return 0;

//			case "inc":
//			case "dec":
//			case "pow":
//			case "sqrt":

			case "+=":
			case "-=":
			case "*=":
			case "/=":
			case "%=":

			case "&=":
			case "^=":
			case "|=":
			case "<<=":
			case ">>=":

			case "++=":
			case "--=":
			case "**=":
			case "//=":

			case "&&=":
			case "^^=":
			case "||=":
				return 1;

			default:
				return -1;
		}
	}

	/**
	 * @return
	 *         0 = no arguments
	 *         1 = one argument needed
	 *         2 = no or one argument needed
	 *         -1 = this method is not a VarFunction
	 */
	public static byte isVarletFunction(final String method) {
		// TODO speed this up!

		switch(method) {
			// TODO set convertSet setLazy, ...
			case "let":
			case "convertLet": // Like "let", but converts the value to the var-type
				return 1;

			case "notLet":
			case "sqrtLet":
				return 0;

			case "addLet":
			case "subLet":
			case "mulLet":
			case "divLet":
			case "modLet":

			case "andLet":
			case "orLet":
			case "xorLet":

			case "bitOrLet":
			case "bitAndLet":
			case "bitXorLet":
				return 1;

			case "incLet":
			case "decLet":
			case "powLet":
			case "rootLet": // TODO RadixLet ?!?
			case "shiftLeftLet":
			case "shiftRightLet":
				return 2;

			default:
				return -1;
		}
	}

	public static I_Object mCalcLet(final CallRuntime cr, final String method, byte methodParState, final I_Object oldVarValue) { //TODO cr.args(... ?!?
		if(methodParState == 2)
			methodParState = cr.argCount() == 0 ? (byte)0 : (byte)1;

		if(methodParState == 0)
			return Lib_Function.mCalcLetNoPar(cr, method, oldVarValue);
		if(methodParState == 1)
			return Lib_Function.mCalcLetWithPar(cr, method, oldVarValue);

		throw Err.todo(method);
	}

	private static String iConvertMethod(final String method) {
		if(method.charAt(0) == '=')
			return method.substring(1);
		if(method.endsWith("="))
			return method.substring(0, method.length() - 1);

		return method;
	}

	/**
	 * ++ --> ++1
	 * ...
	 */
	private static I_Object mCalcLetNoPar(final CallRuntime cr, final String method, final I_Object oldVarValue) {
		cr.argsNone();
		final String calcMethod = Lib_Function.iConvertMethod(method);
		final Call[] parCalls = {};
		final Call c3 = new Call(cr.getSurrBlock(), oldVarValue, calcMethod, parCalls, cr.getDebugInfo());
		final CallRuntime crx = cr.copyCall(c3, false);
		return crx.exec(null, true); // Stream null?!?
	}

	/**
	 * =
	 * +=
	 * ++=
	 * ...
	 */
	private static I_Object mCalcLetWithPar(final CallRuntime cr, final String method, final I_Object oldVarValue) {
		Lib_Error.ifArgs(cr.argCount(), 1, 1, cr, oldVarValue); //TODO cr.args(... ?!?
		final String calcMethod = Lib_Function.iConvertMethod(method);
		final Call c3 = new Call(cr.getSurrBlock(), oldVarValue, calcMethod, cr.call.argCalls, cr.getDebugInfo());
		final CallRuntime crx = cr.copyCall(c3, false);
		return crx.exec(null, true);
	}

}
