/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jaymo_lang.api.JMo_Object;
import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.object.I_Object;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.type.Lib_Class;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


public class ClassFinder {

	private static ClassFinder singleton = null;

	private HashMap<String, Class<?>> javaJMoClasses    = null;
	private final SimpleList<String>  javaImportPackage = new SimpleList<>();
	private final SimpleList<String>  javaImportClass   = new SimpleList<>();


	public static ClassFinder getInstance() {
		if(ClassFinder.singleton == null)
			ClassFinder.singleton = new ClassFinder();
		return ClassFinder.singleton;
	}

	private ClassFinder() {
		this.reset();
	}

	public void addJavaImportClass(final String path, final DebugInfo debug) {
		this.iCheckJavaClassPath(path, true, debug);
		this.javaImportClass.add(path); // Doubles are possible, but not disturbing.
	}

	public void addJavaImportPackage(final String path, final DebugInfo debug) {
		this.iCheckJavaClassPath(path, false, debug);
		this.javaImportPackage.add(path); // Doubles are possible, but not disturbing.
	}

	public Iterable<String> allJMoClasses() {
		this.iInitJavaJMoClasses();
		final Set<String> keys = this.javaJMoClasses.keySet();

		final SimpleList<String> result = new SimpleList<>(keys.size());
		for(final String key : keys)
			result.add(key.substring(4));
		return result;
	}

	public void describe() {
		MOut.print("Import packages:");
		MOut.print(this.javaImportPackage);
		MOut.print("Import classes:");
		MOut.print(this.javaImportClass);
		MOut.print("Classes:");
		MOut.print(this.javaJMoClasses);
	}

//	public Collection<Class<?>> getJMoClasses() {
//		return this.javaJMoClasses.values();
//	}

	/**
	 * Clear all imports
	 */
	public void reset() {
		this.javaJMoClasses = null;
		this.javaImportPackage.clear();
		this.javaImportClass.clear();

		// Default-Imports
		this.javaImportPackage.add("java.lang");
		this.javaImportPackage.add("java.util");
		// Only really necessary, otherwise classes will be overwritten.

		// also useful
//		this.importsPackage.add("java.io");
//		this.importsPackage.add("java.net");
//		this.importsPackage.add("java.awt");	// Overwrites SWT!
//		this.importsPackage.add("java.math");
//		this.importsPackage.add("java.text");
//		this.importsPackage.add("java.time");
	}

	/** Returns null, if nothing is found **/
	public Class<?> searchJavaClass(final String name) {
		Class<?> result = Lib_Class.getClassByPath(this.javaImportClass, name);

		if(result == null) {
			result = Lib_Class.getClassByPackage(this.javaImportPackage, name);
			if(result != null)
				this.javaImportClass.add(result.getCanonicalName()); // remember full class-name  //TODO Also remember Class?!?
		}
		return result;
	}

	/** Returns null, if nothing is found **/
	public Class<?> searchJavaClassPath(final String path) {

		try {
			return Class.forName(path);
		}
		catch(final ClassNotFoundException e) {
			return null;
		}
	}

	/** Returns null, if nothing is found **/
	public Class<?> searchJMoClass(final String name) {
		this.iInitJavaJMoClasses();
		return this.javaJMoClasses.get(name);
	}


	// --- Private ---

	private void iCheckJavaClassPath(final String path, final boolean isClass, final DebugInfo debug) {
		final int len = path.length();

		if(len <= 1)
			throw new CodeError("Invalid Java classpath", "Got: " + path, debug);

		// first char
		char c = path.charAt(0);
		if(!(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_'))
			throw new CodeError("Invalid Java classpath", "Got: " + path, debug);

		// last char
		c = path.charAt(len - 1);
		if(c == '.' || c == '*')
			throw new CodeError("Invalid Java classpath", "Got: " + path, debug);

		for(int i = 1; i < len; i++) { // First char is already checked
			c = path.charAt(i);

			if(isClass && c == '$')
				continue;

			if(!(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '.' || c >= '0' && c <= '9' || c == '_'))
				throw new CodeError("Invalid Java classpath", "Got: " + path, debug);
		}
	}

	private void iInitJavaJMoClasses() {
		if(this.javaJMoClasses == null) // ca. 600 times for TestAuto
			try {
				this.javaJMoClasses = this.iSearchJMoClasses(); // 70-80ms
			}
			catch(final Exception e) {
				throw Err.exit(e);
			}
	}

//	private void iSearchJar(final Set<String> set, final URL url, final String prefix) {
//		try(JarInputStream jin = new JarInputStream(Files.newInputStream(Paths.get(url.toURI())))) {
//			for(ZipEntry ze; (ze = jin.getNextEntry()) != null;)
//				if(ze.isDirectory() && ze.getName().startsWith(prefix))
//					set.add(ze.getName());
//		}
//		catch(final Exception e) {
//			Err.show(e);
//		}
//	}

//	private Set<String> iSearchPackages(final String prefix) {
//		final ClassLoader sysloader = ClassLoader.getSystemClassLoader();
//		final Set<String> set = new HashSet<>(); //LinkedHashSet
//
//		if(sysloader instanceof URLClassLoader)	// works only up to Java 8
//			for(final URL url : ((URLClassLoader)sysloader).getURLs()) {
//				final String urlString = url.toString().toLowerCase();
//				if(urlString.endsWith("jar") || urlString.endsWith("zip"))
//					this.iSearchJar(set, url, prefix);
//			}
//
//		return set;
//	}

	private String iPathConvert(String line) {
		final int start = line.startsWith("/") ? 1 : 0;
		line = line.replace('/', '.');
		return line.substring(start, line.lastIndexOf('.')); // Remove suffix
	}

	private HashMap<String, Class<?>> iSearchJMoClasses() throws Exception {
		final List<Class<?>> classList = new SimpleList<>();

		try {
			final CodeSource src = ClassFinder.class.getProtectionDomain().getCodeSource();
			final ClassLoader classLoader = ClassFinder.class.getClassLoader();

			final Consumer<String> add = line-> {
				final String filename = line.substring(line.lastIndexOf('/') + 1);

				if(filename.endsWith(".class") && filename.startsWith("JMo_") && !filename.contains("$"))
					try {
						final String classPath = this.iPathConvert(line);
//						final Class<?> c = Class.forName(classPath);  // This maybe doesn't load classes
						final Class<?> c = classLoader.loadClass(classPath); // This is better
						if(I_Object.class.isAssignableFrom(c) || JMo_Object.class.isAssignableFrom(c))
							classList.add(c);
					}
					catch(final ClassNotFoundException e) {
						Err.show(e);
						return;
					}
					catch(final NoClassDefFoundError e) {
//						MOut.print("NoClassDefFoundError: "+line+" | "+this.iPathConvert(line));
//						This error is also thrown if an external Jar (even if not needed) cannot be loaded.
//						if(debug)
//							Err.show(e, "Missing Jar-Library");
						return;
					}
			};

			if(src.getLocation().toString().toLowerCase().endsWith(".jar"))
				this.searchClassesFromJar(add);
			else
				this.searchClassesDirect(add);
		}
		catch(final Exception e) {
			Err.exit(e);
		}
		final HashMap<String, Class<?>> map = new HashMap<>();
		for(final Class<?> c : classList)
			map.put(c.getSimpleName(), c);

		return map;
	}

	/**
	 * Durchläuft alle im Projekt verlinkten Projekte und dessen
	 * Projekte/Dateien
	 */
	private void searchClassesDirect(final Consumer<String> add) throws IOException, URISyntaxException, ClassNotFoundException {
		final Enumeration<URL> eu = ClassFinder.class.getClassLoader().getResources("");

		while(eu.hasMoreElements()) {
			final URL url = eu.nextElement();

			/*
			 * Bugfix: Prevent "jar:", that will lead to a FileSystemNotFoundException --> jdk.zipfs/jdk.nio.zipfs.ZipFileSystemProvider.getFileSystem
			 * Now: Only "file" is accepted!
			 */
			if(!url.getProtocol().equals("file"))
				continue;

			// Convert
			final URI uri = url.toURI();
			final Path myPath = Paths.get(uri);
			final String base = myPath.toString();

			final Stream<Path> walk = Files.walk(myPath, Integer.MAX_VALUE);

			for(final Iterator<Path> it = walk.iterator(); it.hasNext();) {
				String line = it.next().toString();
				line = line.substring(base.length());
				add.accept(line); // Add entry
			}
			walk.close();
		}
	}

	/**
	 * Liest alle geladenen Jars
	 */
	private void searchClassesFromJar(final Consumer<String> add) throws IOException, ClassNotFoundException {
		final String classpath = System.getProperty("java.class.path");
		final String[] paths = classpath.split(System.getProperty("path.separator"));

		final SimpleList<String> scanned = new SimpleList<>(paths.length); // TODO Combine with search for Java-Pathes!

		for(final String path : paths) {
			if(scanned.contains(path))
				continue;
			scanned.add(path);

			final File file = new File(path);

			if(file.isFile()) { //  && file.getName().startsWith("jmo")
				final InputStream is = new FileInputStream(file);
				final ZipInputStream zip = new ZipInputStream(is);

				while(true) {
					final ZipEntry e = zip.getNextEntry();
					if(e == null)
						break;
					add.accept(e.getName()); // Add entry
				}
				zip.close();
			}
		}
	}

}
