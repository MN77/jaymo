/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.util.Lib_BigMath;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 01.09.2019
 *          TODO Rename to Lib_AtomicConv
 */
public class Lib_AtomConv {

	/**
	 * @apiNote Converts an atomic type in a greater type. Safely conversion, only without the loss of data.
	 *          TODO Move this function to own class: Lib_AtomicUpgrade
	 */
	public static I_Object autoUpgrade(final CallRuntime cr, final I_Object obj, final String objType, final String safedType) {

		switch(safedType) {
			case "Short":
				switch(objType) {
					case "Byte":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.SHORT);
				}
				return null;
			case "Int":
				switch(objType) {
					case "Byte":
					case "Short":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.INT);
				}
				return null;
			case "Long":
				switch(objType) {
					case "Byte":
					case "Short":
					case "Int":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.LONG);
				}
				return null;
			case "BigInt":
				switch(objType) {
					case "Byte":
					case "Short":
					case "Int":
					case "Long":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.BIGINT);
				}
				return null;

			case "Dec":
				switch(objType) {
					case "Byte":
					case "Short":
					case "Int":
//					case "Float":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.DEC);
				}
				return null;
			case "Float":
				switch(objType) {
					case "Byte":
					case "Short":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.FLOAT);
				}
				return null;

			case "Double":
				switch(objType) {
					case "Byte":
					case "Short":
					case "Int":
					case "Dec":
					case "Float":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.DOUBLE);
				}
				return null;

			case "BigDec":
				switch(objType) {
					case "Byte":
					case "Short":
					case "Int":
					case "Long":
					case "BigInt":
					case "Dec":
					case "Float":
					case "Double":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.BIGDEC);
				}
				return null;

			case "DecNumber":
				switch(objType) {
					case "Byte":
					case "Short":
					case "Int":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.DEC);
					case "Long":
					case "BigInt":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.BIGDEC);
				}
				return null;

			case "Str":
				switch(objType) {
					case "Byte":
					case "Short":
					case "Int":
					case "Long":
					case "BigInt":
					case "Dec":
					case "Float":
					case "Double":
					case "BigDec":
					case "Char":
						return Lib_AtomConv.convert(cr, (I_Atomic)obj, ATOMIC.STR);
				}
				return null;
		}
		return null;
	}

	public static I_Atomic convert(final CallRuntime cr, final I_Atomic obj, final ATOMIC to) {
		final ATOMIC from = obj.getEnum();

		if(to == ATOMIC.STR)
			return from == ATOMIC.STR
				? obj
				: new Str(obj.toString());

		switch(from) {
			case BOOL:
				return Lib_AtomConv.convertBool(cr, to, (Bool)obj);
			case CHAR:
				return Lib_AtomConv.convertChar(cr, to, (Char)obj);
			case BYTE:
				return Lib_AtomConv.convertByte(cr, to, (JMo_Byte)obj);
			case SHORT:
				return Lib_AtomConv.convertShort(cr, to, (JMo_Short)obj);
			case INT:
				return Lib_AtomConv.convertInt(cr, to, (Int)obj);
			case LONG:
				return Lib_AtomConv.convertLong(cr, to, (JMo_Long)obj);
			case BIGINT:
				return Lib_AtomConv.convertBigInt(cr, to, (JMo_BigInt)obj);
			case DEC:
				return Lib_AtomConv.convertDec(cr, to, (JMo_Dec)obj);
			case FLOAT:
				return Lib_AtomConv.convertFloat(cr, to, (JMo_Float)obj);
			case DOUBLE:
				return Lib_AtomConv.convertDouble(cr, to, (JMo_Double)obj);
			case BIGDEC:
				return Lib_AtomConv.convertBigDec(cr, to, (JMo_BigDec)obj);
			case STR:
				return Lib_AtomConv.convertStr(cr, to, (Str)obj);
//			case NIL:
//				return Lib_AtomConv.convertNil(cr, to);
			default:
				throw Lib_AtomConv.iImpossibleConversion(cr, obj.getTypeName(), to, obj);
		}
	}

	private static I_Atomic convertBigDec(final CallRuntime cr, final ATOMIC to, final JMo_BigDec so) {
		final BigDecimal v = so.getValue();

		switch(to) {
			case BOOL:
				return Bool.getObject(v.compareTo(BigDecimal.valueOf(0)) == 1);
			case CHAR:
				return Lib_AtomConv.iDoubleToChar(cr, so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
//				return new JMo_Byte(v.byteValue());
				return new JMo_Byte((byte)v.intValue());
			case SHORT:
				Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v, to);
//				return new JMo_Short(v.shortValue());
				return new JMo_Short((short)v.intValue());
			case INT:
				Lib_ErrorConv.checkConversion(cr, Integer.MIN_VALUE, Int.TOP_VALUE, v, to);
//				return new Int(v.intValue());
				return new Int((int)v.longValue());
			case LONG:
				Lib_ErrorConv.checkConversion(cr, Long.MIN_VALUE, JMo_Long.TOP_VALUE, v, to);
				return new JMo_Long(v.longValue());
			case BIGINT:
				return new JMo_BigInt(v.toBigInteger());
			case DEC:
				Lib_ErrorConv.checkConversion(cr, Dec.MIN_VALUE, JMo_Dec.TOP_VALUE, v, to);
				return new JMo_Dec(Dec.parseDec(v.toPlainString()));
			case FLOAT:
//				Lib_ErrorConv.checkConversion(cr, Float.MIN_VALUE, Float.MAX_VALUE, v, to);
				return new JMo_Float(v.floatValue()); // Handles also nan and infinity
			case DOUBLE:
//				Lib_ErrorConv.checkConversion(cr, Double.MIN_VALUE, Double.MAX_VALUE, v, to);
				return new JMo_Double(v.doubleValue()); // Handles also nan and infinity
			case BIGDEC:
				return so;
			default:
//				throw new RuntimeError(cr, "Impossible conversion", "BigDec --> " + to.name);
		}
		throw Err.impossible();
	}

	private static I_Atomic convertBigInt(final CallRuntime cr, final ATOMIC to, final JMo_BigInt so) {
		final BigInteger v = so.getValue();

		switch(to) {
			case BOOL:
				return Bool.getObject(v.compareTo(BigInteger.valueOf(0)) == 1);
			case CHAR:
				return Lib_AtomConv.iToChar(cr, "BigInt", so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
//				return new JMo_Byte(v.byteValue());
				return new JMo_Byte((byte)v.intValue());
			case SHORT:
				Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v, to);
//				return new JMo_Short(v.shortValue());
				return new JMo_Short((short)v.intValue());
			case INT:
				Lib_ErrorConv.checkConversion(cr, Integer.MIN_VALUE, Int.TOP_VALUE, v, to);
//				return new Int(v.intValue());
				return new Int((int)v.longValue());
			case LONG:
				Lib_ErrorConv.checkConversion(cr, Long.MIN_VALUE, JMo_Long.TOP_VALUE, v, to);
				return new JMo_Long(v.longValue());
			case BIGINT:
				return so;
			case DEC:
				Lib_ErrorConv.checkConversion(cr, Dec.MIN_VALUE, JMo_Dec.TOP_VALUE, v, to);
				return new JMo_Dec(Dec.parseDec(v.toString()));
			case FLOAT:
//				Lib_ErrorConv.checkConversion(cr, Float.MIN_VALUE, Float.MAX_VALUE, v, to);
				return new JMo_Float(v.floatValue()); // Handles also nan and infinity
			case DOUBLE:
//				Lib_ErrorConv.checkConversion(cr, Double.MIN_VALUE, Double.MAX_VALUE, v, to);
				return new JMo_Double(v.doubleValue()); // Handles also nan and infinity
			case BIGDEC:
				return new JMo_BigDec(new BigDecimal(v));
			default:
//				throw new RuntimeError(cr, "Impossible conversion", "BigInt --> " + to.name);
		}
		throw Err.impossible();
	}

	private static I_Atomic convertBool(final CallRuntime cr, final ATOMIC to, final Bool so) {
		final boolean v = so.getValue();

		switch(to) {
			case BOOL:
				return so;
			case CHAR:
				return new Char(v ? '1' : '0');
			case BYTE:
				return new JMo_Byte(v ? (byte)1 : (byte)0);
			case SHORT:
				return new JMo_Short(v ? (short)1 : (short)0);
			case INT:
				return new Int(v ? 1 : 0);
			case LONG:
				return new JMo_Long(v ? 1 : 0);
			case BIGINT:
				return new JMo_BigInt(BigInteger.valueOf(v ? 1 : 0));
			case DEC:
				return new JMo_Dec(Dec.valueOf(v ? 1 : 0));
			case FLOAT:
				return new JMo_Float(v ? 1 : 0);
			case DOUBLE:
				return new JMo_Double(v ? 1 : 0);
			case BIGDEC:
				return new JMo_BigDec(BigDecimal.valueOf(v ? 1 : 0));
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertByte(final CallRuntime cr, final ATOMIC to, final JMo_Byte so) {
		final byte v = so.getValue();

		switch(to) {
			case BOOL:
				return Bool.getObject(v > 0);
			case CHAR:
				return Lib_AtomConv.iToChar(cr, "Byte", so, v);
			case BYTE:
				return so;
			case SHORT:
				return new JMo_Short(v);
			case INT:
				return new Int(v);
			case LONG:
				return new JMo_Long(v);
			case BIGINT:
				return new JMo_BigInt(BigInteger.valueOf(v));
			case DEC:
				return new JMo_Dec(Dec.valueOf(v));
			case FLOAT:
				return new JMo_Float(v);
			case DOUBLE:
				return new JMo_Double(v);
			case BIGDEC:
				return new JMo_BigDec(BigDecimal.valueOf(v));
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertChar(final CallRuntime cr, final ATOMIC to, final Char so) {
		final char v = so.getValue();

		try {

			switch(to) {
				case BOOL:
//					return Bool.getObject(v == '1');
					return Bool.getObject(v != '0' && v != '\0');
				case CHAR:
					return so;
				case BYTE:
					return new JMo_Byte(Byte.parseByte("" + v));
				case SHORT:
					return new JMo_Short(Short.parseShort("" + v));
				case INT:
					return new Int(Integer.parseInt("" + v));
				case LONG:
					return new JMo_Long(Long.parseLong("" + v));
				case BIGINT:
					return new JMo_BigInt(new BigInteger("" + v));
				case DEC:
					return new JMo_Dec(Dec.parseDec("" + v));
				case FLOAT:
					return new JMo_Float(Float.parseFloat("" + v));
				case DOUBLE:
					return new JMo_Double(Double.parseDouble("" + v));
				case BIGDEC:
					return new JMo_BigDec(new BigDecimal("" + v));
				default:
			}
			throw Err.impossible();
		}
		catch(final NumberFormatException e) {
			throw Lib_AtomConv.iImpossibleConversion(cr, "Char", to, so);
		}
	}

	private static I_Atomic convertDec(final CallRuntime cr, final ATOMIC to, final JMo_Dec so) {
		final Dec v = so.getValue();

		switch(to) {
			case BOOL:
				return Bool.getObject(v.isGreaterZero());
			case CHAR:
				return Lib_AtomConv.iDoubleToChar(cr, so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v.longValue(), to);
				return new JMo_Byte(v.byteValue());
			case SHORT:
				Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v.longValue(), to);
				return new JMo_Short(v.shortValue());
			case INT:
				Lib_ErrorConv.checkConversion(cr, Integer.MIN_VALUE, Int.TOP_VALUE, v.longValue(), to);
				return new Int(v.intValue());
			case LONG:
				return new JMo_Long(v.longValue());
			case BIGINT:
				final BigInteger bi = BigInteger.valueOf(v.longValue());
				return new JMo_BigInt(bi);
			case DEC:
				return so;
			case FLOAT:
				return new JMo_Float(v.floatValue());
			case DOUBLE:
				return new JMo_Double(v.doubleValue());
			case BIGDEC:
				return new JMo_BigDec(new BigDecimal("" + so)); // Using String-Constructor is very important because of fractals!
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertDouble(final CallRuntime cr, final ATOMIC to, final JMo_Double so) {
		if(to == ATOMIC.DOUBLE)
			return so;

		final double v = so.getValue();

		if(to != ATOMIC.FLOAT && !Double.isFinite(v)) {	//to != ATOMIC.DOUBLE &&
			final StringBuilder sb = new StringBuilder();
			sb.append("Got: ");

			if(Double.isNaN(v))
				sb.append(ATOMIC.NOT_A_NUMBER);
			else if(v == Double.POSITIVE_INFINITY)
				sb.append(ATOMIC.INFINITY);
			else
				sb.append(ATOMIC.INFINITY_NEGATIVE);

			throw new RuntimeError(cr, "Invalid value for conversion to " + to.name, sb.toString());
		}

		switch(to) {
			case BOOL:
				return Bool.getObject(v > 0);
			case CHAR:
				return Lib_AtomConv.iDoubleToChar(cr, so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
				return new JMo_Byte((byte)v);
			case SHORT:
				Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v, to);
				return new JMo_Short((short)v);
			case INT:
				Lib_ErrorConv.checkConversion(cr, Integer.MIN_VALUE, Int.TOP_VALUE, v, to);
				return new Int((int)v);
			case LONG:
				Lib_ErrorConv.checkConversion(cr, Long.MIN_VALUE, Long.MAX_VALUE, JMo_Long.TOP_VALUE, v, to);
				return new JMo_Long((long)v);
			case BIGINT:
				final BigInteger bi = Lib_BigMath.convertToBigInteger(v);
				return new JMo_BigInt(bi);
			case DEC:
				Lib_ErrorConv.checkConversion(cr, Dec.MIN_VALUE, Dec.MAX_VALUE, JMo_Dec.TOP_VALUE, v, to);
				return JMo_Dec.valueOf(cr, v);
			case FLOAT:
//				Lib_ErrorConv.checkConversion(cr, Float.MIN_VALUE, Float.MAX_VALUE, v, to);
				return new JMo_Float((float)v);
//			case DOUBLE:
//				return so;
			case BIGDEC:
				return new JMo_BigDec(new BigDecimal("" + v)); // Using String-Constructor is very important because of fractals!
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertFloat(final CallRuntime cr, final ATOMIC to, final JMo_Float so) {
		if(to == ATOMIC.FLOAT)
			return so;

		final float v = so.getValue();

		if(to != ATOMIC.DOUBLE && !Float.isFinite(v)) {
			final StringBuilder sb = new StringBuilder();
			sb.append("Got: ");

			if(Float.isNaN(v))
				sb.append(ATOMIC.NOT_A_NUMBER);
			else if(v == Float.POSITIVE_INFINITY)
				sb.append(ATOMIC.INFINITY);
			else
				sb.append(ATOMIC.INFINITY_NEGATIVE);

			throw new RuntimeError(cr, "Invalid value for conversion to " + to.name, sb.toString());
		}

		switch(to) {
			case BOOL:
				return Bool.getObject(v > 0);
			case CHAR:
				return Lib_AtomConv.iDoubleToChar(cr, so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
				return new JMo_Byte((byte)v);
			case SHORT:
				Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v, to);
				return new JMo_Short((short)v);
			case INT:
				Lib_ErrorConv.checkConversion(cr, Integer.MIN_VALUE, Int.TOP_VALUE, v, to);
				return new Int((int)v);
			case LONG:
				Lib_ErrorConv.checkConversion(cr, Long.MIN_VALUE, Long.MAX_VALUE, JMo_Long.TOP_VALUE, v, to);
				return new JMo_Long((long)v);
			case BIGINT:
				final BigInteger bi = Lib_BigMath.convertToBigInteger(v);
				return new JMo_BigInt(bi);
			case DEC:
				Lib_ErrorConv.checkConversion(cr, Dec.MIN_VALUE, Dec.MAX_VALUE, JMo_Dec.TOP_VALUE, v, to);
				return JMo_Dec.valueOf(cr, v);
//			case FLOAT:
//				return so;
			case DOUBLE:
				return new JMo_Double(v);
			case BIGDEC:
				return new JMo_BigDec(new BigDecimal("" + v)); // Using String-Constructor is very important because of fractals!
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertInt(final CallRuntime cr, final ATOMIC to, final Int so) {
		final int v = so.getValue();

		switch(to) {
			case BOOL:
				return Bool.getObject(v > 0);
			case CHAR:
				return Lib_AtomConv.iToChar(cr, "Int", so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
				return new JMo_Byte((byte)v);
			case SHORT:
				Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v, to);
				return new JMo_Short((short)v);
			case INT:
				return so;
			case LONG:
				return new JMo_Long(v);
			case BIGINT:
				return new JMo_BigInt(BigInteger.valueOf(v));
			case DEC:
				return JMo_Dec.valueOf(cr, v);
			case FLOAT:
				return new JMo_Float(v);
			case DOUBLE:
				return new JMo_Double(v);
			case BIGDEC:
				return new JMo_BigDec(BigDecimal.valueOf(v));
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertLong(final CallRuntime cr, final ATOMIC to, final JMo_Long so) {
		final long v = so.getValue();

		switch(to) {
			case BOOL:
				return Bool.getObject(v > 0);
			case CHAR:
				return Lib_AtomConv.iToChar(cr, "Long", so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
				return new JMo_Byte((byte)v);
			case SHORT:
				Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v, to);
				return new JMo_Short((short)v);
			case INT:
				Lib_ErrorConv.checkConversion(cr, Integer.MIN_VALUE, Int.TOP_VALUE, v, to);
				return new Int((int)v);
			case LONG:
				return so;
			case BIGINT:
				return new JMo_BigInt(BigInteger.valueOf(v));
			case DEC:
				Lib_ErrorConv.checkConversion(cr, Dec.MIN_VALUE, Dec.MAX_VALUE, JMo_Dec.TOP_VALUE, v, to);
				return JMo_Dec.valueOf(cr, v);
			case FLOAT:
				return new JMo_Float(v);
			case DOUBLE:
				return new JMo_Double(v);
			case BIGDEC:
				return new JMo_BigDec(BigDecimal.valueOf(v));
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertShort(final CallRuntime cr, final ATOMIC to, final JMo_Short so) {
		final short v = so.getValue();

		switch(to) {
			case BOOL:
				return Bool.getObject(v > 0);
			case CHAR:
				return Lib_AtomConv.iToChar(cr, "Short", so, v);
			case BYTE:
				Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
				return new JMo_Byte((byte)v);
			case SHORT:
				return so;
			case INT:
				return new Int(v);
			case LONG:
				return new JMo_Long(v);
			case BIGINT:
				return new JMo_BigInt(BigInteger.valueOf(v));
			case DEC:
				return JMo_Dec.valueOf(cr, v);
			case FLOAT:
				return new JMo_Float(v);
			case DOUBLE:
				return new JMo_Double(v);
			case BIGDEC:
				return new JMo_BigDec(BigDecimal.valueOf(v));
			default:
		}
		throw Err.impossible();
	}

	private static I_Atomic convertStr(final CallRuntime cr, final ATOMIC to, final Str so) {
		String v = so.getValue();

		try {

			switch(to) {
				case BOOL:
					final String lv = v.toLowerCase();
					if(lv.length() == 0 || lv.equals("false") || lv.equals("0"))
						return Bool.FALSE;
					return Bool.TRUE;
				case CHAR:
					return Lib_AtomConv.iToChar(cr, "Str", so, v);
				case BYTE:
					v = Lib_AtomConv.iRemoveDecimals(v);
//					Lib_ErrorConv.checkConversion(cr, Byte.MIN_VALUE, JMo_Byte.TOP_VALUE, v, to);
					return new JMo_Byte(Byte.parseByte(v)); // TODO "255"
				case SHORT:
					v = Lib_AtomConv.iRemoveDecimals(v);
//					Lib_ErrorConv.checkConversion(cr, Short.MIN_VALUE, JMo_Short.TOP_VALUE, v, to);
					return new JMo_Short(Short.parseShort(v)); // TODO TOP Value
				case INT:
					v = Lib_AtomConv.iRemoveDecimals(v);
//					Lib_ErrorConv.checkConversion(cr, Integer.MIN_VALUE, Int.TOP_VALUE, v, to);
					return new Int(Integer.parseInt(v)); // TODO TOP Value
				case LONG:
					v = Lib_AtomConv.iRemoveDecimals(v);
//					Lib_ErrorConv.checkConversion(cr, Long.MIN_VALUE, JMo_Long.TOP_VALUE, v, to);
					return new JMo_Long(Long.parseLong(v)); // TODO TOP Value
				case BIGINT:
					v = Lib_AtomConv.iRemoveDecimals(v);
					return new JMo_BigInt(new BigInteger(v)); // TODO TOP Value
				case DEC:
					final String s5 = v.replace(',', '.');
//					Lib_ErrorConv.checkConversion(cr, Dec.MIN_VALUE, JMo_Dec.TOP_VALUE, v, to);
					return new JMo_Dec(Dec.parseDec(s5)); // TODO TOP Value
				case FLOAT:
					final String s2 = v.replace(',', '.');
//					Lib_ErrorConv.checkConversion(cr, Float.MIN_VALUE, JMo_Float.TOP_VALUE, v, to);
					return new JMo_Float(Float.parseFloat(s2)); // TODO TOP Value ?!?
				case DOUBLE:
					final String s3 = v.replace(',', '.');
//					Lib_ErrorConv.checkConversion(cr, Double.MIN_VALUE, JMo_Double.TOP_VALUE, v, to);
					return new JMo_Double(Double.parseDouble(s3)); // TODO Top value ?!?
				case BIGDEC:
					final String s4 = v.replace(',', '.');
					return new JMo_BigDec(new BigDecimal(s4));
				default:
			}
		}
		catch(final NumberFormatException e) {
//			return Nil.NIL;
//			throw new ExecError(cr, "Can't convert this String to " + to.name + ".", v);
//			throw new RuntimeError(cr, "Impossible conversion", "Str --> " + to.name);
			throw Lib_AtomConv.iImpossibleConversion(cr, "Str", to, so);
		}
		throw Err.impossible();
	}

	private static Char iDoubleToChar(final CallRuntime cr, final I_Atomic atomic, final Object v) {
		String vs = v.toString();
		if(vs.endsWith(".0"))
			vs = vs.substring(0, vs.length() - 2);
		if(vs.length() != 1)
			throw Lib_AtomConv.iImpossibleConversion(cr, ATOMIC.DEC.name, ATOMIC.CHAR, atomic);
		return new Char(vs.charAt(0));
	}

	private static RuntimeError iImpossibleConversion(final CallRuntime cr, final String from, final ATOMIC to, final I_Atomic value) {
		return new RuntimeError(cr, "Impossible conversion", "(" + from + " --> " + to.name + ") " + value.toString(cr, STYPE.IDENT));
	}

	private static String iRemoveDecimals(final String v) {
		final int len = v.length();

		for(int i = 0; i < len; i++) {
			final char c = v.charAt(i);
			if(c == '.' || c == ',')
				return v.substring(0, i);
		}
		return v;
	}

	private static Char iToChar(final CallRuntime cr, final String from, final I_Atomic atomic, final Object v) {
		final String vs = v.toString();
		if(vs.length() != 1)
			throw Lib_AtomConv.iImpossibleConversion(cr, from, ATOMIC.CHAR, atomic);
		return new Char(vs.charAt(0));
	}

}
