/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.util.Iterator;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.HtmlChars;
import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 11.09.2019
 */
public class Lib_StrFormat {

	private enum SIZE_STATE {
		UNDEFINED,
		ADD,
		FINISHED
	}

	private enum STR_ESCAPE {
		NONE,
		NUL,
		ALL
	}


	private static final String ERR_INVALID_CELL = "Illegal cell definition in format string";


	public static Str fill(final CallRuntime cr, final String format, final I_Object obj) {
		int formIndex = 0;
		final StringBuilder sb = new StringBuilder();

		while(formIndex < format.length()) {
			final Group2<Boolean, String> part = Lib_StrFormat.getNext(format, formIndex);
			formIndex += part.o2.length();

			if(!part.o1) // No placeholder
				sb.append(part.o2);
			else
				sb.append(Lib_StrFormat.format(cr, part.o2, obj));
		}
		return new Str(sb.toString());
	}

	public static Str fill(final CallRuntime cr, final String format, final Iterator<I_Object> objs) {
		int formIndex = 0;
		final StringBuilder sb = new StringBuilder();

		while(formIndex < format.length()) {
			final Group2<Boolean, String> part = Lib_StrFormat.getNext(format, formIndex);
			formIndex += part.o2.length();

			if(!part.o1 || !objs.hasNext()) // No placeholder
				sb.append(part.o2);
			else
				sb.append(Lib_StrFormat.format(cr, part.o2, objs.next()));
		}
		return new Str(sb.toString());
	}

	/**
	 * 1. chars = Field-length
	 * 2. char = Overflow (! = hidden, ? = show)
	 * 3. char = Alignment (< = left, > = right, ^ = center (LCR)
	 * 4. char = Type (i = integer, d = decimal, c = char, s = string, b = bool, t = type)
	 * 5. :
	 * 6. chars = Type-Specific
	 *
	 * Surrounding {} will not be checked for validity here!
	 */
	public static String format(final CallRuntime cr, final String form, final I_Object obj) {
		final int len = form.length();
		String value = Lib_Convert.getStringValue(cr, obj);

		if(len == 2) // if only {}
			return value;

		Boolean cutOverflow = null; // Default = false
		ALIGN align = null; // Default = Auto
		Character type = null; // Default = Auto
		final StringBuilder size = new StringBuilder();
		SIZE_STATE sizeState = SIZE_STATE.UNDEFINED;

		final int dpi = form.indexOf(':');
		final String cell = form.substring(1, dpi >= 0 ? dpi : form.length() - 1);
		final String spec = dpi >= 0 ? form.substring(dpi + 1, form.length() - 1) : "";

		// Analyze format string
		for(final char c : cell.toCharArray()) {

			if(c >= '0' && c <= '9') {

				if(sizeState == SIZE_STATE.FINISHED)
					cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Cell size already set! Got: '" + cell + "'");
				else {
					sizeState = SIZE_STATE.ADD;
					size.append(c);
				}
				continue;
			}
			else if(sizeState == SIZE_STATE.ADD)
				sizeState = SIZE_STATE.FINISHED;

			switch(c) {
				case '!':
					if(cutOverflow != null)
						cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Overflow handling already set. But got: '" + c + "'");
					else
						cutOverflow = true;
					break;
				case '?':
					if(cutOverflow != null)
						cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Overflow handling already set. But got: '" + c + "'");
					else
						cutOverflow = false;
					break;
				case '<':
				case 'L':
					if(align != null)
						cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Align already set. But got: '" + c + "'");
					else
						align = ALIGN.LEFT;
					break;
				case '>':
				case 'R':
					if(align != null)
						cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Align already set. But got: '" + c + "'");
					else
						align = ALIGN.RIGHT;
					break;
				case '^':
				case 'C':
					if(align != null)
						cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Align already set. But got: '" + c + "'");
					else
						align = ALIGN.CENTER;
					break;
				case 'i':
				case 'd':
				case 'c':
				case 's':
				case 'b':
				case 't':
					if(type != null)
						cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Cell type already set. But got: '" + c + "'");
					else
						type = c;
					break;

				default:
					cr.warning(Lib_StrFormat.ERR_INVALID_CELL, "Invalid char, maybe ':' is missing? Got: '" + c + "'");
			}
		}
		final int fieldLen = size.length() == 0 ? 0 : Integer.parseInt(size.toString());

		if(cutOverflow == null)
			cutOverflow = false;

		if(type == null)
			type = obj instanceof Bool
				? 'b'
				: obj instanceof Char
					? 'c'
					: obj instanceof JMo_Dec
						? 'd'
						: obj instanceof Int
							? 'i'
							: obj instanceof JMo_Byte
								? 'i'
								: obj instanceof JMo_Double
									? 'd'
									: obj instanceof JMo_Float
										? 'd'
										: obj instanceof JMo_Long
											? 'i'
											: obj instanceof JMo_Short
												? 'i'
												: obj instanceof JMo_BigInt
													? 'i'
													: obj instanceof JMo_BigDec
														? 'd'
														: 's'; // Default

		// Switch by type
		switch(type) {
			case 's':
				if(align == null)
					align = ALIGN.LEFT;
				value = Lib_StrFormat.iSpecString(cr, obj, value, spec, form);
				break;
			case 'c':
				if(align == null)
					align = ALIGN.LEFT;
				value = Lib_StrFormat.iSpecChar(cr, obj, spec, form);
				break;
			case 'i':
				if(align == null)
					align = ALIGN.RIGHT;
				value = Lib_StrFormat.iSpecInt(cr, obj, spec, form);
				break;
			case 'd':
				if(align == null)
					align = ALIGN.RIGHT;
				value = Lib_StrFormat.iSpecDec(cr, obj, spec);
				break;
			case 'b':
				if(align == null)
					align = ALIGN.LEFT;
				value = Lib_StrFormat.iSpecBool(cr, obj, spec);
				break;
			case 't':
				if(align == null)
					align = ALIGN.LEFT;
				value = Lib_StrFormat.iSpecType(cr, obj, spec);
				break;

			default:
				throw Err.todo(form, value, type);
		}
		return FormString.width(fieldLen, ' ', value, align, cutOverflow);
	}

	/**
	 * @return o1 = true if it is an placeholder; o2 = content
	 */
	public static final Group2<Boolean, String> getNext(final String s, final int offset) {
		boolean formatOpen = false;

		for(int pos = offset; pos < s.length(); pos++) {
			final char c = s.charAt(pos);

			if(c == '\\') {
				pos++;
				continue;
			}
//			if(c == '%') {
//				if(formatOpen)
//					return new Group2(true, s.substring(offset, pos+1));
//				else {
//					if(pos == offset)
//						formatOpen = true;
//					else
//						return new Group2(false, s.substring(offset, pos));
//				}
//			}

			if(c == '{')
				if(pos == offset)
					formatOpen = true;
				else
					return new Group2<>(false, s.substring(offset, pos));

			if(c == '}' && formatOpen)
				return new Group2<>(true, s.substring(offset, pos + 1));
		}
		return new Group2<>(formatOpen, s.substring(offset));
	}


	//--- Private ---

	private static String iSpecBool(final CallRuntime cr, final I_Object o, String sSpec) {
		final boolean b = Lib_Convert.getBoolValue(cr, o);

		if(sSpec.length() > 1) {
			Lib_StrFormat.iSpecThrow(cr, "Bool", sSpec);
			sSpec = sSpec.substring(0, 1);
		}
		if(sSpec.length() == 1)
			switch(sSpec.charAt(0)) { // only one possible
//				case '^':	// this is also center
				case 'u':
					return b ? "TRUE" : "FALSE";
//				case 'v':
				case 'l':
					return b ? "true" : "false";
				case 'c':
				case 'C':
					return b ? "True" : "False";
				default:
					Lib_StrFormat.iSpecFormatThrow(cr, "Bool", sSpec);
			}

		return b ? "true" : "false";
	}

	private static String iSpecChar(final CallRuntime cr, final I_Object o, String sSpec, final String form) {
		final String os = "" + Lib_Convert.getCharValue(cr, o, true);

		if(sSpec.length() == 0)
			return os;

		if(sSpec.length() > 1) {
			Lib_StrFormat.iSpecFormatThrow(cr, "Character", sSpec);
			sSpec = sSpec.substring(0, 1);
		}

		switch(sSpec.charAt(0)) { // only one possible
//			case '^':	// This is also center!
			case 'u':
				return os.toUpperCase();
//			case 'v':
			case 'l':
				return os.toLowerCase();
			default:
				Lib_StrFormat.iSpecCharThrow(cr, "Character", sSpec.charAt(0), sSpec, form);
				return os;
		}
	}

	private static void iSpecCharThrow(final CallRuntime cr, final String type, final char c, final String sSpec, final String form) {
		final String detail = "Invalid special format char '" + sSpec.charAt(0) + "' in '" + form + "'";
		Lib_StrFormat.iSpecThrow(cr, type, detail);
	}

	private static String iSpecDec(final CallRuntime cr, final I_Object o, final String sSpec) {
		final double d = Lib_Convert.getDoubleValue(cr, o); // Throws error, if 'o' is not atomic!
		final JMo_Double jd = (JMo_Double)Lib_AtomConv.convert(cr, (I_Atomic)o, ATOMIC.DOUBLE);
		String os = jd.toString();

		if(sSpec.length() == 0)
			return os;

		String sign = null;
		final StringBuilder sbDigits = new StringBuilder();
		boolean digitBreak = false;

		for(final char c : sSpec.toCharArray())
			if(c == '+') {
				if(sign != null)
					Lib_StrFormat.iSpecFormatThrow(cr, "Decimal", sSpec);	// TODO Inform aboubt double use of '+'! // TODO Dec statt Decimal?
				sign = d < 0 ? "" : "+";
				if(sbDigits.length() > 0)
					digitBreak = true;
			}
			else if(c >= '0' && c <= '9') {
				if(digitBreak)
					Lib_StrFormat.iSpecFormatThrow(cr, "Decimal", sSpec);	// TODO Inform aboubt double use of digits! // TODO Dec statt Decimal?
				sbDigits.append(c);
			}
			else
				Lib_StrFormat.iSpecFormatThrow(cr, "Decimal", sSpec); // TODO Dec statt Decimal?

		if(sbDigits.length() > 0) {
			final int digits = Integer.parseInt(sbDigits.toString());
			if(digits == 0)
				os = "" + Math.round(d);
			else
				os = FormNumber.round(d, digits);
		}
		return sign != null
			? sign + os
			: os;
	}

	private static void iSpecFormatThrow(final CallRuntime cr, final String type, final String sSpec) {
		final String detail = "Invalid special format: '" + sSpec + "'";
		Lib_StrFormat.iSpecThrow(cr, type, detail);
	}

	private static String iSpecInt(final CallRuntime cr, final I_Object o, String sSpec, final String form) {
		final String os = "" + Lib_Convert.getIntValue(cr, o, false);

		if(sSpec.length() == 0)
			return os;

		if(sSpec.length() > 1) {
			Lib_StrFormat.iSpecFormatThrow(cr, "Integer", sSpec);
			sSpec = sSpec.substring(0, 1);
		}

		if(sSpec.charAt(0) == '+') // Only this possible
			return os.charAt(0) == '-'
				? os
				: '+' + os;
		else {
			Lib_StrFormat.iSpecCharThrow(cr, "Integer", sSpec.charAt(0), sSpec, form);
			return os;
		}
	}

	private static String iSpecString(final CallRuntime cr, final I_Object o, final String os, final String sSpec, final String form) {
		if(sSpec.length() == 0)
			return os;

		Character conversionEscape = null;
		Character conversionHtml = null;
		Character caseChange = null;
		Character quote = null;
		Character conversionString = null;
		boolean trim = false;

		// Preparations
		for(final char c : sSpec.toCharArray())
			switch(c) {
				case 'I':
				case 'D':
//				case 'S':
					if(conversionString != null)
						Lib_StrFormat.iSpecThrow(cr, "String", "Only one object to string conversion is allowed [I,D] but got: " + sSpec);
					else
						conversionString = c;
					break;

				case 't': // Trim string
					if(trim)
						Lib_StrFormat.iSpecThrow(cr, "String", "Trim was already set: " + sSpec);
					else
						trim = true;
					break;

				case 'e': // Add \ before: EOF, \
				case 'E': // Add \ before: EOF, \n, \t, \r, \f, \b, \, all<32		# Not: ', "
				case 'r': // Remove all \ escapes
					if(conversionEscape != null)
						Lib_StrFormat.iSpecThrow(cr, "String", "Only one escape conversion is allowed [e,E,r] but got: " + sSpec);
					else
						conversionEscape = c;
					break;

				case 'h': // Convert all SpecialChars to HTML-Entitys --> PHP: htmlspecialchars()	html_entity_decode()
				case 'H': // Convert all possible Chars to HTML-Entitys  --> PHP: htmlentities()
				case 'd': // Convert all HTML-Entitys (Numbers/Names) to normal chars.
//				case 'R': // ???
					if(conversionHtml != null)
						Lib_StrFormat.iSpecThrow(cr, "String", "Only one html conversion is allowed [h,H,d] but got: " + sSpec);
					else
						conversionHtml = c;
					break;

				case 'c': // Capitalize first word
				case 'C': // Capitalize every word
				case 'u': // to upper case
				case 'l': // to lower case
//				case '^': // this is also center!!!
//				case 'v':
					if(caseChange != null)
						Lib_StrFormat.iSpecThrow(cr, "String", "Only one case type is allowed [l,u,c,C] but got: " + sSpec);
					else
						caseChange = c;
					break;

				case '1':
				case '2':
				case '3':
				case '4':
					if(quote != null)
						Lib_StrFormat.iSpecThrow(cr, "String", "Only one quote type is allowed [1,2,3,4] but got: " + sSpec);
					else
						quote = c;
					break;

				default:
					Lib_StrFormat.iSpecCharThrow(cr, "String", c, sSpec, form);
			}

		String result = conversionString == null // || conversionString == 'S'
			? os
			: conversionString == 'I'
				? o.toString(cr, STYPE.IDENT)
				: o.toString(cr, STYPE.DESCRIBE);

		// Trim
		if(trim)
			result = result.trim();

		// Escape - before Quote
		STR_ESCAPE doEscape = STR_ESCAPE.NONE;
		STR_ESCAPE escaped = STR_ESCAPE.NONE;

		if(conversionEscape != null)
			switch(conversionEscape) {
				case 'e':
					doEscape = STR_ESCAPE.NUL;
					break;
				case 'E':
					doEscape = STR_ESCAPE.ALL;
					break;
				case 'r':
					result = FormString.unescapeSpecialChars(result);
					break;
			}

		// Html
		if(conversionHtml != null)
			switch(conversionHtml) {
				case 'h':
					result = HtmlChars.htmlSpecialChars(result);
					break;
				case 'H':
					result = HtmlChars.htmlEntities(result);
					break;
				case 'd': // 'R'
					result = HtmlChars.htmlEntitiesDecode(result);
					break;
			}

		// Case change
		if(caseChange != null)
			switch(caseChange) {
//				case '^': // this is also center!!!
				case 'u':
					result = result.toUpperCase();
					break;
//				case 'v':
				case 'l':
					result = result.toLowerCase();
					break;
				case 'c':
					result = Lib_String.capitalize(result, true);
					break;
				case 'C':
					result = Lib_String.capitalize(result, false);
					break;
				default:
					Err.impossible(caseChange);
			}

		// Quote
		if(quote != null) {
			final boolean quoteEscape = doEscape == STR_ESCAPE.ALL;

			switch(quote) {
				case '1':
					result = FormString.quote(result, '\'', '\\', quoteEscape);
					break;
				case '2':
					result = FormString.quote(result, '"', '\\', quoteEscape);
					break;
				case '3':
					result = FormString.quote(result, '\'', '\'', quoteEscape);
					break;
				case '4':
					result = FormString.quote(result, '"', '\"', quoteEscape);
					break;
				default:
					Err.impossible(caseChange);
			}
			escaped = quoteEscape ? STR_ESCAPE.ALL : STR_ESCAPE.NUL;
		}
		// Escapes not done by quote
		if(doEscape != STR_ESCAPE.NONE && escaped == STR_ESCAPE.NONE) // Other possibilitys are already done by quote
			switch(doEscape) {
				case NUL:
					result = FormString.escapeSlashes(result);
					break;
				case ALL:
					result = FormString.escapeSpecialChars(result, false, false);
					break;
				default:
			}

		return result;
	}

	private static void iSpecThrow(final CallRuntime cr, final String type, final String detail) {
		cr.warning("Illegal styles for '" + type + "' in format string!", detail);
	}

	private static String iSpecType(final CallRuntime cr, final I_Object o, final String sSpec) {
		if(sSpec.length() != 0)
			Lib_StrFormat.iSpecFormatThrow(cr, "Type", sSpec);

		final I_Object o2 = Lib_Convert.getValue(cr, o);
		return o2.getType(cr).toString();
	}

}
