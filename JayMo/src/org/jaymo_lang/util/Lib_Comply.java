/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import de.mn77.base.data.constant.CONDITION;


/**
 * @author Michael Nitsche
 * @created 28.01.2021
 *
 * @implNote Some complience checks, that are faster than regex.
 */
public class Lib_Comply {

	public static boolean checkConstName(final String name) {
		byte state = 1;
		char last = ' ';
		int idx = -1;

		for(final char c : name.toCharArray()) {
			idx++;

			if(state == 1) {

				if(c >= 'A' && c <= 'Z') {
					state = 2;
					continue;
				}
				return false;
			}

			if(state == 2) {

				if(c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_') {
					last = c;
					continue;
				}
				if(c == '?')
					return last != '_' && name.length() == idx + 1;
			}
		}
		return last != '_';
	}

	public static boolean checkEventName(final String name) { // "@[a-z][A-Za-z0-9_]*"
		final int len = name.length();
		if(len < 3) //a single char is no event name ;-)
			return false;

		int pos = 0;

		for(final char c : name.toCharArray()) {
			pos++;

			if(pos == 1)
				if(c == '@')
					continue;
				else
					return false;

			if(pos == 2) {
				if(c < 'a' || c > 'z')
					return false;
				continue;
			}
			if((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '_')
				return false;
		}
		return name.charAt(len - 1) != '_';
	}

	public static boolean checkFunctionName(final String name, CONDITION allowPrivate) {
		final int len = name.length();
		if(len < 1)
			return false;

		boolean first = true;

		for(final char c : name.toCharArray()) {

			if(first) {

				if(c == '_') {
					if(allowPrivate == CONDITION.NEVER || len < 2)
						return false;
					allowPrivate = CONDITION.MAYBE;
					continue;
				}
				else if(allowPrivate == CONDITION.ALWAYS)
					return false;
				if(c < 'a' || c > 'z')
					return false;

				first = false;
				continue;
			}
			if((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '_')
				return false;
		}
		return name.charAt(len - 1) != '_';
	}

	public static boolean checkTypeName(final String name) {
		final int len = name.length();
		if(len < 2) //a single char is not a type name ;-)
			return false;

		boolean first = true;
		int lower = 0;

		for(final char c : name.toCharArray()) {

			if(first) {
				if((c < 'A' || c > 'Z') && c != '_')
					return false;

				first = false;
				continue;
			}
			if((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '_')
				return false;
			if(c >= 'a' && c <= 'z')
				lower++;
		}
		return lower > 0 && name.charAt(len - 1) != '_';
	}

	public static boolean checkVarName(final String name) {
		byte state = 1;
		char last = ' ';
		int idx = -1;

		for(final char c : name.toCharArray()) {
			idx++;

			if(state == 1) {

				if(c >= 'a' && c <= 'z') {
					state = 2;
					continue;
				}
				return false;
			}

			if(state == 2) {

				if(c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_') {
					last = c;
					continue;
				}
				if(c == '?')
					return last != '_' && name.length() == idx + 1;
			}
		}
		return last != '_';
	}

	public static void main(final String[] args) {
//		String[] sa = new String[] {"foo", "bAr", "Bak", "_xyz", "_Xyz", "","_","_a"};
//		for(String s : sa)
//			MOut.print(s+"\t"+Lib_Comply.checkFunctionName(s, CONDITION.ALWAYS));

//		String[] sa = new String[] {"foo", "bAr", "Bak", "XYZ", "Xyz", "","A","a", "Ab", "AB"};
//		for(String s : sa)
//			MOut.print(s+"\t"+Lib_Comply.checkTypeName(s));
	}

}
