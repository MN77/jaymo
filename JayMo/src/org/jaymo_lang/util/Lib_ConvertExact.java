/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.math.BigInteger;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 23.01.2022
 */
public class Lib_ConvertExact {

	public static int bigIntegerToInt(final CallRuntime cr, final BigInteger big) {

		try {
			return big.intValueExact();
		}
		catch(final ArithmeticException ae) {
			throw new RuntimeError(cr, "Value out of reach", "Value to big/small for Int: " + big);
		}
	}

	public static float doubleToFloat(final CallRuntime cr, final double value) {
		if(value > Float.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Float is " + Float.MAX_VALUE);
		if(value < Float.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Float is " + Float.MIN_VALUE);
		return (float)value;
	}

	public static int doubleToInt(final CallRuntime cr, final double value) {
		if(value > Integer.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Int is " + Integer.MAX_VALUE);
		if(value < Integer.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Int is " + Integer.MIN_VALUE);
		return (int)Math.round(value);
	}

	public static long doubleToLong(final CallRuntime cr, final double value) {
		if(value > Long.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Long is " + Long.MAX_VALUE);
		if(value < Long.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Long is " + Long.MIN_VALUE);
		return Math.round(value);
	}

	public static byte intToByte(final CallRuntime cr, final int value) {
		if(value > Byte.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Byte is " + Byte.MAX_VALUE);
		if(value < Byte.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Byte is " + Byte.MIN_VALUE);
		return (byte)value;
	}

	public static short intToShort(final CallRuntime cr, final int value) {
		if(value > Short.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Short is " + Short.MAX_VALUE);
		if(value < Short.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Short is " + Short.MIN_VALUE);
		return (short)value;
	}

	public static byte longToByte(final CallRuntime cr, final long value) {
		if(value > Byte.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Byte is " + Byte.MAX_VALUE);
		if(value < Byte.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Byte is " + Byte.MIN_VALUE);
		return (byte)value;
	}

	public static int longToInt(final CallRuntime cr, final long value) {
		if(value > Integer.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Int is " + Integer.MAX_VALUE);
		if(value < Integer.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Int is " + Integer.MIN_VALUE);
		return (int)value;
	}

	public static short longToShort(final CallRuntime cr, final long value) {
		if(value > Short.MAX_VALUE)
			throw new RuntimeError(cr, "Value too big", "Maximum for Short is " + Short.MAX_VALUE);
		if(value < Short.MIN_VALUE)
			throw new RuntimeError(cr, "Value too small", "Minimum for Short is " + Short.MIN_VALUE);
		return (short)value;
	}

}
