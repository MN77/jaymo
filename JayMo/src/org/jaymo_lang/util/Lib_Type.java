/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.App;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.I_ObjectToString;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.magic.var.A_MagicVar;
import org.jaymo_lang.object.magic.var.MV_APP;
import org.jaymo_lang.object.magic.var.MV_JAYMO;
import org.jaymo_lang.object.passthrough.Const;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.object.pseudo.ConstLet;
import org.jaymo_lang.object.pseudo.NonAtomic;
import org.jaymo_lang.object.pseudo.VarLet;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 29.03.2018
 */
public class Lib_Type {

	public static void checkValidity(final String typeName, final CallRuntime cr) {
		Lib_Type.checkTypeValidity(typeName, null, cr);
	}

	public static void checkValidity(final String typeName, final Parser_Script parser) {
		Lib_Type.checkTypeValidity(typeName, parser, null);
	}

	public static final String[] getJavaTypeNames(final JMo_Java javaObj) {
		final Object obj = javaObj.getJavaInstance();
		final SimpleList<String> list = new SimpleList<>();

		Class<?> cl = obj.getClass();

		while(cl != null) {
			list.add(Lib_Java.packConv_classToFull(cl));
			cl = cl.getSuperclass();
		}
		list.add("Java");
		list.add("Object");
//		Collections.reverse(list);
		return list.toArray(new String[list.size()]);
	}

	/**
	 * @return "TypeName"
	 */
	public static String getName(final Class<?> objClass, final I_ObjectToString obj) {
		Err.ifNull(objClass); // 'obj' can be null

		if(objClass == App.class || objClass == MV_APP.class)
			return App.typeName; // Root
//		if(objClass == Instance.class && obj != null)
//			return ((Instance)obj).getType().getName();
		if(objClass == Function.class)
			return "Function";
		if(objClass == LoopHandle.class)
			return "Loop";
		if(objClass == Var.class)
			return "Var";
		if(objClass == Const.class)
			return "Const";  // ((Const)obj).getTypeName();		// Leads to a cycle
		if(objClass == VarLet.class)
			return "VarLet";
		if(objClass == ConstLet.class)
			return "ConstLet";

		if(obj != null) {
			if(objClass == NonAtomic.class)
				return ((NonAtomic)obj).getTypeName();
			if(objClass == JMo_Java.class)
				return ((JMo_Java)obj).getFullTypeName();

//			if(	I_MagicReplace.class.isAssignableFrom(objClass)) {
			if(A_MagicVar.class.isAssignableFrom(objClass))
				//				Err.ifNull(obj);
				return ((A_MagicVar)obj).getID();
		}
		//----------------------

		String s = objClass.getSimpleName();
		if(objClass == Instance.class)
			s = obj == null ? Instance.class.getSimpleName() : ((Instance)obj).getType().getName();

		if(s.startsWith("JMo_"))
			return s.substring(4);
		if(s.startsWith("A_"))
			return s.substring(2);// + '_';
		if(s.equals("I_Atomic"))
			return "Atomic";
		if(s.startsWith("I_"))
			if(JayMo.STAGE.isDevelopment())
				Err.todo(s);
			else
				return s.substring(2);

		return s;
	}

	/**
	 * @return "TypeName"
	 */
	public static String getName(final I_Object obj) {
		return Lib_Type.getName(obj.getClass(), obj);
	}

	public static Class<?> getType(final CallRuntime cr, I_Object o) {
		o = Lib_Convert.getValue(cr, o);
		return o.getClass();
	}

	public static final Class<?>[] getTypeClasses(final I_Object obj) {
		final SimpleList<Class<?>> list = Lib_Type.getTypeClassesList(obj.getClass());
		final int len = list.size();
		final Class<?>[] result = new Class<?>[len];

		for(int i = 0; i < len; i++)
			result[len - 1 - i] = list.get(i);

		return result;
	}

	public static final SimpleList<Class<?>> getTypeClassesList(Class<?> cl) {
		final SimpleList<Class<?>> list = new SimpleList<>();

		while(cl != null && I_Object.class.isAssignableFrom(cl)) {
			list.add(cl);
			cl = cl.getSuperclass();
		}
		return list;
	}

	public static final String[] getTypeNames(final I_Object obj) {
		// Special cases:
		if(obj instanceof JMo_Java)
			return Lib_Type.getJavaTypeNames((JMo_Java)obj);
		if(obj instanceof MV_JAYMO)
			return new String[]{"JayMo", "Object"};
		if(obj instanceof MV_APP)
			return new String[]{"App", "Object"};

		final SimpleList<Class<?>> typeClasses = Lib_Type.getTypeClassesList(obj.getClass());
		final SimpleList<String> names = new SimpleList<>();

		for(final Class<?> cl : typeClasses) {
			final String name = Lib_Type.getName(cl, obj);
			if(names.size() == 0 || !name.equals(names.get(names.size() - 1)))
				names.add(name);
		}
		final int len = names.size();
		final String[] result = new String[len];

		for(int i = 0; i < len; i++)
//			result[len - 1 - i] = names.get(i);
			result[i] = names.get(i);

		return result;
	}

	/**
	 * @return "\<Type\>"
	 */
	public static String getTypeString(final Class<?> objClass, final I_Object obj) {
		return "<" + Lib_Type.getName(objClass, obj) + ">";
	}

	/**
	 * @return "\<Type\>"
	 */
	public static String getTypeString(final I_Object obj) {
		return "<" + Lib_Type.getName(obj) + ">";
	}

	public static boolean isInstanceOf(final I_Object o, final String type) {
		if(type.equals("Object")) // Fast bypass
			return true;

//		final boolean shortcut = cr.getStrict().isValid_shortenType();
//		type = Lib_Type.replaceAlias(type);

		if(type.startsWith("Java_{"))
			return o instanceof JMo_Java && ((JMo_Java)o).getFullTypeName().equals(type);
		if(type.startsWith("Java_"))
			return o instanceof JMo_Java && ((JMo_Java)o).getJavaClassName().equals(Lib_Prefix.cutJava(type));

		for(final String t : Lib_Type.getTypeNames(o))
			if(t.equals(type))
				return true;
//			if(shortcut && t.endsWith("_") && t.substring(0, t.length() - 1).equals(type))
//				return true;

		I_Object inst = o;

		while(inst != null && inst instanceof Instance) {
			final I_Object parent = ((Instance)inst).internalGetParent();
//			if(parent != null && type.isAssignableFrom(parent.getClass()))
			if(parent != null && Lib_Type.isInstanceOf(parent, type))
				return true;
			inst = parent;
		}
		return false;
	}

	/**
	 * @apiNote Create strings like:
	 *          <Float> 6.7
	 *          <Str> b("uvw")
	 */
	public static String toIdentWithType(final CallRuntime cr, final I_Object obj) { // TODO rename: toDescribeWithType ... oder Typ als argument übergeben
		final I_Object objReal = Lib_Convert.getValue(cr, obj);
		final StringBuilder sb = new StringBuilder();
//		if(!(objReal instanceof I_Atomic)) {
		sb.append(Lib_Type.getTypeString(objReal));
		sb.append(' ');
//		}
		sb.append(objReal.toString(cr, STYPE.IDENT));
		return sb.toString();
	}

	/**
	 * Check if the given object is valid for the given type. If necessary and possible, an update will be done.
	 * If the object doesn't fit, an error will be thrown.
	 */
	public static I_Object typeCheck(final CallRuntime cr, final I_Object obj, final String objTypeName, final String safedTypeName, final String targetInfo) {

		if(Lib_Type.isInstanceOf(obj, safedTypeName))
			return obj;
		else {
			// Upgrade or fail
			final I_Object objUpgraded = cr.getStrict().isValid_AtomicAutoUpgrade()
				? Lib_AtomConv.autoUpgrade(cr, obj, objTypeName, safedTypeName)
				: null;

			if(objUpgraded != null)
				return objUpgraded;
			else
				throw new RuntimeError(cr, "Invalid type for " + targetInfo,
					"Object must be <" + safedTypeName + ">, but got: " + Lib_Type.toIdentWithType(cr, obj));
		}
	}

	public static boolean typeIs(final CallRuntime cr, I_Object o, final Class<?>... ta) {
		o = Lib_Convert.getValue(cr, o);

		final Class<?> typ = o.getClass();
		for(final Class<?> t : ta)
			if(t.isAssignableFrom(typ))
				return true;
		return false;
	}

	public static String typesToString(final I_Object obj) {
		final String[] types = Lib_Type.getTypeNames(obj);
		final StringBuilder sb = new StringBuilder();
		if(types.length > 1)
			sb.append("(");

		for(int tc = 0; tc < types.length; tc++) {
			final String typename = types[tc];
			if(tc != 0)
				sb.append(", ");

			sb.append(typename);
		}
		if(types.length > 1)
			sb.append(")");
		return sb.toString();
	}

	private static void checkTypeValidity(final String typeName, final Parser_Script parser, final CallRuntime cr) {
		final boolean parsetime = parser != null;
		final DebugInfo debug = parsetime ? parser.getDebugInfo() : cr.getDebugInfo();

		if(typeName.length() == 0)
			Lib_Type.invalidTypename("Type can't be empty!", parsetime, cr, debug);
		final char cl = typeName.charAt(typeName.length() - 1);
		if(cl == '_')
			Lib_Type.invalidTypename("Type can't end with underline! Got: " + typeName, parsetime, cr, debug);

		int underlines = 0;
		boolean nextCapital = true;
		boolean openSub = false;

		for(final char cx : typeName.toCharArray()) {

			if(cx == '{') {
				openSub = true;
				nextCapital = false;
				continue;
			}

			if(cx == '}') {
				openSub = false;
				continue;
			}

			if(nextCapital) {
				nextCapital = false;

				if(cx == '_' && parser != null && parser.getCurrentPrefix() != null)
					nextCapital = true;
				else if(cx < 'A' || cx > 'Z')
					Lib_Type.invalidTypename("Type must start with a capital letter! Got: '" + typeName + "'", parsetime, cr, debug);
			}
			if(cx >= 'A' && cx <= 'Z' || cx >= 'a' && cx <= 'z' || cx >= '0' && cx <= '9')
				continue;

			if(openSub && (cx == '.' || cx == '$'))
				continue;

			if(cx == '_') {
				underlines++;
				nextCapital = true;
				continue;
			}
			Lib_Type.invalidTypename("Invalid char '" + cx + "'! Got: " + typeName, parsetime, cr, debug);
		}
		if(underlines > 1)
			Lib_Type.invalidTypename("Type can't have multiple underlines and namespaces: " + typeName, parsetime, cr, debug);

//		if(addNamespace) {
//			final String currentNamespace = parsetime ? parser.getCurrentNamespace() : null;
//			if(underlines == 0 && currentNamespace != null)
//				return currentNamespace + '_' + typeName;
//		}

//		if(underlines > 0 && typeName.startsWith(ObjectManager.PREFIX_STD_NAMESPACE))
//			return typeName.substring(4);

//		return typeName;
	}

	private static void invalidTypename(final String detail, final boolean parsetime, final CallRuntime cr, final DebugInfo debug) {
		if(parsetime)
			throw new CodeError("Invalid Type-Name!", detail, debug);
		else
			throw new RuntimeError(cr, "Invalid Type-Name!", detail);
	}


//	public static String replaceAlias(String type) {

//	 	Works, but currently only for testing. Used in:
//			Lib_Type.isInstanceOf
//			Lib_Parser.getFunctionPars
//			Var.setType


//		if(type == null)
//			return null;
//
//		switch(type) {
//			case "String":
//				return "Str";
//			case "Integer":
//				return "Int";
//			case "Character":
//				return "Char";
//			case "Decimal":
//			case "Double":
//				return "Dec";
//			case "Boolean":
//				return "Bool";
//
//			default:
//				return type;
//		}
//	}

}
