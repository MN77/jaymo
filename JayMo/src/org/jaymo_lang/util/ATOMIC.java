/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;


/**
 * @author Michael Nitsche
 * @created 01.09.2019
 */
public enum ATOMIC {

	// Ordered by priority:
//	NIL("Nil", JMo_Nil.class),
	BOOL("Bool", Bool.class),
	BYTE("Byte", JMo_Byte.class),
	SHORT("Short", JMo_Short.class),
	INT("Int", Int.class),
	LONG("Long", JMo_Long.class),
	BIGINT("BigInt", JMo_BigInt.class),
	DEC("Dec", JMo_Dec.class),
	FLOAT("Float", JMo_Float.class),
	DOUBLE("Double", JMo_Double.class),
	BIGDEC("BigDec", JMo_BigDec.class),
	CHAR("Char", Char.class),
	STR("Str", Str.class);


	public static final String NOT_A_NUMBER      = "not_a_number";
	public static final String INFINITY          = "infinity";
	public static final String INFINITY_POSITIVE = "+infinity";
	public static final String INFINITY_NEGATIVE = "-infinity";
	public static final String INFINITY_UP       = "INFINITY";
	public static final String NOT_A_NUMBER_UP   = "NOT_A_NUMBER";


	public final String   name;
	public final Class<?> clazz;


	ATOMIC(final String name, final Class<?> clazz) {
		this.name = name;
		this.clazz = clazz;
	}

	public final String getTypeString() {
		return "<" + this.name + ">";
	}

}
