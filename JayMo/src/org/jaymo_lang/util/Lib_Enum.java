/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;


/**
 * @author Michael Nitsche
 * @created 15.08.2021
 */
public class Lib_Enum {

	/**
	 * Only fast checking, but no validity check
	 */
	public static boolean isEnum(final String name) {
		for(final char c : name.toCharArray())
			if(c < 'A' | c > 'Z' && c < '0' | c > '9' && c != '_')
				return false;

		return true;
	}

	public static void isValid(final String name, final DebugInfo debugInfo) {
		if(name == null || name.length() == 0)
			throw new CodeError("Invalid enum definition", "Missing name", debugInfo);

		if(name.charAt(0) == '_')
			throw new CodeError("Invalid enum definition", "Name cannot start with underline: " + name, debugInfo);
		if(name.charAt(name.length() - 1) == '_')
			throw new CodeError("Invalid enum definition", "Name cannot end with underline: " + name, debugInfo);

		for(final char c : name.toCharArray())
			if(c < 'A' | c > 'Z' && c < '0' | c > '9' && c != '_')
				throw new CodeError("Invalid enum definition", "Name contains invalid char: " + name, debugInfo);
	}

}
