/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.TypeVariable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.immute.datetime.JMo_Date;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.type.datetime.MDate;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 30.08.2019
 */
public class Lib_Java {

	/**
	 * Checks, if a jmo-object can be assigned to a java-parameter
	 */
	public static boolean isAssignable(final Class<?> wantedJava, final Class<?> testJayMo) {
		if(wantedJava == Object.class || testJayMo == Nil.class || testJayMo == JMo_Java.class || testJayMo == Str.class && wantedJava == String.class)
			return true;

		if(testJayMo == Int.class)
			return wantedJava == int.class || wantedJava == Integer.class;
		if(testJayMo == JMo_Byte.class)
			return wantedJava == byte.class || wantedJava == Byte.class;
		if(testJayMo == JMo_Short.class)
			return wantedJava == short.class || wantedJava == Short.class;
		if(testJayMo == JMo_Long.class)
			return wantedJava == long.class || wantedJava == Long.class;
		if(testJayMo == JMo_Float.class)
			return wantedJava == float.class || wantedJava == Float.class;
		if(testJayMo == JMo_Double.class)
			return wantedJava == double.class || wantedJava == Double.class;
		if(testJayMo == Char.class)
			return wantedJava == char.class || wantedJava == Character.class || wantedJava == int.class; // int is also okay!
		if(testJayMo == Bool.class)
			return wantedJava == boolean.class || wantedJava == Boolean.class;

		// List | Array
		if(testJayMo == JMo_List.class)
			return wantedJava == String[].class || wantedJava == Object[].class;

		// TODO ... instanceof ...?

		// Anything else:
		return false;
	}

	public static boolean isSimpleTypeName(final String type) {
		return type.length() >= 6
			&& type.charAt(0) == 'J'
			&& type.charAt(1) == 'a'
			&& type.charAt(2) == 'v'
			&& type.charAt(3) == 'a'
			&& type.charAt(4) == '_'
			&& type.charAt(5) != '{';
	}

	public static I_Object javaToJmo(final Object java) {
		if(java == null)
			return Nil.NIL;
		if(java instanceof I_Object)
			return (I_Object)java;

		if(java instanceof Boolean)
			return Bool.getObject((Boolean)java);

		if(java instanceof Byte)
			return new JMo_Byte((Byte)java);
		if(java instanceof Short)
			return new JMo_Short((Short)java);
		if(java instanceof Integer)
			return new Int((Integer)java);
		if(java instanceof Long)
			return new JMo_Long((Long)java);

		if(java instanceof Float)
			return new JMo_Float((Float)java);
		if(java instanceof Double)
			return new JMo_Double((Double)java);

		if(java instanceof Character)
			return new Char((Character)java);
		if(java instanceof String)
			return new Str((String)java);

		if(java instanceof String[]) {
			final SimpleList<I_Object> al = new SimpleList<>(((String[])java).length);
			for(final String s : (String[])java)
//				al.add(javaToJmo(s));
				al.add(new Str(s));
			return new JMo_List(al);
		}

		if(java instanceof Date) {
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			final String ds = sdf.format(java);
			return new JMo_Date(new MDate(ds));
		}

		if(java instanceof Collection) {
			final Collection<?> source = (Collection<?>)java;
			final SimpleList<I_Object> target = new SimpleList<>(source.size());
			for(final Object o : source)
				target.add(Lib_Java.javaToJmo(o));
			return new JMo_List(target);
		}

		if(java instanceof HashMap) {
			final HashMap<?, ?> source = (HashMap<?, ?>)java;
			final JMo_Map target = new JMo_Map();
			for(final Entry<?, ?> e : source.entrySet())
				target.internalAdd(Lib_Java.javaToJmo(e.getKey()), Lib_Java.javaToJmo(e.getValue()));
			return target;
		}
		return new JMo_Java(java);
	}

	public static Object jmoToJava(final CallRuntime cr, final I_Object jmo, final Class<?> wanted) {
		return Lib_Java.jmoToJava(cr, jmo, wanted, true);
	}

	public static Object jmoToJava(final CallRuntime cr, final I_Object jmo, final Class<?> wanted, final boolean error) { // TODO Prüfen, wann wirklich ein Fehler geworfen werden muss.
		if(jmo == Nil.NIL)
			return null;
		if(jmo instanceof JMo_Java)
			return ((JMo_Java)jmo).getJavaObject();
		if(jmo instanceof I_Atomic)
			return ((I_Atomic)jmo).getValue();

		if(jmo instanceof JMo_List) {
			final SimpleList<I_Object> source = ((JMo_List)jmo).getInternalCollection();
			return Lib_Java.iListToJava(cr, source, wanted);
		}

		if(jmo instanceof JMo_Map) {
			final HashMap<I_Object, I_Object> source = ((JMo_Map)jmo).copyToHashMap();
			final HashMap<Object, Object> target = new HashMap<>();
			for(final Entry<I_Object, I_Object> e : source.entrySet())
				target.put(Lib_Java.jmoToJava(cr, e.getKey(), null, error), Lib_Java.jmoToJava(cr, e.getValue(), null, error));
			return target;
		}
//		throw Err.todo(jmo.getClass().getSimpleName(), jmo);
		if(error)
			throw new RuntimeError(cr, "No Java representation found", "Conversion from <" + jmo.getTypeName() + "> to Java failed.");
		return jmo;
	}

	public static Object[] jmoToJava(final CallRuntime cr, final I_Object[] args, final Parameter[] pars) {
		final int len = args.length;
		final Object[] result = new Object[len];
		for(int p = 0; p < len; p++)
			result[p] = Lib_Java.jmoToJava(cr, args[p], pars == null ? null : pars[p].getType(), true);
		return result;
	}

	public static ObjectCallResult mExec(final CallRuntime cr, final Object instance, final Class<?> instance_class, final String m, final I_Object streamIt) {
		Err.ifNull(streamIt);

		// --- Check Methods ---
		final I_Object[] args = cr.argsVar(streamIt, 0, 0); // No Block allowed
		final int parsLen = args.length;
		final Method[] methods = instance_class.getMethods();
		for(final Method method : methods)
			if(method.getName().equals(m) && method.getParameterCount() == parsLen) {
				if(!Lib_Java.testPars(method.getParameters(), args))
					continue;

				final Object[] invokePars = Lib_Java.jmoToJava(cr, args, method.getParameters());

				try {
					method.setAccessible(true); // Disable Java access-checking
					Object result = null;

					try {
						result = method.invoke(instance, invokePars); // nstance == null: Sometimes it's ok, sometimes not
					}
					catch(final NullPointerException e) {
						return null;
					}
					return A_Object.stdResult(Lib_Java.javaToJmo(result));
				}
				catch(final IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					final String methodString = "Java(\"" + method.getDeclaringClass().getSimpleName() + "\")." + method.getName();
					final String msg = e instanceof IllegalAccessException
						? "Illegal access to Java-method"
						: e instanceof IllegalArgumentException
							? "Illegal argument for Java-method"
							: "Invocation error";
					throw new ExternalError(cr, msg + ": " + methodString, Lib_Error.collectMessages(e));
				}
			}

		final Class<?>[] innerClasses = instance_class.getClasses();
		for(final Class<?> cl : innerClasses)
			if(cl.getSimpleName().equals(m) && cl.getTypeParameters().length == parsLen) {
				if(parsLen != 0) // TODO
					Lib_Error.notImplementedYet(cr, "Access to inner class with arguments is currently not available!");

				if(!Lib_Java.testPars(cl.getTypeParameters(), args))
					continue;

//				final Object[] invokePars = Lib_Java.jmoToJava(args);

				// Wrap args with Call
				final Call[] cArgs = new Call[args.length];
				for(int i = 0; i < args.length; i++)
					cArgs[i] = new Call(cr, args[i]);

				try {
					Object result = null;

					try {
						result = new JMo_Java(cl, cArgs, cr.getDebugInfo());
					}
					catch(final NullPointerException e) {
						return null;
					}
					return A_Object.stdResult(Lib_Java.javaToJmo(result));
				}
				catch(final IllegalArgumentException e) {
					final String methodString = "Java(\"" + instance_class.getName() + "\")." + cl.getSimpleName();
					final String msg = "Illegal argument for Java-method";
					throw new ExternalError(cr, msg + ": " + methodString, Lib_Error.collectMessages(e));
				}
			}

		// --- Java-Arrays ---
		if(instance != null) {
			final String instanceClassName = instance.getClass().getName();

			if(instanceClassName.charAt(0) == '[') {
				if(m.equals("length"))
					return A_Object.stdResult(new Int(Array.getLength(instance)));

				if(m.equals("get") && args.length == 1) {
					final I_Object paro = cr.argType(args[0], A_IntNumber.class);
					final int idx = Lib_Convert.getIntValue(cr, paro);
					final Object result = Array.get(instance, idx);
					final I_Object jmo = Lib_Java.javaToJmo(result);
					return A_Object.stdResult(jmo);
				}

				if(m.equals("set") && args.length == 2) {
					final I_Object idxo = cr.argType(args[1], A_IntNumber.class);
					final int idx = Lib_Convert.getIntValue(cr, idxo);
					final I_Object obj = args[0];
					final Object javaObject = Lib_Java.jmoToJavaType(cr, instanceClassName.charAt(1), obj);
					Array.set(instance, idx, javaObject);
					return A_Object.stdResult(streamIt);
				}
			}
		}

		// --- Automatic field ---
		try {
			final Field f = instance_class.getField(m);

			if(f != null) {
				final Object value = f.get(instance);
				final I_Object jmoValue = Lib_Java.javaToJmo(value);
				return A_Object.stdResult(jmoValue);
			}
		}
		catch(NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {}
		// --- Nothing found -> return ---
		return null;
	}

	/**
	 * java.util.ArrayList --> Java_{java.util.ArrayList}
	 */
	public static String packConv_classToFull(final Class<?> clazz) {
		final String canonical = clazz.getCanonicalName();
		final StringBuilder sb = new StringBuilder(32); // TODO Whats best?
		sb.append("Java_{");

		if(canonical == null)
			sb.append(clazz.getSimpleName());
		else
			sb.append(canonical);

		sb.append('}');
		return sb.toString();
	}

	/**
	 * java.util.ArrayList --> Java_ArrayList
	 */
	public static String packConv_classToSimple(final Class<?> clazz) {
		return "Java_" + clazz.getSimpleName();
	}

	/**
	 * Java_{java.util.ArrayList} --> java.util.ArrayList
	 */
	public static String packConv_FullToClass(final String full) {
		return full.substring(6, full.length() - 1);
	}

	public static boolean testPar(final Parameter parameter, final I_Object object) {
		final Class<?> pClass = parameter.getType();
		final int parModif = parameter.getModifiers();
		final Class<?> oClass = object.getClass();

		if(parModif != 0)
			Err.todo(pClass, parModif, oClass);

		return Lib_Java.isAssignable(pClass, oClass);
	}

	public static boolean testPars(final Parameter[] parameters, final I_Object[] args) {
		if(parameters.length != args.length)
			return false;

		for(int pn = 0; pn < parameters.length; pn++)
			if(!Lib_Java.testPar(parameters[pn], args[pn]))
				return false;

		return true;
	}

	public static Class<?> toJavaType(final Object object) {
		if(object == null)
			return Nil.class;

		if(object instanceof Boolean)
			return boolean.class;

		if(object instanceof Byte)
			return byte.class;
		if(object instanceof Short)
			return short.class;
		if(object instanceof Integer)
			return int.class;
		if(object instanceof Long)
			return long.class;

		if(object instanceof Float)
			return float.class;
		if(object instanceof Double)
			return double.class;

		if(object instanceof Character)
			return char.class;

		return object.getClass();
	}

	private static Object iListToJava(final CallRuntime cr, final List<I_Object> source, final Class<?> wanted) {

		if(wanted != null) {

			if(wanted == Object[].class) {
				final Object[] arr = new Object[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getValue(cr, source.get(i));
				return arr;
			}

			if(wanted == String[].class) {
				final String[] arr = new String[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getStringValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Byte[].class) {
				final Byte[] arr = new Byte[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getByteValue(cr, source.get(i));
				return arr;
			}

			if(wanted == byte[].class) {
				final byte[] arr = new byte[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getByteValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Short[].class) {
				final Short[] arr = new Short[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getShortValue(cr, source.get(i));
				return arr;
			}

			if(wanted == short[].class) {
				final short[] arr = new short[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getShortValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Integer[].class) {
				final Integer[] arr = new Integer[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getIntValue(cr, source.get(i));
				return arr;
			}

			if(wanted == int[].class) {
				final int[] arr = new int[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getIntValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Long[].class) {
				final Long[] arr = new Long[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getLongValue(cr, source.get(i));
				return arr;
			}

			if(wanted == long[].class) {
				final long[] arr = new long[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getLongValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Boolean[].class) {
				final Boolean[] arr = new Boolean[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getBoolValue(cr, source.get(i));
				return arr;
			}

			if(wanted == boolean[].class) {
				final boolean[] arr = new boolean[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getBoolValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Float[].class) {
				final Float[] arr = new Float[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getFloatValue(cr, source.get(i));
				return arr;
			}

			if(wanted == float[].class) {
				final float[] arr = new float[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getFloatValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Double[].class) {
				final Double[] arr = new Double[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getDoubleValue(cr, source.get(i));
				return arr;
			}

			if(wanted == double[].class) {
				final double[] arr = new double[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getDoubleValue(cr, source.get(i));
				return arr;
			}

			if(wanted == Character[].class) {
				final Character[] arr = new Character[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getCharValue(cr, source.get(i));
				return arr;
			}

			if(wanted == char[].class) {
				final char[] arr = new char[source.size()];
				for(int i = 0; i < source.size(); i++)
					arr[i] = Lib_Convert.getCharValue(cr, source.get(i));
				return arr;
			}
		}
		// Alternate: SimpleList
		final SimpleList<Object> al = new SimpleList<>(source.size());
		for(final I_Object o : source)
			al.add(Lib_Java.jmoToJava(cr, o, null, true));
		return al;
	}

	private static Object jmoToJavaType(final CallRuntime cr, final char type, final I_Object obj) {
		final Object java = Lib_Java.jmoToJava(cr, obj, null, true);

		if(java instanceof Number) {
			if(type == 'I')
				return ((Number)java).intValue();
			if(type == 'B')
				return ((Number)java).byteValue();
			if(type == 'S')
				return ((Number)java).shortValue();
			if(type == 'L')
				return ((Number)java).longValue();

			if(type == 'F')
				return ((Number)java).floatValue();
			if(type == 'D')
				return ((Number)java).doubleValue();
		}
//		if(type == 'C' && java instanceof Character) {
//			return java;
//		}

		return java;
	}

	private static boolean testPars(final TypeVariable<?>[] parameters, final I_Object[] args) {
		if(parameters.length != args.length)
			return false;
		// TODO More detailed tests and type check
		return true;
	}

}
