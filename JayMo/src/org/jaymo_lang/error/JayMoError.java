/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.error;

import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 2020-10-22
 *
 * @apiNote
 *          JayMo internal errors
 */
public class JayMoError extends ErrorBaseDebug {

	private static final long  serialVersionUID = -2059004754539807572L;
	public static final String ERROR_TYPE_INFO  = "Internal JayMo error";


	public JayMoError(final CallRuntime cr, final String text, final String detail) {
		this(new Throwable(), cr, text, detail);
	}

	public JayMoError(final String text, final String detail, final DebugInfo debugInfo) {
		this(new Throwable(), text, detail, debugInfo);
	}

	public JayMoError(final Throwable t, final CallRuntime cr, final String text, final String detail) {
		super(text, detail, cr, cr.call == null
			? null
			: cr.getDebugInfo());
		this.iAddJavaTrace(t);
	}

	public JayMoError(final Throwable t, final String text, final String detail, final DebugInfo debugInfo) {
		super(text, detail, null, debugInfo);
		this.iAddJavaTrace(t);
	}

	@Override
	public String getErrorTypeInfo() {
		return JayMoError.ERROR_TYPE_INFO;
	}

	private void iAddJavaTrace(final Throwable t) {
		if(t != null && MOut.isModeDev())
			try {
				this.addTrace(t.getStackTrace()); // Add Java StackTrace
			}
			catch(final Throwable tx) {}
	}

}
