/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.error;

import org.jaymo_lang.parser.I_DebugInfoSource;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 *
 * @apiNote
 *          Code-Errors at Parse-Time and Run-Time. The affected code MUST be corrected! There is no other solution!
 *          (e.g.: Call of unknown function, Unknown type, invalid amount of arguments, ...)
 */
public class CodeError extends ErrorBaseDebug {

	private static final long serialVersionUID = -2059004754539806572L;


	public CodeError(final CallRuntime cr, final String text, final String detail) {
		// Call darf null sein, damit in App die ARGS gesetzt werden können!
		super(text, detail, cr, cr.call == null
			? null
			: cr.getDebugInfo());
	}

	/**
	 * For Runtime-Errors, wheres no CallRuntime available (exp.: .compareTo)
	 */
	public CodeError(final String text, final String detail, final DebugInfo debugInfo) {
		super(text, detail, null, debugInfo);
	}

	public CodeError(final String text, final String detail, final I_DebugInfoSource debugSource) {
		super(text, detail, null, debugSource.getDebugInfo());
	}

	@Override
	public String getErrorTypeInfo() {
		return "Invalid source code";
	}

}
