/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.error;

import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ErrorComposer;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.I_ErrorDetails;
import de.mn77.base.error.I_ErrorInfo;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 *         2018-05-28
 */
public abstract class ErrorBaseDebug extends ErrorBase implements I_ErrorDetails, I_ErrorInfo {

	private static final long serialVersionUID = 8587625424636880857L;

	private final DebugInfo     source;
	private final String        call;
	private final Instance      instance;
	private final String        instanceStringNested;
	private final String        object;
	private final CallRuntime   crOrigin;
	private StackTraceElement[] ste = null;


	protected ErrorBaseDebug(final String message, final String detail, final CallRuntime cr, final DebugInfo source) {
		// "cr" and "source" can be "null"!!!
		super(message, detail);
		this.crOrigin = cr;
		this.source = source;
		this.object = cr == null || cr.getCurrentObject() == null || cr.call.hasObject()
			? null
			: cr.getCurrentObject().toString(cr, STYPE.IDENT);
		this.call = cr == null || cr.call == null
			? null
			: cr.call.toString(cr, STYPE.IDENT, true);
		this.instance = cr == null
			? null
			: cr.instance;
		this.instanceStringNested = this.instance == null // Because 'cr' is only needed for this
			? null
			: this.instance.toString(cr, STYPE.IDENT);

		if(MOut.isLogUsed())
			MOut.logErr(this.toInfo());
	}

	public void addDetail(final Object... oa) {
		Err.todo(oa);
	}

	public void addTrace(final StackTraceElement[] ste) {
		this.ste = ste;
	}

	public String getCall() {
		return this.call;
	}

	public CallRuntime getCallRuntime() {
		return this.crOrigin;
	}

	public Iterable<Object> getDetails() {
		final SimpleList<Object> list = new SimpleList<>();
		if(this.getDetail() != null)
			list.add("Detail  : " + this.getDetail());
//		if(this.source != null)
//			list.add("Source  : " + this.source);
//		if(this.source != null && this.source.getRawFile() != null)
//			list.add("File    : " + this.source.getRawFile());
//		if(this.source != null && this.source.getRawLine() != null)
//			list.add("Line    : " + this.source.getRawLine());
		if(this.call != null)
			list.add("Call    : " + (this.object != null ? this.object : "") + this.call);
//		if(this.instance != null)
		if(this.instanceStringNested != null)
//			list.add("Instance: " + this.instance.toStringNested(cr));
			list.add("Instance: " + this.instanceStringNested); // Because no 'cr' available

		// Add Java StackTrace:
		if(this.ste != null && MOut.isModeDev())
			for(final StackTraceElement e : this.ste)
				list.add("  Java@ " + e.toString());

		return list;
	}

	public abstract String getErrorTypeInfo();

	public String getFile() {
		return this.source == null
			? null
			: this.source.getRawFile();
	}

	public String getInstance() {
		return this.instance.toString();
	}

	public Integer getLine() {
		return this.source == null
			? null
			: this.source.getRawLine();
	}

	public String getSource() {
		return "" + this.source;
	}

	public String toInfo() {
		return new ErrorComposer(this, this.crOrigin).compose();
	}

}
