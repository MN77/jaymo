/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.runtime;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.FunctionPar;
import org.jaymo_lang.model.MultiCallItems;
import org.jaymo_lang.object.LoopHandle;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Group4;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.09.2019
 */
public class VarConstEnv {

	private static int counter = 0;

	private final int         id;
	public final VarEnv       vars;
	public final ConstEnv     cons;
	private final VarConstEnv parent;

	private MultiCallItems                                         multicall    = null;
	private LoopHandle                                             loop         = null;
	private Group4<Block, CallRuntime, CallRuntime, FunctionPar[]> funcInitData = null;
	private Group2<CallRuntime, CallRuntime>                       initType     = null;
	private LoopHandle                                             childLoop    = null;


	public VarConstEnv(final VarConstEnv parent) {
		this.parent = parent;
		final VarEnv parentVars = parent == null
			? null
			: parent.vars;
		final ConstEnv parentCons = parent == null
			? null
			: parent.cons;
		this.vars = new VarEnv(parentVars);
		this.cons = new ConstEnv(parentCons);
		this.id = ++VarConstEnv.counter;

		if(parent != null && parent.childLoop != null)
			this.loop = parent.childLoop;
	}

	public VarConstEnv(final VarEnv vars, final ConstEnv cons, final VarConstEnv parent) {
		this.parent = parent;
		this.vars = vars;
		this.cons = cons;
		this.id = ++VarConstEnv.counter;

		if(parent != null && parent.childLoop != null)
			this.loop = parent.childLoop;
	}


	// --- Multi-Call ---

	public VarConstEnv copyConst(final DebugInfo debug) {
		final VarEnv v = new VarEnv(null);
		return new VarConstEnv(v, this.cons, null);
	}

	// --- Loop ---

	public Group4<Block, CallRuntime, CallRuntime, FunctionPar[]> getFuncInit(final CallRuntime cr) {
		if(this.funcInitData != null)
			return this.funcInitData;
		if(this.parent != null)
			return this.parent.getFuncInit(cr);
		throw Err.invalid("There's no data to initialize!");
	}

	public int getID() {
		return this.id;
	}

	// --- Function Parameter-Init ---

	/**
	 * For debug
	 */
	public VarConstEnv getParent() {
		return this.parent;
	}

	public Group2<CallRuntime, CallRuntime> initTypeGet() {
		VarConstEnv cur = this;
		while(cur.initType == null && cur.parent != null)
			cur = cur.parent;
		return cur.initType;
	}

	public void initTypeSet(final CallRuntime crOld, final CallRuntime cpNew2) {
		this.initType = new Group2<>(crOld, cpNew2);
	}

	// --- Init Type ---

	public LoopHandle loopGet(final CallRuntime cr) {
		if(this.loop != null)
			return this.loop;
		if(this.parent != null)
			return this.parent.loopGet(cr);
		throw new CodeError(cr, "Unknown loop!", "There's no loop known, in this and the parent blocks");
	}

	public void loopSet(final LoopHandle handle) {
		if(this.loop != null)
			Err.invalid("Loop is already set!");
		this.loop = handle;
	}


	// --------------------------------

	public void setChildLoop(final LoopHandle handle) {
		this.childLoop = handle;
	}

	public void setFuncInit(final Block block, final CallRuntime cpOutside, final CallRuntime crNew, final FunctionPar[] vars) {
		this.funcInitData = new Group4<>(block, cpOutside, crNew, vars);
	}

	@Override
	public String toString() {
//		final String sp = this.parent == null
//			? ""
//			: " --> " + this.parent.toString();

		final StringBuilder sb = new StringBuilder();
		sb.append("VCE: ");
		sb.append(this.id);

		VarConstEnv parent = this.parent;

		while(parent != null) {
			sb.append(" -> ");
			sb.append(parent.id);
			parent = parent.parent;
		}
		return sb.toString();
	}

//	/**
//	 * @return A full copy of the current state with no parent
//	 */
//	public VarConstEnv freezeCopy() {
//		final VarEnv v = this.vars.freezeCopy();
//		final VarEnv c = this.cons.freezeCopy();
//		final VarConstEnv copy = new VarConstEnv(v, c, null);
//		copy.multicall = this.multicall;
//		copy.loop = this.loop;
//		return copy;
//	}

//	/**
//	 * @return New environment with a copy of selected vars & consts
//	 */
//	public VarConstEnv freezeCopy(String[] vcNamesToCopy, DebugInfo debug) {
//		VarEnv v = new VarEnv(null, false);
//		VarEnv c = new VarEnv(null, true);
//
//		if(vcNamesToCopy != null)
//		for(String name : vcNamesToCopy) {
//			char c0 = name.charAt(0);
//			boolean isConst = c0 >= 'A' && c0 <= 'Z';
//
//			if(isConst) {
//				Group2<String, I_Object> g = this.cons.getToCopy(name, debug);
//				c.setCopy(name, g);
//			}
//			else {
//				Group2<String, I_Object> g = this.vars.getToCopy(name, debug);
//				v.setCopy(name, g);
//			}
//		}
//
//		return new VarConstEnv(v, c, null);
//	}

//	/**
//	 * @return New environment with a copy of all consts and selected vars
//	 */
//	public VarConstEnv copyForEventHandler(final String[] varNamesToCopy, final DebugInfo debug) {
//		final VarEnv v = new VarEnv(null, false);
//
//		if(varNamesToCopy != null)
//			for(final String name : varNamesToCopy) {
//				final Group2<String, I_Object> g = this.vars.getToCopy(name, debug);
//				v.setCopy(name, g);
//			}
//
//		return new VarConstEnv(v, this.cons, null);
//	}

	public MultiCallItems useMultiCall() {
		if(this.multicall == null && this.parent != null)
			this.multicall = this.parent.useMultiCall();
		if(this.multicall == null)
			this.multicall = new MultiCallItems();
		return this.multicall;
	}

}
