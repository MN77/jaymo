/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.runtime;

/**
 * @author Michael Nitsche
 * @created 26.03.2022
 */
public enum STYPE {

	REGULAR,
	NESTED,
	IDENT,
	DESCRIBE;

	/*
	 * REGULAR = The text or an text representation of a object. Multi line, result can be empty
	 * Example: a, 9, abc123, [3,5,7,a,z]
	 * NESTED = Short one-line version
	 * Example: a, 9, abc123, List<3>
	 * IDENT = Short one-line ident string
	 * Example: 'a', 9, "abc123", [3b,5,7l,'a',"z"]
	 * DESCRIBE = human friendly and full describing text of a object
	 * Example: 'a', 9i, "abc123", [3b,5i,7l,'a',"z"]
	 */


	public STYPE getNested() {
		return this == REGULAR
			? NESTED
//			: this == DESCRIBE
//				? IDENT
			: this;
	}

}
