/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.runtime;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ReturnException;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.App;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.magic.var.MV_THIS;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @created 23.05.2018
 *          03.09.2019 Rewrite
 *
 * @apiNote
 *          Instance of an self defined Type
 */
public class Instance extends A_EventObject implements I_AutoBlockDo {

	private final Type        type;
	private final App         app;
	private final Call[]      args;
	private VarConstEnv       main_env = null;
	private I_Object          parent   = null;
	private final CallRuntime parentCr;


	/**
	 * :_FUNC
	 * +_FUNC
	 */
	public Instance(final CallRuntime cr, final App app, final Type type, final Call[] args) {
		this.parentCr = cr;
		this.type = type;
		this.app = app;
		this.args = args; // may be null
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		if(this.type.getAutoBlockFunction() == null)
			throw new CodeError(cr, "This type has no auto-block-function!", "Type: " + this.type.getName());

//		final Block surrBlock, final I_Object obj, final String met, final Call[] args, final DebugInfo debugInfo
		final String newMethod = this.type.getAutoBlockFunction();
		final Call call2 = new Call(cr.getSurrBlock(), this, newMethod, cr.call.argCalls, cr.getDebugInfo());

		if(cr.hasBlock())
			call2.setBlock(cr.getCallBlock());
		if(cr.hasStream())
			call2.setStream(cr.getStream());
		final CallRuntime crNew = cr.copyCall(call2, false);
		return this.callMethod(crNew, newMethod).obj;
	}

	public App getApp() {
		return this.app == null
			? (App)this
			: this.app;
	}

	@Override
	public A_Immutable getConstant(final CallRuntime cr, final String name) {
		return this.type.getEnum(name, cr.getDebugInfo());
	}

	public VarConstEnv getMainEnv() {
		return this.main_env;
	}

	public Type getType() {
		return this.type;
	}

	// ----- CallMethods -----

	@Override
	public void init(final CallRuntime crOld) {
		final CallRuntime crNew = new CallRuntime(this.parentCr, this, crOld.call);
		this.main_env = crNew.vce;

		// Always initialize, even if it is a control object!

		I_Object[] argObjs = null;

		if(this.type.isControl()) {
			// Init Parameter-Vars with default-Values?

			final int parLen = this.type.getVars() == null ? 0 : this.type.getVars().length;
			argObjs = new I_Object[parLen];

//			parObjs = new I_Object[this.args.length];
//			for(int i = 0; i < parObjs.length; i++) {
//				FunctionPar fp = this.type.getVars()[i];
//				if(fp.defaultValue != null)
//					parObjs[i] = fp.defaultValue;
//				else {
//					parObjs[i] = Nil.NIL;	// No, it should be unset
////					if(fp.type == null)
////						parObjs[i] = new Int(0);
//				}
//			}

			crNew.vce.initTypeSet(crOld, crNew);
		}
		else if(this.args != null) {
			argObjs = new I_Object[this.args.length];
			for(int i = 0; i < argObjs.length; i++)
				argObjs[i] = crOld.execInit(this.args[i], this); // crOld: So _THIS leads not to the current Instance
		}
		else
			argObjs = new I_Object[0];
		this.type.getBlock().initTypeBlock(crOld, crNew, argObjs);

		this.parent = this.type.getParent(); // This is NonAtomic or A_Immutable

		// Execute root body of parent
		if(this.parent != null) {
			final Call parentCall = new Call(crOld, this.parent);
			final CallRuntime crParent = crNew.copyCall(parentCall, true);
			this.parent = crParent.exec(this, true);
		}
		// Check for overwritten events:
		I_Object test = this.parent;

		while(test != null && test instanceof A_EventObject) {
			final Iterable<String> eventlist = this.type.getEvents().getList();
			if(eventlist != null)
				for(final String ev : eventlist)
					if(((A_EventObject)test).validateEvent(crNew, '@' + ev))
						throw new CodeError(crNew, "Invalid event-definition", "This event is already defined in a parent type: @" + ev); // Line-Number will be wrong
			test = test instanceof Instance
				? ((Instance)test).internalGetParent()
				: null;
		}
		// Execute root body
		this.type.getBlock().execTypeBlock(crNew);
	}

	public I_Object internalGetParent() {
		return this.parent;
	}


	// --- Div ---

	@Override
	public String toString() {
//		return this.getTypeName();
		return this.type.getName();
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		// TODO: Alle Funktionen privat oder direkt hier rein?

		switch(type) {
			case REGULAR:
			case NESTED:
//				return "Instance(" + this.type.getName() + ")";
//				return this.type.getName();

				if(this.type.getFunctions().knows("toStr")) {
					final Function f = this.getType().getFunctions().get(cr, "toStr");
					final CallRuntime cr2 = cr.copyNull();
					final I_Object result = f.getBlock().execOverwrite(cr2, this, f);
					return Lib_Convert.getStringValue(cr, result); // Allowed are anything, that can be converted to a String
				}
				else
					return this.toString();
			case IDENT:
				if(this.type.getFunctions().knows("toIdent")) {
					final Function f = this.getType().getFunctions().get(cr, "toIdent");
					final CallRuntime cr2 = cr.copyNull();
					final I_Object result = f.getBlock().execOverwrite(cr2, this, f);
					return Lib_Convert.getStringValue(cr, result); // Allowed are anything, that can be converted to a String
				}
				else
					return this.type.getName();
			case DESCRIBE:
				if(this.type.getFunctions().knows("toDescribe")) {
					final Function f = this.getType().getFunctions().get(cr, "toDescribe");
					final CallRuntime cr2 = cr.copyNull();
					final I_Object result = f.getBlock().execOverwrite(cr2, this, f);
					return Lib_Convert.getStringValue(cr, result); // Allowed are anything, that can be converted to a String
				}
				else
					return this.type.getName();
			default:
				throw Err.impossible(type);
		}
	}

	@Override
	public boolean validateEvent(final CallRuntime cr, final String event) {
		return this.type.getEvents().knows(event.substring(1)); // Send without '@'
	}

	@Override
	protected final ObjectCallResult callEvent(final CallRuntime cr, final String event) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		cr.instance.eventRunRaw(cr, event, args.length == 1 ? args[0] : this);
		return new ObjectCallResult(this, false); // true?!?
	}

	@Override
	protected ObjectCallResult callMethod(final CallRuntime crOutside, final String method) {

		switch(method) {
			case "_init": //_reinit, _args
				return this.iInit(crOutside);
			case "_sleep":
				this.iSleep(crOutside);
				return A_Object.stdResult(this);
		}

		// Functions
		if(!this.type.getFunctions().knows(method) && this.parent != null) {
			final Call cParent = new Call(crOutside.getSurrBlock(), this.parent, method, crOutside.call.argCalls, crOutside.getDebugInfo());
			if(crOutside.call.hasBlock())
				cParent.setBlock(crOutside.call.getBlock());

			final CallRuntime crParent = new CallRuntime(crOutside, this, cParent, crOutside.vce);

			// Copy it-Value
			final Var itVar = new Var(Var.IT);
			final I_Object itValue = itVar.get(crOutside);
			itVar.let(crOutside, crParent, itValue);

			final I_Object result = crParent.exec(this, true);
			return new ObjectCallResult(result, false); // TODO Sometimes true?
		}
		// Allow only access to the main-environment
		final CallRuntime crInside = crOutside.copyVCE(this, this.main_env, true);
		final Function f = this.type.getFunctions().get(crInside, method);

		// Check access to private functions
		if(f.isPrivate() && !(crOutside.call.object instanceof MV_THIS))
			throw new CodeError(crOutside, "Invalid access to private function", "This function is only accessable within the type definition: " + f.getNames()[0]);

		try {
//			Prevent cycle of death!
//			if(lastThread != null &&
//				Thread.currentThread().equals(lastThread))
//			throw new ExecError(cr, "Cycle of death!", cr.call.toString());
//			lastThread = Thread.currentThread();

			return new ObjectCallResult(f.getBlock().execFunction(crOutside, crInside, f), f.hasControl());
		}
		catch(final ReturnException r) {
			Lib_Exec.checkExceptionIsEnd(crInside, r.get());
			return new ObjectCallResult(r.get().getResult(), f.hasControl());
		}
	}

	/**
	 * For App
	 */
	protected void setMainEnv(final VarConstEnv vce) {
		if(this.main_env != null)
			Err.forbidden("Is already set!");
		this.main_env = vce;
	}

	private ObjectCallResult iInit(final CallRuntime crOld) {
		Lib_Error.ifNotControl(crOld, this.type);
		Lib_Error.ifNotBetween(crOld, 0, 2, crOld.argCount(), "Arguments for _init");

		final Group2<CallRuntime, CallRuntime> init = crOld.vce.initTypeGet();

		switch(crOld.argCount()) {
			case 0: // Init all
				I_Object[] parObjs = null;
				if(this.args != null) {
					parObjs = new I_Object[this.args.length];
					for(int i = 0; i < parObjs.length; i++)
						parObjs[i] = init.o1.execInit(this.args[i], this); // crOld: So _THIS leads not to the current Instance
				}
				else
					parObjs = new I_Object[0];

				this.type.getBlock().reInit(init.o1, init.o2, parObjs);
				break;
			case 1: // Init one
				final int parLen1 = this.type.getVars().length;
				//TODO TEST THIS!!! Use of args vs. type.getVars?!?
//				if(this.args == null || this.args.length == 0)
				if(parLen1 == 0)
					throw new RuntimeError(crOld, "Invalid call of _init", "This type has no parameters to initialize");

				final I_Object funcPars1 = crOld.args(this, A_IntNumber.class)[0];
				final int parToInit1 = Lib_Convert.getIntValue(crOld, funcPars1);
				Lib_Error.ifArgs(parToInit1, 1, parLen1, crOld, this);

				final I_Object parObj1 = init.o1.execInit(this.args[parToInit1 - 1], this); // TODO null ... replace with current object or streamIT

				this.type.getBlock().reInit(init.o1, init.o2, this.args.length, parToInit1, parObj1);
				break;


			case 2: // Init one with _EACH
				final int parLen2 = this.type.getVars().length;
				//TODO TEST THIS!!! Use of args vs. type.getVars?!?
//				if(this.args == null || this.args.length == 0)
				if(parLen2 == 0)
					throw new RuntimeError(crOld, "Invalid call of _init", "This type has no parameters to initialize");

				final I_Object[] funcPars2 = crOld.args(this, A_IntNumber.class, I_Object.class);
				final int parToInit2 = Lib_Convert.getIntValue(crOld, funcPars2[0]);
				Lib_Error.ifArgs(parToInit2, 1, parLen2, crOld, this);
				final I_Object eachValue2 = funcPars2[1];

				final I_Object parObj2 = init.o1.execEachInit(this.args[parToInit2 - 1], this, eachValue2);

				this.type.getBlock().reInit(init.o1, init.o2, this.args.length, parToInit2, parObj2);
				break;
		}
		return new ObjectCallResult(this, false);
	}

	/**
	 * °_sleep(IntNumber millisec)Same | Pause this thread for millisec
	 * °_sleep(IntNumber sec, IntNumber millisec)Same | Pause this thread for seconds, millisec
	 * °_sleep(IntNumber min, IntNumber sec, IntNumber millisec)Same | Pause this thread for minutes, seconds, millisec
	 * °_sleep(IntNumber hours, IntNumber min, IntNumber sec, IntNumber millisec)Same | Pause this thread for hours, minutes, seconds, millisec
	 */
	private void iSleep(final CallRuntime cr) {
		final I_Object[] oa = cr.argsFlex(this, 1, 4);
		final int len = oa.length;
		final int[] values = new int[len];
		for(int i = 0; i < len; i++)
			values[i] = Lib_Convert.getIntValue(cr, oa[i]);

		int ms = 0;

		switch(len) {
			case 1:
				ms = values[0];
				break;
			case 2:
				ms = values[0] * 1000 + values[1];
				break;
			case 3:
				ms = values[0] * 60_000 + values[1] * 1000 + values[2];
				break;
			case 4:
				ms = values[0] * 3_600_000 + values[1] * 60_000 + values[2] * 1000 + values[3];
				break;
		}
		Sys.sleep(ms);
	}

}
