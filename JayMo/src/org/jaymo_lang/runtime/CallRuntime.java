/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.runtime;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.ErrorBase;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.error.RuntimeWarning;
import org.jaymo_lang.model.App;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.model.FunctionPar;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.StrictManager;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.I_HasConstructor;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.I_ObjectReplace;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.immute.JMo_Enum;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.magic.I_MagicReplace;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.object.pseudo.A_MemLet;
import org.jaymo_lang.object.pseudo.FuncLet;
import org.jaymo_lang.object.pseudo.MultiCall;
import org.jaymo_lang.object.pseudo.NonAtomic;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.VarLet;
import org.jaymo_lang.object.struct.A_Sequence;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.parser.I_DebugInfoSource;
import org.jaymo_lang.util.Lib_Arguments;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_MagicVar;
import org.jaymo_lang.util.Lib_Prio;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 29.07.2019
 */
public class CallRuntime implements I_DebugInfoSource {

	public final CallRuntime parent; // Can be null
	public final Instance    instance;
	public final Call        call;
	public final VarConstEnv vce;
	public final CallRuntime funcOutside; // Outside (Environment with call that calls current Function)

	private I_Object[] args_result = null;
	private I_Object   curObject   = null; // This will be changed with every execution. It's only for debug info.


	public CallRuntime(final CallRuntime parent, final Instance instance, final Call call) {
		this(parent, instance, call, null, NEW_VCE.NEW_LEVEL, null);
	}

	/**
	 * @apiNote This is only for Instance to exec parent functions
	 */
	public CallRuntime(final CallRuntime parent, final Instance instance, final Call call, final VarConstEnv parentVCE) {
		this(parent, instance, call, parentVCE, NEW_VCE.NEW_LEVEL, null);
	}

	/**
	 * call: may also be null!
	 */
	private CallRuntime(final CallRuntime parent, final Instance instance, final Call call, final VarConstEnv parentVCE, final NEW_VCE varEnvNew, final CallRuntime funcOutside) {
		this.parent = parent;
		this.instance = instance;
		this.call = call;
		this.funcOutside = funcOutside;
		this.vce = varEnvNew == NEW_VCE.KEEP_PARENT
			? parentVCE
			: varEnvNew == NEW_VCE.WRAP_PARENT
				? new VarConstEnv(parentVCE.vars, parentVCE.cons, parentVCE)
				: new VarConstEnv(parentVCE); // NEW_LEVEL
	}

	// --- Copy ---

	public int argCount() {
		return this.call.getArgCount();
	}

	/**
	 * Init only one argument
	 */
	public I_Object argEach(final I_Object itStream, final int argIdx, final I_Object itEach, final Class<?> type) {
		return Lib_Arguments.checkArgEach(this, itStream, argIdx, itEach, type);
	}

	/**
	 * @apiNote Checks, if the given single argument is a Enum from the current object type.
	 */
	public JMo_Enum argEnum(final I_Object itStream) {
		final JMo_Enum e = (JMo_Enum)Lib_Arguments.checkArgs(this, itStream, new Class<?>[]{JMo_Enum.class})[0];
		if(!e.getOriginTypeName().equals(itStream.getTypeName()))
			throw new CodeError(this, "Invalid enum", "Enum from current type is needed, but got enum from type: " + e.getOriginTypeName());
		return e;
	}

	public I_Object[] args(final I_Object itStream, final Class<?>... types) {
		return Lib_Arguments.checkArgs(this, itStream, types);
	}

	/**
	 * Init Args with _EACH, offset for VarLet+calc, FuncLet, calc
	 */
	public I_Object argsEach(final I_Object itStream, final int offset, final I_Object[] itEach, final Class<?> type) {
		return Lib_Arguments.checkArgsEach(this, itStream, offset, itEach, type);
	}

	/**
	 * @apiNote Use this only if it's really really sensefull! Return type for methods must always be the same!
	 */
	public I_Object[] argsExt(final I_Object itStream, final Class<?>[]... types) {
		return Lib_Arguments.checkArgsExt(this, itStream, types);
	}

	public I_Object[] argsFlex(final I_Object itStream, final int arg_min, final int arg_max) {
		return Lib_Arguments.checkArgsFlex(this, itStream, arg_min, arg_max);
	}

	public void argsNone() {
		if(this.call.getArgCount() > 0)
			throw new CodeError(this, "Invalid number of arguments", "No arguments allowed");
//		return Lib_Arguments.checkArgs(this, null, null);
	}

	public I_Object argsOneAdvance(final I_Object itStream, final int argIdx, final Class<? extends I_Object> type) {
		return Lib_Arguments.checkArgAdvance(this, itStream, argIdx, null, type, false); // TODO argAmount null?!?
	}

	/**
	 * @apiNote Use this only if it's really sensefull!
	 */
	public I_Object argsOneAdvanceExt(final I_Object itStream, final int argIdx, final Class<?>... types) {
		return Lib_Arguments.checkArgAdvanceExt(this, itStream, argIdx, null, types, false); // TODO argAmount null?!?
	}

	// --- Div ---

//	public void describe(int left) {
//		MOut.line();
//		String space = Lib_Parser.space(left);
//		MOut.print(space + "CurProc " + this.hashCode());
//		left++;
//		space = Lib_Parser.space(left);
//
//		MOut.print(space + "Instance: " + this.instance.toStringIdent());
//		MOut.print(space + "VCE: " + this.vce.toString());
//		MOut.print(space + "Call: " + (this.call == null
//			? "null"
//			: this.call.toStringRuntime(this)));
//		if(this.call != null) {
//			MOut.print(space + "SurrBlock: " + this.call.surrounding);
////			MOut.print(space + "Method: " + this.call.method);
//			MOut.print(space + "CallBlock: " + this.call.getBlock());
//			MOut.print(space + "Vars (Types): ");
//			if(this.getSurrBlock() != null && this.getSurrBlock().getVarManager() != null)
//				this.getSurrBlock().getVarManager().describe(left + 1);
//			else
//				MOut.print(space + "  none");
//		}
//	}

	public I_Object[] argsVar(final I_Object itStream, final int arg_min, final int varArgsOffset) {
		final I_Object[] args = Lib_Arguments.checkArgsFlex(this, itStream, arg_min, null);

		if(args.length == varArgsOffset + 1 && args[varArgsOffset] instanceof A_MemLet) {
			final A_MemLet vl = (A_MemLet)args[varArgsOffset];
			final A_Sequence list = (A_Sequence)this.argType(vl.getMem().get(this), A_Sequence.class);
			final I_Iterable<? extends I_Object> al = list.getInternalCollection();
			final I_Object[] result = new I_Object[varArgsOffset + al.size()];
			Lib_Error.ifArgs(result.length, arg_min, null, this, itStream);
			if(varArgsOffset > 0)
				System.arraycopy(args, 0, result, 0, varArgsOffset);
			for(int i = 0; i < al.size(); i++)
				result[varArgsOffset + i] = al.get(i);
//			return al.toArray(new I_Object[al.size()]);
			return result;
		}
		else {
			for(int i = 0; i < args.length; i++)
				args[i] = Lib_Convert.getValue(this, args[i]);
			return args;
		}
	}

	/** Type check for function argument **/
	public I_Object argType(final I_Object arg, final Class<?> type) {
		return Lib_Arguments.checkArg(this, arg, type, null, null, false);
	}

	/**
	 * Type check for function argument
	 *
	 * @apiNote Use this only if it's really sensefull!
	 */
	public I_Object argTypeExt(final I_Object arg, final Class<?>... types) {
		return Lib_Arguments.checkArgExt(this, arg, types, null, null, false);
	}

	// --- Get ---

	/** Type check for function argument, when raw access to a Var or Const is needed **/
	@SuppressWarnings("unchecked")
	public <T extends I_Mem> T argTypeMem(final I_Object arg, final Class<T> type) {
		Lib_Arguments.checkArgMem(this, arg, type, null, null, false);
		return (T)arg;
	}

	public void blockForbidden() {
		Lib_Arguments.checkBlockForbidden(this);
	}

	public void blockNecessary() {
		Lib_Arguments.checkBlockNecessary(this);
	}

	public CallRuntime copyBlockItem(final Call call) {
		return new CallRuntime(this, this.instance, call, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
	}

	/**
	 * To create an new Variable-Environment with an new Call
	 */
	public CallRuntime copyCall(final Call call, final boolean newVarEnvLevel) {
		return new CallRuntime(this, this.instance, call, this.vce, newVarEnvLevel ? NEW_VCE.NEW_LEVEL : NEW_VCE.KEEP_PARENT, this.funcOutside);
	}

	public CallRuntime copyEach(final String method) {
		final Call call = new Call(this.getSurrBlock(), null, method, this.call.argCalls, this.getDebugInfo());
		return new CallRuntime(this, this.instance, call, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
	}

	public CallRuntime copyEventHandler(final Call call) {
		// Speed of all solutions is nearly equal.

		final VarConstEnv copy = this.vce; // Possible --> there may be "funny" side effects
//		final VarConstEnv copy = this.vce.freezeCopy();	// Freeze the current state completly --> okay, but maybe not logic
//		final VarConstEnv copy = this.vce.freezeCopy(vcNamesToCopy, this.getDebugInfo());	// okay --> complex and bad
//		final VarConstEnv copy = this.vce.copyForEventHandler(varNamesToCopy, this.getDebugInfo()); // possible, but a kind of irritating
//		final VarConstEnv copy = this.vce.copyConst(this.getDebugInfo()); // very best way

		return new CallRuntime(this, this.instance, call, copy, NEW_VCE.KEEP_PARENT, this.funcOutside);
	}


	// --- Argument-Checks ---

	public CallRuntime copyFunction(final CallRuntime outside) {
		return new CallRuntime(this, this.instance, this.call, this.instance.getMainEnv(), NEW_VCE.NEW_LEVEL, outside);
//		return new CallRuntime(this, this.instance, this.call, this.vce, NEW_VCE.NEW_LEVEL, outside);	// Possible, but no clean way
	}

	public CallRuntime copyFunctional(final Call c, final Call[] args, final DebugInfo debug) {
		final Block emptyBlock = new Block(null, null, null);
		final Call callCopy = new Call(emptyBlock, c.object, c.method, args, debug);
		if(c.hasStream())
			callCopy.setStream(c.getStream());
		final Instance newInstance = new Instance(this, this.instance.getApp(), this.instance.getType(), null);
		newInstance.setAlreadyInit();
//		return new CallRuntime(newInstance, callCopy, this.vce, NEW_VCE.NEW_LEVEL, null); // Possible, but a FuncLet should not have access to the surrounding environment!
		return new CallRuntime(this, newInstance, callCopy, null, NEW_VCE.NEW_LEVEL, null);
	}

	public CallRuntime copyLoop(final LoopHandle handle) {
		final CallRuntime result = new CallRuntime(this, this.instance, this.call, this.vce, NEW_VCE.WRAP_PARENT, this.funcOutside);
		result.vce.loopSet(handle);
		return result;
	}

	/**
	 * @apiNote Copy CallRuntime with empty call added.
	 */
	public CallRuntime copyNull() {
//		final Call c = new Call(this.getSurrBlock(), Nil.NIL, this.getDebugInfo());
		final Call c = new Call(this, null);
		return new CallRuntime(this, this.instance, c, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
	}

	public CallRuntime copyPass() {
//		return new CallRuntime(this.instance, this.call, this.vce, NEW_VCE.WRAP_PARENT, this.funcOutside);
		return new CallRuntime(this, this.instance, this.call, this.vce, NEW_VCE.NEW_LEVEL, this.funcOutside);
	}

	public CallRuntime copyVCE(final Instance newInstance, final VarConstEnv base_vce, final boolean varEnvNew) {
		return new CallRuntime(this, newInstance, this.call, base_vce, varEnvNew ? NEW_VCE.NEW_LEVEL : NEW_VCE.KEEP_PARENT, this.funcOutside);
	}

	public I_Object exec(final I_Object streamIt, final boolean isArgument) {
		return this.exec(streamIt, null, (byte)-1, isArgument).o1;
	}

	public I_Object execEachInit(final Call call, final I_Object streamIt, final I_Object eachIt) {
		Err.ifNull(call, streamIt, eachIt);
		final CallRuntime cpInit = new CallRuntime(this, this.instance, call, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
		final Group2<I_Object, Call> g = cpInit.exec(streamIt, eachIt, (byte)0, false);
		if(g.o2 != null)
			Err.invalid(g.o2);
		return g.o1;
	}

	public I_Object execInit(final Call call, final I_Object streamIt) {
		Err.ifNull(call, streamIt);
		final CallRuntime cpInit = new CallRuntime(this, this.instance, call, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
		return cpInit.exec(streamIt, false); // TODO: arg = true?!?
	}

	public Group2<I_Object, Call> execObject(final I_Object with, final byte leftprio, final boolean isArg) {
		this.curObject = with;

		if(with instanceof I_HasConstructor) {
			final Return constructorResult = ((I_HasConstructor)with).constructor(this);
			if(constructorResult != null)
				return new Group2<>(constructorResult, null);
		}

		// --- Call ohne Methode
		if(this.call.method == null) {

			if(this.call.hasBlock()) {
				if(isArg)
					throw new CodeError(this, "Invalid use of block and auto-block-function.", "Don't append a block to an argument");

				final I_Object this2 = Lib_Convert.getValue(this, with);

				if(this2 instanceof I_AutoBlockDo) {
					if(with instanceof I_Mem) // Prevent Auto-Block after Var or Const
						throw new CodeError(this, "Auto-Block can't be used after a variable or constant.", "Use it with a function, for example '.pass' or '.each': " + with);

					this.getStrict().checkAutoBlock(this);
					final I_Object ab_result = ((I_AutoBlockDo)this2).autoBlockDo(this);
					return new Group2<>(ab_result, null);
				}
				else
					throw new CodeError(this, "This type has no auto-block-function.", "Use it with a function, for example '.pass' or '.each': " + with.getTypeName());
			}

//			if(with instanceof Var && !((Var)with).isInitialized(this)) // This is correct, but collides with Auto-VarLet
			if(!isArg && with instanceof I_Mem && !((I_Mem)with).isInitialized(this)) {
				final String vc = with instanceof Var ? "Variable" : "Constant";
				throw new CodeError(this, vc + " without value.", "This " + vc + " has no allocated value: " + with.toString());
			}
			return new Group2<>(with, null);
		}
		Call next = this.call.getBlock() != null ? null : this.call.getStream(); // Dont use and exec stream when block is present!

		// --- Maybe prefer Stream	- NO Block is allowed!
		while(next != null && Lib_Prio.streamPrio(this.call, next) && this.call.getArgCount() == 1) {
			final I_Object arg = this.args(with, I_Object.class)[0];
			final Group2<I_Object, Call> streamPrioResult = this.execStream(arg, next, leftprio == -1 ? this.call.prio : leftprio); // TODO leftprio prüfen!!!
			this.args_result = new I_Object[]{streamPrioResult.o1};
			next = streamPrioResult.o2;
		}
		// --- Call self! ---
		final ObjectCallResult callResult = with.call(this);

		if(callResult.is_Attachment_Exec)
			return new Group2<>(callResult.obj, null);


		// --- Anhang ausführen

		final String method = this.call.method;
		I_Object currentresult = callResult.obj;

		if(this.call.getBlock() != null) {

			// Don't do something, if this function has full attachment control!
			if(this.getType().getFunctions().knows(method)) {
				final Function f = this.getType().getFunctions().get(this, method);
				if(f.hasControl())
					return new Group2<>(currentresult, null);
			}
//			if(currentresult instanceof I_AutoBlockDo) {
//				this.getStrict().checkAutoBlockAfterFunc(this);
//
//				final Call call2 = new Call(this.getSurrBlock(), currentresult, "do", null, this.getDebugInfo());
//				call2.setBlock(this.call.getBlock());
//				if(this.call.getStream() != null)
//					call2.setStream(this.call.getStream());
//				final CallRuntime crNew = this.copyCall(call2, false);
//
//				currentresult.initOnce(crNew);
//
//				final I_Object abResult = ((I_AutoBlockDo)currentresult).autoBlockDo(crNew);
//				return new Group2<>(abResult, null);
//			}
//			else
//				throw Err.forbidden("A function can't have a block without I_AutoBlockDo or hasAttachmentControl!");

//			final I_Object bResult = Lib_Exec.execBlockStream(cr, currentresult);
//			return new Group2<>(bResult, null);


			/*
			 * === Auto-Pass ===
			 * Working fine, but however, there is a high probability that you will do something with it that you don't want to do that way.
			 * So, currently its only allowed for mem and "=> var"
			 *
			 * this.getStrict().checkAutoPass(this);
			 */
			final Block block = this.getCallBlock();
			if(block != null)
//				try {
//					currentresult = block.exec(this, currentresult);
//				}
//				catch(final ReturnException e) {
//					return new Group2<>(e.get(), next);
//				}
				throw new CodeError(this, "No block allowed for this function!", "Use for example: '.pass'");
		}
		if(next == null)
			return new Group2<>(currentresult, next);

		while(next != null) {
			if(leftprio > -1 && next.prio < this.call.prio)
				return new Group2<>(currentresult, next);

			final Group2<I_Object, Call> streamResult = this.execStream(currentresult, next, leftprio); //(byte)-1		leftprio
			next = streamResult.o2;
			currentresult = streamResult.o1;
		}
		return new Group2<>(currentresult, next);
	}

	public Group2<I_Object, Call> execStream(final I_Object streamIt, final Call next, final byte leftprio) { //TODO next umbenennen, leftprio zu leftPrio
		if(next == null)
			return new Group2<>(streamIt, null);

		// Break clear logical results
		switch(next.method) {
			case "&&":
				if(streamIt == Bool.FALSE)
					return new Group2<>(streamIt, null);
				break;
			case "||":
				if(streamIt == Bool.TRUE)
					return new Group2<>(streamIt, null);
				break;

			case "!&&":
				if(streamIt == Bool.FALSE)
					return new Group2<>(Bool.TRUE, null);
				break;
			case "!||":
				if(streamIt == Bool.TRUE)
					return new Group2<>(Bool.FALSE, null);
				break;
		}
		final CallRuntime crStream = new CallRuntime(this, this.instance, next, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
		return crStream.exec(streamIt, null, leftprio, false);
	}

	public App getApp() {
		return this.instance.getApp();
	}

	public I_Object getArgAdvance(final CallRuntime cr, final I_Object streamIt, final int funcArgIdx, final FunctionPar[] funcPars, final boolean buffer, final I_Object eachItObj) {
		if(buffer && this.args_result != null)
			return this.args_result[funcArgIdx];

		final I_Object o = this.call.object != null
			? this.call.object
			: streamIt;

		int diff = funcPars.length - this.call.argCalls.length;
		for(int fp = funcPars.length - 1; fp > funcArgIdx; fp--)
			if(diff > 0 && funcPars[fp].defaultValue != null)
				diff--;

		if(diff > 0 && funcPars[funcArgIdx].defaultValue != null)
			return cr.execInit(funcPars[funcArgIdx].defaultValue, streamIt);

		final Call argCall = this.call.argCalls[funcArgIdx - diff];
		return eachItObj == null
			? this.execInit(argCall, o)
			: this.copyCall(argCall, false).exec(o, eachItObj, (byte)-1, true).o1;
	}

	public I_Object getArgAdvance(final I_Object streamIt, final int argIdx) {

		if(this.args_result != null)
			return this.args_result[argIdx];
		else {
			final I_Object o = this.call.object != null
				? this.call.object
				: streamIt;

			return this.execInit(this.call.argCalls[argIdx], o);
		}
	}


	// --- Single Argument-Check

	public I_Object getArgEach(final I_Object streamIt, final int argIdx, final I_Object eachItObj) {
		final I_Object o = this.call.object != null
			? this.call.object
			: streamIt;

		Lib_Error.ifArgs(argIdx + 1, 1, this.call.argCalls.length, this, o);

		I_Object argTemp = Nil.NIL;

		final Call ac = this.call.argCalls[argIdx];

//		if(pc.object instanceof VarLet)
//			throw new CodeError(this, "Invalid use of VarLet!", "This function doesn't accept a VarLet.");
//
//		if(pc.object instanceof FuncLet) {
//			final FuncLet fl = (FuncLet)pc.object;
//
//			final Call[] par_calls = new Call[eachIt.length];
//			for(int i = 0; i < eachIt.length; i++)
//				par_calls[i] = new Call(this.getSurrBlock(), eachIt[i], this.getDebugInfo());
//
//			final Call c = new Call(this.getSurrBlock(), null, null, par_calls, this.getDebugInfo());
//			final CallRuntime cpFL = this.copyCall(c, true);
//
//			parTemp = fl.exec(cpFL);
//		}
//		else
		argTemp = this.copyCall(ac, false).exec(o, eachItObj, (byte)-1, true).o1;

//		this.par_result = new I_Object[]{parTemp};
		this.args_result = null;
		return argTemp;
	}

	public I_Object[] getArgs(final I_Object streamIt) {
		return this.getArgs(streamIt, true);
	}

	/**
	 * @implNote
	 *           Execute Argument-Calls
	 */
	public I_Object[] getArgs(final I_Object streamIt, final boolean buffer) {
		if(buffer && this.args_result != null)
			return this.args_result;

		final int len = this.call.argCalls == null
			? 0
			: this.call.argCalls.length;

//		This is possible, but not worth the effort!
//		if(noPars && len > 0) // Any other combination of Parameters and Arguments could be possible because of defaultValue and varArgs
//			throw new CodeError(this, "Invalid amount of arguments", "Expected 0, but got "+len+" argument(s).");

		final I_Object[] argsTemp = new I_Object[len];

		if(len > 0) {
			final I_Object o = this.call.object == null || this.call.object instanceof I_ObjectReplace
				? streamIt
				: this.call.object;

			for(int pp = 0; pp < len; pp++) {
				final Call arg = this.call.argCalls[pp];
				final CallRuntime cpArg = new CallRuntime(this, this.instance, arg, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
				argsTemp[pp] = cpArg.exec(o, true);
			}
		}
		this.args_result = argsTemp; // Using argsTemp is better for async.
		return this.args_result;
	}

	/**
	 * @implNote
	 *           Always allow: _EACH, FuncLet, VarLet + _EACH
	 *           Executes only Argument-Calls from offset!
	 */
	public I_Object getArgsEach(final I_Object streamIt, final int offset, final I_Object[] eachIt) {
		if(this.args_result != null)
			return this.args_result[0];

		Err.ifTooSmall(1, eachIt.length);
		final I_Object eachItObj = eachIt.length == 1 ? eachIt[0] : new JMo_List(eachIt);

		final int len = this.call.argCalls == null
			? 0
			: this.call.argCalls.length;

		final I_Object o = this.call.object != null
			? this.call.object
			: streamIt;

		Lib_Error.ifArgs(len, offset + 1, offset + 2, this, o);

		final int fLen = len - offset;
		Err.ifOutOfBounds(1, 2, fLen);
		I_Object argTemp = Nil.NIL;

		if(fLen == 2) {
			// --- Arg. 1 --- VarLet
			final I_Object arg1 = this.call.argCalls[offset].object;

//			if(this.getStrict().isValid_AutoVarLet() && arg1 instanceof Var)
//				arg1 = new VarLet((Var)arg1); // Auto-VarLet

			if(!(arg1 instanceof VarLet))
				throw new CodeError(this, "Invalid argument!", "Argument " + (offset + 1) + " isn't a VarLet!");

			final VarLet vl = (VarLet)arg1;
			vl.getMem().let(this, this, eachItObj);

			// --- Arg. 2 --- Proc
			final Call pc = this.call.argCalls[offset + 1];
			argTemp = this.copyCall(pc, false).exec(o, eachItObj, (byte)-1, true).o1; // eachItObj? Okay, its possible
		}
		else { // fLen == 1
			final Call pc = this.call.argCalls[offset];

			if(pc.object instanceof VarLet)
				throw new CodeError(this, "Invalid use of VarLet!", "The test is missing, which belongs to the VarLet.");

			if(pc.object instanceof FuncLet) {
				final FuncLet fl = (FuncLet)pc.object;

				final Call[] argCalls = new Call[eachIt.length];
				for(int i = 0; i < eachIt.length; i++)
					argCalls[i] = new Call(this, eachIt[i]);

				final Call c = new Call(this.getSurrBlock(), null, null, argCalls, this.getDebugInfo());
				final CallRuntime cpFL = this.copyCall(c, true);

				argTemp = fl.exec(cpFL);
			}
			else
				argTemp = this.copyCall(pc, false).exec(o, eachItObj, (byte)-1, true).o1;
		}
		argTemp = Lib_Convert.getValue(this, argTemp);
		if(argTemp instanceof FuncLet)
			argTemp = ((FuncLet)argTemp).eachExec(this, eachItObj);

		this.args_result = new I_Object[]{argTemp};
		return argTemp;
	}


	// --- Exec ---

	public Block getCallBlock() {
		return this.call.getBlock();
	}

	/**
	 * @apiNote This will be changed with every execution. It's only for debug info.
	 */
	public I_Object getCurrentObject() {
		return this.curObject;
	}

	public DebugInfo getDebugInfo() {
		return this.call.debugInfo;
	}

	public Call getStream() {
		return this.call.getStream();
	}

	public StrictManager getStrict() {
		return this.instance.getApp().strict;
	}

	public Block getSurrBlock() {
		return this.call.surrounding;
	}

	public Type getType() {
		return this.instance.getType();
	}

	public boolean hasArguments() {
		return this.call.getArgCount() > 0;
	}

	public boolean hasBlock() {
		return this.call.hasBlock();
	}

	public boolean hasStream() {
		return this.call.hasStream();
	}

	/** Type check for instance argument **/
	public I_Object instanceArgType(final int argIdx, final I_Object arg, final Class<?> type) {
		return Lib_Arguments.checkArg(this, arg, type, argIdx, null, true);
	}

	/**
	 * Type check for instance argument
	 *
	 * @apiNote Use this only if it's really sensefull!
	 **/
	public I_Object instanceArgTypeExt(final int argIdx, final I_Object arg, final Class<?>... types) {
		return Lib_Arguments.checkArgExt(this, arg, types, argIdx, null, true);
	}

	@Override
	public String toString() {
		final String sc = this.call == null
			? "null"
			: this.call.toString();
		return "" + sc + " | " + this.instance.toString() + " | " + this.vce.toString();
	}

	public void warning(final String message, final String detail) {
		final RuntimeWarning t = new RuntimeWarning(this, message, detail);
		this.getApp().warning(this, t);
	}

	// --- Finals ---

	/**
	 * Returns: Group2<Result, NextCall>
	 */
	private Group2<I_Object, Call> exec(final I_Object streamIt, final I_Object eachIt, final byte leftprio, final boolean isArgument) {
		Err.ifNull(this.call);

		if(this.getApp().toBeTerminated())
//			return new Group2<>(new Return(Return.LEVEL.EXIT, Nil.NIL), null);
			return new Group2<>(Nil.NIL, null);

		try {
			Group2<I_Object, Call> res = null;
			final I_Object objNew = this.call.object;

			I_Object with = objNew != null
				? objNew
				: streamIt;

			// Short return for Atomic's
			if(this.call.method == null && with instanceof A_Atomic && !this.call.hasBlock()) { // && this.call.getStream() == null
				if(this.call.hasStream())
					throw Err.invalid(this.call);
				return new Group2<>(with, null);
			}
			if(with instanceof I_MagicReplace)
				with = Lib_MagicVar.replace(with, this, streamIt, eachIt);

			if(with == null)
				if(this.call.object == null)
					throw Err.invalid("Call without Object!", this.call.toString(this, STYPE.IDENT), this.getDebugInfo());
				else
					throw new CodeError(this, "Missing Object, nothing to work with", "Invalid use of MagicConst: " + this.call.object.toString(this, STYPE.IDENT));

			// Non-Atomic Objects
			if(with instanceof NonAtomic)
				with = ((NonAtomic)with).create(this);

			// Initialize new object
			with.initOnce(this);

			// Check, if has a Loop-Arg
			final boolean multicall = this.iHasMulticall(this.call.argCalls);

			// Execute the Object
			if(!multicall)
				res = this.execObject(with, leftprio, isArgument);
			else {
				final Call[] args = this.call.argCalls;
				final int aLen = args.length;
				final I_Object o = this.call.object != null
					? this.call.object
					: with;

				// --- Argument auswerten
				final I_Object[] argsResult = new I_Object[aLen];

				for(int pp = 0; pp < args.length; pp++) {
					final Call arg = args[pp];
					if(arg.object instanceof MultiCall)
						((MultiCall)arg.object).setStream(o);

					final CallRuntime cpArg = new CallRuntime(this, this.instance, arg, this.vce, NEW_VCE.KEEP_PARENT, this.funcOutside);
					argsResult[pp] = cpArg.exec(o, isArgument);
				}
				res = new Group2<>(this.iExecMultiCall(with, 0, argsResult), null);
			}
			// Hints generieren
//			if(this.getApp().genHints) {
//				final HintManager hints = this.getApp().getHintManager();
//				final String met = this.call.getMethod();
//				if(objNew == null)
//					MOut.print("Object is null: " + met);
//				else if(met != null && objNew.getClass() != Instance.class) {
//					hints.addType(objNew.getTypeName(), objNew.getTypes());
//					if(!this.call.getSurrounding().gType().getFunctions().knows(met))
//						hints.addCall(this, res.getTypeName(), met, this.parResultGet(this));
//				}
//			}

			return res;
		}
		catch(final ErrorBase t) {
			throw t;
		}
		catch(final Err_Runtime t) { // Convert MN77-Runtime-Errors to JayMo-Runtime-Errors
			if(this.instance.getApp().isDebug()) // show raw error
				MOut.error(t);
			final String ste = this.stackTraceElementToString(t.getStackTrace()[1]);
			throw new RuntimeError(this, t.getMessage() + ste, ConvertSequence.toString(", ", t.getDetails()));
		}
		catch(final Throwable t) { // Convert all other errors to JayMo-Errors
			// Catching: NoClassDefFoundError, ClassNotFoundException, ...
			if(this.instance.getApp().isDebug()) // show raw error
				MOut.error(t);

			String msg = null;

			try {
				final StackTraceElement[] stea = t.getStackTrace(); // This is maybe not available at StackOverflow
				final String ste0 = this.stackTraceElementToString(stea[0]);
				final String ste1 = this.stackTraceElementToString(stea[1]);
				msg = t.getClass().getSimpleName() + " " + ste0 + ", " + ste1;
			}
			catch(final Throwable tx) {
				msg = t.getClass().getSimpleName();
			}
			throw new JayMoError(t, this, msg, t.getMessage());
		}
	}

	private I_Object iExecMultiCall(final I_Object with, final int aPos, final I_Object[] current) {
		// --- Prepare
		final int len = current.length;

		if(aPos == len) { // if ppos points after end of current
			this.args_result = current; // Overwrite a already set value is for MultiCall allowed!
			final Group2<I_Object, Call> gr = this.execObject(with, (byte)-1, true);
			return gr.o1;
		}
		final I_Object[] current2 = new I_Object[len];
		System.arraycopy(current, 0, current2, 0, len);

		// --- Set new Args
		if(current[aPos] instanceof MultiCall) {
			I_Object result = with;
			final MultiCall curMC = (MultiCall)current[aPos];
			curMC.exec(this, this.getSurrBlock(), with);

			curMC.resetPointer();

			while(curMC.hasNext()) {
				current2[aPos] = curMC.getNext();
				result = this.iExecMultiCall(with, aPos + 1, current2);

				// Update Var - no, there is no assignment!
//				if(with instanceof Var)
//					((Var)with).let(this, this, result);
			}
			return result;
		}
		else
			return this.iExecMultiCall(with, aPos + 1, current2);
	}

	private boolean iHasMulticall(final Call[] calls) {
		if(calls != null)
			for(final Call c : calls)
				if(c.object instanceof MultiCall)
					return true;
		return false;
	}

	private String stackTraceElementToString(final StackTraceElement st) {
		String ste = st.getFileName() == null ? "" : st.getFileName() + ":" + st.getLineNumber();
		if(ste.length() > 0)
			ste = '(' + ste + ')';
		return ste;
	}

}
