/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.runtime;

import java.util.HashMap;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.passthrough.Const;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Java;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 08.12.2022
 *
 * @apiNote
 *          Base implementation for VarEnv and ConstEnv
 *
 * @implNote Store var as names, because of Hot-Execution (Parse to new Var-Object with same name)
 */
public abstract class A_MemEnv {

	private final A_MemEnv                            parent;
	private HashMap<String, Group2<String, I_Object>> map = null; // Key --> Type, Value


	public A_MemEnv(final A_MemEnv parent) {
		this.parent = parent;
	}


//	public void describe(final int left) {
//		this.init();
//		final String space = Lib_Parser.space(left);
//		MOut.print(space + this.toString());
//
//		for(final Entry<String, Group2<String, I_Object>> g : this.map.entrySet())
//			MOut.print(space + "  " + g.getKey() + " --> " + g.getValue().o2.toString());
//
//		MOut.print(space + "Parent:");
//		if(this.parent == null)
//			MOut.print(space + "  nil");
//		else
//			this.parent.describe(left + 1);
//	}

	public final void register(final CallRuntime cr, final I_Mem mem) {
		if(this.knows(mem, mem.getName()))
			return;

//		this.iInit();	// Already done with 'knows'

//		final Group2<String, I_Object> g = this.map.getOrDefault(var.getName(), null);	// Sinnvoll ?!?
//		if(g != null)
//			Err.invalid("Type already set!");

		this.map.put(mem.getName(), new Group2<>(null, null));	// TODO prüfen, dass null, null keine Probleme verursacht!
	}

//	public String getType(final I_Mem vc) {
//		this.iInit();
//		final Group2<String, I_Object> g = this.map == null
//			? null
//			: this.map.getOrDefault(vc.getName(), null);
//
//		if(g != null)
//			return g.o1;
//
//		if(this.parent != null)
//			return this.parent.getType(vc);
//
//		return null;
//	}

	public final void remove(final Var v) {
		if(this.map == null)
			return;
		this.map.remove(v.getName());
	}

	public final void setType(final CallRuntime cr, final I_Mem vc, final String type) {
		if(this.knows(vc, vc.getName()))
			return;

		if(!ObjectManager.isTypeKnown(cr, type))
			throw new CodeError(cr, "Unknown type name", "Got: '" + type + "'");

		this.iInit();
		final Group2<String, I_Object> g = this.map.getOrDefault(vc, null);
		if(g != null)
			Err.invalid("Type already set!");
		this.map.put(vc.getName(), new Group2<>(type, null));
	}

	@Override
	public final String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()); // TODO VarEnv, ConstEnv
		sb.append("<");
		sb.append(this.map == null
			? "0"
			: "" + this.map.size());
		sb.append(">(");
		sb.append(this.map == null
			? ""
			: ConvertSequence.toString(",", this.map.keySet()));
		sb.append(')');
		if(this.parent != null)
			sb.append(" --> Parent: " + this.parent);
		return sb.toString();
	}

	/**
	 * @return Returns the stored value or null if it is not initialized;
	 */
	protected I_Object get(final CallRuntime cr, final I_Mem vc, final String name) {
		this.iInit();

		// Stored here
		if(this.map.containsKey(name))
			return this.map.get(name).o2;

		// Maybe in parent
		if(this.parent != null && this.parent.knows(vc, name))
			return this.parent.get(cr, vc, name);

		throw Lib_Error.memNotInitialized(cr, vc);
	}

	/**
	 * @return Returns true, if the var/const has a defined value.
	 */
	protected boolean isInitialized(final I_Mem vc, final String name) {
//		return this.knows(vc);
		this.iInit();

		final Group2<String, I_Object> g = this.map.getOrDefault(name, null);
		if(g == null)
			return this.parent != null
				? this.parent.isInitialized(vc, name)
				: false;

		return g.o2 != null;
	}

	protected boolean knows(final I_Mem vc, final String name) {
		this.iInit();
		return this.map.containsKey(name)
			? true
			: this.parent != null
				? this.parent.knows(vc, name)
				: false;
	}

	/**
	 * With Type-Check!
	 */
	protected void set(final CallRuntime cr, final I_Mem vc, I_Object obj, final boolean nilable, final boolean convert, final String vcName) {	// TODO Sort parameters
		this.iInit();

		if(obj == null)
			throw new JayMoError(cr, "No Object to set", vcName);

		// If unknown, check parent
		if(this.parent != null && !this.map.containsKey(vcName) && this.parent.knows(vc, vcName)) {
			this.parent.set(cr, vc, obj, nilable, convert, vcName);
			return;
		}
		// Get stored Type
		String objType = Lib_Type.getName(obj.getClass(), obj);
		String safedType = null;
		final boolean isKnown = this.map.containsKey(vcName);
		if(isKnown)
			safedType = this.map.get(vcName).o1;

		// Get Value from Vars/Consts/...
		obj = Lib_Convert.getValue(cr, obj); // Necessary because of VarLets

		// Check, Const already set
		if(vc instanceof Const) {
			final Group2<String, I_Object> mem = this.map.get(vcName);
			if(mem != null && mem.o2 != null)
				throw new RuntimeError(cr, "Constant value is already set and cannot be changed.", vcName + "=" + obj.toString());
//				cr.warning("Constant value is already set and cannot be changed.", vcName + "=" + obj.toString());
//				return;
		}
		// Nil-Check
		final boolean isNil = obj == Nil.NIL;

		if(!nilable && isNil)
			throw new RuntimeError(cr, "Invalid value for " + vc.getMemTypeString(),
				"'" + vcName + " = " + obj.toString() + "' is not valid, maybe try '" + vcName + "? = " + Nil.NIL.toString() + "'");

		// Type-Check
		if(obj != null)
			if(safedType == null) {
//				if(typesafe && !objType.equals("Nil"))
				if(!isNil)
					safedType = objType;
//				if(safedType == MC_THIS.class)
//					throw Err.forbidden("Nicht typsicher!", safedType, obj, objType);
			}
			else {
				//if(isKnown) {

				if(convert) { // Ist beides safedType ... somit nur 1x prüfen!
					obj = Lib_Convert.memConvert(cr, safedType, objType, obj);
					objType = Lib_Type.getName(obj);
				}
				// Effective type check:
				if(!isNil)
					obj = Lib_Type.typeCheck(cr, obj, objType, safedType, vc.getMemTypeString() + ": " + vcName);
			}

		// For Java-Objects, replace Simple with Full type:
		// Java_ArrayList --> Java_{java.util.ArrayList}
		if(obj instanceof JMo_Java && Lib_Java.isSimpleTypeName(safedType))
			safedType = ((JMo_Java)obj).getFullTypeName();

		// Set
		this.map.put(vcName, new Group2<>(safedType, obj));
	}

	private void iInit() {
		if(this.map == null)
			this.map = new HashMap<>();
	}


//	public Group2<String, I_Object> getToCopy(final String name, final DebugInfo debug) {
//		if(name.equals(Var.IT))
//			Err.invalid(name); // TO DO!
//
//		this.init();
//
//		// Stored here
//		if(this.map.containsKey(name))
//			return this.map.get(name);
//
//		// Maybe in parent
//		if(this.parent != null) // && this.parent.knows(vc)
//			return this.parent.getToCopy(name, debug);
//
//		throw new CodeError("Access to uninitialized " + this.getTypeString(), name, debug);
//	}

//	public void setCopy(final String name, final Group2<String, I_Object> g) {
//		this.init();
//		this.map.put(name, g);
//	}

//	public VarEnv freezeCopy() {
//		final VarEnv copy = new VarEnv(null, this.isConst);
//		final HashMap<String, Group2<String, I_Object>> map = new HashMap<>();
//		this.iDeepFreezeCopy(map, this);
//		copy.map = map;
//		return copy;
//	}

//	private void iDeepFreezeCopy(final HashMap<String, Group2<String, I_Object>> map, final VarEnv ve) {
//		if(ve.map != null)
//			for(final String name : ve.map.keySet())
//				if(!map.containsKey(name))
//					//					MOut.line(name + " / "+ ve.list.get(name).o1 +" / "+ ve.list.get(name).o2);
//					map.put(name, ve.map.get(name));
//		if(ve.parent != null)
//			this.iDeepFreezeCopy(map, ve.parent);
//	}

}
