/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang;

import java.io.File;
import java.io.InputStream;

import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.file.Lib_Jar;
import de.mn77.base.version.DEVSTAGE;
import de.mn77.base.version.data.I_VersionData;
import de.mn77.base.version.data.VersionData_ABC;
import de.mn77.base.version.data.VersionData_Date;


/**
 * @author Michael Nitsche
 * @created 20.06.2021
 */
public class JayMo {

	/*
	 * Upon reaching Version 1.0, maybe go to monthly Version-Numbers and release like: 2019-09
	 * Or quarterly with 2019-1 to 2019-4
	 * Or 2020,4 und 2020,4,1 (year, month, release)
	 * Or year, major, minor: 2020,5,2
	 */
	private static final VersionData_ABC  VERSION      = new VersionData_ABC(0, 22, 0); // Major, Minor, Fix
	private static final VersionData_Date VERSION_BETA = new VersionData_Date(DEVSTAGE.BETA);
	private static final VersionData_Date VERSION_SID  = new VersionData_Date(DEVSTAGE.SID);
	public static final DEVSTAGE          STAGE        = DEVSTAGE.RELEASE; // RELEASE, BETA, SID

	public static final String PACKAGE_DOMAIN_BASE = "org.jaymo_lang";
	public static final String JAR_PATH_INFO       = "/org/jaymo_lang/info";

	public static final String NAME = "JayMo";

	public static final String COPYRIGHT_YEAR = "2017-2022";
	public static final String COPYRIGHT_NAME = "Michael Nitsche (DE, Übersee)";

	public static final String WEB_HOME_FULL  = "https://www.jaymo-lang.org";
	public static final String WEB_HOME_SHORT = "www.jaymo-lang.org";

	public static final int NESTED_MAX = 40; // Max string length for nested objects

	//#################################################################


	public static String exec(final String jaymo) {
		return JayMo.exec(jaymo, null);
	}

	public static String exec(final String jaymo, final String[] args) {
		final Parser_App parser = new Parser_App();
		final App app = parser.parseText(jaymo);
		return app.exec(args);
	}

	public static String execFile(final File f) throws Exception {
		return JayMo.execFile(f, null);
	}

	public static String execFile(final File f, final String[] args) throws Exception {
		final Parser_App parser = new Parser_App();
		final App app = parser.parseFile(f);
		return app.exec(args);
	}

	public static String execJarFile(final String path) throws Exception {
		return JayMo.execJarFile(path, null);
	}

	public static String execJarFile(final String jarPath, final String[] args) throws Exception {
		final InputStream is = Lib_Jar.getStream(jarPath);
		final String jaymo = Lib_Stream.readUTF8(is);

		final String filePath = Lib_Jar.getAbsolutePath(jarPath);
		final File file = new File(filePath);

		final Parser_App parser = new Parser_App();
		final App app = parser.parseText(file.getParent(), file.getName(), jaymo);
		return app.exec(args);
	}

	public static Parser_App getParser() {
		return new Parser_App();
	}

	public static App parseFile(final File file) throws Exception {
		final Parser_App parser = new Parser_App();
		return parser.parseFile(file);
	}

	public static App parseText(final String jaymo) {
		final Parser_App parser = new Parser_App();
		return parser.parseText(jaymo);
	}

	public static I_VersionData version() {
		return JayMo.STAGE == DEVSTAGE.RELEASE
			? JayMo.VERSION
			: JayMo.STAGE == DEVSTAGE.BETA
				? JayMo.VERSION_BETA
				: JayMo.VERSION_SID;
	}

}
