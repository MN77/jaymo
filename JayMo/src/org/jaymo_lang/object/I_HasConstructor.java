/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 07.07.2020
 * @apiNote
 *          Marks an JMo_Object, that has an constructor. This will be executed also if it stands alone
 */
public interface I_HasConstructor extends I_Object {

	/**
	 * @return The return value can only be a 'Return' or 'null'.
	 */
	Return constructor(final CallRuntime cr);

}
