/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.test;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_HasConstructor;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Output;


/**
 * @author Michael Nitsche
 * @created 10.09.2021
 */
public class JMo_ArgumentTestDummy extends A_Object implements I_ControlObject, I_HasConstructor {

	final ArgCallBuffer[] args;


	public JMo_ArgumentTestDummy(final Call a1, final Call a2, final Call a3, final Call a4, final Call a5, final Call a6, final Call a7, final Call a8, final Call a9, final Call a10) {
		this.args = new ArgCallBuffer[]{
			new ArgCallBuffer(0, a1),
			new ArgCallBuffer(1, a2),
			new ArgCallBuffer(2, a3),
			new ArgCallBuffer(3, a4),
			new ArgCallBuffer(4, a5),
			new ArgCallBuffer(5, a6),
			new ArgCallBuffer(6, a7),
			new ArgCallBuffer(7, a8),
			new ArgCallBuffer(8, a9),
			new ArgCallBuffer(9, a10),
		};
	}

	public Return constructor(final CallRuntime cr) {
		final StringBuilder sb = new StringBuilder();

		for(final ArgCallBuffer arg : this.args) {
			sb.append(Lib_Convert.getValue(cr, arg.get()).toString(cr, STYPE.DESCRIBE));
			sb.append(',');
		}
		sb.deleteCharAt(sb.length() - 1);
		Lib_Output.out(cr.getApp(), sb.toString(), true);
		return null;
	}

	@Override
	public void init(final CallRuntime cr) {
		this.args[0].init(cr, this, Nil.class);
		this.args[1].init(cr, this, Bool.class);
		this.args[2].init(cr, this, Int.class);
		this.args[3].init(cr, this, JMo_Long.class);
		this.args[4].init(cr, this, JMo_Dec.class);
		this.args[5].init(cr, this, JMo_Double.class);
		this.args[6].init(cr, this, Char.class);
		this.args[7].init(cr, this, Str.class);
		this.args[8].init(cr, this, I_Atomic.class);
		this.args[9].init(cr, this, A_Object.class);
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "testNil":
				return A_Object.stdResult(cr.args(this, Nil.class)[0]);
			case "testBool":
				return A_Object.stdResult(cr.args(this, Bool.class)[0]);
			case "testByte":
				return A_Object.stdResult(cr.args(this, JMo_Byte.class)[0]);
			case "testShort":
				return A_Object.stdResult(cr.args(this, JMo_Short.class)[0]);
			case "testInt":
				return A_Object.stdResult(cr.args(this, Int.class)[0]);
			case "testLong":
				return A_Object.stdResult(cr.args(this, JMo_Long.class)[0]);
			case "testFloat":
				return A_Object.stdResult(cr.args(this, JMo_Float.class)[0]);
			case "testDec":
				return A_Object.stdResult(cr.args(this, JMo_Dec.class)[0]);
			case "testDouble":
				return A_Object.stdResult(cr.args(this, JMo_Double.class)[0]);
			case "testChar":
				return A_Object.stdResult(cr.args(this, Char.class)[0]);
			case "testStr":
				return A_Object.stdResult(cr.args(this, Str.class)[0]);
			case "testObject":
				return A_Object.stdResult(cr.args(this, I_Object.class)[0]);
			case "testList":
				return A_Object.stdResult(cr.args(this, JMo_List.class)[0]);
		}
		return null;
	}

}
