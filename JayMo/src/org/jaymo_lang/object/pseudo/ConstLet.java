/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.passthrough.Const;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 14.04.2021
 */
public class ConstLet extends A_MemLet {

	private final Const con;


	public ConstLet(final Const con) {
		this.con = con;
	}

	@Override
	public Const getMem() {
		return this.con;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
//		return ":" + this.var.toString();
		return "ConstLet[" + this.con.getName() + "]";
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {
		return null;
	}

}
