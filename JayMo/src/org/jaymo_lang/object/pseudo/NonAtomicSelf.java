/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 10.03.2020
 *
 * @apiNote
 *          For a instance of an self created type
 */
public class NonAtomicSelf extends NonAtomic {

	private final Type   type;
	private final Call[] args;


	public NonAtomicSelf(final Type type, final Call[] args) {
		super(Instance.class, null);
		this.type = type;
		this.args = args;
	}

	@Override
	public I_Object create(final CallRuntime cr) {
		return new Instance(cr, cr.getApp(), this.type, this.args);
	}

//	@Override
//	public String getTypeName() {
//		return this.type.getName();
//	}

//	@Override
//	public String toString() {
//		return this.type.getName();
//	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.type.getName();
//		return this.getTypeName();
	}

}
