/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 18.10.2020
 *
 * @apiNote
 *          Creates a individual instance for a Java object
 * @implNote
 *           This type is necessary, to handle and separate different Java-Objects
 */
public class NonAtomicJava extends NonAtomic {

	private final Call[] args;


	public NonAtomicJava(final Call[] args) {
		super(JMo_Java.class, null);
		this.args = args;
	}

	@Override
	public I_Object create(final CallRuntime cr) {
		return new JMo_Java(this.args, cr.getDebugInfo());
	}

	/**
	 * @return Prefix and Classname: "Java_Class"
	 */
	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.toString());

		if(this.args != null && this.args.length >= 1) {
			sb.append("(");
			sb.append(this.args[0].toString(cr, STYPE.IDENT));
			sb.append(")");
		}
		return sb.toString();
	}

}
