/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

import org.jaymo_lang.api.JMo_Object;
import org.jaymo_lang.api.SandBox;
import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.JMo_Enum;
import org.jaymo_lang.object.immute.JMo_Type;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 12.03.2019
 */
public class NonAtomic implements I_Object {

	private final Class<? extends I_Object>   c;
	private final Class<? extends JMo_Object> c2;
	private final Call[]                      args;
	private final String                      name;


	public NonAtomic(final Class<? extends I_Object> type, final Call[] args) { // , DebugInfo debugInfo
		this.c = type;
		this.c2 = null;
		this.args = args;
		this.name = Lib_Type.getName(type, null);
	}

	@SuppressWarnings("unchecked")
	public NonAtomic(final Class<SandBox> type1, final Class<?> type2, final Call[] args) { //, DebugInfo debugInfo
		this.c = type1;
		this.c2 = (Class<? extends JMo_Object>)type2;
		this.args = args;
		this.name = Lib_Type.getName(type2, null);
	}

	public ObjectCallResult call(final CallRuntime cr) {
		throw Err.forbidden();
	}

	public int compareTo(final CallRuntime cr, final I_Object o) {
		throw Err.forbidden();
	}

	public int compareTo(final I_Object o) {
		throw Err.forbidden();
	}

	public I_Object create(final CallRuntime cr) {
		// ----- Sandbox -----

		if(this.c2 != null)
			//			Constructor<? extends I_Object> ca = c.getConstructor(Class.class, Call[].class);
//			JMo_Sandbox sandbox = (JMo_Sandbox)ca.newInstance(args);
			return new SandBox(cr.getApp(), this.c2, this.args);

		// ----- Without Arguments -----

		if(this.args == null || this.args.length == 0)
			try {
				return this.c.newInstance();
			}
			catch(final InstantiationException e1) {
				if(Modifier.isAbstract(this.c.getModifiers()))
					throw new CodeError(cr, "Can't create object", "This type cannot be initiated: <" + this.name + '>'); // is abstract
				else
					throw new CodeError(cr, "Can't create object", "Maybe missing arguments for type: <" + this.name + '>');
			}
			catch(final IllegalAccessException e2) {
				Err.exit(e2);
			}

		// ----- With Arguments -----

		@SuppressWarnings("unchecked")
		final Class<? extends Call>[] partypes = new Class[this.args.length];
		for(int i = 0; i < this.args.length; i++)
			partypes[i] = this.args[i] != null
				? this.args[i].getClass()
				: Call.class;

//		DEBUG
//		MOut.print(c.getName());
//		for(Constructor<?> con : c.getConstructors())
//			MOut.print(con);
//		MOut.print((Object)partypes);
//		DEBUG

		try {
			final Constructor<? extends I_Object> ca = this.c.getConstructor(partypes);
			return ca.newInstance((Object[])this.args);
		}
		catch(final NoSuchMethodException e1) {

			// --- Try it with a Call[] ---
			try {
				final Constructor<? extends I_Object> ca = this.c.getConstructor(Call[].class);
				return ca.newInstance((Object)this.args);
			}
			catch(final NoSuchMethodException e2) {
				final String info = this.name + "(" + ConvertObject.toTextDebug(this.args) + ")";
				if(!Lib_Type.getName(this.c, null).equals(this.name))
					throw Err.invalid("Invalid Class: " + info + ", Object: " + this.c.getSimpleName() + ", Name: " + this.name);

				final SimpleList<String> alc = new SimpleList<>();

				final Constructor<?>[] cons = this.c.getConstructors();

				for(final Constructor<?> con : cons) {
					final Parameter[] args = con.getParameters();
					if(args.length > 0 && args[0].getType() != Call.class)
						continue;

					final SimpleList<String> alp = new SimpleList<>(args.length);
					for(final Parameter arg : args)
						alp.add(arg.getName());

					final String sArgs = ConvertSequence.toString(", ", alp);
					alc.add(this.name + "(" + sArgs + ")");
				}
				final String sCons = ConvertSequence.toString(" | ", alc);
				throw new CodeError(cr, "Invalid arguments for type", "Got " + this.args.length + " argument(s) for \"" + this.name + "\". Possible: " + sCons);
			}
			catch(SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// Err.show(e.getCause(), false);
				throw new JayMoError("Object instantiation exception", e.getClass().getSimpleName() + " | " + e.getMessage() + " | " + this.c.getCanonicalName(), cr.getDebugInfo());
			}
		}
		catch(final InstantiationException e) {
			throw new JayMoError("Object instantiation exception", "Is this an abstract class? " + this.c.getCanonicalName(), cr.getDebugInfo());
		}
		catch(SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
			throw new JayMoError("Object instantiation exception", e2.getClass().getSimpleName() + " | " + e2.getMessage() + " | " + this.c.getCanonicalName(), cr.getDebugInfo());
		}
	}

	public boolean equalsLazy(final Object obj) {
		throw Err.forbidden();
	}

	public boolean equalsStrict(final Object obj) {
		throw Err.forbidden();
	}

	public JMo_Enum getConstant(final CallRuntime cr, final String name) {
		throw Err.forbidden();
	}

	/** Only for Converter **/
	public Call[] getInternalPar() {
		return this.args;
	}

	public JMo_Type getType(final CallRuntime cr) {
		return new JMo_Type(this.name, cr);
	}

	public String getTypeName() {
//		throw Err.forbidden(this.name);
		return this.name; //TODO correct?!? vmtl. schon.
	}

	public String[] getTypeNames() {
		throw Err.forbidden();
	}

	public void initOnce(final CallRuntime cr) {}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
//		return "NonAtomic(" + this.name + ")";
		return this.name;
	}

}
