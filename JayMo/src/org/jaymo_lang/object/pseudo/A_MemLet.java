/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 02.01.2022
 */
public abstract class A_MemLet extends A_Object {

	public void checkRegistered(final CallRuntime cr) {
		final I_Mem mem = this.getMem();
		if(!mem.isInitialized(cr))
			if(mem instanceof Var)
				cr.vce.vars.register(cr, mem);
			else
				cr.vce.cons.register(cr, mem);
	}

	public abstract I_Mem getMem();

	@Override
	public final void init(final CallRuntime cr) {
		this.checkRegistered(cr);
	}

	@Override
	protected final ObjectCallResult call2(final CallRuntime cr, final String method) {

		/**
		 * °name()Str # Returns the name of the variable/constant
		 */
		switch(method) {
			case "get":
				return this.mGet(cr);

			case "let":
			case "set":
				return this.mSet(cr, false);
			case "convertLet": // Like "let", but converts the value to the var-type
//			case "convertSet": // Like "let", but converts the value to the var-type
			case "setLazy":
				return this.mSet(cr, true);

			case "isSet":
				return this.mIsSet(cr);
			case "isVar":
//			case "isVariable":
				return this.mIsType(cr, false);
			case "isConst":
//			case "isConstant":
				return this.mIsType(cr, true);

			case "name":
				cr.argsNone();
				return A_Object.stdResult(new Str(this.getMem().getName()));
		}
		return this.call3(cr, method);
	}

	protected abstract ObjectCallResult call3(CallRuntime cr, String method);

	/**
	 * °isSet()Bool # Returns true, if the variable or constant has a defined value.
	 */
	protected ObjectCallResult mIsSet(final CallRuntime cr) {
		cr.argsNone();
		final boolean b = this.getMem().isInitialized(cr);
		return A_Object.stdResult(Bool.getObject(b));
	}

	/**
	 * °isVar()Bool # Returns true, if this is a VarLet.
	 * °isConst()Bool # Returns true, if this is a ConstLet.
	 */
	protected ObjectCallResult mIsType(final CallRuntime cr, final boolean isConst) {
		cr.argsNone();
		boolean b = this instanceof VarLet;
		if(isConst)
			b = !b;
		return A_Object.stdResult(Bool.getObject(b));
	}

	/**
	 * °let ^ set
	 * °convertLet ^ setLazy
	 * °set(Object o)Same # Assign object 'o' to the variable or constant
	 * °setLazy(Object o)Same # Convert and assign object 'o' to the variable or constant
	 */
	protected ObjectCallResult mSet(final CallRuntime cr, final boolean convert) {
		final I_Object arg = cr.args(this, I_Object.class)[0];
		final CallRuntime crNew = cr.copyVCE(cr.instance, cr.vce, false);
		this.getMem().let(cr, crNew, arg, convert);
		return A_Object.stdResult(this);
	}

	/**
	 * °get()Object # Return the value of the object.
	 */
	private ObjectCallResult mGet(final CallRuntime cr) {
		cr.argsNone();
		final I_Object o = this.getMem().get(cr);
		return A_Object.stdResult(o);
	}

}
