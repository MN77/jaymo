/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.error.CustomError;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ErrorComposer;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 14.04.2018
 */
public class JMo_Error extends A_Object {

	private enum EBD {
		SOURCE,
		CALL,
		DETAIL,
		INSTANCE,
		GROUP
	}


	private final Throwable     ex;
	private final ArgCallBuffer par1;
	private final ArgCallBuffer par2;


	/**
	 * +Error(String message, String detail)
	 * ### %.throw
	 */
	public JMo_Error(final Call par1, final Call par2) {
		this.ex = new Throwable();
		this.par1 = new ArgCallBuffer(0, par1);
		this.par2 = new ArgCallBuffer(1, par2);
	}

	public JMo_Error(final Throwable t) {
		this.ex = t;
		this.par1 = null;
		this.par2 = null;
	}

	@Override
	public void init(final CallRuntime cr) {
		if(this.par1 != null)
			this.par1.init(cr, this, Str.class);
		if(this.par2 != null)
			this.par2.init(cr, this, Str.class);
	}

	/**
	 * °show()Same # Print all informations of this error
	 */
	public I_Object mShow(final CallRuntime cr) {
		cr.argsNone();
		final String s = new ErrorComposer(this.ex, cr, this.iGetMessage()).compose();
		Lib_Output.out(cr.getApp(), s, true);
		return this;
	}

	@Override
	public String toString() {
		return this.toString(null, STYPE.REGULAR);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case NESTED:
			case IDENT:

//				return "Error<" + group + '>';
				return this.par1 != null && this.par2 != null ? CustomError.class.getSimpleName() : this.ex.getClass().getSimpleName();
			case REGULAR:
				if(this.par1 != null && this.par2 != null)
					if(cr == null)
						return "Error[" + this.par1.toString() + "," + this.par2.toString() + ']';
					else
						return "Error[" + this.par1.toString(cr, STYPE.IDENT) + "," + this.par2.toString(cr, STYPE.IDENT) + ']';

				if(this.ex instanceof ErrorBaseDebug) {
					final ErrorBaseDebug ebd = (ErrorBaseDebug)this.ex;
					String result = "Error[\"" + this.iGetMessage() + '"';
					result += "," + '"' + ebd.getDetail() + '"';
					result += ",\"" + ebd.getSource() + '"';
					return result + ']';
				}

				return this.toString();
			case DESCRIBE:
				final StringBuilder sb = new StringBuilder();
				sb.append(this.getTypeName());

				// Self defined Error
				if(this.par1 != null && this.par2 != null) {
					sb.append('[');
					sb.append(this.par1.toString(cr, STYPE.DESCRIBE));
					sb.append(',');
					sb.append(this.par2.toString(cr, STYPE.DESCRIBE));
					sb.append(']');
				}
				else if(this.ex instanceof ErrorBaseDebug) {// No self defined Error
					final ErrorBaseDebug ebd = (ErrorBaseDebug)this.ex;
					sb.append("[\"" + this.iGetMessage() + '"');
					sb.append(",\"" + ebd.getDetail() + '"');
					sb.append(",\"" + ebd.getSource() + '"');
					sb.append(']');
				}
				return sb.toString();
			default:
				throw Err.impossible(type);
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		// All methods are without args and block, but ... not checking here!

		/**
		 * °message()Str # Returns the message of this error
		 */
		switch(method) {
//			case "do":
			case "throw":
				this.mThrow(cr);
				return new ObjectCallResult(Nil.NIL, true);
			case "show": // TODO Here okay?
				return A_Object.stdResult(this.mShow(cr));

			case "message":
				cr.argsNone();
				return A_Object.stdResult(new Str(this.iGetMessage()));

			case "source":
				return this.mErrorBaseDebug(cr, EBD.SOURCE);
			case "call":
				return this.mErrorBaseDebug(cr, EBD.CALL);
			case "detail":
				return this.mErrorBaseDebug(cr, EBD.DETAIL);
			case "instance":
				return this.mErrorBaseDebug(cr, EBD.INSTANCE);
			case "group":
				return this.mErrorBaseDebug(cr, EBD.GROUP);
			case "file":
				return A_Object.stdResult(this.mFileLine(cr, true));
			case "line":
				return A_Object.stdResult(this.mFileLine(cr, false));

			case "info":
				return A_Object.stdResult(this.mInfo(cr));

			default:
				return null;
		}
	}

	private String iErrorBaseDebugText(final CallRuntime cr, final EBD source) {
		if(!(this.ex instanceof ErrorBaseDebug))
			return null;

		String s = null;
		final ErrorBaseDebug ebd = (ErrorBaseDebug)this.ex;

		switch(source) {
			case SOURCE:
				s = ebd.getSource();
				break;
			case CALL:
				s = ebd.getCall();
				break;
			case DETAIL:
				s = ebd.getDetail();
				break;
			case INSTANCE:
				s = ebd.getInstance();
				break;
			case GROUP:
//				s = ebd.getErrorTypeInfo();
				s = this.ex.getClass().getSimpleName();
				break;

			default:
				Err.impossible(source);
		}
		return s;
	}

	private String iGetMessage() {
		return this.par1 != null ? ((Str)this.par1.get()).getValue() : this.ex.getMessage();
	}

	/**
	 * °source()Str? # Returns the source of this error
	 * °call()Str? # Returns the call of this error
	 * °detail()Str? # Returns the detail-info of this error
	 * °instance()Str? # Returns the instance of this error
	 * °group()Str # Returns the group of this error
	 */
	private ObjectCallResult mErrorBaseDebug(final CallRuntime cr, final EBD source) {
		cr.argsNone();
		final String value = this.iErrorBaseDebugText(cr, source);
		return A_Object.stdResult(value == null ? Nil.NIL : new Str(value));
	}

	/**
	 * °file()Str # Returns the file of this error
	 * °line()Int? # Returns the line-number of this error
	 */
	private I_Object mFileLine(final CallRuntime cr, final boolean useFile) {
		cr.argsNone();
		if(!(this.ex instanceof ErrorBaseDebug))
			return Nil.NIL;

		final String source = ((ErrorBaseDebug)this.ex).getSource();

		if(useFile) {
			String file = FilterString.till(":", true, source);
			if(file.length() > 0 && file.charAt(file.length() - 1) == ':')
				file = file.substring(0, file.length() - 1);
			return new Str(file);
		}
		else {
			String line = FilterString.right(":", source);
			if(line.length() > 0 && line.charAt(0) == ':')
				line = line.substring(1);
			return line.length() == 0
				? Nil.NIL
				: new Int(Integer.parseInt(line));
		}
	}

	/**
	 * °info()Str # Returns a string with message, detail and source
	 */
	private I_Object mInfo(final CallRuntime cr) {
		cr.argsNone();
		String result = this.iGetMessage();
		final String value = this.iErrorBaseDebugText(cr, EBD.DETAIL);
		if(value != null)
			result += " | " + value;
		result += " | " + this.iErrorBaseDebugText(cr, EBD.SOURCE);
		return new Str(result);
	}

	/**
	 * °throw()Nil # Throw this errors
	 */
	private void mThrow(final CallRuntime cr) {
		cr.argsNone();
		if(this.par1 != null && this.par2 != null)
			throw new CustomError(cr, Lib_String.unquote(this.par1.toString(cr, STYPE.REGULAR)), Lib_String.unquote(this.par2.toString(cr, STYPE.REGULAR)));
		throw new CustomError(cr, "Thrown again", this.toString(cr, STYPE.REGULAR));
	}

}
