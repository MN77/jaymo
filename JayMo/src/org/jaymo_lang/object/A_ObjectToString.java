/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Type;


/**
 * @author Michael Nitsche
 * @created 28.08.2021
 */
public abstract class A_ObjectToString implements I_ObjectToString {

	public final String getTypeName() {
		final Class<?> cl = this.getClass();
		return Lib_Type.getName(cl, this);
	}

	/**
	 * Default implementation, that should be override
	 */
	@Override
	public String toString() {
		return this.getTypeName();
	}

	/**
	 * Default implementation, that should be override
	 */
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.getTypeName();
	}

}
