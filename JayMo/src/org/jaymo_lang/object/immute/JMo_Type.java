/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2021-04-03
 */
public class JMo_Type extends A_Immutable {

	private final ArgCallBuffer arg;
	private String              name;
	private final boolean       checkTypeExists;


	/**
	 *
	 */
	public JMo_Type(final Call arg) {
		Err.ifNull(arg);
		this.arg = new ArgCallBuffer(0, arg);
		this.checkTypeExists = true;
	}

	public JMo_Type(final String name, final CallRuntime cr) { // JayMo internal
		Err.ifNull(name, cr);
		Lib_Type.checkValidity(name, cr);
		this.arg = null;
		this.name = name;
		this.checkTypeExists = false;
	}

	public JMo_Type(final String name, final Parser_Script parser) {
		Err.ifNull(name, parser);
		Lib_Type.checkValidity(name, parser);
		this.arg = null;
		this.name = name;
		this.checkTypeExists = true;
	}

	@Override
	public boolean equals(final Object other) {
		if(other instanceof JMo_Type)
			return ((JMo_Type)other).name.equals(this.name);
		return this == other;
	}

	@Override
	public boolean equalsLazy(final Object other) {
		if(other instanceof Str)
			return ((Str)other).getValue().equals(this.name);
		return this.equals(other);
	}

	public String getName() {
		return this.name;
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.arg != null) {
			final Str str = this.arg.init(cr, this, Str.class);
			final String typeName = Lib_Convert.getStringValue(cr, str);
			Lib_Type.checkValidity(typeName, cr);
			this.name = typeName;
		}
		if(this.checkTypeExists && !ObjectManager.isTypeKnown(cr, this.name))
			throw new RuntimeError(cr, "Invalid or unknown type", "Type not found: '" + this.name + "'");
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
//		return "Type(\"" + this.name + "\")";
		return "<" + this.name + '>';
	}

	/**
	 * °name()Str # Returns the name of this type.
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "name":
				cr.argsNone();
				return A_Object.stdResult(new Str(this.name));

			// TODO:
			//case "isJava":
			//case "isAtomic":
			//case "isSelf":
			//case "isSelfDefined":

			default:
				return null;
		}
	}

}
