/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute.datetime;

import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.type.datetime.I_Date;
import de.mn77.base.data.type.datetime.I_Time;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 20.01.2020
 */
public class Lib_DateTime_Format {

	public static Str format(final CallRuntime cr, final String format, final I_Date date, final I_Time time) {
		final StringBuilder sb = new StringBuilder();

		int start = 0;

		while(start < format.length()) {
			int move = 0;

			if(format.charAt(start) == '~' && start < format.length() - 1) {
				sb.append(format.charAt(start + 1));
				move = 2;
			}
			if(move == 0 && date != null)
				move = Lib_DateTime_Format.formatDate(sb, format, start, date);
			if(move == 0 && time != null)
				move = Lib_DateTime_Format.formatTime(sb, format, start, time);

			if(move == 0) {
				sb.append(format.charAt(start));
				move = 1;
			}
			Err.ifEqual(move, 0);
			start += move;
		}
		return new Str(sb.toString());
	}

	private static int formatDate(final StringBuilder sb, final String format, final int offset, final I_Date date) {
		int move = 0;

		if(format.startsWith("YYYY", offset)) {
			sb.append(FormNumber.width(4, date.getYear(), false));
			move = 4;
		}
		else if(format.startsWith("YY", offset)) {
			final String tmp = "" + date.getYear();
			if(tmp.length() >= 2)
				sb.append(tmp.substring(tmp.length() - 2, tmp.length()));
			else
				sb.append(FormNumber.width(2, tmp, false));
			move = 2;
		}
		else if(format.startsWith("Y", offset)) {
			sb.append("" + date.getYear());
			move = 1;
		}
		else if(format.startsWith("MM", offset)) {
			sb.append(FormNumber.width(2, date.getMonth(), false));
			move = 2;
		}
		else if(format.startsWith("M", offset)) {
			sb.append("" + date.getMonth());
			move = 1;
		}
		else if(format.startsWith("DD", offset)) {
			sb.append(FormNumber.width(2, date.getDay(), false));
			move = 2;
		}
		else if(format.startsWith("D", offset)) {
			sb.append("" + date.getDay());
			move = 1;
		}
		return move;
	}

	private static int formatTime(final StringBuilder sb, final String format, final int offset, final I_Time time) {
		int move = 0;

		if(format.startsWith("hh", offset)) {
			sb.append(FormNumber.width(2, time.getHours(), false));
			move = 2;
		}
		else if(format.startsWith("h", offset)) {
			sb.append("" + time.getHours());
			move = 1;
		}

		if(format.startsWith("ii", offset)) {
			int t = time.getHours();
			if(t > 12)
				t -= 12;
			if(t == 0)
				t = 12;
			sb.append(FormNumber.width(2, t, false));
			move = 2;
		}
		else if(format.startsWith("i", offset)) {
			int t = time.getHours();
			if(t > 12)
				t -= 12;
			if(t == 0)
				t = 12;
			sb.append("" + t);
			move = 1;
		}
		else if(format.startsWith("mm", offset)) {
			sb.append(FormNumber.width(2, time.getMinutes(), false));
			move = 2;
		}
		else if(format.startsWith("m", offset)) {
			sb.append("" + time.getMinutes());
			move = 1;
		}
		else if(format.startsWith("ss", offset)) {
			sb.append(FormNumber.width(2, time.getSeconds(), false));
			move = 2;
		}
		else if(format.startsWith("s", offset)) {
			sb.append("" + time.getSeconds());
			move = 1;
		}

		else if(format.startsWith("p", offset)) {
			final String ap = time.getHours() < 12 ? "AM" : "PM";
			sb.append(ap);
			move = 1;
		}
		return move;
	}

}
