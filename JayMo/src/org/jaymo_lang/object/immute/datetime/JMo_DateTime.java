/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute.datetime;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.type.datetime.I_Date;
import de.mn77.base.data.type.datetime.I_DateTime;
import de.mn77.base.data.type.datetime.I_Time;
import de.mn77.base.data.type.datetime.Lib_DateTime;
import de.mn77.base.data.type.datetime.MDateTime;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 2019-11-07
 * @apiNote The base is: "day"
 */
public class JMo_DateTime extends A_DateTimeBase {

	private I_DateTime            datetime;
	private final ArgCallBuffer[] init;


	public JMo_DateTime() {
		this.init = null;
		this.datetime = null;
	}

	public JMo_DateTime(final Call arg) {
		this.init = new ArgCallBuffer[]{new ArgCallBuffer(0, arg)};
		this.datetime = null;
	}

	public JMo_DateTime(final Call date, final Call time) {
		this.init = new ArgCallBuffer[]{
			new ArgCallBuffer(0, date),
			new ArgCallBuffer(1, time)};
	}

	public JMo_DateTime(final Call y, final Call m, final Call d) {
		this.init = new ArgCallBuffer[]{
			new ArgCallBuffer(0, y),
			new ArgCallBuffer(1, m),
			new ArgCallBuffer(2, d)};
	}

	public JMo_DateTime(final Call y, final Call m, final Call d, final Call hh, final Call mm, final Call ss) {
		this.init = new ArgCallBuffer[]{
			new ArgCallBuffer(0, y),
			new ArgCallBuffer(1, m),
			new ArgCallBuffer(2, d),
			new ArgCallBuffer(3, hh),
			new ArgCallBuffer(4, mm),
			new ArgCallBuffer(5, ss)};
	}

	public JMo_DateTime(final Call y, final Call m, final Call d, final Call hh, final Call mm, final Call ss, final Call nn) {
		this.init = new ArgCallBuffer[]{
			new ArgCallBuffer(0, y),
			new ArgCallBuffer(1, m),
			new ArgCallBuffer(2, d),
			new ArgCallBuffer(3, hh),
			new ArgCallBuffer(4, mm),
			new ArgCallBuffer(5, ss),
			new ArgCallBuffer(6, nn)};
		this.datetime = null;
	}

	/**
	 * +DateTime() # DateTime
	 *
	 * +DateTime(String text) # DateTime( "2019-10-30 12:34:56" )
	 * +DateTime(Long msec) # DateTime( 1579669628000l )
	 *
	 * +DateTime(Date date, Time time) # DateTime( Date(2019,10,30), Time(12,34,56) )
	 * +DateTime(IntNumber year, IntNumber month, IntNumber day) # DateTime(2019,10,30)
	 * +DateTime(IntNumber year, IntNumber month, IntNumber day, IntNumber hour, IntNumber min, IntNumber sec) # DateTime(2019,10,30,12,34,56)
	 * +DateTime(IntNumber year, IntNumber month, IntNumber day, IntNumber hour, IntNumber min, IntNumber sec, IntNumber mSec) # DateTime(2019,10,30,12,34,56,789)
	 */
	public JMo_DateTime(final I_DateTime datetime) {
		this.init = null;
		this.datetime = datetime;
	}

	@Override
	public boolean equals(final Object arg) {
		return arg instanceof JMo_DateTime
			? this.datetime.isEqual(((JMo_DateTime)arg).getInternalDateTime())
			: false;
	}

	@Override
	public boolean equalsLazy(final Object other) {

		if(other instanceof Str) {
			final String s = ((Str)other).getValue();

			switch(s.length()) {
				case 19:
					return s.equals(this.datetime.toStringShort()) || s.equals(this.datetime.toStringDE());
				case 23:
					return s.equals(this.datetime.toString());
				default:
					return false;
			}
		}
		return this.equals(other);
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.init == null) {
			if(this.datetime == null)
				this.datetime = new MDateTime();
			return;
		}
		final int argsLen = this.init.length;

		if(argsLen == 1) {
			final I_Object o = this.init[0].initExt(cr, this, A_IntNumber.class, Str.class);

			try {

				if(o instanceof A_IntNumber) {
					final long ms = Lib_Convert.getLongValue(cr, o);
					this.datetime = new MDateTime(ms);
				}
//				else if(o instanceof A_DecNumber) {
//					final double v = Lib_Convert.getDoubleValue(cr, o);
//					this.datetime = new MDateTime((long)(v*24*60*60*1000));
//				}
				else {
					final String s = Lib_Convert.getStringValue(cr, o);
					this.datetime = new MDateTime(s);
				}
			}
			catch(final Err_Runtime err) {
				throw new RuntimeError(cr, "Can't create DateTime-Object", err.getMessage() + " Got: DateTime(" + o.toString(cr, STYPE.IDENT) + ")");
			}
			return;
		}

		if(argsLen == 2) {
			final JMo_Date d = this.init[0].init(cr, this, JMo_Date.class);
			final JMo_Time t = this.init[1].init(cr, this, JMo_Time.class);

			try {
				this.datetime = new MDateTime(d.getInternalDate(), t.getInternalTime());
			}
			catch(final Err_Runtime err) {
				throw new RuntimeError(cr, "Can't create DateTime-Object", err.getMessage() + " Got: DateTime(" + d.toString() + "," + t.toString() + ")");
			}
			return;
		}
		final int iy = Lib_Convert.getIntValue(cr, this.init[0].init(cr, this, A_IntNumber.class));
		final int im = Lib_Convert.getIntValue(cr, this.init[1].init(cr, this, A_IntNumber.class));
		final int id = Lib_Convert.getIntValue(cr, this.init[2].init(cr, this, A_IntNumber.class));
		final int ihh = argsLen < 4 ? 0 : Lib_Convert.getIntValue(cr, this.init[3].init(cr, this, A_IntNumber.class));
		final int imm = argsLen < 5 ? 0 : Lib_Convert.getIntValue(cr, this.init[4].init(cr, this, A_IntNumber.class));
		final int iss = argsLen < 6 ? 0 : Lib_Convert.getIntValue(cr, this.init[5].init(cr, this, A_IntNumber.class));
		final int inn = argsLen < 7 ? 0 : Lib_Convert.getIntValue(cr, this.init[6].init(cr, this, A_IntNumber.class));

		try {
			this.datetime = new MDateTime(iy, im, id, ihh, imm, iss, inn);
		}
		catch(final Err_Runtime err) {
			throw new RuntimeError(cr, "Invalid datetime", "DateTime(" + iy + "," + im + "," + id + "," + ihh + "," + imm + "," + iss + ")");
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(this.datetime == null)
			return Lib_Type.getName(this);

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.datetime.toStringShort();
			default:
				return Lib_Type.getName(this) + "(\"" + this.datetime.toString() + "\")";
		}
	}

	@Override
	public String toStringFull() {
		if(this.datetime == null)
			return Lib_Type.getName(this);
		return this.datetime.toString();
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "getYear":
				return A_Object.stdResult(this.mGetYear(cr));
			case "getMonth":
				return A_Object.stdResult(this.mGetMonth(cr));
			case "getDay":
				return A_Object.stdResult(this.mGetDay(cr));
			case "getHour":
			case "getHours":
				return A_Object.stdResult(this.mGetHours(cr));
			case "getMin":
			case "getMinutes":
				return A_Object.stdResult(this.mGetMinutes(cr));
			case "getSec":
			case "getSeconds":
				return A_Object.stdResult(this.mGetSeconds(cr));
			case "weekDay":
			case "dayOfWeek":
				return A_Object.stdResult(this.mDayOfWeek(cr));

			case "addTime":
				return A_Object.stdResult(this.mAddSubTime(cr, true));
			case "addSec":
			case "addSeconds":
				return A_Object.stdResult(this.mAddSubSeconds(cr, true));
			case "addMin":
			case "addMinutes":
				return A_Object.stdResult(this.mAddSubMinutes(cr, true));
			case "addHour":
			case "addHours":
				return A_Object.stdResult(this.mAddSubHours(cr, true));
			case "addDay":
			case "addDays":
				return A_Object.stdResult(this.mAddSubDays(cr, true));
			case "addMonth":
			case "addMonths":
				return A_Object.stdResult(this.mAddSubMonths(cr, true));
			case "addYear":
			case "addYears":
				return A_Object.stdResult(this.mAddSubYears(cr, true));

			case "subTime":
				return A_Object.stdResult(this.mAddSubTime(cr, false));
			case "subSec":
			case "subSeconds":
				return A_Object.stdResult(this.mAddSubSeconds(cr, false));
			case "subMin":
			case "subMinutes":
				return A_Object.stdResult(this.mAddSubMinutes(cr, false));
			case "subHour":
			case "subHours":
				return A_Object.stdResult(this.mAddSubHours(cr, false));
			case "subDay":
			case "subDays":
				return A_Object.stdResult(this.mAddSubDays(cr, false));
			case "subMonth":
			case "subMonths":
				return A_Object.stdResult(this.mAddSubMonths(cr, false));
			case "subYear":
			case "subYears":
				return A_Object.stdResult(this.mAddSubYears(cr, false));

			case "setDay":
				return A_Object.stdResult(this.mSetDay(cr));
			case "setMonth":
				return A_Object.stdResult(this.mSetMonth(cr));
			case "setYear":
				return A_Object.stdResult(this.mSetYear(cr));
			case "setHour":
				return A_Object.stdResult(this.mSetHour(cr));
			case "setMinute":
				return A_Object.stdResult(this.mSetMinute(cr));
			case "setSecond":
				return A_Object.stdResult(this.mSetSecond(cr));

			case "date":
				return A_Object.stdResult(this.mDate(cr));
			case "time":
				return A_Object.stdResult(this.mTime(cr));

			case "diffDays":
				return A_Object.stdResult(this.mDiffDays(cr));
//			case "diffMonths":
//				return stdResult(diffMonths(cr));
//			case "diffYears":
//				return stdResult(diffYears(cr));
			case "diffHours":
				return A_Object.stdResult(this.mDiffHours(cr));
			case "diffMinutes":
				return A_Object.stdResult(this.mDiffMinutes(cr));
			case "diffSeconds":
				return A_Object.stdResult(this.mDiffSeconds(cr));
			case "diff":
			case "diffMSec":
			case "diffMilliSeconds":
				return A_Object.stdResult(this.mDiffMilliSeconds(cr));

			default:
				return null;
		}
	}

	@Override
	protected I_Date getInternalDate() {
		return this.datetime.getDate();
	}

	protected I_DateTime getInternalDateTime() {
		return this.datetime;
	}

	@Override
	protected I_Time getInternalTime() {
		return this.datetime.getTime();
	}

	@Override
	protected A_DateTimeBase incdec2(final boolean inc, final int amount) {

		if(inc) {
			final I_DateTime date2 = this.datetime.getAddSeconds(amount);
			return new JMo_DateTime(date2);
		}
		else {
			final I_DateTime date2 = this.datetime.getAddSeconds(-amount);
			return new JMo_DateTime(date2);
		}
	}

	@Override
	protected boolean[] mCompare2(final CallRuntime cr) {
		final JMo_DateTime d = (JMo_DateTime)cr.args(this, JMo_DateTime.class)[0];
		final boolean isEqual = this.datetime.isEqual(d.getInternalDateTime());
		final boolean isGreater = this.datetime.isGreater(d.getInternalDateTime());
		return new boolean[]{isEqual, isGreater};
	}

	@Override
	protected JMo_DateTime mLimit(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, JMo_DateTime.class, JMo_DateTime.class);
		final I_DateTime min = ((JMo_DateTime)args[0]).datetime;
		final I_DateTime max = ((JMo_DateTime)args[1]).datetime;

		if(this.datetime.isGreater(max))
			return (JMo_DateTime)args[1];
		if(min.isGreater(this.datetime))
			return (JMo_DateTime)args[0];
		return this;
	}

	@Override
	protected I_Object mStyle(final CallRuntime cr, final COUNTRY country) {
		cr.argsNone();

		switch(country) {
			case DE:
				return Lib_DateTime_Format.format(cr, "DD.MM.YYYY hh:mm:ss", this.getInternalDate(), this.getInternalTime());
			case EN:
				return Lib_DateTime_Format.format(cr, "DD/MM/YYYY hh:mm:ss", this.getInternalDate(), this.getInternalTime());
			case US:
				return Lib_DateTime_Format.format(cr, "M/DD/YYYY i:mm:ss p", this.getInternalDate(), this.getInternalTime());
			default:
				throw Err.impossible(country);
		}
	}

	@Override
	protected I_Object pAddSub(final CallRuntime cr, final A_Number arg, final boolean add) {
		double d = Lib_Convert.getDoubleValue(cr, arg);
		if(!add)
			d = -d;
		final long ms = Lib_DateTime.toMilliSec(d);
		final I_DateTime dt2 = this.datetime.getAddMilliSeconds(ms);
		return new JMo_DateTime(dt2);
	}

	@Override
	protected JMo_Long pToMilliSeconds() {
		return new JMo_Long(this.datetime.getValueMilliSeconds());
	}

	/**
	 * °addDay ^ addDays
	 * °addDays(IntNumber days)DateTime # Return a new DateTime with days added
	 * °subDay ^ subDays
	 * °subDays(IntNumber days)DateTime # Return a new Date-Object with days subtracted
	 */
	private JMo_DateTime mAddSubDays(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_DateTime date2 = this.datetime.getAddDays(pari);
		return new JMo_DateTime(date2);
	}

	/**
	 * °addHour ^ addHours
	 * °addHours(IntNumber hours)DateTime # Return a new DateTime with hours added
	 * °subHour ^ subHours
	 * °subHours(IntNumber hours)DateTime # Return a new Date-Object with hours subtracted
	 */
	private JMo_DateTime mAddSubHours(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_DateTime date2 = this.datetime.getAddHours(pari);
		return new JMo_DateTime(date2);
	}

	/**
	 * °addMin ^ addMinutes
	 * °addMinutes(IntNumber minutes)DateTime # Return a new DateTime with minutes added
	 * °subMin ^ subMinutes
	 * °subMinutes(IntNumber minutes)DateTime # Return a new Date-Object with minutes subtracted
	 */
	private JMo_DateTime mAddSubMinutes(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_DateTime date2 = this.datetime.getAddMinutes(pari);
		return new JMo_DateTime(date2);
	}

	/**
	 * °addMonth ^ addMonths
	 * °addMonths(IntNumber months)DateTime # Return a new Date-Object with added months
	 * °subMonth ^ subMonths
	 * °subMonths(IntNumber months)DateTime # Return a new Date-Object with months subtracted
	 */
	private JMo_DateTime mAddSubMonths(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_DateTime date2 = this.datetime.getAddMonths(pari);
		return new JMo_DateTime(date2);
	}

	/**
	 * °addSec ^ addSeconds
	 * °addSeconds(IntNumber seconds)DateTime # Return a new DateTime with seconds added
	 * °subSec ^ subSeconds
	 * °subSeconds(IntNumber seconds)DateTime # Return a new DateTime with seconds subtracted
	 */
	private I_Object mAddSubSeconds(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int val = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			val = -val;
		final I_DateTime date2 = this.datetime.getAddSeconds(val);
		return new JMo_DateTime(date2);
	}

	/**
	 * °addTime(Time time)DateTime # Return a new DateTime with given hours, minutes, seconds and milliseconds added
	 * °subTime(Time time)DateTime # Return a new DateTime with given hours, minutes, seconds and milliseconds subtracted
	 */
	private I_Object mAddSubTime(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, JMo_Time.class)[0];
		long val = ((JMo_Time)arg).getInternalTime().getValueMilliSeconds();
		if(!add)
			val = -val;
		final I_DateTime date2 = this.datetime.getAddMilliSeconds(val);
		return new JMo_DateTime(date2);
	}

	/**
	 * °addYear ^ addYears
	 * °addYears(IntNumber years)DateTime # Return a new Date-Object with added years
	 * °subYear ^ subYears
	 * °subYears(IntNumber years)DateTime # Return a new Date-Object with years subtracted
	 */
	private JMo_DateTime mAddSubYears(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_DateTime date2 = this.datetime.getAddYears(pari);
		return new JMo_DateTime(date2);
	}

	/**
	 * °date()Date Gets a date object
	 */
	private JMo_Date mDate(final CallRuntime cr) {
		cr.argsNone();
		return new JMo_Date(this.datetime.getDate());
	}

	/**
	 * °weekDay ^ getDayOfWeek
	 * °dayOfWeek()Int Gets the day of the week. 1 = Monday, 7 = Sunday
	 */
	private Int mDayOfWeek(final CallRuntime cr) {
		cr.argsNone();
		int dow = this.datetime.getDayOfWeek();
		if(dow == 0)
			dow = 7;
		return new Int(dow);
	}

	/**
	 * °diffDays(DateTime other)Int # Gets the difference of days to another DateTime
	 */
	private Int mDiffDays(final CallRuntime cr) {
		final JMo_DateTime arg = (JMo_DateTime)cr.args(this, JMo_DateTime.class)[0];
		final int diff = this.datetime.diffDays(arg.getInternalDateTime());
		return new Int(diff);
	}

	/**
	 * °diffHours(DateTime other)Long # Gets the difference of hours to another DateTime
	 */
	private JMo_Long mDiffHours(final CallRuntime cr) {
		final JMo_DateTime arg = (JMo_DateTime)cr.args(this, JMo_DateTime.class)[0];
		final long diff = this.datetime.diffHours(arg.getInternalDateTime());
		return new JMo_Long(diff);
	}

	/**
	 * °diff ^ diffMilliSeconds
	 * °diffMSec ^ diffMilliSeconds
	 * °diffMilliSeconds(DateTime other)Long # Gets the difference of seconds to another DateTime
	 */
	private JMo_Long mDiffMilliSeconds(final CallRuntime cr) {
		final JMo_DateTime arg = (JMo_DateTime)cr.args(this, JMo_DateTime.class)[0];
		final long diff = this.datetime.diffMilliSeconds(arg.getInternalDateTime());
		return new JMo_Long(diff);
	}

	/**
	 * °diffMinutes(DateTime other)Long # Gets the difference of minutes to another DateTime
	 */
	private JMo_Long mDiffMinutes(final CallRuntime cr) {
		final JMo_DateTime arg = (JMo_DateTime)cr.args(this, JMo_DateTime.class)[0];
		final long diff = this.datetime.diffMinutes(arg.getInternalDateTime());
		return new JMo_Long(diff);
	}

	/**
	 * °diffSeconds(DateTime other)Long # Gets the difference of seconds to another DateTime
	 */
	private JMo_Long mDiffSeconds(final CallRuntime cr) {
		final JMo_DateTime arg = (JMo_DateTime)cr.args(this, JMo_DateTime.class)[0];
		final long diff = this.datetime.diffSeconds(arg.getInternalDateTime());
		return new JMo_Long(diff);
	}

	/**
	 * °getDay()Int Gets the day of this datetime
	 */
	private Int mGetDay(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.datetime.getDay());
	}

	/**
	 * °getHour ^ getHours
	 * °getHours()Int Gets the hours of this datetime
	 */
	private Int mGetHours(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.datetime.getHours());
	}

	/**
	 * °getMin ^ getMinutes
	 * °getMinutes()Int Gets the minutes of this datetime
	 */
	private Int mGetMinutes(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.datetime.getMinutes());
	}

	/**
	 * °getMonth()Int Gets the month of this datetime
	 */
	private Int mGetMonth(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.datetime.getMonth());
	}

	/**
	 * °getSec ^ getSeconds
	 * °getSeconds()Int Gets the seconds of this datetime
	 */
	private Int mGetSeconds(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.datetime.getSeconds());
	}

	/**
	 * °getYear()Int Gets the year of this datetime
	 */
	private Int mGetYear(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.datetime.getYear());
	}

	/**
	 * °setDay(IntNumber day)DateTime # Return a new Date-Object with changed day
	 */
	private JMo_DateTime mSetDay(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_DateTime dt2 = this.datetime.getSetDay(pari);
		return new JMo_DateTime(dt2);
	}

	/**
	 * °setHour(IntNumber hour)DateTime # Return a new Date-Object with changed hour
	 */
	private JMo_DateTime mSetHour(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_DateTime dt2 = this.datetime.getSetHour(pari);
		return new JMo_DateTime(dt2);
	}

	/**
	 * °setMinute(IntNumber minute)DateTime # Return a new Date-Object with changed year
	 */
	private JMo_DateTime mSetMinute(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_DateTime dt2 = this.datetime.getSetMinute(pari);
		return new JMo_DateTime(dt2);
	}

	/**
	 * °setMonth(IntNumber month)DateTime # Return a new Date-Object with changed month
	 */
	private JMo_DateTime mSetMonth(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_DateTime dt2 = this.datetime.getSetMonth(pari);
		return new JMo_DateTime(dt2);
	}

	/**
	 * °setSecond(IntNumber second)DateTime # Return a new Date-Object with changed year
	 */
	private JMo_DateTime mSetSecond(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_DateTime dt2 = this.datetime.getSetSecond(pari);
		return new JMo_DateTime(dt2);
	}

	/**
	 * °setYear(IntNumber year)DateTime # Return a new Date-Object with changed year
	 */
	private JMo_DateTime mSetYear(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_DateTime dt2 = this.datetime.getSetYear(pari);
		return new JMo_DateTime(dt2);
	}

	/**
	 * °time()Time Gets a time object
	 */
	private JMo_Time mTime(final CallRuntime cr) {
		cr.argsNone();
		return new JMo_Time(this.datetime.getTime());
	}

}
