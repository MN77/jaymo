/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute.datetime;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.type.datetime.I_Date;
import de.mn77.base.data.type.datetime.I_DateTime;
import de.mn77.base.data.type.datetime.I_Time;
import de.mn77.base.data.type.datetime.MDate;
import de.mn77.base.data.type.datetime.MDateTime;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @implNote This Object is immutable
 * @apiNote The base is: "day"
 */
public class JMo_Date extends A_DateTimeBase {

	private I_Date                date = null;
	private final ArgCallBuffer[] init;


	public JMo_Date() {
		this.init = null;
	}

	public JMo_Date(final Call str) {
		this.init = new ArgCallBuffer[]{new ArgCallBuffer(0, str)};
	}

	public JMo_Date(final Call y, final Call m, final Call d) {
		this.init = new ArgCallBuffer[]{new ArgCallBuffer(0, y), new ArgCallBuffer(0, m), new ArgCallBuffer(0, d)};
	}

	/**
	 * +Date() # Date
	 *
	 * +Date(String text) # Date("2019-10-30")
	 * +Date(Long msec) # Date(2030479200000l)
	 *
	 * +Date(Number year, Number month, Number day) # Date(2019,10,30)
	 */
	public JMo_Date(final I_Date date) {
		this.init = null;
		this.date = date;
	}

	@Override
	public boolean equals(final Object arg) {
		return arg instanceof JMo_Date
			? this.date.isEqual(((JMo_Date)arg).getInternalValue())
			: false;
	}

	@Override
	public boolean equalsLazy(final Object other) {

		if(other instanceof Str) {
			final String s = ((Str)other).getValue();
			return s.equals(this.date.toString()) || s.equals(this.date.toStringDE());
		}
		return this.equals(other);
	}

	@Override
	public void init(final CallRuntime cr) {
		if(this.date != null)
			return;
		if(this.init == null)
			this.date = new MDate();
		else if(this.init.length == 1) {
			final I_Object o = this.init[0].initExt(cr, this, A_IntNumber.class, Str.class);

			try {

				if(o instanceof A_IntNumber) {
					final long ms = Lib_Convert.getLongValue(cr, o);
					this.date = new MDate(ms);
				}
//				else if(o instanceof A_DecNumber) {
//					final double v = Lib_Convert.getDoubleValue(cr, o);
//					this.date = new MDate((long)(v*24*60*60*1000));
//				}
				else {
					final String s = Lib_Convert.getStringValue(cr, o);
					this.date = new MDate(s);
				}
			}
			catch(final Err_Runtime err) {
				throw new RuntimeError(cr, "Can't create Date-Object", err.getMessage() + " Got: Date(" + o.toString(cr, STYPE.IDENT) + ")");
			}
		}
		else {
			final int iy = Lib_Convert.getIntValue(cr, this.init[0].init(cr, this, Int.class));
			Lib_Error.ifNotBetween(cr, 0, 2500, iy, "Year"); // Limit
			final int im = Lib_Convert.getIntValue(cr, this.init[1].init(cr, this, Int.class));
			Lib_Error.ifNotBetween(cr, 1, 12, im, "Month");
			final int id = Lib_Convert.getIntValue(cr, this.init[2].init(cr, this, Int.class));
			Lib_Error.ifNotBetween(cr, 1, 31, id, "Day");

			try {
				this.date = new MDate(iy, im, id);
			}
			catch(final Err_Runtime err) {
				throw new RuntimeError(cr, err.getMessage(), "Date(" + iy + "," + im + "," + id + ")");
			}
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(this.date == null)
			return "Date";

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.date.toString();
			default:
				return Lib_Type.getName(this) + "(\"" + this.date.toString() + "\")";
		}
	}

	@Override
	public String toStringFull() {
		return this.toString(null, STYPE.REGULAR);
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "getYear":
				return A_Object.stdResult(this.mYear(cr));
			case "getMonth":
				return A_Object.stdResult(this.mMonth(cr));
			case "getDay":
				return A_Object.stdResult(this.mDay(cr));
			case "weekDay":
			case "dayOfWeek":
				return A_Object.stdResult(this.mDayOfWeek(cr));

			case "addDay":
			case "addDays":
				return A_Object.stdResult(this.mAddSubDays(cr, true));
			case "addMonth":
			case "addMonths":
				return A_Object.stdResult(this.mAddSubMonths(cr, true));
			case "addYear":
			case "addYears":
				return A_Object.stdResult(this.mAddSubYears(cr, true));

			case "subDay":
			case "subDays":
				return A_Object.stdResult(this.mAddSubDays(cr, false));
			case "subMonth":
			case "subMonths":
				return A_Object.stdResult(this.mAddSubMonths(cr, false));
			case "subYear":
			case "subYears":
				return A_Object.stdResult(this.mAddSubYears(cr, false));

			case "setDay":
				return A_Object.stdResult(this.mSetDay(cr));
			case "setMonth":
				return A_Object.stdResult(this.mSetMonth(cr));
			case "setYear":
				return A_Object.stdResult(this.mSetYear(cr));

			case "combine":
				return A_Object.stdResult(this.mCombine(cr));

			case "diff":
			case "diffDays":
				return A_Object.stdResult(this.mDiffDays(cr));
			case "diffMonths":
				return A_Object.stdResult(this.mDiffMonths(cr));
			case "diffYears":
				return A_Object.stdResult(this.mDiffYears(cr));

			default:
				return null;
		}
	}

	@Override
	protected I_Date getInternalDate() {
		return this.date;
	}

	@Override
	protected I_Time getInternalTime() {
		return null;
	}

	@Override
	protected A_DateTimeBase incdec2(final boolean inc, final int amount) {

		if(inc) {
			final I_Date date2 = this.date.getAddDays(amount);
			return new JMo_Date(date2);
		}
		else {
			final I_Date date2 = this.date.getAddDays(-amount);
			return new JMo_Date(date2);
		}
	}

	/**
	 * °<(Date other)Bool Is this date lesser than the other?
	 * °<=(Date other)Bool Is this date lesser or equal the other?
	 * °>(Date other)Bool Is this date greater than the other?
	 * °>=(Date other)Bool Is this date greater or equal the other?
	 */
	@Override
	protected boolean[] mCompare2(final CallRuntime cr) {
		final JMo_Date d = (JMo_Date)cr.args(this, JMo_Date.class)[0];
		final boolean isEqual = this.date.isEqual(d.getInternalValue());
		final boolean isGreater = this.date.isGreater(d.getInternalValue());
		return new boolean[]{isEqual, isGreater};
	}

	@Override
	protected JMo_Date mLimit(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, JMo_Date.class, JMo_Date.class);
		final I_Date min = ((JMo_Date)args[0]).date;
		final I_Date max = ((JMo_Date)args[1]).date;

		if(this.date.isGreater(max))
			return (JMo_Date)args[1];
		if(min.isGreater(this.date))
			return (JMo_Date)args[0];
		return this;
	}

	@Override
	protected I_Object mStyle(final CallRuntime cr, final COUNTRY country) {
		cr.argsNone();

		switch(country) {
			case DE:
				return Lib_DateTime_Format.format(cr, "DD.MM.YYYY", this.getInternalDate(), null);
			case EN:
				return Lib_DateTime_Format.format(cr, "DD/MM/YYYY", this.getInternalDate(), null);
			case US:
				return Lib_DateTime_Format.format(cr, "M/DD/YYYY", this.getInternalDate(), null);
			default:
				throw Err.impossible(country);
		}
	}

	@Override
	protected I_Object pAddSub(final CallRuntime cr, final A_Number num, final boolean add) {
//		return this.mAddSubDays(cr, add);
		int pari = Lib_Convert.getIntValue(cr, num);
		if(!add)
			pari = -pari;
		final I_Date date2 = this.date.getAddDays(pari);
		return new JMo_Date(date2);
	}

	@Override
	protected JMo_Long pToMilliSeconds() {
		return new JMo_Long(this.date.getValueMilliSeconds());
	}

	private I_Date getInternalValue() {
		return this.date;
	}

	/**
	 * °addDay ^ addDays
	 * °addDays(IntNumber days)Date # Return a new date object with days added
	 * °subDay ^ subDays
	 * °subDays(IntNumber days)Date # Return a new date object with days subtracted
	 */
	private I_Object mAddSubDays(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_Date date2 = this.date.getAddDays(pari);
		return new JMo_Date(date2);
	}

	/**
	 * °addMonth ^ addMonths
	 * °addMonths(IntNumber months)Date # Return a new Date-Object with added months
	 * °subMonth ^ subMonths
	 * °subMonths(IntNumber months)Date # Return a new date object with months subtracted
	 */
	private JMo_Date mAddSubMonths(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_Date date2 = this.date.getAddMonths(pari);
		return new JMo_Date(date2);
	}

	/**
	 * °addYear ^ addYears
	 * °addYears(IntNumber years)Date # Return a new Date-Object with added years
	 * °subYear ^ subYears
	 * °subYears(IntNumber years)Date # Return a new date object with years subtracted
	 */
	private JMo_Date mAddSubYears(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_Date date2 = this.date.getAddYears(pari);
		return new JMo_Date(date2);
	}

	/**
	 * °combine(Time time)DateTime # Combine date and time to a new DateTime
	 */
	private I_Object mCombine(final CallRuntime cr) {
		final I_Object arg = cr.args(this, JMo_Time.class)[0];
		final I_DateTime dt = new MDateTime(this.date, ((JMo_Time)arg).getInternalTime());
		return new JMo_DateTime(dt);
	}

	/**
	 * °getDay()Int Gets the day of this date
	 */
	private Int mDay(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.date.getDay());
	}

	/**
	 * °weekDay ^ dayOfWeek
	 * °dayOfWeek()Int # Gets the day of the week. 1 = Monday, 7 = Sunday
	 */
	private Int mDayOfWeek(final CallRuntime cr) {
		cr.argsNone();
		int dow = this.date.getDayOfWeek();
		if(dow == 0)
			dow = 7;
		return new Int(dow);
	}

	/**
	 * °diff ^ diffDays
	 * °diffDays(Date other)Int Gets the difference of days to another date
	 */
	private Int mDiffDays(final CallRuntime cr) {
		final JMo_Date arg = (JMo_Date)cr.args(this, JMo_Date.class)[0];
		final int diff = this.date.diffDays(arg.getInternalValue());
		return new Int(diff);
	}

	/**
	 * °diffMonths(Date other)Int Gets the difference of month to another date
	 */
	private Int mDiffMonths(final CallRuntime cr) {
		final JMo_Date arg = (JMo_Date)cr.args(this, JMo_Date.class)[0];
		final int diff = this.date.diffMonths(arg.getInternalValue());
		return new Int(diff);
	}

	/**
	 * °diffYears(Date other)Int Gets the difference of years to another date
	 */
	private Int mDiffYears(final CallRuntime cr) {
		final JMo_Date arg = (JMo_Date)cr.args(this, JMo_Date.class)[0];
		final int diff = this.date.diffYears(arg.getInternalValue());
		return new Int(diff);
	}

	/**
	 * °getMonth()Int Gets the month of this date
	 */
	private Int mMonth(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.date.getMonth());
	}

	/**
	 * °setDay(IntNumber day)Date # Return a new Date-Object with changed day
	 */
	private JMo_Date mSetDay(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_Date date2 = this.date.getSetDay(pari);
		return new JMo_Date(date2);
	}

	/**
	 * °setMonth(IntNumber month)Date # Return a new Date-Object with changed month
	 */
	private JMo_Date mSetMonth(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_Date date2 = this.date.getSetMonth(pari);
		return new JMo_Date(date2);
	}

	/**
	 * °setYear(IntNumber year)Date # Return a new Date-Object with changed year
	 */
	private JMo_Date mSetYear(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_Date date2 = this.date.getSetYear(pari);
		return new JMo_Date(date2);
	}

	/**
	 * °getYear()Int Gets the year of this date
	 */
	private Int mYear(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.date.getYear());
	}

}
