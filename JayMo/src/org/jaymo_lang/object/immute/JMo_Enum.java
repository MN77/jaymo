/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2021-08-15
 */
public class JMo_Enum extends A_Immutable {

	private final String typeName;
	private final String name;
	private Integer      ordinal = null;


	public JMo_Enum(final String typeName, final String name) {
		this.typeName = typeName;
		this.name = name;
	}

	public JMo_Enum(final Type type, final String name) {
		this.typeName = type.getName();
		this.name = name;
	}

	@Override
	public boolean equals(final Object other) {
		return other instanceof JMo_Enum
			? ((JMo_Enum)other).typeName.equals(this.typeName) && ((JMo_Enum)other).name.equals(this.name)
			: this == other;
	}

	@Override
	public boolean equalsLazy(final Object other) {
		return this.equals(other);
	}

	public String getName() {
		return this.name;
	}

	public String getOriginTypeName() {
		return this.typeName;
	}

	@Override
	public void init(final CallRuntime cr) {}

	public void setOrdinal(final int ord) {
		Err.ifNot(this.ordinal == null);
		this.ordinal = ord;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.typeName + "." + this.name;
	}

	/**
	 * °name()Str # Returns the name of this enum.
	 * °ordinal()Int # Returns the ordinal number of this Enum
	 * °parent()Type # Returns the parent type of this Enum.
	 * °typename ^ getTypeName
	 * °typeName()Str # Returns the name of the parent type.
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "name":
				cr.argsNone();
				return A_Object.stdResult(new Str(this.name));
			case "ordinal":
				cr.argsNone();
				return A_Object.stdResult(new Int(this.ordinal));
			case "parent":
				cr.argsNone();
				return A_Object.stdResult(new JMo_Type(this.typeName, cr));
			case "typename":
			case "typeName":
				cr.argsNone();
				return A_Object.stdResult(new Str(this.typeName));

			default:
				return null;
		}
	}

}
