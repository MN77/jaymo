/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.util.Lib_String;


/**
 * @author Michael Nitsche
 * @created 28.10.2022
 */
public class JMo_Charset extends A_Immutable {

	private final ArgCallBuffer arg0;
	private Charset             charset = null;


	/**
	 * +Charset() # Charset.UTF8
	 * +Charset(Str name) # Charset( "UTF-8" )
	 */
	public JMo_Charset() {
		this.arg0 = null;
		this.charset = Charset.defaultCharset();
	}

	public JMo_Charset(final Call c) {
		this.arg0 = new ArgCallBuffer(0, c);
	}

	public JMo_Charset(final Charset cs) {
		this.arg0 = null;
		this.charset = cs;
	}

	public JMo_Charset(final String cs) {
		this.arg0 = null;
		this.charset = Charset.forName(cs);
	}

	@Override
	public boolean equals(final Object other) {
		if(other instanceof JMo_Charset)
			return this.getCharset().equals(((JMo_Charset)other).getCharset());
		return false;
	}

	@Override
	public boolean equalsLazy(final Object other) {
		return this.equals(other);
	}

	public Charset getCharset() {
		return this.charset;
	}

	/**
	 * °ASCII
	 * °US_ASCII
	 *
	 * °ISO8859_1
	 * °ISO_8859_1
	 * °ISO8859_15
	 * °ISO_8859_15
	 *
	 * °UTF8
	 * °UTF_8
	 * °UTF16
	 * °UTF_16
	 * °UTF16LE
	 * °UTF_16LE
	 * °UTF16BE
	 * °UTF_16BE
	 *
	 * °CP850
	 * °CP_850
	 * °CP1252
	 * °CP_1252
	 */
	@Override
	public final A_Immutable getConstant(final CallRuntime cr, final String name) {
		return new JMo_Charset(this.iMapping(cr, name));
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.arg0 != null) {
			final Str o = (Str)cr.argType(this.arg0.get(), Str.class);
			this.charset = this.iMapping(cr, o.getValue());
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		return null;
	}

	private Charset iMapping(final CallRuntime cr, String name) {
		name = Lib_String.replace(name, '-', '_').toUpperCase();

		switch(name) {
			case "US_ASCII":
			case "ASCII":
				return StandardCharsets.US_ASCII;

			case "UTF8":
			case "UTF_8":
				return StandardCharsets.UTF_8;
			case "UTF16":
			case "UTF_16":
				return StandardCharsets.UTF_16;
			case "UTF16LE":
			case "UTF_16LE":
				return StandardCharsets.UTF_16LE;
			case "UTF16BE":
			case "UTF_16BE":
				return StandardCharsets.UTF_16BE;

			case "ISO8859_1":
			case "ISO_8859_1":
				return StandardCharsets.ISO_8859_1;
			case "ISO8859_15":
			case "ISO_8859_15":
				return Charset.forName("ISO8859-15");

			case "CP850":
			case "CP_850":
				return Charset.forName("Cp850");
			case "CP1252":
			case "CP_1252":
				return Charset.forName("Cp1252");

			default:
				throw new RuntimeError(cr, "Unknown charset definition", "Got: " + name);

			// sun.nio.cs.Unicode
		}
	}

}
