/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import java.io.InputStream;
import java.util.ArrayList;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeWarning;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Function;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.stream.Lib_InputStream;
import de.mn77.base.sys.file.Lib_Jar;


/**
 * @author Michael Nitsche
 * @created 10.08.2022
 */
public class JMo_Help extends A_Object {

	private static final String JAR_HELP_TYPE_FUNC    = "/jar/help/functions.txt";
	private static final String JAR_HELP_TYPE_NEW     = "/jar/help/constructors.txt";
	private static final String JAR_HELP_TYPE_EXTENDS = "/jar/help/extends.txt";


	/**
	 * +Help()
	 * +?foo ^ Help.quickly(\"foo\") # ?Number.round
	 * +??foo ^ Help.explain(\"foo\") # ??Number.round
	 */
	public JMo_Help() {}

	@Override
	public void init(final CallRuntime cr) {}

	/**
	 * °get()Object # Returns object1 at condition true, otherwise object2
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "quick":	// fast
			case "quickly":
				this.mQuickly(cr);
				return A_Object.stdResult(this);
			case "explain":
				this.mExplain(cr);
				return A_Object.stdResult(this);

			default:
				return null;
		}
	}

	private void iAddFunctionsOfType(final SimpleList<String> functions, final ArrayList<String> funcLines, final boolean starts, final String filter, String t) {
		t = t + '.';
		final int len = t.length();

		for(String line : funcLines)
			if(line.startsWith(t)) {
				line = line.substring(len);

				if(starts) {
					if(line.toLowerCase().startsWith(filter))
						functions.add(line);
				}
				else if(FilterString.before(new char[]{'(', '§'}, false, line.toLowerCase()).contains(filter))
					functions.add(line);
			}
	}

	private boolean iCheckType(final String type) {
		final int len = type.length();
		if(len < 2)
			return false;

		for(final char c : type.toCharArray())
			if((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '_')
				return false;

		return type.charAt(len - 1) != '_'; // TODO Prefix?
	}

	/**
	 * @return Returns a group of boolean and string. The boolean is true, when searching with 'startsWith', false when searching with 'contains'.
	 * @param search:
	 *            Search string could be empty!!!
	 */
	private Group2<Boolean, String> iFilter(String search) {
		search = search.trim();
		if(search.endsWith("*"))
			search = search.substring(0, search.length() - 1);

		if(search.startsWith("*"))
			return new Group2<>(false, search.substring(1));

		return new Group2<>(true, search);
	}

	private void iOutputFunction(final CallRuntime cr, String data) {
		data = FilterString.before(new char[]{'#'}, false, data).trim();
		data = Lib_String.replace(data, '§', "   --> ");
		Lib_Output.out(cr, data, true);
	}

	private void iShowConstructors(final CallRuntime cr, final String search) {
		final Group2<Boolean, String> filter = this.iFilter(search);

		try {
			final InputStream is = Lib_Jar.getStream(JMo_Help.JAR_HELP_TYPE_NEW);
			final ArrayList<String> lines = Lib_InputStream.readLines(is, true);

			for(final String line : lines)
				if(filter.o1) {
					if(line.toLowerCase().startsWith(filter.o2))
						Lib_Output.out(cr, line, true);
				}
				else if(line.toLowerCase().contains(filter.o2))
					Lib_Output.out(cr, line, true);
		}
		catch(final Err_FileSys e) {
			Err.exit(e);
		}
	}

	private void iShowFunctionDetailed(final CallRuntime cr, String search) {

		try {
			final InputStream is = Lib_Jar.getStream(JMo_Help.JAR_HELP_TYPE_FUNC);
			final ArrayList<String> lines = Lib_InputStream.readLines(is, true);
			search = search.toLowerCase();
			final String filterDirect = search + '(';
			final String filterLink = "§" + search;

			final String typeLow = FilterString.untilFirst('.', search).toLowerCase();

			final SimpleList<String> data = new SimpleList<>(lines.size());
			final SimpleList<String> aliases = new SimpleList<>(lines.size());

			for(final String line : lines) {
				final String lineLow = line.toLowerCase();
				if(lineLow.startsWith(filterDirect))
					data.add(line);
				else if(lineLow.startsWith(typeLow) && lineLow.contains(filterLink))
					aliases.add(line);
			}

			if(data.size() == 0 && aliases.size() > 0) {
				search = FilterString.afterFirst('§', aliases.get(0));
				this.iShowFunctionDetailed(cr, search);
				return;
			}

			if(data.size() == 0) {
				Lib_Output.out(cr, "Unknown function", true);
				return;
			}
			//--------------------

//			final String type = FilterString.beforeFirst('.', data.get(0)).trim();
			final String func = FilterString.beforeFirst('(', data.get(0)).trim();
			final String returns = FilterString.beforeFirst('#', FilterString.afterFirst(')', data.get(0))).trim();

			final StringBuilder out = new StringBuilder();

			// Output of header
//			out.append(type);
//			out.append('.');
			out.append(func);
			out.append('\n');
			out.append("  => ");
			out.append(returns);
			out.append('\n');

			// Output of aliases
			for(final String alias : aliases) {
//				out.append('\n');
				out.append("  <--  ");
				out.append(alias);
//				out.append(func);
				out.append('\n');
			}

			// Output of argument versions
			for(final String f : data) {
				out.append('\n');
				String funcArgs = FilterString.fromFirst('.', FilterString.untilFirst(')', f)).trim();
				final String description = FilterString.afterFirst('#', f).trim();

				if(funcArgs.endsWith("()"))
					funcArgs = funcArgs.substring(0, funcArgs.length() - 2);

				out.append(funcArgs);
				out.append('\n');

				out.append("# ");
				out.append(description);
				out.append('\n');
//				out.append("  ");
			}
			Lib_Output.out(cr, out.toString(), false);
		}
		catch(final Err_FileSys e) {
			Err.exit(e);
		}
	}

	private void iShowFunctions(final CallRuntime cr, final String type, final SimpleList<String> functions) {
		functions.sort();

		for(final String func : functions) {
			final boolean isMath = Lib_Function.isMathematicFunction(func);
			final StringBuilder sb = new StringBuilder();
			sb.append(type);
			sb.append(isMath ? ' ' : '.');
			sb.append(func);
			this.iOutputFunction(cr, sb.toString());
		}
	}

	/**
	 * @apiNote Search and show functions of a single type
	 * @implNote Multi type like '*' is no more allowed.
	 */
	private void iShowFunctions(final CallRuntime cr, final String searchLow, final String search) {
		String type = FilterString.beforeFirst('.', searchLow);
		final Group2<Boolean, String> filter = this.iFilter(searchLow.substring(type.length() + 1));

		if(!this.iCheckType(type))
			throw new RuntimeWarning(cr, "Invalid type name", "Got: " + search);

		try {
			final InputStream is2 = Lib_Jar.getStream(JMo_Help.JAR_HELP_TYPE_EXTENDS);
			final ArrayList<String> extendsLines = Lib_InputStream.readLines(is2, true);

			final ArrayList<String> types = new ArrayList<>();
//			types.add(type);
			String curType = type;

			while(curType.length() > 0) {
				boolean hit = false;

				for(final String line : extendsLines) {

					// Correct case: csv --> Csv
					if(line.toLowerCase().startsWith(type.toLowerCase() + '§')) {
						final String t = FilterString.beforeFirst('§', line);

						if(!t.equals(type))
							type = t;
//							types.add(type);
					}

					if(line.toLowerCase().startsWith(curType.toLowerCase() + '§')) {
						curType = FilterString.afterFirst('§', line);
						if(curType.length() > 0)
							types.add(curType);
						hit = true;
						break;
					}
				}

				if(!hit)
					break;
			}

			final InputStream is = Lib_Jar.getStream(JMo_Help.JAR_HELP_TYPE_FUNC);
			final ArrayList<String> funcLines = Lib_InputStream.readLines(is, true);

			final SimpleList<String> functionsSelf = new SimpleList<>();
			final SimpleList<String> functionsExt = new SimpleList<>();
			final SimpleList<String> functionsObj = new SimpleList<>();

			this.iAddFunctionsOfType(functionsSelf, funcLines, filter.o1, filter.o2, type);

			for(final String t : types)
				if(t.equals("Object"))
					this.iAddFunctionsOfType(functionsObj, funcLines, filter.o1, filter.o2, t);
				else
					this.iAddFunctionsOfType(functionsExt, funcLines, filter.o1, filter.o2, t);


			// --- Output ---
			this.iShowFunctions(cr, type, functionsSelf);

			if(functionsSelf.size() > 0 && functionsExt.size() > 0)
				Lib_Output.out(cr, "----- Abstract -------------------------", true);

			this.iShowFunctions(cr, type, functionsExt);

			if((functionsSelf.size() > 0 || functionsExt.size() > 0) && functionsObj.size() > 0)
				Lib_Output.out(cr, "----- Object ---------------------------", true);

			this.iShowFunctions(cr, type, functionsObj);
		}
		catch(final Err_FileSys e) {
			throw new ExternalError(cr, e.getMessage(), search); // "Internal file read error",
		}
	}

	private void iShowTypeDetailed(final CallRuntime cr, final String s) {
		cr.getStrict().checkSandbox(cr, "jaymo.allTypes"); // TODO zentral 	// Prüfen!!!
		final SimpleList<String> names = ObjectManager.allTypes(cr);

		String data = null;
		final String search = s.toLowerCase();

		for(final String name : names)
			if(name.toLowerCase().startsWith(s)) {
				data = name;
				break;
			}

		if(data == null) {
			this.iShowTypes(cr, search);
			return;
		}
		// ---------------------------

		final String typeName = FilterString.beforeFirst('(', data);
		final Class<?> clazz = ObjectManager.searchJavaJMoClass(data);
//		MOut.print(clazz);

//		String name = Lib_Type.getName(clazz, null);

		final SimpleList<Class<?>> list = Lib_Type.getTypeClassesList(clazz);
		final SimpleList<String> types = new SimpleList<>(list.size());

		for(final Class<?> cl : list) {
			final String name = Lib_Type.getName(cl, null);
			if(!name.equals(typeName))
				types.add(name);
		}
		final String extendings = ConvertSequence.toString(" -> ", types);

		final StringBuilder sb = new StringBuilder();
		sb.append("= ");
		sb.append(typeName);
		sb.append(" =");
		sb.append('\n');
		sb.append("-> ");
		sb.append(extendings);

		// TODO: Es wird eine eigene Text-Datei benötigt ... mit "Type§Extends!Attributes#Description'


		Lib_Output.out(cr, sb.toString(), true);
	}

	private void iShowTypes(final CallRuntime cr, final String search) {
		cr.getStrict().checkSandbox(cr, "jaymo.allTypes"); // TODO zentral
		final Group2<Boolean, String> filter = this.iFilter(search);

		final SimpleList<String> names = ObjectManager.allTypes(cr);

		for(final String name : names)
			if(filter.o1) {
				if(name.toLowerCase().startsWith(filter.o2))
					Lib_Output.out(cr, name, true);
			}
			else if(name.toLowerCase().contains(filter.o2))
				Lib_Output.out(cr, name, true);
	}

	/**
	 * °explain(Str s)Same # Detailed help about types, constructors, functions and function arguments.
	 */
	private void mExplain(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String s = Lib_Convert.getStringValue(cr, arg).toLowerCase();

		// Method
		if(s.indexOf('.') > -1)
			this.iShowFunctionDetailed(cr, s); // TODO What about +-*/!& ... ?
		// Constructor
		else if(s.indexOf('(') > -1)
			this.iShowConstructors(cr, s);
		// Type
		else
			this.iShowTypeDetailed(cr, s);
	}

	/**
	 * °quick ^ quickly
	 * °quickly(Str s)Same # Quick help about types, constructors, functions and function arguments.
	 */
	private void mQuickly(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String search = Lib_Convert.getStringValue(cr, arg);
		final String lower = search.toLowerCase();

		// Method
		if(lower.indexOf('.') > -1)
			this.iShowFunctions(cr, lower, search); // TODO What about +-*/!& ... ?
		// Constructor
		else if(lower.indexOf('(') > -1)
			this.iShowConstructors(cr, lower);
		// Type
		else
			this.iShowTypes(cr, lower);
	}

}
