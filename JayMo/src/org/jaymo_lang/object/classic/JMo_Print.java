/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.VarArgsCallBuffer;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_HasConstructor;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Output;


/**
 * @author Michael Nitsche
 * @created 12.12.2020
 */
public class JMo_Print extends A_Object implements I_ControlObject, I_HasConstructor {

	final VarArgsCallBuffer buffer;


	public JMo_Print() {
		this.buffer = null;
	}

	/**
	 * +Print(Object values...)
	 */
	public JMo_Print(final Call... calls) {
		this.buffer = new VarArgsCallBuffer(calls);
	}

	public Return constructor(final CallRuntime cr) {

		if(this.buffer != null) {
			final I_Object[] args = this.buffer.get();
//			Lib_Error.ifArgs(args.length, 1, null, cr, this); // args < 1 = Impossible

			for(final I_Object arg : args)
				//				Lib_Output.out(cr, Lib_Convert.getValue(cr, args[i]), i == len - 1);
				Lib_Output.out(cr, Lib_Convert.getValue(cr, arg), true);
		}
		else
			Lib_Output.out(cr.getApp(), "", true);
		// Empty new line

		return null;
	}

	@Override
	public void init(final CallRuntime cr) {
		if(this.buffer != null)
			this.buffer.init(cr, this);
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		return null;
	}

}
