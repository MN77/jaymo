/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.I_AutoBlockList;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Count;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 29.03.2018
 * @implNote Using Count instead of Counter, because count is a verb.
 */
public class JMo_Count extends A_Object implements I_AutoBlockDo, I_AutoBlockList, I_ControlObject {

	private final ArgCallBuffer start, end, step;


	/**
	 * !Counter-Object, that counts from start to end, maybe with stepping.
	 * +Count( Number start, Number end ) # Count( 1, 9 )
	 * +Count( Number start, Number end, Number step ) # Count( 1, 9, 2 )
	 * +Count( Char start, Char end ) # Count( 'a', 'e' )
	 * +Count( Char start, Char end, IntNumber step ) # Count( 'a', 'k' ,2 )
	 * +{ Number start, Number end } ^ Count( Number start, Number end ).each # { 3, 9 }.print
	 * +{ Number start, Number end, Number step } ^ Count( Number start, Number end, Number step ).each # { 19, 4, -3 }.print
	 * +{ Char start, Char end } ^ Count( Char start, Char end }.each # { 'a', 'z' }.print
	 * +{ Char start, Char end, IntNumber step } ^ Count( Char start, Char end, IntNumber step ).each # { 'a', 'k', 3 }.print
	 * %.each
	 * 'Count( 4 ).each.print
	 * '
	 * 'Count( 2, 5 )
	 * ' it.print
	 * '
	 * 'Count( 3, 9, 2 )
	 * ' it.print
	 * '
	 * '{ 7 }
	 * ' %.print
	 * '
	 * '{ 3, 8 }
	 * ' %.print
	 */
	public JMo_Count(final Call end_or_range) {
		this(end_or_range, null, null);
	}

	public JMo_Count(final Call start, final Call end) {
		this(start, end, null);
	}

	public JMo_Count(final Call start, final Call end, final Call step) {
		this.start = new ArgCallBuffer(0, start);
		this.end = end == null ? null : new ArgCallBuffer(1, end);
		this.step = step == null ? null : new ArgCallBuffer(2, step);
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.iEach(cr, false, true).obj;
	}

	@Override
	public SimpleList<I_Object> autoBlockToList(final CallRuntime cr) {
		return ((JMo_List)this.iEach(cr, true, false).obj).getInternalCollection();
	}

	@Override
	public void init(final CallRuntime cr) {
		this.start.initExt(cr, this, Lib_Count.typesStart);
		if(this.end != null)
			this.end.initExt(cr, this, Lib_Count.typesEnd);
		if(this.step != null)
			this.step.init(cr, this, A_Number.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		String arg = this.start.toString(cr, STYPE.NESTED);
		if(this.end != null)
			arg += "," + this.end.toString(cr, STYPE.NESTED);
		if(this.step != null)
			arg += "," + this.step.toString(cr, STYPE.NESTED);
		return "Count(" + arg + ")";
	}


	// --- Functions ---

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "each":
				cr.argsNone();
				return this.iEach(cr, false, true);
			case "toList":
				cr.argsNone();
				return this.iEach(cr, true, true);

			default:
				return null;
		}
	}

	/**
	 * °each()%Atomic # Execute block and stream with every value.
	 * °toList()List # Create a new List with all values
	 **/
	private ObjectCallResult iEach(final CallRuntime cr, final boolean toList, final boolean execBlockStream) {
		return Lib_Count.each(cr, this, this.start, this.end, this.step, toList, execBlockStream);
	}

}
