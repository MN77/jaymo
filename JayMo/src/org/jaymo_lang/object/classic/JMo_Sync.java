/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 19.10.2022
 */
public class JMo_Sync extends A_Object implements I_ControlObject, I_AutoBlockDo {

	/**
	 * +Sync()
	 */
	public JMo_Sync() {}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mRun(cr).obj;
	}

	@Override
	public void init(final CallRuntime cr) {}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "run":
				return this.mRun(cr);
		}
		return null;
	}

	/**
	 * °run()%Object # Synchronized execution of block and stream.
	 */
	private ObjectCallResult mRun(final CallRuntime cr) {
		cr.argsNone();
		return this.mSync(cr);
	}

}
