/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 02.11.2019
 */
public class JMo_Repeat extends A_Object implements I_AutoBlockDo, I_ControlObject {

	private final ArgCallBuffer value;


	/**
	 * !Repeat a block or stream
	 * +Repeat(Bool b) # Repeat(true)
	 * %.cycle
	 */
	public JMo_Repeat(final Call c) {
		Err.ifNull(c);
		this.value = new ArgCallBuffer(0, c);
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mRepeat(cr).obj;
	}

	@Override
	public void init(final CallRuntime cr) {
		this.value.init(cr, this, Bool.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return "Repeat(" + this.value.toString(cr, type.getNested()) + ')';
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
//			case "do": //Override .do
			case "each":
				return this.mRepeat(cr);

			default:
				return null;
		}
	}

	/**
	 * °each()%Object # Execute block and stream. After that, if the argument is true, repeat and execute again.
	 */
	private ObjectCallResult mRepeat(final CallRuntime crOld) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			return new ObjectCallResult(this, true);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;
		Boolean b = null;

		do {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, Bool.TRUE);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
			b = Lib_Convert.getBoolValue(crNew, this.value.getInitLoop(crNew, this, Bool.class));
		}
		while(b);
//		return new Result_Obj(result, true);
		return new ObjectCallResult(Bool.FALSE, true);
	}

}
