/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.pseudo.FuncLet;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.BLOCKED;
import org.jaymo_lang.util.Lib_Comply;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.data.constant.CONDITION;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 08.02.2020
 */
public class JMo_FunctionMap extends A_Object { //implements I_AutoBlockDo {

	private static final String[] BLOCKED_FUNCTION_NAMES = {"add", "sub", "remove", "clear", "copy", "size", "len", "length",
		"get", "set", "isEmpty", "keys", "objects", "hasKey", "knowsKey", "hasObject", "knowsObject"};

	private final SimpleList<String>   keys;
	private final SimpleList<I_Object> objects;


	public JMo_FunctionMap() {
		this(null, new SimpleList<>(), new SimpleList<>());
	}

	public JMo_FunctionMap(final CallRuntime cr, final SimpleList<String> keys, final SimpleList<I_Object> objects) {
		if(keys.size() != objects.size())
			Err.direct("Keys and objects have different sizes!", keys.size(), objects.size());

		this.keys = keys;
		this.objects = objects;

		if(keys.size() > 1)
			for(final String key : keys)
				this.iValidateKey(cr, key, false);
	}

	@Override
	public void init(final CallRuntime cr) {}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();

		if(type == STYPE.DESCRIBE) {
//			sb.append("FunctionMap(");

			for(int i = 0; i < this.keys.size(); i++) {
				String sk = '"' + this.keys.get(i) + '"';
				sk = Lib_Output.indentLines(sk, false);
				sb.append(sk);

				sb.append(" -> "); // => is let to the right, : is get

				String sv = this.objects.get(i).toString(cr, STYPE.DESCRIBE); // left+1  .trim()
				sv = Lib_Output.indentLines(sv, false);
				sb.append(sv);

				sb.append('\n');
			}
			Lib_Output.removeEnd(sb, '\n');
		}
		else {
			sb.append("FunctionMap[");

			for(int i = 0; i < this.keys.size(); i++) {
				if(i > 0)
					sb.append(',');
				sb.append('"' + this.keys.get(i).toString() + '"');
//				sb.append("=>");
//				sb.append(Lib_Output.toString(this.objects.get(i), true));
			}
			sb.append("]");
		}
		return sb.toString();
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		// , boolean alsParameter

		switch(method) {
			case "add":
				return A_Object.stdResult(this.mAdd(cr));
			case "sub":
			case "remove":
				return A_Object.stdResult(this.mRemove(cr));
			case "clear":
				return A_Object.stdResult(this.mClear(cr));

			case "copy":
				return A_Object.stdResult(this.mCopy(cr));
//			case "getSize":
//			case "getLen":
			case "len":
			case "length":
				return A_Object.stdResult(this.mLength(cr));
			case "get":
				return A_Object.stdResult(this.mGet(cr));
			case "set":
				return A_Object.stdResult(this.mSet(cr));
			case "select":
				return A_Object.stdResult(this.mSelect(cr));
			// case "each": return each(c);
			case "isEmpty":
				return A_Object.stdResult(this.mIsEmpty(cr));
			case "keys":
				return A_Object.stdResult(this.mKeys(cr));
			case "objects":
				return A_Object.stdResult(this.mObjects(cr));

			case "hasKey":
			case "containsKey":
				return A_Object.stdResult(this.mContainsKey(cr));
			case "hasObject":
			case "containsObject":
				return A_Object.stdResult(this.mContainsObject(cr));

			default:
				if(!cr.getApp().strict.isValid_ShortenMethod() || method.length() < 3) { //TODO! < 3 ???
					final int index = this.keys.indexOf(method);
					if(index != -1)
						return this.iExec(cr, this.objects.get(index));
				}
				else {
					// first clear hit
					int hit = -1;
					for(int i = 0; i < this.keys.size(); i++)
						if(this.keys.get(i).startsWith(method))
							if(hit == -1)
								hit = i;
							else
								throw new CodeError(cr, "Unclear function name", "Got: " + method);

					if(method.length() < 3 && this.keys.get(hit).length() != method.length()) //TODO! < 3 ???
						throw new CodeError(cr, "To short function name", "Got: " + method);

					if(hit != -1)
						return this.iExec(cr, this.objects.get(hit));
				}

				return null;
		}
	}

	private ObjectCallResult iExec(final CallRuntime cr, I_Object o) {

		if(o instanceof FuncLet) {
			final FuncLet fl = (FuncLet)o;
			o = fl.exec(cr);
		}
		return A_Object.stdResult(o);
	}

	private int iGetIndex(final CallRuntime cr, final String key, final boolean error) {
		final int index = this.keys.indexOf(key);
		if(index >= 0)
			return index;

		if(error)
			throw new RuntimeError(cr, "Unknown keyname", "Got: " + key);
		else
			return -1;
	}

	private void iValidateKey(final CallRuntime cr, final String key, final boolean check_doubles) {
		// Malformed names
		if(!Lib_Comply.checkFunctionName(key, CONDITION.MAYBE))
			throw new RuntimeError(cr, "Malformed keyname", "Got: " + key);

		// Check forbidden Names
		for(final String blocked : BLOCKED.FUNCTIONS)
			if(key.startsWith(blocked))
				throw new RuntimeError(cr, "Forbidden keyname", "Blocked: " + blocked + ", got: " + key);
		for(final String blocked : JMo_FunctionMap.BLOCKED_FUNCTION_NAMES)
			if(key.startsWith(blocked))
				throw new RuntimeError(cr, "Forbidden keyname", "Blocked: " + blocked + ", got: " + key);

		if(check_doubles) {
			// Don't add doubles
			final int index = this.keys.indexOf(key);
			if(index != -1)
				throw new RuntimeError(cr, "Duplicated keyname", "Got: " + key);
		}
	}

	/**
	 * °add(Str key, Object value)Same # Add a single entry.
	 */
	private JMo_FunctionMap mAdd(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, Str.class, I_Object.class);
		final Str key = (Str)oa[0];
		final I_Object object = oa[1];
		final String name = key.getValue();

		this.iValidateKey(cr, name, true);

		this.keys.add(name);
		this.objects.add(object);
		return this;
	}

	/**
	 * °clear()Same # Remove all items in this map.
	 */
	private JMo_FunctionMap mClear(final CallRuntime cr) {
		cr.argsNone();
		this.keys.clear();
		this.objects.clear();
		return this;
	}

	/**
	 * °hasKey ^ containsKey
	 * °containsKey(Str key)Bool # Returns true, if the map contains this key.
	 */
	private Bool mContainsKey(final CallRuntime cr) {
		final Str search = (Str)cr.args(this, Str.class)[0];
		final int idx = this.iGetIndex(cr, search.getValue(), false);
		return idx == -1 ? Bool.FALSE : Bool.TRUE;
	}

	/**
	 * °hasObject ^ containsObject
	 * °containsObject(Object o)Bool # Returns true, if the map contains the object.
	 */
	private Bool mContainsObject(final CallRuntime cr) {
		final I_Object search = cr.args(this, I_Object.class)[0];
		for(final I_Object o : this.objects)
			if(o.equals(search))
				return Bool.TRUE;
		return Bool.FALSE;
	}

	/**
	 * °copy()FunctionMap # Create a copy of this FunctionMap.
	 */
	private JMo_FunctionMap mCopy(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<String> copyKeys = this.keys.copy();
		final SimpleList<I_Object> copyObjects = this.objects.copy();
		return new JMo_FunctionMap(cr, copyKeys, copyObjects);
	}

	/**
	 * °get(Str key)Object # Get the value linked to 'key'.
	 */
	private I_Object mGet(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final Str key = (Str)cr.argType(arg, Str.class);
		final int idx = this.iGetIndex(cr, key.getValue(), true);
		return this.objects.get(idx);
	}

	/**
	 * °isEmpty()Bool # Returns true if this map is empty.
	 */
	private Bool mIsEmpty(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this.keys.size() == 0);
	}

	/**
	 * °keys()List # Returns a new List with all keys.
	 */
	private JMo_List mKeys(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> clone = new SimpleList<>(this.keys.size());
		for(final String s : this.keys)
			clone.add(new Str(s));
		return new JMo_List(clone);
	}

	/**
	 * °len ^ length
	 * °length()Int # Returns the size of this map.
	 */
	private Int mLength(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.keys.size());
	}

	/**
	 * °objects()List # Returns a new List with all objects.
	 */
	private JMo_List mObjects(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> copy = this.objects.copy();
		return new JMo_List(copy);
	}

	// --- Private ---

	/**
	 * °sub^remove
	 * °remove(Str key)Same # Remove a single item.
	 */
	private JMo_FunctionMap mRemove(final CallRuntime cr) {
		final Str key = (Str)cr.args(this, Str.class)[0];
		final int i = this.iGetIndex(cr, key.getValue(), true);
		this.keys.remove(i);
		this.objects.remove(i);
		return this;
	}

	/**
	 * °select(Str key...)List # Get the values of these keys.
	 */
	private I_Object mSelect(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 0, 0);
		final SimpleList<I_Object> result = new SimpleList<>(oa.length);

		for(final I_Object key : oa) {
			final Str skey = (Str)cr.argType(key, Str.class);
			final int idx = this.iGetIndex(cr, skey.getValue(), true);
			final I_Object g = this.objects.get(idx);
			result.add(g);
		}
		return new JMo_List(result);
	}

	/**
	 * °set(Str key, Object o)Same # Set object 'o' for the already added key 'key'.
	 */
	private JMo_FunctionMap mSet(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, Str.class, I_Object.class);
//		MOut.dev("set:", toString(), oa);
		final Str key = (Str)oa[0];
		final I_Object value = oa[1];
		final int i = this.iGetIndex(cr, key.getValue(), true);
		this.objects.set(i, value);
		return this;
	}

}
