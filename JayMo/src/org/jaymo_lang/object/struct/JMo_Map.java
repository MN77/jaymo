/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import java.util.HashMap;
import java.util.List;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ReturnException;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.VarArgsCallBuffer;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_KeyValue;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.VarLet;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.type.Lib_Compare;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_Map extends A_Sequence implements I_AutoBlockDo {

	private final SimpleList<I_Object> keys, values;
	private final VarArgsCallBuffer    init;
	private boolean                    fixedLength     = false;
	private boolean                    fixedValueTypes = false;


	/**
	 * +Map()
	 * +Map(Object o1, Object o2, ...) # Map( "abc"-> 3, "def"-> 5 )
	 * %.each
	 */
	public JMo_Map() {
		this.keys = new SimpleList<>();
		this.values = new SimpleList<>();
		this.init = null;
	}

	public JMo_Map(final Call... items) {
		this.keys = new SimpleList<>();
		this.values = new SimpleList<>();
		this.init = new VarArgsCallBuffer(items);
	}

	public JMo_Map(final SimpleList<I_Object> keys, final SimpleList<I_Object> objects) {
		this.keys = keys;
		this.values = objects;
		this.init = null;
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mEach(cr).obj;
	}

	public HashMap<I_Object, I_Object> copyToHashMap() {
		final HashMap<I_Object, I_Object> result = new HashMap<>();
		for(int i = 0; i < this.keys.size(); i++)
			result.put(this.keys.get(i), this.values.get(i));
		return result;
	}

	public HashMap<String, I_Object> copyToStringMap(final CallRuntime cr) {
		final HashMap<String, I_Object> result = new HashMap<>();
		for(int i = 0; i < this.keys.size(); i++)
			result.put(Lib_Convert.getStringValue(cr, this.keys.get(i)), this.values.get(i));
		return result;
	}

	public ArrayTable<I_Object> copyToTable() {
		final ArrayTable<I_Object> tab = new ArrayTable<>(2);
		for(int i = 0; i < this.keys.size(); i++)
			tab.addRow(this.keys.get(i), this.values.get(i));
		return tab;
	}

	@Override
	public boolean equals(final Object obj) {

		if(obj instanceof JMo_Map) {
			// Keys
			final SimpleList<I_Object> otherKeys = ((JMo_Map)obj).keys;
			if(this.keys.size() != otherKeys.size())
				return false;
			for(int i = 0; i < this.keys.size(); i++)
				if(!this.keys.get(i).equals(otherKeys.get(i)))
					return false;

			// Values
			final SimpleList<I_Object> otherValues = ((JMo_Map)obj).values;
			if(this.values.size() != otherValues.size())
				return false;
			for(int i = 0; i < this.values.size(); i++)
				if(!this.values.get(i).equals(otherValues.get(i)))
					return false;

			return true;
		}
		else
			return false;
	}

	@Override
	public boolean equalsLazy(final Object obj) {
		return this.equals(obj);
	}

	@Override
	public I_Series<? extends I_Object> getInternalCollection() {
		throw Err.impossible();
//		SimpleList<I_Object> al = new SimpleList<>();
//		for(int i=0; i<this.keys.size(); i++) {
//			JMo_KeyValue kv = new JMo_KeyValue(this.keys.get(i), this.values.get(i));
//			al.add(kv);
//		}
//		return al;
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.init != null) {
			final I_Object[] ios = this.init.init(cr, this);
			this.keys.ensureCapacity(ios.length);
			this.values.ensureCapacity(ios.length);

			for(int i = 0; i < ios.length; i++) {
				final JMo_KeyValue kv = (JMo_KeyValue)cr.instanceArgType(i, ios[i], JMo_KeyValue.class);
				this.keys.add(kv.getKey());
				this.values.add(kv.getValue());
			}
		}
	}

	/**
	 * No double check!!!
	 */
	public void internalAdd(final I_Object key, final I_Object value) {
		if(this.fixedLength)
			Err.forbidden(key, value);

		this.keys.add(key);
		this.values.add(value);
	}

	public List<I_Object> internalGetKeys() {
		return this.keys;
	}

	public List<I_Object> internalGetValues() {
		return this.values;
	}

	@Override
	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
		final I_Object key = cr.argType(keys[offset], I_Object.class);
		if(!this.keys.contains(key))
			if(lazy)
				return Nil.NIL;
			else
				throw new RuntimeError(cr, "Unknown key for Map.", "Key is missing! Got: " + key);

		if(offset == keys.length - 1) {
			final int pos = this.iGetIndex(cr, key, true);
			return this.values.get(pos);
		}
		else {
			final int pos = this.iGetIndex(cr, key, true);
			final I_Object deeper = this.values.get(pos);
			if(!(deeper instanceof A_Sequence))
				throw new RuntimeError(cr, "Invalid type of value.", "Value to key " + key.toString(cr, STYPE.IDENT) + " isn't a sequence, so I can't go into it!");
			final A_Sequence deeper2 = (A_Sequence)deeper;
			return deeper2.sequenceDeepGet(cr, keys, offset + 1, lazy);
		}
	}

	@Override
	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {
		final I_Object key = cr.argType(keys[offset], I_Object.class);
		int idx = this.iGetIndex(cr, key, !lazy);

		if(offset == keys.length - 1) {
			if(idx == -1)
				if(lazy) {
					this.iFixedLength(cr);
					this.keys.add(key);
					this.values.add(value);
					return;
				}
				else
					throw new RuntimeError(cr, "Unknown key for Map.", "Key is missing! Got: " + key);

			this.values.set(idx, value);
		}
		else {
			if(idx == -1)
				if(lazy) {
					this.iFixedLength(cr);
					this.keys.add(key);
					this.values.add(new JMo_Map());
					idx = this.values.size() - 1;
				}
				else
					throw new RuntimeError(cr, "Unknown key for Map.", "Key is missing! Got: " + key);
			final I_Object deeper = this.values.get(idx);
			if(!(deeper instanceof A_Sequence))
				throw new RuntimeError(cr, "Invalid type of value.", "Value to key " + key.toString(cr, STYPE.IDENT) + " isn't a sequence, so I can't go into it!");
			final A_Sequence deeper2 = (A_Sequence)deeper;
			deeper2.sequenceDeepSet(cr, keys, offset + 1, value, lazy);
		}
	}

	@Override
	public String toString() {
		return this.getTypeName() + '<' + this.keys.size() + '>';
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case IDENT:
			case NESTED:
				return this.toString();
			case REGULAR:
				final StringBuilder sb = new StringBuilder();
				int maxKeyLen = 0;
				for(final I_Object key : this.keys)
					maxKeyLen = Math.max(maxKeyLen, key.toString().length());

				for(int i = 0; i < this.keys.size(); i++) {
					final String key = this.keys.get(i).toString();
					sb.append(FormString.length(maxKeyLen, key, false));
					sb.append(" -> ");
					final I_Object val = this.values.get(i);

					sb.append(val == this ? A_Sequence.CYCLE_IDENT : val.toString(cr, STYPE.NESTED));
					// Go deep:
//					String vs = Lib_Output.indentLines(val.toString(), false, true);
//					sb.append( vs );

					sb.append('\n');
				}

				Lib_Output.removeEnd(sb, '\n');
				return sb.toString();
			default:
				final StringBuilder sb2 = new StringBuilder();

				for(int i = 0; i < this.keys.size(); i++) {
					String sk = this.keys.get(i).toString(cr, STYPE.DESCRIBE);
					sk = Lib_Output.indentLines(sk, false);
					sb2.append(sk);
					sb2.append(" -> "); // "=>" is let to the right // ":" is get

					final I_Object ov = this.values.get(i);
					String sv = ov == this ? A_Sequence.CYCLE_IDENT : ov.toString(cr, STYPE.DESCRIBE);
					sv = Lib_Output.indentLines(sv, true);
					sb2.append(sv);

					sb2.append('\n');
				}

				Lib_Output.removeEnd(sb2, '\n');
				return sb2.toString();
		}
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {
		// , boolean alsParameter

		switch(method) {
			case "add":
				return A_Object.stdResult(this.mAdd(cr));
			case "addAll":
				return A_Object.stdResult(this.mAddAll(cr));
			case "sub":
			case "remove":
				return A_Object.stdResult(this.mRemove(cr));
			case "clear":
				return A_Object.stdResult(this.mClear(cr));

			case "count":
				return A_Object.stdResult(this.mIncDec(cr, true, true));
			case "inc":
				return A_Object.stdResult(this.mIncDec(cr, false, true));
			case "dec":
				return A_Object.stdResult(this.mIncDec(cr, false, false));

			case "each":
				return this.mEach(cr);

			case "keys":
				return A_Object.stdResult(this.mKeys(cr));
			case "values":
				return A_Object.stdResult(this.mValues(cr));

			case "hasKey":
			case "containsKey":
				return A_Object.stdResult(this.mContainsKey(cr));
			case "hasValue":
			case "containsValue":
				return A_Object.stdResult(this.mContainsValue(cr));

			case "toList":
				return A_Object.stdResult(this.mToList(cr));
			case "toTable":
				return A_Object.stdResult(this.mToTable(cr));

			/**
			 * °fixLength()Same # Fix the lenght of this List
			 * °fixTypes()Same # Fix the type of each position in this List
			 * °fix()Same # Fix length and types of this List
			 */
			case "fixLength":
				cr.argsNone();
				this.fixedLength = true;
				return A_Object.stdResult(this);
			case "fixTypes":
				cr.argsNone();
				this.fixedValueTypes = true;
				return A_Object.stdResult(this);
			case "fix":
				this.fixedLength = true;
				this.fixedValueTypes = true;
				return A_Object.stdResult(this);

			case "sort":
				return A_Object.stdResult(this.mSort(cr, method));

			default:
				return null;
		}
	}

	@Override
	protected JMo_Map copy(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> copyKeys = this.keys.copy();
		final SimpleList<I_Object> copyValues = this.values.copy();
		return new JMo_Map(copyKeys, copyValues); // Don't copy locks
	}

	@Override
	protected JMo_KeyValue getFirst(final CallRuntime cr) {
		return new JMo_KeyValue(this.keys.get(0), this.values.get(0));
	}

	@Override
	protected JMo_KeyValue getLast(final CallRuntime cr) {
		final int idx = this.keys.size() - 1;
		return new JMo_KeyValue(this.keys.get(idx), this.values.get(idx));
	}

	@Override
	protected void mSequenceSetPut(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 2, 1); //.args(this, I_Object.class, I_Object.class);	//parsVarArgs

		if(oa.length > 2) {
			this.sequenceDeepSet(cr, oa, 1, oa[0], lazy);
			return;
		}
		this.sequenceSet(cr, oa[1], oa[0], lazy);
	}

	@Override
	protected void sequenceContent(final CallRuntime cr, final I_Object[] content) {
		this.keys.clear();
		this.values.clear();

		for(final I_Object element : content) {
			final JMo_KeyValue kv = (JMo_KeyValue)cr.argType(element, JMo_KeyValue.class);
			this.iAdd(cr, kv.getKey(), kv.getValue());
		}
	}

	@Override
	protected boolean sequenceEmpty() {
		return this.keys.isEmpty();
	}

	@Override
	protected I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, final I_Object key) {
		final int i = this.iGetIndex(cr, key, !lazy);
		return lazy && i == -1 ? Nil.NIL : this.values.get(i);
	}

	@Override
	protected I_Object sequenceSelectGet(final CallRuntime cr, final I_Object key, final boolean lazy) {
		final int i = this.iGetIndex(cr, key, false);
		return lazy && i == -1 ? Nil.NIL : this.values.get(i);
	}

	@Override
	protected void sequenceSet(final CallRuntime cr, final I_Object key, I_Object value, final boolean lazy) {
		final int pos = this.iGetIndex(cr, key, !lazy);

		if(lazy && pos < 0)
			this.iAdd(cr, key, value);
		else {

			if(this.fixedValueTypes) {
				final I_Object curo = this.values.get(pos);
				if(!(curo == Nil.NIL))
					value = Lib_Type.typeCheck(cr, value, Lib_Type.getName(value), Lib_Type.getName(curo), "value");
			}
			this.values.set(pos, value);
		}
	}

	@Override
	protected int sequenceSize() {
		return this.keys.size();
	}

	private void iAdd(final CallRuntime cr, final I_Object key, final I_Object value) {
		this.iFixedLength(cr);
		// Don't add doubles
		final int i = this.iGetIndex(cr, key, false);
		if(i != -1)
			throw new RuntimeError(cr, "Duplicated Key", "Got: " + key);

		this.keys.add(key);
		this.values.add(value);
	}

	private void iFixedLength(final CallRuntime cr) {
		if(this.fixedLength)
			throw new RuntimeError(cr, "Can't change size of fixed Map", "Adding/Removing elements is not allowed!");
	}

	/**
	 * If the key exists, return the index. Otherwise return -1
	 */
	private int iGetIndex(final CallRuntime cr, final I_Object key, final boolean error) {
		final String keyString = Lib_Convert.getStringValue(cr, key);

		for(int i = 0; i < this.keys.size(); i++) {
			final I_Object k = this.keys.get(i);

			if(k instanceof A_Immutable) {
				if(k.equals(key) || key instanceof Str && keyString.equals(Lib_Convert.getStringValue(cr, k))) // Special case for ':'-get: "m:123.print"
					return i;
			}
			else if(k.hashCode() == key.hashCode())
				return i;
//			if( this.keys.get(i).toString().equals(key.toString()) )
//				return i;
		}
		if(error)
			throw new RuntimeError(cr, "Unknown key", "Got: " + key);
		else
			return -1;
	}

	/**
	 * °add(KeyValue kv) Same # Add a new entry to this Map.
	 * °add(Object key, Object value) Same # Add a new entry to this Map.
	 */
	private JMo_Map mAdd(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 1, 2);
		this.iFixedLength(cr);

		if(args.length == 1) {
			final JMo_KeyValue kv = (JMo_KeyValue)cr.argType(args[0], JMo_KeyValue.class);
			this.iAdd(cr, kv.getKey(), kv.getValue());
		}
		else
			this.iAdd(cr, args[0], args[1]);
		return this;
	}

	/**
	 * °addAll(KeyValue kv...)Same # Add all KeyValue's.
	 */
	private JMo_Map mAddAll(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		this.iFixedLength(cr);

		for(final I_Object arg : args) {
			final JMo_KeyValue kv = (JMo_KeyValue)cr.argType(arg, JMo_KeyValue.class);
			this.iAdd(cr, kv.getKey(), kv.getValue());
		}
		return this;
	}

	/**
	 * °clear()Same # Remove all keys and values from this Map.
	 */
	private JMo_Map mClear(final CallRuntime cr) {
		cr.argsNone();
		this.iFixedLength(cr);
		this.keys.clear();
		this.values.clear();
		return this;
	}

	/**
	 * °hasKey ^ containsKey
	 * °containsKey(Object o)Bool # Returns true, if the Map contains the given object as key.
	 */
	private Bool mContainsKey(final CallRuntime cr) {
		final I_Object search = cr.args(this, I_Object.class)[0];
		final int knows = this.iGetIndex(cr, search, false);
		return knows == -1 ? Bool.FALSE : Bool.TRUE;
	}

	/**
	 * °hasValue ^ containsValue
	 * °containsValue(Object o)Bool # Returns true, if the Map contains the given object as value.
	 */
	private Bool mContainsValue(final CallRuntime cr) {
		final I_Object search = cr.args(this, I_Object.class)[0];
		for(final I_Object o : this.values)
			if(o.equals(search)) // TODO wirklich equals?!?
				return Bool.TRUE;
		return Bool.FALSE;
	}

	/**
	 * °each()%KeyValue # Execute block and stream for every KeyValue.
	 * °each(VarLet var)%KeyValue # Execute block and stream for every KeyValue and assign it to 'var'.
	 */
	private ObjectCallResult mEach(final CallRuntime crOld) {
		final I_Object[] args = crOld.argsFlex(this, 0, 1);
		final boolean varlet = args.length == 1;
		if(varlet)
			crOld.argType(args[0], VarLet.class);

		final Call stream = crOld.getStream();
		final Block block = crOld.getCallBlock();

		if(stream == null && block == null)
			throw new CodeError(crOld, "No Stream or Block for 'each'", null);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);

		I_Object res = this;

		for(int pos = 0; pos < this.keys.size(); pos++) {
			handle.startLap();
			final JMo_KeyValue item = new JMo_KeyValue(this.keys.get(pos), this.values.get(pos));

			if(varlet) {
				final Var o = ((VarLet)args[0]).getMem();
				o.let(crNew, crNew, item);
			}
			if(block != null)
				try {
					res = block.exec(crNew, item);
				}
				catch(final ReturnException e) {
					final Return temp = e.get();

					switch(temp.getLevel()) {
						case NEXT:
							continue; // res=temp.getResult();
						default:
							return temp.getLoopResult();
					}
				}
			if(stream != null && block == null)
				res = crNew.execInit(stream, item);
		}

		if(stream != null && block != null) {
			if(this.keys.size() == 0)
				Err.todo("Map is empty!");
			final int last = this.keys.size() - 1;
			final JMo_KeyValue item = new JMo_KeyValue(this.keys.get(last), this.values.get(last));
			res = crNew.execInit(stream, item);
		}
		return new ObjectCallResult(res, true);
	}

	/**
	 * °count(Object key)Same # Increment the value of the given key by 1, add keys that do not exist.
	 * °inc(Object key)Same # Increment the value of the given key by 1
	 * °dec(Object key)Same # Decrement the value of the given key by 1
	 */
	private JMo_Map mIncDec(final CallRuntime cr, final boolean lazy, final boolean inc) {
		final I_Object key = cr.args(this, I_Object.class)[0];
		final int idx = this.iGetIndex(cr, key, !lazy);

		final int add = inc ? 1 : -1;

		if(lazy && idx < 0)
			this.iAdd(cr, key, new Int(1));
		else {
			I_Object value = this.values.get(idx);

			switch(value.getTypeName()) {
				case "Byte":
				case "Short":
					if(this.fixedValueTypes)
						throw new RuntimeError(cr, "Fixed values", "Value can't be converted to Int for key: " + key);
				case "Int":
					value = new Int(Lib_Convert.getIntValue(cr, value) + add);
					break;
				case "Long":
					value = new JMo_Long(Lib_Convert.getLongValue(cr, value) + add);
					break;
				case "Dec":
					final Dec dec = Lib_Convert.getDecValue(cr, value);
					value = new JMo_Dec(inc ? dec.inc() : dec.dec());
					break;
				case "Float":
					if(this.fixedValueTypes)
						throw new RuntimeError(cr, "Fixed values", "Value can't be converted to Double for key: " + key);
					// Run through
				case "Double":
					value = new JMo_Double(Lib_Convert.getDoubleValue(cr, value) + add);
					break;
				default:
					throw new RuntimeError(cr, "Invalid value type", "For inc/dec the type of value must be a normal atomic number, but got: " + value.getTypeName());
			}
			this.values.set(idx, value);
		}
		return this;
	}

	/**
	 * °keys()List # Returns a new List with all keys of this map.
	 */
	private JMo_List mKeys(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> clone = this.keys.copy();
		return new JMo_List(clone);
	}

	/**
	 * °sub^remove
	 * °remove(Object key)Same # Remove a single key with the linked value.
	 */
	private JMo_Map mRemove(final CallRuntime cr) {
		final I_Object key = cr.args(this, I_Object.class)[0];
		this.iFixedLength(cr);
		final int i = this.iGetIndex(cr, key, true);
		this.keys.remove(i);
		this.values.remove(i);
		return this;
	}

	/**
	 * °sort()Same # Sort the map
	 * °sort(Bool each)Same # Sort this map by passing every pair as a List with KeyValue's to 'each' and fetch the boolean result.
	 * °sort(FuncLet fl)Same # Sort this map by passing every pair as a List with KeyValue's to 'fl' and fetch the boolean result.
	 * °sort(VarLet var, Bool b)Same # Sort this map by assign every pair as a List with KeyValue's to 'var' and fetch the result of 'b'.
	 */
	private JMo_Map mSort(final CallRuntime cr, final String method) {
		final int parCount = cr.argCount();
		Lib_Error.ifArgs(parCount, 0, 2, cr, this);

		I_Object buffer1 = null;
		I_Object buffer2 = null;

		final boolean auto = parCount == 0;

		if(auto)
			cr.argsNone();

		int smallest;

		for(int l = 1; l < this.keys.size(); l++) {
			smallest = l;

			for(int m = l + 1; m <= this.keys.size(); m++) {
				buffer1 = this.keys.get(smallest - 1);
				buffer2 = this.keys.get(m - 1);

				boolean okay = true;

				if(auto) {
					Object so1 = buffer1;
					Object so2 = buffer2;

					if(buffer1 instanceof I_Atomic)
						so1 = ((I_Atomic)buffer1).getValue();
					if(buffer2 instanceof I_Atomic)
						so2 = ((I_Atomic)buffer2).getValue();

					okay = Lib_Compare.isGreater(so2, so1);
				}
				else {
					final I_Object value1 = this.values.get(smallest - 1);
					final I_Object value2 = this.values.get(m - 1);

					final JMo_KeyValue kv1 = new JMo_KeyValue(buffer1, value1);
					final JMo_KeyValue kv2 = new JMo_KeyValue(buffer2, value2);

					final I_Object[] test = {kv1, kv2};
					final Bool correct = (Bool)cr.copyEach(method).argsEach(this, 0, test, Bool.class);
					okay = correct.getValue();
				}
				if(!okay)
					smallest = m;
			}

			if(l != smallest) {
				final int pos1 = smallest - 1;
				final int pos2 = l - 1;

				buffer1 = this.keys.get(pos1);
				buffer2 = this.keys.get(pos2);
				this.keys.set(pos1, buffer2);
				this.keys.set(pos2, buffer1);

				buffer1 = this.values.get(pos1);
				buffer2 = this.values.get(pos2);
				this.values.set(pos1, buffer2);
				this.values.set(pos2, buffer1);
			}
		}
		return this;
	}

	/**
	 * °toList()List # Convert the Map to a List.
	 */
	private I_Object mToList(final CallRuntime cr) {
		cr.argsNone();
		final int len = this.keys.size();
		final SimpleList<I_Object> list = new SimpleList<>(len);
		for(int i = 0; i < len; i++)
			list.add(new JMo_KeyValue(this.keys.get(i), this.values.get(i)));
		return new JMo_List(list);
	}

	/**
	 * °toTable()Table # Convert the Map to a Table.
	 */
	private I_Object mToTable(final CallRuntime cr) {
		cr.argsNone();
		final ArrayTable<I_Object> tab = new ArrayTable<>(2);
		final int len = this.keys.size();
		for(int i = 0; i < len; i++)
			tab.addRow(this.keys.get(i), this.values.get(i));
		return new JMo_Table(tab);
	}

	/**
	 * °values()List # Returns a new List with all values of this map.
	 */
	private JMo_List mValues(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> clone = this.values.copy();
		return new JMo_List(clone);
	}

}
