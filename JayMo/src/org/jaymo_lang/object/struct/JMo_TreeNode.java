/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import java.util.List;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.VarArgsCallBuffer;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.immute.JMo_KeyValue;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 *         26.10.2020 Erstellt
 */
public class JMo_TreeNode extends A_Sequence {

//	private final I_Set<JMo_TreeNode> nodes = new MSet();
	protected final SimpleList<JMo_TreeNode> nodes = new SimpleList<>();
	private I_Atomic                         name  = null;
	private I_Object                         value = null;
	private final VarArgsCallBuffer          callBuffer;


	public JMo_TreeNode(final Call... varArgs) {
		this.callBuffer = new VarArgsCallBuffer(varArgs);
	}

	public JMo_TreeNode(final I_Atomic name) {
		this.name = name;
		this.callBuffer = null;
	}

	public JMo_TreeNode(final I_Atomic name, final I_Object value) {
//		Err.ifNull(name); // name can be null = Root // value can also be null
		this.name = name;
		this.value = value;
		this.callBuffer = null;
	}

	@Override
	public boolean equals(final Object obj) {
		return this == obj;
	}

	// ... Debug

	@Override
	public boolean equalsLazy(final Object obj) {
		return this.equals(obj);
	}

	@Override
	public I_Series<? extends I_Object> getInternalCollection() {
		return this.nodes;
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.callBuffer != null) {
			final I_Object[] args = this.callBuffer.init(cr, this);
			Lib_Error.ifArgs(args.length, 2, null, cr, this);

			this.name = (I_Atomic)cr.argType(args[0], I_Atomic.class);
			this.value = cr.argType(args[1], null);

			for(int i = 2; i < args.length; i++) {
				final JMo_TreeNode node = (JMo_TreeNode)cr.argType(args[i], JMo_TreeNode.class);
				this.nodes.add(node);
			}
		}
	}

	public Group2<I_Atomic, I_Object> internalNameValue() {
		return new Group2<>(this.name, this.value);
	}

	public List<JMo_TreeNode> internalNodes() {
		return this.nodes;
	}

	@Override
	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
		JMo_TreeNode result = null;
		for(final JMo_TreeNode node : this.nodes)
			if(node.name.equals(keys[offset])) {
				result = node;
				break;
			}

		if(lazy && result == null) {
//			if(arg instanceof JMo_TreeNode)
//				this.nodes.add((JMo_TreeNode)arg);
//			else
			final A_Atomic apar = (A_Atomic)cr.argType(keys[offset], A_Atomic.class);
			final JMo_TreeNode node = new JMo_TreeNode(apar);
			this.nodes.add(node);
			result = node;
		}
		if(result == null)
			throw new RuntimeError(cr, "Node not found", "Can't find node with name: " + keys[0].toString());

		if(keys.length - offset == 1)
			return result;

		return result.sequenceDeepGet(cr, keys, offset + 1, lazy);
	}

	@Override
	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {

		if(keys.length - offset == 0) {
			this.value = value;
			return;
		}
		JMo_TreeNode found = null;
		for(final JMo_TreeNode node : this.nodes)
			if(node.name.equals(keys[offset]))
				found = node;

		if(lazy && found == null) {
//			if(arg instanceof JMo_TreeNode)
//				this.nodes.add((JMo_TreeNode)arg);
//			else
			final A_Atomic apar = (A_Atomic)cr.argType(keys[offset], A_Atomic.class);
			final JMo_TreeNode node = new JMo_TreeNode(apar);
			this.nodes.add(node);
			found = node;
		}
		if(found == null)
			throw new RuntimeError(cr, "Node not found", "Can't find node with name: " + keys[offset].toString());

		found.sequenceDeepSet(cr, keys, offset + 1, value, lazy);
	}

	@Override
	public String toString() {
		return this.name == null
			? "Tree<" + this.nodes.size() + '>'
//			: "TreeNode<"+this.name.toStringIdent()+','+this.value.toStringIdent()+','+this.nodes.size()+'>';
//			: "TreeNode<"+this.nodes.size()+'>';
			: "TreeNode[" + this.name.toString(null, STYPE.IDENT) + ']';
	}


//	--- Protected

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case NESTED:
			case IDENT:
				return this.toString();
			case REGULAR:
				final StringBuilder sb = new StringBuilder();
				this.iToString(cr, sb, 0);
				Lib_Output.removeEnd(sb, '\n');
				return sb.toString();
			case DESCRIBE:
				final StringBuilder sb2 = new StringBuilder();
				this.iToStringDescribe(sb2, cr, 0);
				Lib_Output.removeEnd(sb2, '\n');
				return sb2.toString();
			default:
				throw Err.impossible(type);
		}
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "getName":
				return A_Object.stdResult(this.mGetName(cr));
			case "setName":
				return A_Object.stdResult(this.mSetName(cr));
			case "getValue":
				return A_Object.stdResult(this.mGetValue(cr));
			case "setValue":
				return A_Object.stdResult(this.mSetValue(cr));
			case "+":
			case "add":
			case "addNode":
				return A_Object.stdResult(this.mAdd(cr));
			case "addNodes":
				return A_Object.stdResult(this.mAddNodes(cr));

			case "nodes":
				return A_Object.stdResult(this.mNodes(cr));

			case "delete":
				return A_Object.stdResult(this.mRemoveNodes(cr));
			case "--":
//			case "dec":
			case "remove":
			case "removeNames":
				return A_Object.stdResult(this.mRemoveNames(cr));
			case "removeValues":
				return A_Object.stdResult(this.mRemoveValues(cr));
		}
		return null;
	}

	@Override
	protected A_Sequence copy(final CallRuntime cr) {
		final JMo_TreeNode copy = new JMo_TreeNode(this.name, this.value);
		copy.nodes.addAll(this.nodes);
		return copy;
	}

	@Override
	protected I_Object getFirst(final CallRuntime cr) {
		return this.nodes.size() == 0 ? Nil.NIL : this.nodes.get(0);
	}

	@Override
	protected I_Object getLast(final CallRuntime cr) {
		return this.nodes.size() == 0 ? Nil.NIL : this.nodes.get(this.nodes.size() - 1);
	}

	@Override
	protected void mSequenceSetPut(final CallRuntime cr, final boolean lazy) {
		final I_Object[] args = cr.argsVar(this, 2, 1);

		if(args.length > 2) {
			this.sequenceDeepSet(cr, args, 1, args[0], lazy);
			return;
		}
		this.sequenceSet(cr, args[1], args[0], lazy);
	}

	@Override
	protected void sequenceContent(final CallRuntime cr, final I_Object[] values) {

		for(int i = 0; i < this.nodes.size(); i++) {
			final JMo_TreeNode node = (JMo_TreeNode)cr.argType(values[i], JMo_TreeNode.class);
			this.nodes.set(i, node);
		}
	}

	@Override
	protected boolean sequenceEmpty() {
		return this.nodes.isEmpty();
	}

	@Override
	protected I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, final I_Object arg) {
		for(final JMo_TreeNode node : this.nodes)
			if(node.name.equals(arg))
				return node;

		if(lazy) {
//			if(arg instanceof JMo_TreeNode)
//				this.nodes.add((JMo_TreeNode)arg);
//			else
			final A_Atomic apar = (A_Atomic)cr.argType(arg, A_Atomic.class);
			final JMo_TreeNode node = new JMo_TreeNode(apar);
			this.nodes.add(node);
			return node;
		}
		throw new RuntimeError(cr, "Node not found", "Can't find a node with name: '" + arg.toString() + "'");
	}

	@Override
	protected I_Object sequenceSelectGet(final CallRuntime cr, final I_Object key, final boolean lazy) {
		JMo_TreeNode hit = null;
		for(final JMo_TreeNode node : this.nodes)
			if(node.name.equals(key)) {
				hit = node;
				break;
			}
		if(!lazy && hit == null)
			throw new RuntimeError(cr, "Node not found", "Can't find node with name: " + key.toString());
		return hit == null && lazy ? Nil.NIL : hit.value;
	}

	@Override
	protected void sequenceSet(final CallRuntime cr, final I_Object pos, final I_Object obj, final boolean lazy) {
		JMo_TreeNode found = null;
		for(final JMo_TreeNode node : this.nodes)
			if(node.name.equals(pos))
				found = node;

		if(lazy && found == null) {
//			if(arg instanceof JMo_TreeNode)
//				this.nodes.add((JMo_TreeNode)arg);
//			else
			final A_Atomic apar = (A_Atomic)cr.argType(pos, A_Atomic.class);
			final JMo_TreeNode node = new JMo_TreeNode(apar);
			this.nodes.add(node);
			found = node;
		}
		if(found == null)
			throw new RuntimeError(cr, "Node not found", "Can't find node with name: " + pos.toString());

		found.value = obj;
	}

	@Override
	protected int sequenceSize() {
		return this.nodes.size();
	}


	// --- Private ---

	private void deepNameRemove(final I_Object toRemove) {

		for(int i = this.nodes.size() - 1; i >= 0; i--) {
			final JMo_TreeNode node = this.nodes.get(i);
			if(node.name.equals(toRemove))
				this.nodes.remove(i);
			else
				node.deepNameRemove(toRemove);
		}
	}

	private void deepValueRemove(final I_Object toRemove) {

		for(int i = this.nodes.size() - 1; i >= 0; i--) {
			final JMo_TreeNode node = this.nodes.get(i);
			if(node.value != null && node.value.equals(toRemove))
				this.nodes.remove(i);
			else
				node.deepValueRemove(toRemove);
		}
	}

	private void iToString(final CallRuntime cr, final StringBuilder sb, int indent) {
		final boolean isRoot = this.name == null;
		final boolean hasValue = this.value != null && this.value != Nil.NIL;

		if(!isRoot) {
			final String space = Lib_Parser.space(indent);
			final String line = this.name.toString(cr, STYPE.NESTED);
			sb.append(space + line);

			if(hasValue)
				sb.append(": " + this.value.toString(cr, STYPE.NESTED)); // TODO cycle of death!!!

			sb.append('\n');
			indent++;
		}
		for(final JMo_TreeNode node : this.nodes)
			node.iToString(cr, sb, indent);
	}

	private void iToStringDescribe(final StringBuilder sb, final CallRuntime cr, int indent) {
		final String space = Lib_Parser.space(indent);
		final boolean isRoot = this.name == null;

//		if(isRoot)
//			sb.append( "Tree(\n" );
//		else
		if(!isRoot) {
			sb.append(space);
			final String sn = this.name.toString(cr, STYPE.DESCRIBE);
			sb.append(sn);

			if(this.value != null && this.value != Nil.NIL) {
				sb.append(": "); // ->
				String sv = this.value instanceof JMo_TreeNode ? this.value.getTypeName() : this.value.toString(cr, STYPE.DESCRIBE);
				sv = Lib_Output.indentLines(sv, false);
				sb.append(sv);
			}
			sb.append('\n');
			indent++;
		}
		for(final JMo_TreeNode node : this.nodes)
			node.iToStringDescribe(sb, cr, indent);

//		if(isRoot)
//			sb.append(space+")\n");
	}

	/**
	 * °+ ^ add
	 * °addNode ^ add
	 * °add(TreeNode node) TreeNode # Add this node to the tree.
	 * °add(KeyValue keyvalue) TreeNode # Create a new node with key and value.
	 * °add(Atomic key) TreeNode # Create a new node with this key.
	 * °add(Atomic key, Object value) TreeNode # Create a new node with this key and value.
	 */
	private JMo_TreeNode mAdd(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 1, 2);
		final I_Object arg0 = args[0];
		final int len = args.length;

		if(len == 1 && arg0 instanceof JMo_TreeNode) {
			this.nodes.add((JMo_TreeNode)arg0);
			return (JMo_TreeNode)arg0;
		}
		else if(len == 1 && arg0 instanceof JMo_KeyValue) {
			final JMo_KeyValue kv = (JMo_KeyValue)arg0;
			if(!(kv.getKey() instanceof A_Atomic))
				throw new RuntimeError(cr, "Invalid key for Tree", "Key must be Atomic, but got: " + kv.getKey().toString(cr, STYPE.IDENT));
			final JMo_TreeNode result = new JMo_TreeNode((A_Atomic)kv.getKey(), kv.getValue());
			this.nodes.add(result);
			return result;
		}
		else {
			final A_Atomic aPar = (A_Atomic)cr.argType(arg0, A_Atomic.class);
			final JMo_TreeNode result = len == 1 ? new JMo_TreeNode(aPar) : new JMo_TreeNode(aPar, args[1]);
			this.nodes.add(result);
			return result;
		}
	}

	/**
	 * °addNodes(TreeNode nodes...)Same # Add nodes to this node.
	 */
	private JMo_TreeNode mAddNodes(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		for(final I_Object arg : args)
			if(arg instanceof JMo_TreeNode)
				this.nodes.add((JMo_TreeNode)arg);
			else {
				final A_Atomic aPar = (A_Atomic)cr.argType(arg, A_Atomic.class);
				this.nodes.add(new JMo_TreeNode(aPar));
			}
		return this;
	}

	/**
	 * °getName()Atomic # Return the name of this node.
	 */
	private I_Object mGetName(final CallRuntime cr) {
		cr.argsNone();
		return this.name == null ? Nil.NIL : this.name;
	}

	/**
	 * °getValue()Object # Returns the value of this node.
	 */
	private I_Object mGetValue(final CallRuntime cr) {
		cr.argsNone();
		return this.value == null ? Nil.NIL : this.value;
	}

	/**
	 * °nodes()List # Returns a list with all sub nodes of this node.
	 */
	private JMo_List mNodes(final CallRuntime cr) {
		cr.argsNone();

		final SimpleList<JMo_TreeNode> base = this.name == null
			? this.nodes.get(0).nodes
			: this.nodes;

		final SimpleList<I_Object> nl = new SimpleList<>(base.size());

		nl.addAll(base);

		return new JMo_List(nl);
	}

	/**
	 * °-- ^ removeNames
	 * °remove ^ removeNames
	 * °removeNames(Atomic names...)Same # Removes all nodes which has one of the given names.
	 */
	private JMo_TreeNode mRemoveNames(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		for(I_Object arg : args) {
			arg = cr.argType(arg, I_Atomic.class);

			for(int i = this.nodes.size() - 1; i >= 0; i--) {
				final JMo_TreeNode node = this.nodes.get(i);
				if(node.name != null && node.name.equals(arg))
					this.nodes.remove(i);
				else
					node.deepNameRemove(arg);
			}
//			throw new ExecError(cr, "Node not found", "Can't find node with name: "+arg.toString());
		}
		return this;
	}

	/**
	 * TODO: Must be reworked!!!
	 * °delete(Atomic names...)Same # Delete the nodes with this names.
	 */
	private I_Object mRemoveNodes(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		for(final I_Object arg : args) {
			cr.argType(arg, A_Atomic.class);
			boolean removed = false;

			for(int i = 0; i < this.nodes.size(); i++) {
				final JMo_TreeNode node = this.nodes.get(i);

				if(node.name.equals(arg)) {
					this.nodes.remove(i);
					removed = true;
					break;
				}
			}
			if(!removed)
				throw new RuntimeError(cr, "Node not found", "Can't find a node with name: '" + arg.toString() + "'");
		}
		return this;
	}

	/**
	 * °removeValues(Object o...)Same # Removes all nodes which has on of the given values.
	 */
	private JMo_TreeNode mRemoveValues(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		for(final I_Object arg : args)
			for(int i = this.nodes.size() - 1; i >= 0; i--) {
				final JMo_TreeNode node = this.nodes.get(i);
				if(node.value != null && node.value.equals(arg))
					this.nodes.remove(i);
				else
					node.deepValueRemove(arg);
			}
//			throw new ExecError(cr, "Node not found", "Can't find node with name: "+arg.toString());
		return this;
	}

	/**
	 * °setName(Atomic name)Same # Set the name of this node.
	 */
	private JMo_TreeNode mSetName(final CallRuntime cr) {
		if(this.name == null)
			throw new RuntimeError(cr, "Invalid name for root node", "Can't set the name for root of the tree.");
		this.name = (A_Atomic)cr.args(this, A_Atomic.class)[0];
		return this;
	}

	/**
	 * °setValue(Object obj)Same # Set the value of this node.
	 */
	private JMo_TreeNode mSetValue(final CallRuntime cr) {
		if(this.name == null)
			throw new RuntimeError(cr, "Invalid value for root node", "Can't set a value for root of the tree.");
		this.value = cr.args(this, I_Object.class)[0];
		return this;
	}

//	-------------------------------------------------------------------------------------------

//	public void removeAllNodes() {
//		for(JMo_TreeNode kind : this.nodes)
//			kind.minusUnterElemente();
//		while(this.nodes.size()>0)
//			this.nodes.minusLetzten();
//	}
//
//	public JMo_TreeNode[] getAllNodes() {
//		return this.nodes.zuArray(JMo_TreeNode.class);
//	}

}

