/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.FuncLet;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Sequence;

import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 08.02.2020
 */
public class JMo_Macro extends A_Sequence implements I_AutoBlockDo {

	private final SimpleList<I_Object> items;


	public JMo_Macro() {
		this(null, new SimpleList<>());
	}

	public JMo_Macro(final CallRuntime cr, final SimpleList<I_Object> items) {
		this.items = items;
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mExec(cr);
	}

	@Override
	public boolean equals(final Object obj) {
		return this == obj;
	}

	@Override
	public boolean equalsLazy(final Object obj) {
		return this.equals(obj);
	}

	@Override
	public I_Series<? extends I_Object> getInternalCollection() {
		return this.items;
	}

	@Override
	public void init(final CallRuntime cr) {}

	@Override
	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int i = Lib_Convert.getIntValue(cr, oi);
		final int pos = Lib_Sequence.realPosition(cr, i, this.items.size(), lazy);
		if(pos > this.items.size())
			if(lazy)
				return Nil.NIL;
			else
				Lib_Error.ifNotBetween(cr, 1, this.items.size(), pos, "position in makro");

		if(offset == keys.length - 1)
			return this.items.get(pos - 1);
		else
			throw new RuntimeError(cr, "Invalid access for Macro.", "A Macro is only in one dimension and has no deeper objects");
	}

	@Override
	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int i = Lib_Convert.getIntValue(cr, oi);
		final int pos = Lib_Sequence.realPosition(cr, i, this.items.size(), lazy);

		if(lazy) {
			this.items.ensureCapacity(pos);
			while(this.items.size() < pos)
				this.items.add(Nil.NIL);
		}
		if(offset == keys.length - 1)
			this.items.set(pos - 1, value);
		else
			throw new RuntimeError(cr, "Invalid access for Macro.", "A Macro is only in one dimension and has no deeper objects");
	}

	@Override
	public int sequenceSize() {
		return this.items.size();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		for(final I_Object item : this.items) {
			sb.append(item.toString());
			sb.append('\n');
		}
		Lib_Output.removeEnd(sb, '\n');
		return sb.toString();
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case IDENT:
			case NESTED:
				return this.getTypeName() + '<' + this.items.size() + '>';
			default:
				final StringBuilder sb = new StringBuilder();
				for(final I_Object item : this.items) {
					sb.append(item.toString(cr, type.getNested()));
					sb.append('\n');
				}
				Lib_Output.removeEnd(sb, '\n');
				return sb.toString();
		}
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {
		// , boolean alsParameter

		switch(method) {
			case "add":
				return A_Object.stdResult(this.mAdd(cr));
//			case "sub":
//			case "remove":
//				return A_Object.stdResult(this.remove(cr));
			case "clear":
				return A_Object.stdResult(this.mClear(cr));

//			case "get":
//				return A_Object.stdResult(this.get(cr));
//			case "set":
//				return A_Object.stdResult(this.set(cr));
			// case "each": return each(c);
			case "items":
				return A_Object.stdResult(this.mGetItems(cr));

			case "exec":
				return A_Object.stdResult(this.mExec(cr));

			default:
				return null;
		}
	}

	@Override
	protected JMo_Macro copy(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> clone = this.items.copy();
		return new JMo_Macro(cr, clone);
	}

	@Override
	protected I_Object getFirst(final CallRuntime cr) {
		return this.items.size() == 0
			? Nil.NIL
			: this.items.get(0);
	}

	@Override
	protected I_Object getLast(final CallRuntime cr) {
		return this.items.size() == 0
			? Nil.NIL
			: this.items.get(this.items.size() - 1);
	}

	@Override
	protected void mSequenceSetPut(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.args(this, A_IntNumber.class, FuncLet.class);
		this.sequenceSet(cr, oa[0], oa[1], lazy);
	}

	@Override
	protected void sequenceContent(final CallRuntime cr, final I_Object[] values) {

		for(int i = 0; i < this.items.size(); i++) {
			final FuncLet func = (FuncLet)cr.argType(values[i], FuncLet.class);
			this.items.set(i, func);
		}
	}

	@Override
	protected boolean sequenceEmpty() {
		return this.items.isEmpty();
	}

	@Override
	protected I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, final I_Object arg) {
		final A_IntNumber pos = (A_IntNumber)cr.argType(arg, A_IntNumber.class);
		//TODO pull(-3)
		final int pos2 = Lib_Convert.getIntValue(cr, pos);
		if(lazy && pos2 > this.items.size())
			return Nil.NIL;
		Lib_Error.ifNotBetween(cr, 1, this.items.size(), pos2, "position");
		return this.items.get(pos2 - 1);
	}

	@Override
	protected I_Object sequenceSelectGet(final CallRuntime cr, final I_Object key, final boolean lazy) {
		final int oi = Lib_Convert.getIntValue(cr, key);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.items.size(), lazy);
		return pos == -1 ? Nil.NIL : this.items.get(pos - 1);
	}

	@Override
	protected void sequenceSet(final CallRuntime cr, final I_Object key, final I_Object value, final boolean lazy) {
		final int pos = Lib_Convert.getIntValue(cr, key);

		if(lazy) {
			this.items.ensureCapacity(pos);
			while(pos > this.items.size())
				this.items.add(Nil.NIL);
		}
		Lib_Error.ifNotBetween(cr, 1, this.items.size(), pos, "position");
		final FuncLet o = (FuncLet)value;

		this.items.set(pos - 1, o);
	}

	/**
	 * °add(FuncLet item...)Same # Add one or more FuncLet's to this Macro.
	 */
	private JMo_Macro mAdd(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		this.items.ensureCapacity(this.items.size() + args.length);

		for(final I_Object arg : args) {
			final FuncLet func = (FuncLet)cr.argType(arg, FuncLet.class);
			this.items.add(func);
		}
		return this;
	}

	/**
	 * °clear()Same # Remove all functions from this Macro.
	 */
	private JMo_Macro mClear(final CallRuntime cr) {
		cr.argsNone();
		this.items.clear();
		return this;
	}

	/**
	 * °exec()Same # Execute this makro.
	 */
	private JMo_Macro mExec(final CallRuntime cr) {
		cr.argsNone();
		for(final I_Object item : this.items)
			if(item != Nil.NIL)
				((FuncLet)item).exec(cr);
		return this;
	}

	/**
	 * °items()List # Returns a List with all items in this Macro.
	 * TODO: toList ?!?
	 */
	private JMo_List mGetItems(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> clone = this.items.copy();
		return new JMo_List(clone);
	}

}
