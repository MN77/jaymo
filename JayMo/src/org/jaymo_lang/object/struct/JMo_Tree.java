/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 *         26.10.2020 Erstellt
 */
public class JMo_Tree extends JMo_TreeNode implements I_AutoBlockDo {

	/**
	 * +Tree()
	 */
	public JMo_Tree() {
		super((I_Atomic)null);
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return null;
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "parse":
				this.mParse(cr);
				return A_Object.stdResult(this);
			case "build":
				this.mBuild(cr);
				return A_Object.stdResult(this);
		}
		return super.call3(cr, method);
	}

	private int iParseAddNode(final CallRuntime cr, final JMo_TreeNode node, final String[] lines, final int next, final int baseIndent) {
		JMo_TreeNode last = null;

		for(int i = next; i < lines.length; i++) {
			final String s = lines[i];
			if(s.trim().length() == 0)
				continue;

			final int curIndent = Lib_Parser.getDepth(s);

			if(curIndent < baseIndent)
				return i;

			if(curIndent == baseIndent) {
				final Group2<Str, I_Object> name_value = this.iSplitNameValue(s.trim());
				final JMo_TreeNode result = new JMo_TreeNode(name_value.o1, name_value.o2);
				last = result;
				node.nodes.add(result);
			}
			else {
				Err.ifToBig(baseIndent + 1, curIndent); //TODO title
				i = this.iParseAddNode(cr, last, lines, i, curIndent) - 1;
			}
		}
		return lines.length;
	}

	private String[] iParseSplit(final String spar) {
		final SimpleList<String> list = new SimpleList<>();
		String curline = "";
		for(final char c : spar.toCharArray())
			if(c == '\n') {
				if(curline.trim().endsWith(","))
					continue;
				list.add(curline);
				curline = "";
			}
			else
				curline += c;
		if(curline.length() > 0)
			list.add(curline);


		return list.toArray(new String[list.size()]);
	}

	private Group2<Str, I_Object> iSplitNameValue(final String s) {
		String name = s;
		String value = null;

		final int idx = s.indexOf(':');

		if(idx >= 0) {
			name = s.substring(0, idx).trim();
			value = s.substring(idx + 1).trim();
		}
		return new Group2<>(new Str(name), value == null ? null : new Str(value));
	}

	/**
	 * °build(TreeNode nodes...)Same # Add all nodes to a empty tree.
	 * This is nearly comparable to "addNodes"
	 */
	private void mBuild(final CallRuntime cr) {
		if(this.internalNodes().size() > 0)
			throw new RuntimeError(cr, "Tree build error", "Tree is not empty!");

		final I_Object[] args = cr.argsVar(this, 0, 0);
		for(final I_Object arg : args)
			this.internalNodes().add((JMo_TreeNode)cr.argType(arg, JMo_TreeNode.class));
	}

	/**
	 * °parse(Str text)Same # Parse the text and fill this tree.
	 */
	private void mParse(final CallRuntime cr) {
		if(this.internalNodes().size() > 0)
			throw new RuntimeError(cr, "Tree build error", "Tree is not empty!");

		final Str arg = (Str)cr.args(this, Str.class)[0];
		final String spar = Lib_Convert.getStringValue(cr, arg);
//		final String[] lines = spar.split("\n");
		final String[] lines = this.iParseSplit(spar);
//		MOut.temp(spar, lines);
		int base = -1;
		JMo_TreeNode last = null;

		for(int i = 0; i < lines.length; i++) {
			final String s = lines[i];
			if(s.trim().length() == 0)
				continue;
			if(base == -1)
				base = Lib_Parser.getDepth(s);
//			MOut.temp(base, s);

			final int curIndent = Lib_Parser.getDepth(s);
			Lib_Error.ifTooSmall(cr, base, curIndent); // TODO Title!

			if(curIndent == base) {
				final Group2<Str, I_Object> name_value = this.iSplitNameValue(s.trim());
				final JMo_TreeNode result = new JMo_TreeNode(name_value.o1, name_value.o2);
				last = result;
				this.nodes.add(result);
			}
			else
				i = this.iParseAddNode(cr, last, lines, i, curIndent) - 1;
		}
	}

}
