/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.sys;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.filesys.JMo_Dir;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.SysDir;


/**
 * @author Michael Nitsche
 * @created 15.10.2020
 * @implNote TODO System.properties
 */
public class JMo_Sys extends A_ObjectSimple {

	@Override
	public void init(final CallRuntime cr) {
		cr.getApp().strict.checkSandbox(cr, "Sys");
	}

	/**
	 * +Sys() # Sys
	 *
	 * °arch ^ architecture
	 * °architecture()Str # Returns the architecture of the system.
	 * °osName()Str # Returns the name of the operating system.
	 * °osVersion()Str # Returns the version of the operating system.
	 * °fontEncoding()Str # Returns the font encoding.
	 * °land()Str # Returns the land.
	 * °language()Str # Returns the language.
	 * °user ^ username
	 * °username()Str # Returns the username.
	 * °javaVersion()Str # Returns the Java version.
	 * °separatorPath()Str # Returns the path separator.
	 * °separatorDir ^ separatorFile
	 * °separatorFile()Str # Returns the file separator.
	 * °separatorLine()Str # Returns the line separator.
	 * °processUptime()Long # Returns the uptime of the current Java process.
	 * °root ^ rootDir
	 * °rootDir()Dir # Returns the root directory.
	 * °temp ^ tempDir
	 * °tempDir()Dir # Returns the temporary directory.
	 * °home ^ homeDir
	 * °homeDir()Dir # Returns the user home directory.
	 * °current ^ currentDir
	 * °currentDir()Dir # Returns the current directory.
	 */
	@Override
	protected I_Object call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "arch":
			case "architecture":
				cr.argsNone();
				return new Str(Sys.getArchitectur());
			case "osName":
				cr.argsNone();
				return new Str(Sys.getOsName());
			case "osVersion":
				cr.argsNone();
				return new Str(Sys.getOsVersion());
			case "fontEncoding":
				cr.argsNone();
				return new Str(Sys.getFileEncoding());
			case "land":
				cr.argsNone();
				return new Str(Sys.getLand());
			case "language":
				cr.argsNone();
				return new Str(Sys.getLanguage());
			case "user":
			case "username":
				cr.argsNone();
				return new Str(Sys.getUserName());
//			case "desktop":
//				cr.argsNone();
//				return new Str(Sys.getDesktop());	// Returns null
			case "javaVersion":
				cr.argsNone();
				return new Str(Sys.getJavaVersion());
			case "separatorPath":
				cr.argsNone();
				return new Str(Sys.getSeparatorPath());
			case "separatorDir":
			case "separatorFile":
				cr.argsNone();
				return new Str(Sys.getSeperatorDir());
			case "separatorLine":
				cr.argsNone();
				return new Str(Sys.getSeparatorLine());
			case "processUptime":
				cr.argsNone();
				return new JMo_Long(Sys.getProcessUptimeMSek());
			case "root":
			case "rootDir":
				cr.argsNone();
				return new JMo_Dir(SysDir.root().getFile());
			case "temp":
			case "tempDir":
				cr.argsNone();
				return new JMo_Dir(SysDir.temp().getFile());
			case "home":
			case "homeDir":
				cr.argsNone();
				return new JMo_Dir(SysDir.home().getFile());
			case "current":
			case "currentDir":
				cr.argsNone();
				return new JMo_Dir(SysDir.current().getFile());
			case "hostname":
			case "hostName":
				return this.mHostname(cr);

			case "env":
				return this.mEnv(cr);
			case "envMap":
				return this.mEnvMap(cr);

			default:
				return null;
		}
	}

	/*
	 * °env(Str name)Str # Read the value of a environment variable.
	 */
	private I_Object mEnv(final CallRuntime cr) {
		final String name = ((Str)cr.args(this, Str.class)[0]).getValue();
		final String value = System.getenv(name);
		return value == null
			? Nil.NIL
			: new Str(value);
	}

	/*
	 * °envMap()Map # Read all available environment variables.
	 */
	private JMo_Map mEnvMap(final CallRuntime cr) {
		cr.argsNone();
		final Map<String, String> map = System.getenv();
		final SimpleList<I_Object> keys = new SimpleList<>(map.size());
		final SimpleList<I_Object> values = new SimpleList<>(map.size());

		for(final Entry<String, String> e : map.entrySet()) {
			keys.add(new Str(e.getKey()));
			values.add(new Str(e.getValue()));
		}
		return new JMo_Map(keys, values);
	}

	/**
	 * °hostname ^ hostName
	 * °hostName()Str? # Returns the host name of the system.
	 */
	private I_Object mHostname(final CallRuntime cr) {
		cr.argsNone();

		try {
			final InetAddress address = InetAddress.getLocalHost();
			return new Str(address.getHostName());
		}
		catch(final UnknownHostException e) {
			return Nil.NIL;
		}
	}

}
