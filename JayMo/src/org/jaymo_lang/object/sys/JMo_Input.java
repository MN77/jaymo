/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.sys;

import java.util.Scanner;
import java.util.function.Function;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 24.02.2019
 */
public class JMo_Input extends A_Object {

	private static Scanner                    sc              = new Scanner(System.in); // close, open another one ... doesn't work
	private static Function<Class<?>, String> alternateSource = null;


	public static void setSource(final Function<Class<?>, String> f) {
		JMo_Input.alternateSource = f;
	}

	/**
	 * +Input() # Input
	 */
	@Override
	public void init(final CallRuntime cr) {
		cr.getApp().strict.checkSandbox(cr, "Input");

		if(JMo_Input.alternateSource == null)
			cr.getStrict().checkWebstart(cr, "Input");
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "readStr":
			case "str":
				return A_Object.stdResult(this.mReadStr(cr));

			case "readInt":
			case "int":
				return A_Object.stdResult(this.mReadInt(cr));

			case "bool":
			case "readBool":
				return A_Object.stdResult(this.mReadBool(cr));

			case "readDec":
			case "dec":
				return A_Object.stdResult(this.mReadDec(cr));

			case "readDouble":
			case "double":
				return A_Object.stdResult(this.mReadDouble(cr));

			default:
				return null;
		}
	}

	// --- Methods ---

	/**
	 * Read the input from System.in or alternateSource
	 *
	 * @return Return can be a String or null
	 */
	private String iRead(final Class<?> type) {
		return JMo_Input.alternateSource == null
			? JMo_Input.sc.nextLine()
			: JMo_Input.alternateSource.apply(type);
	}

	/**
	 * °bool ^ readBool
	 * °readBool()Bool? # Read a boolean from the command-line
	 */
	private A_Immutable mReadBool(final CallRuntime cr) {
		cr.argsNone();
		String s = this.iRead(Bool.class);
		if(s == null)
			return Nil.NIL;
		s = s.trim().toLowerCase();

		switch(s) {
			case "true":
			case "1":
				return Bool.TRUE;
			case "false":
			case "0":
				return Bool.FALSE;
			default:
				return Nil.NIL;
		}
	}

	/**
	 * °dec ^ readDec
	 * °readDec()Dec? # Read a decimal number from the command-line
	 */
	private A_Immutable mReadDec(final CallRuntime cr) {
		cr.argsNone();

		try {
			String s = this.iRead(JMo_Dec.class);
			if(s == null)
				return Nil.NIL;
			s = s.replace(',', '.');
			final double d = Double.parseDouble(s);
			return JMo_Dec.valueOf(cr, d);
		}
		catch(final NumberFormatException e) {
			return Nil.NIL;
		}
	}

	/**
	 * °double ^ readDouble
	 * °readDouble()Double? # Read a decimal number from the command-line
	 */
	private A_Immutable mReadDouble(final CallRuntime cr) {
		cr.argsNone();

		try {
			String s = this.iRead(JMo_Double.class);
			if(s == null)
				return Nil.NIL;
			s = s.replace(',', '.');
			final double d = Double.parseDouble(s);
			return new JMo_Double(d);
		}
		catch(final NumberFormatException e) {
			return Nil.NIL;
		}
	}

	/**
	 * °int ^ readInt
	 * °readInt()Int? # Read a integer number from the command-line
	 */
	private A_Immutable mReadInt(final CallRuntime cr) {
		cr.argsNone();

		try {
			String s = this.iRead(Int.class);
			if(s == null)
				return Nil.NIL;
			s = s.replace(',', '.');
			final int i = Integer.parseInt(s);
			return new Int(i);
		}
		catch(final NumberFormatException e) {
			return Nil.NIL;
		}
	}

//	public Int getByte() {
//		Scanner sc = new Scanner(System.in);
//		int i = sc.nextByte();
//		sc.close();
//		return new Int(i);
//	}

	/**
	 * °str ^ readStr
	 * °readStr()Str? # Read a string from the command-line
	 */
	private A_Immutable mReadStr(final CallRuntime cr) {
		cr.argsNone();
		final String s = this.iRead(Str.class);
		return s == null ? Nil.NIL : new Str(s);
	}

}
