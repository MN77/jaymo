/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.sys;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_FunctionMap;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.sys.cmd.SYSCMD_IO;
import de.mn77.base.sys.cmd.SysCmd;
import de.mn77.base.sys.cmd.SysCmdData;
import de.mn77.base.sys.cmd.SysCmdResult;


/**
 * @author Michael Nitsche
 */
public class JMo_Cmd extends A_Object {

	private enum INOUT {
		NONE,
		BUFFER,
		LIVE
	}


	private final ArgCallBuffer cmd;


	/**
	 * !Executes a shell command
	 * +Cmd(String cmd) # Cmd("echo 5")
	 * +´´ ^ Cmd(String cmd).live # ´echo 5´
	 * +`` ^ Cmd(String cmd).live # `echo 5`
	 */
	public JMo_Cmd(final Call c) {
		this.cmd = new ArgCallBuffer(0, c);
	}

	@Override
	public void init(final CallRuntime cr) {
		cr.getApp().strict.checkSandbox(cr, "Cmd");

		this.cmd.init(cr, this, Str.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return "Cmd(" + this.cmd.toString(cr, STYPE.IDENT) + ")";
	}

//	public I_Object autoBlockDo(final CurProc cr) {
//		return exec(cr, true);
//	}

	/**
	 * live is maybe slow because of System-output
	 * exec,quiet,pipe,(wait) have an equally speed
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
//			case "do": // Override for Cmd
			case "exec":
			case "live":
				return A_Object.stdResult(this.mLive(cr));

			case "buffer":
//				return A_Object.stdResult(this.buffer(cr, false));
//			case "quiet":
				return A_Object.stdResult(this.mBuffer(cr, true));
			case "error":
				return A_Object.stdResult(this.mError(cr, true));

			case "forget":
				return A_Object.stdResult(this.mForget(cr));

//			case "getStr":
//			case "pipe":
			case "output":
				return A_Object.stdResult(this.mOutput(cr));

			case "wait": // TODO Is this faster?!?
				return A_Object.stdResult(this.mWait(cr));

			default:
				return null;
		}
	}

	/**
	 * °buffer()FunctionList # Executes the command, wait and return FunctionList
	 */
	private JMo_FunctionMap mBuffer(final CallRuntime cr, final boolean quiet) {
		cr.argsNone();
		final SysCmdResult res = this.run(cr, true, INOUT.BUFFER);

		final String output = FilterString.removeNewlineEnd(res.output);
		final String errors = FilterString.removeNewlineEnd(res.error);

		final SimpleList<String> names = new SimpleList<>();
		final SimpleList<I_Object> objects = new SimpleList<>();
		names.add("result"); // getResult?!? But 'get' is forbidden
		objects.add(new Int(res.result));
		names.add("output"); // getOutput?!? But 'get' is forbidden
		objects.add(new Str(output));
		names.add("errors"); // getErrors?!? But 'get' is forbidden
		objects.add(new Str(errors));

		final JMo_FunctionMap with = new JMo_FunctionMap(cr, names, objects);

		if(!quiet)
			Lib_Output.out(cr.getApp(), output, true);

		return with;
	}

	/**
	 * °error()Str # Executes the command, wait and return the error stream
	 */
	private Str mError(final CallRuntime cr, final boolean quiet) {
		cr.argsNone();
		final SysCmdResult res = this.run(cr, true, INOUT.BUFFER);

		final String output = FilterString.removeNewlineEnd(res.output);
		final String errors = FilterString.removeNewlineEnd(res.error);

		if(!quiet)
			Lib_Output.out(cr.getApp(), output, true);

		return new Str(errors);
	}

	/**
	 * °forget()Nil # Executes the command in background, hides the output and returns Nil
	 */
	private Nil mForget(final CallRuntime cr) {
		cr.argsNone();
		this.run(cr, false, INOUT.NONE); // Result gets lost
		return Nil.NIL;
	}

	/**
	 * °exec ^ live
	 * °live()Int # Executes the command and returns the exit value
	 * ||#°exec(Bool wait)FunctionList#Nil Executes the command. If &quot;wait&quot;, it returns the FunctionList, elsewhere &quot;Nil&quot;
	 */
	private I_Object mLive(final CallRuntime cr) {
		cr.argsNone();
		final SysCmdResult res = this.run(cr, true, INOUT.LIVE);
		return new Int(res.result);
	}

	/**
	 * ###°getStr ^ pipe
	 * °output()Str # Execute the command and return the output from StdOut as Str
	 */
	private I_Object mOutput(final CallRuntime cr) {
		cr.argsNone();
		final SysCmdResult res = this.run(cr, true, INOUT.BUFFER);

//		if(res.o1.result.get() != 0)
//			throw new ExecError(cr, "System-command terminated with an error", "The command \"" + res.o1.command.get() + "\" ended with exit code: " + res.o1.result.get());

		final String output = FilterString.removeNewlineEnd(res.output);
		final String errors = FilterString.removeNewlineEnd(res.error);

		if(errors != null && errors.length() > 0)
			Lib_Output.out(cr.getApp(), errors, true);

		return new Str(output);
	}

	/**
	 * °wait()Int # Execute the command, wait until its finished and return the exit value
	 */
	private I_Object mWait(final CallRuntime cr) {
		cr.argsNone();
		final SysCmdResult res = this.run(cr, true, INOUT.NONE);
		return new Int(res.result);
	}

	private SysCmdResult run(final CallRuntime cr, final boolean wait, final INOUT io) {
		final String commandString = ((Str)this.cmd.get()).getValue();

		final SYSCMD_IO dataIO = io == INOUT.LIVE
			? SYSCMD_IO.LIVE
			: io == INOUT.BUFFER
				? SYSCMD_IO.BUFFER
				: SYSCMD_IO.DISCARD;

		final SysCmdData data = new SysCmdData(true, wait, dataIO, commandString, null);

		try {
			final SysCmd cmd = new SysCmd();
			return cmd.exec(data);
		}
		catch(final Exception f) {
			throw new ExternalError(cr, "Command execution error: " + f.getClass().getSimpleName(), f.getMessage());
		}
	}

}
