/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.passthrough;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.JMo_Group;


/**
 * @author Michael Nitsche
 * @created 01.10.2019
 *
 *          Object for ()
 */
public class Group extends JMo_Group implements I_PassThrough {	// TODO passthrough necessary?!?

	public Group(final Call arg) {
		super(arg);
	}

}
