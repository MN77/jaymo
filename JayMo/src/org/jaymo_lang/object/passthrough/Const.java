/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.passthrough;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.pseudo.ConstLet;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.ConstEnv;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Function;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Const extends A_Object implements I_PassThrough, I_Mem {

	private final String  name;
	private final boolean nilable;
	private String        initType = null;


	public Const(final String name) {
		Err.ifNull(name);
		this.name = name;
		this.nilable = name.endsWith("?");
	}

	public I_Object get(final CallRuntime cr) {
		final I_Object result = cr.vce.cons.get(cr, this);
		if(result == null)
			throw Lib_Error.memNotInitialized(cr, this);
		return result;
	}

	public final String getMemTypeString() {
		return "constant";
	}

	public String getName() {
		return this.name;
	}

	@Override
	public void init(final CallRuntime cr) {}

	public boolean isInitialized(final CallRuntime cr) {
		return cr.vce.cons.isInitialized(this);
	}

	public void let(final CallRuntime crOld, final CallRuntime crNew, final I_Object o) {
		this.let(crOld, crNew, o, false);
	}

	public void let(final CallRuntime crOld, final CallRuntime crNew, final I_Object o, final boolean convert) {

		if(this.initType != null) {
			crNew.vce.cons.setType(crNew, this, this.initType);
			this.initType = null;
		}
		crNew.vce.cons.set(crOld, this, o, this.nilable, convert);
	}

	public void setType(final String type, final DebugInfo debugInfo) {
		this.initType = type;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.name;
			default:
				Err.ifNull(cr);
				final StringBuilder sb = new StringBuilder();
				sb.append(this.name);
				final ConstEnv ve = cr.vce.cons;

				if(ve.isInitialized(this)) {
					final I_Object data = ve.get(cr, this);
					sb.append('[');
					sb.append(data.toString(cr, STYPE.IDENT));
					sb.append(']');
				}

				return sb.toString();
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "=":
//			case "let":
				this.mAssign(cr, false);
				return A_Object.stdResult(this);
			case "~=":
				this.mAssign(cr, true);
				return A_Object.stdResult(this);
		}
		if(Lib_Function.isVarFunction(method) > -1)
			throw new CodeError(cr, "Invalid method for constant", "Can't change the once assigned value of a constant with: " + method);

		return this.get(cr).call(cr);
	}

	private void mAssign(final CallRuntime cr, final boolean convert) {
//		final I_Object streamit = cr.vce.cons.get(cr, this, null); // By constants this is always 'null'
		final I_Object streamit = new ConstLet(this); // TODO better solution? Creating ConstLet on the fly is not really possible.
		final I_Object arg = cr.args(streamit, I_Object.class)[0];
		this.let(cr, cr, arg, convert);
	}

}
