/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.passthrough;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.VarLet;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.runtime.VarEnv;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Function;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Var extends A_EventObject implements I_PassThrough, I_Mem {

	public static final String IT_FULL       = "it?";
	public static final String IT            = "it";
	public static final String CHANGED_EVENT = "@varChanged";

	private final String  name;
	private final boolean nilable;
	private String        initType = null;


	public Var(final String name) {
		Err.ifNull(name);
		final int len = name.length();

		if(len >= 2 && name.charAt(len - 1) == '?') {
			this.name = name.substring(0, len - 1);
			this.nilable = true;
		}
		else {
			this.name = name;
			this.nilable = false;
		}
	}

	@Override
	public ObjectCallResult callEvent(final CallRuntime cr, final String event) {
		throw new CodeError(cr, "Invalid call of a event", "Got: " + event);
	}

	public I_Object get(final CallRuntime cr) {
		final I_Object result = cr.vce.vars.get(cr, this);
		if(result == null)
			if(this.nilable)
				return Nil.NIL;
			else
				throw Lib_Error.memNotInitialized(cr, this);

		return result;
	}

	public final String getMemTypeString() {
		return "variable";
	}

	public String getName() {
		return this.name;
	}

	@Override
	public void init(final CallRuntime cr) {}

	public boolean isInitialized(final CallRuntime cr) {
		return cr.vce.vars.isInitialized(this);
	}

	public void let(final CallRuntime crOld, final CallRuntime crNew, final I_Object o) {
		this.let(crOld, crNew, o, false);
	}

	public void let(final CallRuntime crOld, final CallRuntime crNew, final I_Object o, final boolean convert) {
		if(this.initType != null)
			crNew.vce.vars.setType(crNew, this, this.initType);
//			this.initType = null; // NO! It must be set at every init! Exp.: Functions
		crNew.vce.vars.set(crOld, this, o, this.nilable, convert);
		this.eventRunRaw(crNew, Var.CHANGED_EVENT, o);
	}

	@Override
	public I_Object mPrint(final CallRuntime cr, final boolean newline) {
		this.get(cr); // Init
		return super.mPrint(cr, newline);
	}

	public void setType(final String type, final DebugInfo debugInfo) {
		if(this.name.equals(Var.IT))
			throw new CodeError("Invalid variable definition", "Can't set a fixed type for the already defined magic variable: " + this.name, debugInfo);
//		this.initType = Lib_Type.replaceAlias(type);
		this.initType = type;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.name);

		if(this.initType != null && !this.name.equals(Var.IT)) {
			sb.append('<');
			sb.append(this.initType);
			sb.append('>');
		}
		return sb.toString();
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(type != STYPE.DESCRIBE)
			return this.toString();

		final VarEnv ve = cr.vce.vars;
		if(!ve.isInitialized(this))
			return this.toString();

		final StringBuilder sb = new StringBuilder();
		sb.append(this.name);

		final I_Object data = ve.get(cr, this);
		sb.append('[');
		sb.append(data.toString(cr, type));
		sb.append(']');

		return sb.toString();
	}

	@Override
	public boolean validateEvent(final CallRuntime cr, final String event) {
		return event.equals(Var.CHANGED_EVENT);
	}

	@Override
	protected ObjectCallResult callMethod(final CallRuntime cr, final String method) {

		switch(method) {
			case "=":
//			case "let":
				this.mAssign(cr, false);
				return A_Object.stdResult(this);
//			case "=~":
			case "~=":
//			case "convertLet": // Like "let", but converts the value to the var-type
				this.mAssign(cr, true);
				return A_Object.stdResult(this);
//			case "getValue":
//			case "value":
//				cr.argsNone();
//				return A_Object.stdResult(this.get(cr));
		}
		final byte isVarFunction = Lib_Function.isVarFunction(method);

		if(isVarFunction > -1) {
			final I_Object oldValue = cr.vce.vars.get(cr, this);
			if(oldValue == null)
				throw new RuntimeError(cr, "Variable is not initialized", "The variable '" + this.name + "' has no value/type, please initialize it first.");
			final I_Object newValue = Lib_Function.mCalcLet(cr, method, isVarFunction, oldValue);
			this.let(cr, cr, newValue, true);
			return new ObjectCallResult(this, false); // TODO true?
		}
		return this.iCallValue(cr, method);
	}

	private ObjectCallResult iCallValue(final CallRuntime cr, final String method) {
		final ObjectCallResult result = this.get(cr).call(cr);
		if(result == null || result.obj == null)
			Err.invalid("Call returns no result!");

		// Update Var after: Inc, Dec, ...
//		switch(method) {
//			case "//":
//			case "**":
//			case "++":
//			case "--":
//				// Currently ONLY ONE method will touch the value, not the entire stream
//				final I_Object valueCopy = this.get(cr);
//				final Call callToSet = new Call(cr.getSurrBlock(), valueCopy, method, cr.call.parCalls, cr.getDebugInfo());
//				final ObjectCallResult result2 = valueCopy.call(cr.copyCall(callToSet, false));
//				if(result2.is_Attachment_Exec)
//					Err.invalid(method, result, result2, this);
//				this.iSet(cr, cr, result2.obj, true);
//		}

		return result;
	}

	private void mAssign(final CallRuntime cr, final boolean convert) {
//		final I_Object streamit = cr.vce.vars.get(cr, this, null); // result can be null
		final I_Object streamit = new VarLet(this); // TODO What's better? Creating on the fly in Lib_MagicVar.replace is not good: x.nilUse(nil,cur+2).print
		final I_Object arg = cr.args(streamit, I_Object.class)[0];
		this.let(cr, cr, arg, convert);
	}

}
