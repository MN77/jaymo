/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_Type;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 04.01.2018
 */
public interface I_Object extends I_ObjectToString, Comparable<I_Object> { //Comparable<I_Object>,	//TODO Wird das nicht mehr gebraucht?

	ObjectCallResult call(CallRuntime cr);

	//	int compareTo(I_Object o);
	int compareTo(CallRuntime cr, I_Object o);

	boolean equals(Object o);

	boolean equalsLazy(final Object obj);

	boolean equalsStrict(final Object obj);

	A_Immutable getConstant(CallRuntime cr, String name);

	JMo_Type getType(CallRuntime cr);

	String[] getTypeNames();

	void initOnce(CallRuntime cr);

}
