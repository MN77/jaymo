/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.filesys;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Charset;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.immute.datetime.JMo_DateTime;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.sys.JMo_Cmd;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.type.datetime.MDateTime;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.cmd.SYSCMD_IO;
import de.mn77.base.sys.cmd.SysCmd;
import de.mn77.base.sys.cmd.SysCmdData;
import de.mn77.base.sys.cmd.SysCmdResult;
import de.mn77.base.sys.file.Lib_FileSys;
import de.mn77.base.sys.file.Lib_RandomAccess;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 */
public class JMo_File extends JMo_Path implements I_Object {

	public JMo_File(final Call arg) {
		super(arg);
	}

	public JMo_File(final java.io.File file) {
		super(file);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final File f = this.getInternalFile();
		if(f == null)
			return "File";

		switch(type) {
			case REGULAR:
				return f.getAbsolutePath().toString();
			case NESTED:
			case IDENT:
				return "File";
			case DESCRIBE:
			default:
				return "File(" + '"' + f.getAbsolutePath().toString() + '"' + ")";
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		final ObjectCallResult result = super.call2(cr, method);
		if(result != null)
			return result;

		switch(method) {
			case "suffix":
				return A_Object.stdResult(this.mSuffix(cr));
			case "dir":
			case "directory":
				return A_Object.stdResult(this.mDirectory(cr));
			case "size":
				return A_Object.stdResult(this.mSize(cr));
			case "created":
				return A_Object.stdResult(this.mCreated(cr));
			case "modified":
				return A_Object.stdResult(this.mModified(cr));
			case "exec":
				return A_Object.stdResult(this.mExec(cr));
			case "open":
				return A_Object.stdResult(this.mOpen(cr));
//				case "command":
			case "cmd":
				return A_Object.stdResult(this.mCmd(cr));

			// case "copy": result=copy(c); break; //to name, to Dir
			case "touch":
				return A_Object.stdResult(this.mTouch(cr));
			case "create":
				return A_Object.stdResult(this.mCreate(cr));
			case "clear":
				return A_Object.stdResult(this.mClear(cr));


			case "lines":
			case "readLines":
				return A_Object.stdResult(this.mReadLines(cr));
//			case "each":
//			case "eachLine":
//				return eachLine(cr);
			case "read":
				return A_Object.stdResult(this.mReadString(cr));
			case "readBytes":
				return A_Object.stdResult(this.mReadBytes(cr));

			case "append":
				return A_Object.stdResult(this.mAppend(cr));
			case "appendBytes":
				return A_Object.stdResult(this.mAppendBytes(cr));
			case "set":
				return A_Object.stdResult(this.mSet(cr));
			case "setBytes":
				return A_Object.stdResult(this.mSetBytes(cr));
			case "write":
				return A_Object.stdResult(this.mWrite(cr));
			case "writeBytes":
				return A_Object.stdResult(this.mWriteBytes(cr));

			case "rename":
				return A_Object.stdResult(this.mRename(cr));
			case "move":
				return A_Object.stdResult(this.mMove(cr));
			case "copy":
				return A_Object.stdResult(this.mCopy(cr));
			case "delete":
				return A_Object.stdResult(this.mDelete(cr));

			case "direct":
				return A_Object.stdResult(this.mDirect(cr));
			case "toRandomAccess":
				return A_Object.stdResult(this.mToRandomAccess(cr));
			case "directRead":
				return A_Object.stdResult(this.mDirectRandomAccess(cr, "r"));
			case "directReadWrite":
			case "directWrite":
				return A_Object.stdResult(this.mDirectRandomAccess(cr, "rw"));
//			case "directSync":

			default:
				return null;
		}
	}

	private SimpleList<I_Object> iReadLines(final CallRuntime cr) {
		final SimpleList<I_Object> list = new SimpleList<>();

		try {
			final String content = Lib_TextFile.read(this.getInternalFile());
			final Collection<String> lines = ConvertString.toLines(content);
			list.ensureCapacity(lines.size());

			for(final String line : lines)
				list.add(new Str(line));

			return list;
		}
		catch(final Err_FileSys e) {
			throw new ExternalError(cr, "File read error", e.getMessage());
		}
	}

	/**
	 * °append(Atomic val)Same # Appends a atomic value at the end of the file
	 * °append(Atomic val, Charset cs)Same # Appends a atomic value at the end of the file
	 */
	private JMo_File mAppend(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 1, 2);
		final I_Object o = cr.argType(args[0], A_Atomic.class);

		final String s = Lib_Convert.getStringValue(cr, o);

		try {
			Charset cs = null;

			if(args.length == 2) {
				final JMo_Charset jcs = (JMo_Charset)cr.argType(args[1], JMo_Charset.class);
				cs = jcs.getCharset();
			}
			Lib_TextFile.append(this.getInternalFile(), s, cs);
			return this;
		}
		catch(final Err_FileSys e) {
			// e.getMessage() = "File/Directory-Access-Error"
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
	}

	/**
	 * °appendBytes(ByteArray val)Same # Append some bytes to the file end of the file
	 */
	private JMo_File mAppendBytes(final CallRuntime cr) {
		final JMo_ByteArray arg = (JMo_ByteArray)cr.args(this, JMo_ByteArray.class)[0];

		try {
			Lib_RandomAccess.append(this.getInternalFile(), arg.getValue());
		}
		catch(final Exception err) {
			// e.getMessage() = "File/Directory-Access-Error"
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
		return this;
	}

	/**
	 * °clear()Same # Set the length to 0, if the file doesn't exist create it.
	 */
	private JMo_File mClear(final CallRuntime cr) {
		cr.argsNone();

		try {
			Lib_RandomAccess.clear(this.getInternalFile());
			return this;
		}
		catch(final Exception err) {
			throw new ExternalError(cr, "File access error", this.getInternalFile().getAbsolutePath());
		}
	}

	/**
	 * °cmd(Object args...)Cmd # Create a shell-command with this file
	 */
	private I_Object mCmd(final CallRuntime cr) {
		final I_Object args[] = cr.argsVar(this, 0, 0);

		final StringBuilder sb = new StringBuilder();
		sb.append(this.getInternalFile().getAbsolutePath());

		for(final I_Object arg : args) {
			sb.append(' ');
			sb.append(Lib_Convert.getStringValue(cr, arg));
		}
		final Str s = new Str(sb.toString());
		final Call c = new Call(cr, s);
		return new JMo_Cmd(c);
	}

	/**
	 * °copy(Str|Path dest)File # Copy this file to 'dest'
	 */
	private JMo_File mCopy(final CallRuntime cr) {
		final I_Object arg = cr.argsExt(this, new Class<?>[]{Str.class, JMo_Path.class})[0];
		final Path source = this.getInternalFile().toPath();

		File target = null;

		if(arg instanceof Str) {
			final Str str = (Str)arg;
			final String newNamePath = Lib_Convert.getStringValue(cr, str);
			target = new File(newNamePath);
		}
		else
			target = ((JMo_Path)arg).getInternalFile();

		try {
			final boolean isDir = target.isDirectory();

			if(isDir)
				target = target.toPath().resolve(source.getFileName()).toFile();

			Files.copy(source, target.toPath()); //,REPLACE_EXISTING
//				Files.move(source, source.resolveSibling("newname"));
			return new JMo_File(target);
		}
		catch(final NoSuchFileException e) {
			throw new ExternalError(cr, "File-Move-Error", "Source file doesn't exist: \"" + source.toFile().getAbsolutePath() + "\"");
		}
		catch(final FileAlreadyExistsException e) {
			throw new ExternalError(cr, "File-Move-Error", "Target already exist: \"" + target.getAbsolutePath() + "\"");
		}
		catch(final IOException e) {
			Err.show(e);
			throw new ExternalError(cr, "File-Move-Error", "Can't move \"" + source.toFile().getAbsolutePath() + "\" --> \"" + target.getAbsolutePath() + '"');
		}
	}

	/**
	 * °create()Same # Create an empty file.
	 */
	private JMo_File mCreate(final CallRuntime cr) {
		cr.argsNone();
		boolean ok = false;

		try {
			final File f = this.getInternalFile();
			if(f.exists())
				throw new ExternalError(cr, "Can't create File", "File already exists: " + this.getInternalFile().getAbsolutePath());
			ok = f.createNewFile();
		}
		catch(final IOException e) {
			ok = false;
		}
		if(!ok)
			throw new ExternalError(cr, "Can't create File", this.getInternalFile().getAbsolutePath());
		return this;
	}

	/**
	 * °created()DateTime # Returns the datetime the file was created.
	 */
	private JMo_DateTime mCreated(final CallRuntime cr) {
		cr.argsNone();
		BasicFileAttributes attr;

		try {
			attr = Files.readAttributes(this.getInternalFile().toPath(), BasicFileAttributes.class);
		}
		catch(final IOException e) {
//			Err.show(e, true);
			throw new ExternalError(cr, "File-Read-Error", e.getMessage());
		}
		final long l = attr.creationTime().toMillis();
		return new JMo_DateTime(new MDateTime(l));
	}

	/**
	 * °delete()Nil # Deletes the file!
	 */
	private Nil mDelete(final CallRuntime cr) {
		// ATTENTION
//		throw new ExtError(cr, "File-Delete-Forbidden", pFile().getCanonicalPath());

		cr.argsNone();
		final File f = this.getInternalFile();

		if(f.exists()) {
			final boolean deleted = f.delete();
			if(!deleted)
				throw new ExternalError(cr, "File-Delete-Error", this.getInternalFile().getAbsolutePath());
		}
		return Nil.NIL;
	}

	/**
	 * °direct(Str mode=\"rw\")RandomAccessFile # Creates a handle for direct reading/writing
	 */
	private I_Object mDirect(final CallRuntime cr) {
		final I_Object[] directArg = cr.argsFlex(null, 0, 1);
		final String directArgS = directArg.length == 0
			? "rw"
			: Lib_Convert.getStringValue(cr, cr.argType(directArg[0], Str.class));
		return new JMo_RandomAccessFile(cr, this.getInternalFile(), directArgS);
	}

	/**
	 * °dir ^ directory
	 * °directory()Dir # Returns the directory of this file
	 */
	private JMo_Dir mDirectory(final CallRuntime cr) {
		cr.argsNone();
		final String path = this.getInternalFile().getAbsoluteFile().getParent();
		return new JMo_Dir(new File(path));
	}

	/**
	 * °directRead()RandomAccessFile # Creates a handle for direct reading
	 * °directWrite ^ directReadWrite
	 * °directReadWrite()RandomAccessFile # Creates a handle for direct read and write
	 */
	private I_Object mDirectRandomAccess(final CallRuntime cr, final String mode) {
		cr.argsNone();
		return new JMo_RandomAccessFile(cr, this.getInternalFile(), mode);
	}

	/**
	 * °exec(Object args...)Int # Execute this file
	 */
	private Int mExec(final CallRuntime cr) {
		final I_Object args[] = cr.argsVar(this, 0, 0);

		try {
			final String[] execArgs = new String[args.length];
			for(int i = 0; i < args.length; i++)
				execArgs[i] = Lib_Convert.getStringValue(cr, args[i]);

			final SysCmdData data = new SysCmdData(false, true, SYSCMD_IO.LIVE, this.getInternalFile().getAbsolutePath(), execArgs);
			final SysCmdResult result = new SysCmd().exec(data);
			return new Int(result.result);
		}
		catch(final Exception e) {
			throw new ExternalError(cr, "Can't execute File", this.getInternalFile().getAbsolutePath());
		}
	}

	/**
	 * °modified()DateTime # Returns the datetime of the last modify
	 */
	private JMo_DateTime mModified(final CallRuntime cr) {
		cr.argsNone();
		final long l = this.getInternalFile().lastModified();
		return new JMo_DateTime(new MDateTime(l));
	}

	/**
	 * °move(Str|Dir dest)File # Move this file to 'dest' directory
	 */
	private JMo_File mMove(final CallRuntime cr) {
		final I_Object arg = cr.argsExt(this, new Class<?>[]{Str.class, JMo_Dir.class})[0];
		final Path source = this.getInternalFile().toPath();

		File target = null;

		if(arg instanceof Str) {
			final Str str = (Str)arg;
			final String newNamePath = Lib_Convert.getStringValue(cr, str);
			target = new File(newNamePath);
		}
		else
			target = ((JMo_Dir)arg).getInternalFile();

		try {
			final boolean isDir = target.isDirectory();

			if(isDir)
				target = target.toPath().resolve(source.getFileName()).toFile();

			Files.move(source, target.toPath()); //,REPLACE_EXISTING
//				Files.move(source, source.resolveSibling("newname"));
			this.changeFile(target);
			return this;
		}
		catch(final NoSuchFileException e) {
			throw new ExternalError(cr, "File-Move-Error", "Source file doesn't exist: \"" + source.toFile().getAbsolutePath() + "\"");
		}
		catch(final FileAlreadyExistsException e) {
			throw new ExternalError(cr, "File-Move-Error", "Target already exist: \"" + target.getAbsolutePath() + "\"");
		}
		catch(final IOException e) {
			Err.show(e);
			throw new ExternalError(cr, "File-Move-Error", "Can't move \"" + source.toFile().getAbsolutePath() + "\" --> \"" + target.getAbsolutePath() + '"');
		}
	}

	/**
	 * °open()Same # Open the file with the default application
	 * //TODO .edit
	 */
	private JMo_File mOpen(final CallRuntime cr) {
		cr.argsNone();

		try {
			Lib_FileSys.defaultOpen(this.getInternalFile());
		}
		catch(final Err_FileSys e) {
			throw new ExternalError(cr, "Can't open file", this.getInternalFile().getAbsolutePath());
		}
		return this;
	}

	/**
	 * °readBytes()ByteArray # Read all bytes from the file
	 */
	private JMo_ByteArray mReadBytes(final CallRuntime cr) {
		cr.argsNone();
		RandomAccessFile raf = null;

		try {
			raf = new RandomAccessFile(this.getInternalFile(), "r");
			final long len = raf.length();
			if(len > Integer.MAX_VALUE)
				throw new ExternalError(cr, "File-Access-Error", "File is too big: \"" + this.getInternalFile().getAbsolutePath() + '"');
			final byte[] ba = new byte[(int)len];

			final int got = raf.read(ba);
			if(got != len)
				throw new ExternalError(cr, "File-Access-Error", "Can't read full file: \"" + this.getInternalFile().getAbsolutePath() + '"');

			return new JMo_ByteArray(ba);
		}
		catch(final IOException e) {
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
		finally {

			try {
				if(raf != null)
					raf.close();
			}
			catch(final Exception e) {
				Err.show(e);
			}
		}
	}

	/**
	 * °lines ^ readlines
	 * °readLines()List # Read the file and split the content by lines.
	 */
	private JMo_List mReadLines(final CallRuntime cr) {
		cr.argsNone(); // It returns a List, and that have an Autoblock-Do
		final SimpleList<I_Object> list = this.iReadLines(cr);
		return new JMo_List(list);
	}

	/**
	 * °read()Str # Read a UTF-8 string from the file
	 * °read(Charset cs)Str # Read a string from the file
	 */
	private Str mReadString(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		String content = null;

		try {

			if(args.length == 0)
				content = Lib_TextFile.read(this.getInternalFile());	// TODO UTF8?
			else {
				final JMo_Charset jcs = (JMo_Charset)cr.argType(args[0], JMo_Charset.class);
				content = Lib_TextFile.read(this.getInternalFile(), jcs.getCharset());
			}
			return new Str(content);
		}
		catch(final Err_FileSys e) {
			// e.getMessage() = "File/Directory-Access-Error"
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
	}

	/**
	 * °rename(Str newName)Same # Rename the file
	 */
	private JMo_File mRename(final CallRuntime cr) {
		final Str arg = (Str)cr.args(this, Str.class)[0];
		String newName = Lib_Convert.getStringValue(cr, arg);

		// No Path allowed
		if(newName.contains(Sys.getSeperatorDir()))
			throw new ExternalError(cr, "File-Rename failed", "No target-path allowed");

		final File oldFile = this.getInternalFile().getAbsoluteFile();
		newName = oldFile.getParent() + Sys.getSeperatorDir() + newName;
		final File newFile = new File(newName);
		final boolean done = oldFile.renameTo(newFile);
		if(!done)
			throw new ExternalError(cr, "File-Rename failed", oldFile.getAbsolutePath() + " --> " + newFile.getAbsolutePath());

		this.changeFile(newFile);
		return this;
	}

	/**
	 * °set(Atomic val)Same # Set the content of this file. Existing content will be overwritten.
	 * °set(Atomic val, Charset cs)Same # Set the content of this file. Existing content will be overwritten.
	 */
	private JMo_File mSet(final CallRuntime cr) {
		final I_Object[] oa = cr.argsFlex(this, 1, 2);
		final I_Object so = cr.argType(oa[0], A_Atomic.class);
		Charset cs = null;

		if(oa.length == 2) {
			final JMo_Charset jcs = (JMo_Charset)cr.argType(oa[1], JMo_Charset.class);
			cs = jcs.getCharset();
		}

		try {
			final String s = Lib_Convert.getStringValue(cr, so);
			Lib_TextFile.set(this.getInternalFile(), s, cs);
		}
		catch(final Exception err) {
			// e.getMessage() = "File/Directory-Access-Error"
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
		return this;
	}

//	/**	---> .lines.each
//	 * °each ^ eachLine
//	 * °eachLine()List
//	 */
//	private Result_Obj eachLine(final CurProc cr) {
//		cr.parsFlex(JMo_List.class, this, null);
//		final SimpleList<I_Object> list = iReadLines(cr);
//		return new Result_Obj(new JMo_List(list).autoBlockDo(cr), true);
//	}

	/**
	 * °setBytes(ByteArray val)Same # Write bytes to the file
	 */
	private JMo_File mSetBytes(final CallRuntime cr) {
		final JMo_ByteArray ba = (JMo_ByteArray)cr.args(this, JMo_ByteArray.class)[0];

		try {
			Lib_RandomAccess.set(this.getInternalFile(), ba.getValue());
		}
		catch(final Exception err) {
			// e.getMessage() = "File/Directory-Access-Error"
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
		return this;
	}

	/**
	 * °size()Long # Returns the size of this file
	 */
	private JMo_Long mSize(final CallRuntime cr) {
		cr.argsNone();
		return new JMo_Long(this.getInternalFile().length());
	}

	/**
	 * °suffix()Str # Returns the suffix of this file
	 */
	private Str mSuffix(final CallRuntime cr) {
		cr.argsNone();
		final String suffix = Lib_FileSys.getSuffix(this.getInternalFile());
		return new Str(suffix);
	}

	/**
	 * °toRandomAccess(Str mode)RandomAccessFile # Creates a handle for direct read and write
	 */
	private I_Object mToRandomAccess(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String mode = Lib_Convert.getStringValue(cr, arg);
		return new JMo_RandomAccessFile(cr, this.getInternalFile(), mode);
	}

	/**
	 * °touch()Same # Create empty file if it doesn't exist. Update timestamp to now.
	 */
	private JMo_File mTouch(final CallRuntime cr) {
		cr.argsNone();

		try {
			final File f = this.getInternalFile();

			if(f.exists()) {
				if(!f.setLastModified(System.currentTimeMillis()))
					throw new ExternalError(cr, "Can't update modified timestamp", this.getInternalFile().getAbsolutePath());
			}
			else if(!f.createNewFile())
				throw new ExternalError(cr, "Can't create File", this.getInternalFile().getAbsolutePath());
		}
		catch(final IOException e) {
			throw new ExternalError(cr, "Can't touch File", this.getInternalFile().getAbsolutePath());
		}
		return this;
	}

	/**
	 * °write(Atomic val, IntNumber position)Same # Write a atomic value to the file, starting at specified position
	 * °write(Atomic val, IntNumber position, Charset cs)Same # Write a atomic value to the file, starting at specified position
	 */
	private JMo_File mWrite(final CallRuntime cr) {
		final I_Object[] oa = cr.argsFlex(this, 2, 3);

		final I_Object so = cr.argType(oa[0], A_Atomic.class);

		final I_Object po = cr.argType(oa[1], A_IntNumber.class);
		final long pos = Lib_Convert.getLongValue(cr, po);
		Lib_Error.ifTooSmall(cr, 1, pos);

		Charset cs = null;

		if(oa.length == 3) {
			final JMo_Charset jcs = (JMo_Charset)cr.argType(oa[2], JMo_Charset.class);
			cs = jcs.getCharset();
		}

		try {
			final String s = Lib_Convert.getStringValue(cr, so);
			Lib_TextFile.write(this.getInternalFile(), pos - 1, s, cs);
		}
		catch(final Exception err) {
			// e.getMessage() = "File/Directory-Access-Error"
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
		return this;
	}

	/**
	 * °writeBytes(ByteArray val)Same # Write bytes to the file
	 * °writeBytes(ByteArray val, IntNumber position)Same # Write bytes to the file, starting at specified position
	 */
	private JMo_File mWriteBytes(final CallRuntime cr) {
		final I_Object[] oa = cr.argsFlex(this, 1, 2);
		final I_Object so = cr.argType(oa[0], JMo_ByteArray.class);
		Long pos = null;

		if(oa.length == 2) {
			final I_Object po = cr.argType(oa[1], A_IntNumber.class);
			pos = Lib_Convert.getLongValue(cr, po);
			Lib_Error.ifTooSmall(cr, 1, pos);
		}

		try {
			final byte[] ba = ((JMo_ByteArray)so).getValue();
			final Long index = pos == null
				? null
				: pos - 1;

			Lib_RandomAccess.write(this.getInternalFile(), index, ba);
		}
		catch(final Exception err) {
			// e.getMessage() = "File/Directory-Access-Error"
			throw new ExternalError(cr, "File-Access-Error", "File: \"" + this.getInternalFile().getAbsolutePath() + '"');
		}
		return this;
	}

}
