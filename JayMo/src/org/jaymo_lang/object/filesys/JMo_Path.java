/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.filesys;

import java.io.File;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_Path extends A_Object implements I_Object {

	private Call arg;
	private File file = null;


	public JMo_Path(final Call arg) {
		Err.ifNull(arg);
		this.arg = arg;
	}

	public JMo_Path(final File f) {
		this.arg = null;
		this.file = f;
	}

	@Override
	public boolean equals(final Object o) {
		if(o instanceof JMo_Path)
			return ((JMo_Path)o).file.equals(this.file);
		else
			return super.equals(o);
	}

	public File getInternalFile() {
		return this.file;
	}

	@Override
	public void init(final CallRuntime cr) {
		cr.getStrict().checkSandbox(cr, this.getTypeName());

		if(this.arg != null) {
			I_Object filename = cr.execInit(this.arg, this);
			filename = cr.argType(filename, Str.class);

			final String filestr = ((Str)filename).getValue();
			this.file = new File(filestr);
			this.arg = null;
		}
	}

	@Override
	public Boolean isGreater2(final I_Object o) {
		if(o instanceof JMo_Path)
			return this.file.compareTo(((JMo_Path)o).file) > 0;
		return null;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(this.file == null)
			return "Path";

		switch(type) {
			case REGULAR:
				return this.file.getAbsolutePath().toString();
			case NESTED:
			case IDENT:
				return "Path";
			default:
				return "Path(" + '"' + this.file.getAbsolutePath().toString() + '"' + ")";
		}
	}


//	private Bool is(boolean file) {
//		if(file) {
//			boolean b=item.isFile();
//			return Bool.getObject(b);
//		} else {
//			boolean b=item.isDirectory();
//			return Bool.getObject(b);
//		}
//	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

//		S_Object result=null;
//
		/**
		 * °to()FileSys # If the path is a directory, return a Dir-Object. Elsewhere return a File-Object
		 * °toFile()File # Create a File-Object
		 * °toDir()Dir # Create a Dir-Object
		 */
		switch(method) {
			case "exists":
				return A_Object.stdResult(this.mExists(cr));
			case "==":
			case "equals":
				return A_Object.stdResult(this.mEquals2(cr));

			case "parent":
				return A_Object.stdResult(this.mParent(cr));
			case "name":
				return A_Object.stdResult(this.mName(cr));

			case "isDir":
				return A_Object.stdResult(this.mIsDir(cr));
			case "isFile":
				return A_Object.stdResult(this.mIsFile(cr));
			case "isHidden":
				return A_Object.stdResult(this.mIsHidden(cr));

//			case "do": // For Path, to passthrough
			case "to":
				final JMo_Path f1 = this.file.isDirectory()
					? new JMo_Dir(this.file)
					: new JMo_File(this.file);
				return A_Object.stdResult(f1);

			case "toFile":
				if(this.file != null && this.file.isDirectory())
					throw new ExternalError(cr, "Path to File error", "This is a directory, not a file: " + this.file.getAbsoluteFile());
				final JMo_File f2 = new JMo_File(this.file);
				return A_Object.stdResult(f2);

			case "toDir":
				if(this.file != null && this.file.isFile())
					throw new ExternalError(cr, "Path to Dir error", "This is a file, not a directory: " + this.file.getAbsoluteFile());
				final JMo_Dir f3 = new JMo_Dir(this.file);
				return A_Object.stdResult(f3);

			case "path":
			case "absolute":
			case "absolutePath":
				return A_Object.stdResult(this.mAbsolutePath(cr));

			default:
				return null;
		}
//			case "isFile": result=is(true); break;
//			case "isPath": result=is(false); break;
//			case "isDir": result=is(false); break;
//		}
//		if(result!=null)
//			return new Result_Obj(result, false);
//
//		if(this.item.isFile()) {
//			jmo.object.filesys.JMo_File f=new
//			jmo.object.filesys.JMo_File(this.item);
//			return f.call2(c);
//		}
//		if(this.item.isDirectory()) {
//			JMo_Dir f=new JMo_Dir(this.item);
//			return f.call2(c);
//		}

//		return new Result_Obj(path.call(cr), true); // TODO call2 ?!?
		// return this.path.call2(c); // TODO call ?!?
	}

	protected void changeFile(final File f) {
		this.file = f;
	}

	/**
	 * °path ^ getAbsolutePath
	 * °absolute ^ getAbsolutePath
	 * °absolutePath()Str # Returns the absolute path of this directory/file
	 */
	private Str mAbsolutePath(final CallRuntime cr) {
		cr.argsNone();
		final String path = this.file.getAbsolutePath();
		return new Str(path);
	}

	/**
	 * °== ^ equals
	 * °equals(FileSys other)Bool # Checks whether this file or directory equals to other
	 */
	private Bool mEquals2(final CallRuntime cr) {
		final JMo_Path arg = (JMo_Path)cr.args(this, JMo_Path.class)[0];
		final File pf = arg.file;
		return Bool.getObject(this.file.equals(pf));
	}

	/**
	 * °exists()Bool # Checks whether the file or directory exists
	 */
	private Bool mExists(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this.file.exists());
	}

	/**
	 * °isDir()Bool # Checks whether the current object is a directory
	 */
	private Bool mIsDir(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this.file.isDirectory());
	}

	/**
	 * °isFile()Bool # Checks whether the current object is a file
	 */
	private Bool mIsFile(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this.file.isFile());
	}

	/**
	 * °isHidden()Bool # Checks whether the current object is hidden
	 */
	private Bool mIsHidden(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this.file.isHidden());
	}

	/**
	 * °name()Str # Get the name
	 */
	private Str mName(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.file.getName());
	}

	/**
	 * °parent()Dir # Returns the parent directory
	 */
	private JMo_Dir mParent(final CallRuntime cr) {
		cr.argsNone();
		return new JMo_Dir(this.file.getParentFile());
	}

}
