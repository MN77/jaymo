/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.filesys;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.I_Integer;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_Array;


/**
 * @author Michael Nitsche
 * @created 01.11.2020
 *
 *          Modes:
 *          r = Read only, fast
 *          rw = Read/Write, flush on request, timestamp update on close, fast
 *          rwd = Read/Write, sync write, timestamp update on close, slow
 *          rws = Read/Write, sync write, timestamp update immediately, very slow
 */
public class JMo_RandomAccessFile extends A_Object {

	private final ArgCallBuffer cMode, cFile;
	private final String[]      allowed = {"r", "rw", "rws", "rwd"};
	private String              mode    = "r";
	private File                file    = null;
	private RandomAccessFile    raf     = null;


	/**
	 * !Handle to a file, for direct read and write access
	 * +RandomAccessFile(File|Str file, Str mode)
	 * 'File("xyz.txt").directRead.pass
	 * ' %.seek(5)
	 * ' %.read.print
	 */
	public JMo_RandomAccessFile(final Call file, final Call mode) {
		this.cFile = new ArgCallBuffer(0, file);
		this.cMode = new ArgCallBuffer(1, mode);
	}

	public JMo_RandomAccessFile(final CallRuntime cr, final File f, final String mode) {
		this.file = f;
		this.cFile = null;
		this.iSetMode(cr, mode);
		this.cMode = null;
	}

	@Override
	public void init(final CallRuntime cr) {
		cr.getStrict().checkSandbox(cr, "RandomAccessFile");

		if(this.cFile != null) {
			final I_Object fo = this.cFile.initExt(cr, this, JMo_File.class, Str.class);
			if(fo instanceof JMo_File)
				this.file = ((JMo_File)fo).getInternalFile();
			else
				this.file = new File(((Str)fo).getValue());
		}

		if(this.cMode != null) {
			final Str o = this.cMode.init(cr, this, Str.class);
			this.iSetMode(cr, Lib_Convert.getStringValue(cr, o));
		}

		try {
			this.raf = new RandomAccessFile(this.file, this.mode);
		}
		catch(final FileNotFoundException e) {
			throw new ExternalError(cr, "File access error", e.getMessage());
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final String typeName = Lib_Type.getName(this);

		switch(type) {
			case NESTED:
			case IDENT:
				return typeName;
			case REGULAR:
			case DESCRIBE:
			default:
				final StringBuilder sb = new StringBuilder();
				sb.append(typeName);
				sb.append('(');
				sb.append(FormString.quote(this.file.getAbsolutePath(), '"', '\\', false));
				sb.append(",\"");
				sb.append(this.mode);
				sb.append("\")");
				return sb.toString();
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "close":
				this.mClose(cr);
				return A_Object.stdResult(Nil.NIL);

			case "seek":
				this.mSeek(cr);
				return A_Object.stdResult(this);
			case "seekStart":
				this.mSeekStart(cr);
				return A_Object.stdResult(this);
			case "seekEnd":
				this.mSeekEnd(cr);
				return A_Object.stdResult(this);

//			case "getLen":
//			case "getLength":
			case "size":
				return this.mSize(cr);

			case "read":
				return this.mRead(cr);
			case "readByte":
				return this.mReadByte(cr);
			case "readBytes":
				return this.mReadBytes(cr);
			case "readInt":
				return this.mReadInt(cr);
			case "readString":
				return this.mReadString(cr);

			case "write":
				this.mWrite(cr);
				return A_Object.stdResult(this);
			case "writeByte":
				this.mWriteByte(cr);
				return A_Object.stdResult(this);
			case "writeBytes":
				this.mWriteBytes(cr);
				return A_Object.stdResult(this);
			case "writeInt":
				this.mWriteInt(cr);
				return A_Object.stdResult(this);
			case "writeString":
				this.mWriteString(cr);
				return A_Object.stdResult(this);
			case "flush":
				this.mFlush(cr);
				return A_Object.stdResult(this);
		}
		return null;
	}

	private ExternalError iError(final CallRuntime cr, final IOException e) {
		throw new ExternalError(cr, "File access error", e.getMessage());
	}

	private void iSetMode(final CallRuntime cr, final String newMode) {

		if(Lib_Array.contains(this.allowed, newMode))
			this.mode = newMode;
		else {
			this.mode = "rw"; // Default
			cr.warning("Invalid mode for file access",
				"Allowed are [\"" + ConvertArray.toString("\",\"", (Object[])this.allowed) + "\"], but got \"" + newMode + "\". Using default: \"" + this.mode + '"');
		}
	}

	/**
	 * °close()Nil # Close and release the file. No further access is possible.
	 */
	private void mClose(final CallRuntime cr) {
		cr.argsNone();

		try {
			if(this.raf != null)
				this.raf.close();
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °flush()Same # Force and synchronize all unwritten data to the underlaying device.
	 */
	private void mFlush(final CallRuntime cr) {
		cr.argsNone();

		try {
			this.raf.getFD().sync();
		}
		catch(final IOException e) {
			throw new ExternalError(cr, e.getMessage(), this.file.getAbsolutePath());
		}
	}

	/**
	 * °read()Int # Read a single byte and return it as Int value (0-255)
	 */
	private ObjectCallResult mRead(final CallRuntime cr) {
		cr.argsNone();

		try {
			final int i = this.raf.read();
			return A_Object.stdResult(new Int(i));
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °readByte()Byte # Read a single byte and return it as Byte value (-128-127)
	 */
	private ObjectCallResult mReadByte(final CallRuntime cr) {
		cr.argsNone();

		try {
			final byte b = this.raf.readByte();
			return A_Object.stdResult(new JMo_Byte(b));
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °readBytes(IntNumber length)List # Read 'length' bytes and return it as List.
	 */
	private ObjectCallResult mReadBytes(final CallRuntime cr) {
		final I_Object o = cr.args(this, A_IntNumber.class)[0];
		final int len = Lib_Convert.getIntValue(cr, o);

		try {
//			if(this.raf.getFilePointer()+len >= this.raf.length())
//				len = (int)(this.raf.length() - this.raf.getFilePointer());

			final byte[] btr = new byte[len];
			this.raf.readFully(btr, 0, len);

			final SimpleList<I_Object> al = new SimpleList<>(len);
			for(int i = 0; i < len; i++)
				al.add(new JMo_Byte(btr[i]));

			return A_Object.stdResult(new JMo_List(al));
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °readInt()Int # Read four bytes and return it as a single Int value.
	 */
	private ObjectCallResult mReadInt(final CallRuntime cr) {
		cr.argsNone();

		try {
			final int i = this.raf.readInt();
			return A_Object.stdResult(new Int(i));
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °readString(IntNumber bytes, Bool utf8)Str # Read 'bytes' single bytes from the file and create a Str. Take care, UTF-8-Chars may need more than one single byte!
	 */
	private ObjectCallResult mReadString(final CallRuntime cr) {
		final I_Object[] o = cr.args(this, A_IntNumber.class, Bool.class);
		int len = Lib_Convert.getIntValue(cr, o[0]);
		final boolean utf8 = Lib_Convert.getBoolValue(cr, o[1]);
		final StringBuilder sb = new StringBuilder();

		try {
			if(this.raf.getFilePointer() + len >= this.raf.length())
				len = (int)(this.raf.length() - this.raf.getFilePointer());

			if(utf8) {
				final byte[] btr = new byte[len];
				this.raf.readFully(btr, 0, len);
				final String s = new String(btr, "UTF-8");
				sb.append(s);
//				sb.append(raf.readUTF()); //Java-Eigenes Format, das nur mit writeUTF erzeugt wird
			}
			else {
//				for(int i=1; i<=len; i++) {
//					sb.append((char)this.raf.readByte());
//				}
				final byte[] btr = new byte[len];
				this.raf.readFully(btr, 0, len);

				for(int i = 0; i < len; i++) {
					int bi = btr[i];
					bi = bi & 255;
//					if(bi<0)
//						bi+=255;
//					MOut.temp(bi, (char)bi, btr[i], (char)btr[i]);
					sb.append((char)bi);
				}
			}
			return A_Object.stdResult(new Str(sb.toString()));
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

//	private ObjectCallResult readChar(CallRuntime cr) {
//		cr.argsNone();
//		try {
//			int i = this.raf.read();
//			return A_Object.stdResult( new Int(i) );
//		}
//		catch(IOException e) {
//			throw new ExtError(cr,"File access error",e.getMessage());
//		}
//	}

	/**
	 * °seek(IntNumber pos)Same # Move file-pointer to position 'pos'
	 */
	private void mSeek(final CallRuntime cr) {
		final I_Object o1 = cr.args(this, I_Integer.class)[0];
		final long l = Lib_Convert.getLongValue(cr, o1);
		Lib_Error.ifTooSmall(cr, 1, l);

		try {
			this.raf.seek(l - 1); // pos --> index
		}
		catch(final IOException e) {
			this.iError(cr, e);
		}
	}

	/**
	 * °seekEnd()Same # Move file-pointer to the end of the file. This is useful to append some data.
	 */
	private void mSeekEnd(final CallRuntime cr) {
		cr.argsNone();

		try {
			this.raf.seek(this.raf.length());
		}
		catch(final IOException e) {
			this.iError(cr, e);
		}
	}

	/**
	 * °seekStart()Same # Move file-pointer to the first position
	 */
	private void mSeekStart(final CallRuntime cr) {
		cr.argsNone();

		try {
			this.raf.seek(0);
		}
		catch(final IOException e) {
			this.iError(cr, e);
		}
	}

	/**
	 * #°getLen ^ getLength
	 * #°getSize ^ getLength
	 * °size()Long # Returns the size of this file
	 */
	private ObjectCallResult mSize(final CallRuntime cr) {
		cr.argsNone();

		try {
			final long len = this.raf.length();
			return A_Object.stdResult(new JMo_Long(len));
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °write(Int value)Same # Write a single byte (0-255) at current position to the file.
	 */
	private void mWrite(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int i = Lib_Convert.getIntValue(cr, o);
		Lib_Error.ifNotBetween(cr, 0, 255, i, "Position to seek");

		try {
			this.raf.write(i);
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °writeByte(Byte value)Same # Write a single byte (-128-127) at current position to the file.
	 */
	private void mWriteByte(final CallRuntime cr) {
		final I_Object o = cr.args(this, JMo_Byte.class)[0];
		final byte b = Lib_Convert.getByteValue(cr, o);

		try {
			this.raf.writeByte(b);
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °writeBytes(Byte value...)Same # Write a bytes (-128-127) to the file.
	 */
	private void mWriteBytes(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);

		try {
			final int len = oa.length;
			final byte[] ba = new byte[len];
			for(int i = 0; i < len; i++)
				ba[i] = Lib_Convert.getByteValue(cr, cr.argType(oa[i], JMo_Byte.class));
			this.raf.write(ba);
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °writeInt(Int value)Same # Write a Int to the file, which takes 4 bytes of space.
	 */
	private void mWriteInt(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int i = Lib_Convert.getIntValue(cr, o);

		try {
			this.raf.writeInt(i);
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

	/**
	 * °writeString(Str s, Bool utf8)Same # Write the Str to the file. If UTF-8 is used, chars may need more than one single byte!
	 */
	private void mWriteString(final CallRuntime cr) {
		final I_Object[] o = cr.args(this, Str.class, Bool.class);
		final String s = Lib_Convert.getStringValue(cr, o[0]);
		final boolean utf8 = Lib_Convert.getBoolValue(cr, o[1]);

		try {
			if(utf8)
				this.raf.write(s.getBytes("UTF-8"));
			else
				this.raf.writeBytes(s);
		}
		catch(final IOException e) {
			throw this.iError(cr, e);
		}
	}

}
