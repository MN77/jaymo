/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomicMath;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_Float extends A_DecNumber implements I_Decimal, I_Atomic {

	private final float value;


	/**
	 * ! Object of a immutable 32 bit decimal number between +/-1,4E-45 and +/-3,4E+38.
	 * + 12.34f
	 * + -12.34f
	 * + 12f
	 * + -12f
	 */
	public JMo_Float(final float val) {
//		this.value = (float)Lib_Math.normalize(val,6);	// 10	or 6, see also Dec
		this.value = val; // Not limited by default.
	}

	public ATOMIC getEnum() {
		return ATOMIC.FLOAT;
	}

	public Float getValue() {
		return this.value;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(Float.isNaN(this.value))
			return ATOMIC.NOT_A_NUMBER;
		if(this.value == Float.POSITIVE_INFINITY)
			return ATOMIC.INFINITY_POSITIVE;
		if(this.value == Float.NEGATIVE_INFINITY)
			return ATOMIC.INFINITY_NEGATIVE;

		if(type == STYPE.DESCRIBE) {
			final String s = "" + this.value;
			if(s.indexOf('E') < 0) // TODO Testen
				return s + "f";

//			 DecimalFormat df = new DecimalFormat("#");
//		        df.setMaximumFractionDigits(8);

//			String.format("%.12f", myvalue)
			final String sVal = new BigDecimal(this.value).toPlainString();

			final StringBuilder sb = new StringBuilder();
			sb.append(sVal);

			if(sVal.indexOf('.') == -1)
				sb.append(".0");

			sb.append('f');
			return sb.toString();
		}
		return type == STYPE.IDENT
			? this.value + "f"
			: "" + this.value;
	}

	@Override
	protected ObjectCallResult call5(final CallRuntime cr, final String method) {
		return null;
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		return new JMo_Float(Float.MAX_VALUE);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return new JMo_Float(Float.MIN_VALUE);
	}

	@Override
	protected A_Number numberCalc0(final CallRuntime cr, final NOP0 op) {

		switch(op) {
			case NEG:
				return new JMo_Float(-this.value);
			case ABS:
				return new JMo_Float(Math.abs(this.value));
			case EXP:
				return new JMo_Double(Math.exp(this.value));
			case INC:
				return new JMo_Float(this.value + 1);
			case DEC:
				return new JMo_Float(this.value - 1);
			case POW:
				return new JMo_Double(this.value * this.value);
			case ROOT:
				return new JMo_Double(Math.sqrt(this.value));
			case LOG10:
				return new JMo_Double(Math.log10(this.value));
			case LOGN:
				return new JMo_Double(Math.log(this.value));
		}
		throw Err.impossible(op);
	}

	@Override
	protected A_Number numberCalc1(final CallRuntime cr, final NOP1 op, final A_Number arg) {
		if(arg instanceof I_BigNumber)
			return Lib_AtomicMath.calcBigDec(cr, op, BigDecimal.valueOf(this.value), Lib_Convert.getBigDecimalValue(cr, arg));
		else
			return Lib_AtomicMath.calcDouble(cr, op, this.value, Lib_Convert.getDoubleValue(cr, arg));
	}

	@Override
	protected A_Number numberCalcSame(final CallRuntime cr, final A_Number num, final boolean inc) {
		final double arg = Lib_Convert.getDoubleValue(cr, num);
		return new JMo_Float((float)(inc ? this.value + arg : this.value - arg));
	}

}
