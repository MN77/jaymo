/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.COMPARE;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Sequence;

import de.mn77.base.data.convert.ConvertChar;
import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.numsys.NumSys_Hex;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.type.Lib_Compare;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Char extends A_Chars implements I_Atomic {

	private final char value;


	/**
	 * ! Object of a immutable single (Java) UTF-16-Character
	 * + 'a'
	 */
	public Char(final char val) {
		this.value = val;
	}

	public ATOMIC getEnum() {
		return ATOMIC.CHAR;
	}

	public Character getValue() {
		return this.value;
	}

	@Override
	public Boolean isGreater3(final I_Atomic o) {
		if(o instanceof Char)
//			return Character.valueOf(this.value).compareTo(((Char)o).getValue());
			return Lib_Compare.isGreaterChar(this.value, ((Char)o).getValue());
		return null;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(type == STYPE.REGULAR)
			return "" + this.value;

		final boolean quote = type != STYPE.NESTED;
		final StringBuilder sb = new StringBuilder();

		if(quote)
			sb.append('\'');

		if(this.value > 31 && this.value < 55296 || this.value == 9 || this.value >= 57344) // Invalid: D800 - DFFF
			sb.append(this.value);
		else {
			sb.append("\\u");
			sb.append(FormNumber.width(4, NumSys_Hex.toHex(this.value), false));
		}
		if(quote)
			sb.append('\'');

		return sb.toString();
	}

	@Override
	protected ObjectCallResult call4(final CallRuntime cr, final String method) {

		switch(method) {
			case "++":
			case "inc":
				return A_Object.stdResult(this.mIncDec(cr, true));
			case "--":
			case "dec":
				return A_Object.stdResult(this.mIncDec(cr, false));

			case "/":
			case "div":
				/**
				 * °/^div
				 * °div()Object # Leads to an error, because a single char currently can't divided.
				 * Note: 'c'/1 could be okay
				 */
				throw new RuntimeError(cr, "One single character cannot be split.", "Dividing <Char>: " + this.toString(cr, STYPE.IDENT) + " / …"); // Because all other atomic types has this function
			case "-":
			case "sub":
				/**
				 * °-^sub
				 * °sub()Object # Leads to an error. Please use '.dec' or '--'
				 */
				throw new RuntimeError(cr, "Cannot subtract anything from a single character.", "Subtraction from <Char>: " + this.toString(cr, STYPE.IDENT) + " - …"); // Because all other atomic types has this function

			case "ord":
				return A_Object.stdResult(this.mOrd(cr));
			case "alphabet":
				return A_Object.stdResult(this.mAlphabet(cr));

			case "toUnicode":
			case "unicode":
				return A_Object.stdResult(this.mUnicode(cr));

			/**
			 * °isLetter()Bool # Returns true, if this char is a Letter (a-z and A-Z)
			 */
			case "isLetter":
				return A_Object.stdResult(Bool.getObject(Character.isLetter(this.value)));

			default:
				return null;
		}
	}

	@Override
	protected final boolean charsIsBlank() {
		return this.value == ' ' || this.value == '\t' || Character.isWhitespace(this.value);
	}

	@Override
	protected final boolean charsIsCaseDown() {
		return Character.isLowerCase(this.value);
	}

	@Override
	protected final boolean charsIsCaseUp() {
		return Character.isUpperCase(this.value);
	}

	@Override
	protected final boolean charsIsNumber() {
		return Character.isDigit(this.value);
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		return new Char(Character.MAX_VALUE);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return new Char(Character.MIN_VALUE);
	}

	@Override
	protected Str mAdd(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		String result = "" + this.value;

		for(final I_Object arg : args) {
			final String par2 = Lib_Convert.getStringValue(cr, arg);
			result += par2;
		}
		return new Str(result);
	}

	@Override
	protected Char mCaseDown(final CallRuntime cr) {
		cr.argsNone();
		return new Char(("" + this.value).toLowerCase().charAt(0));
	}

	@Override
	protected Char mCaseUp(final CallRuntime cr) {
		cr.argsNone();
		return new Char(("" + this.value).toUpperCase().charAt(0));
	}

	@Override
	protected Char mChangeChar(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, A_IntNumber.class, A_Chars.class);
		int pos = Lib_Convert.getIntValue(cr, oa[0]);
		pos = Lib_Sequence.realPosition(cr, pos, 1, false);
		final I_Object o = oa[1];
		final String n = Lib_Convert.getStringValue(cr, o);
		return new Char(n.charAt(0));
		// this.value=new String(ca);
	}

	@Override
	protected I_Object mCharAt(final CallRuntime cr, final boolean lazy) {
		int pos = Lib_Convert.getIntValue(cr, cr.args(this, A_IntNumber.class)[0]);
		pos = Lib_Sequence.realPosition(cr, pos, 1, lazy); // Check position is valid
		if(pos != 1)
			return Nil.NIL;
		return this;
	}

	@Override
	protected Bool mComparsion(final CallRuntime cr, final COMPARE m) {
		final I_Object o = cr.args(this, A_Chars.class)[0]; // Theoretisch möglich: I_Atomic
//		final int arg = Lib_Convert.getIntValue(cr, o, true);
//
//		switch(m) {
//			case G:
//				return Bool.getObject(this.value > arg);
//			case L:
//				return Bool.getObject(this.value < arg);
//			case GE:
//				return Bool.getObject(this.value >= arg);
//			case LE:
//				return Bool.getObject(this.value <= arg);
//			default:
//				throw Err.impossible(m);
//		}

		final String arg = Lib_Convert.getStringValue(cr, o);
		final String val = "" + this.value;

		switch(m) {
			case G:
				return Bool.getObject(val.compareTo(arg) > 0);
			case L:
				return Bool.getObject(val.compareTo(arg) < 0);
			case GE:
				return Bool.getObject(val.compareTo(arg) >= 0);
			case LE:
				return Bool.getObject(val.compareTo(arg) <= 0);
			default:
				throw Err.impossible(m);
		}
	}

	@Override
	protected Str mMultiply(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int i = Lib_Convert.getIntValue(cr, arg);
		final String s = Lib_String.sequence(this.value, i);
		return new Str(s);
	}

	@Override
	protected JMo_List mSelect(final CallRuntime cr, final boolean lazy) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		final SimpleList<I_Object> list = new SimpleList<>(args.length);

		for(final I_Object arg : args) {
			int pos = Lib_Convert.getIntValue(cr, cr.argType(arg, A_IntNumber.class));
			pos = Lib_Sequence.realPosition(cr, pos, 1, lazy);
			if(lazy && (pos < 1 || pos > 1))
				list.add(Nil.NIL);
			else
				list.add(new Char(this.value));
		}
		return new JMo_List(list);
	}

	@Override
	protected final Str mUpdate(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 1, 1);
		final I_Object obj = cr.argType(oa[0], null);
		final char c = Lib_Convert.getCharValue(cr, obj);
		final StringBuilder sb = new StringBuilder();
		sb.append(this.value);

		for(int i = 1; i < oa.length; i++) {
			final int pos1 = Lib_Convert.getIntValue(cr, cr.argType(oa[i], A_IntNumber.class));
			int pos2 = Lib_Sequence.realPosition(cr, pos1, sb.length(), lazy);

			if(pos2 < 0 && lazy) {
				pos2 = pos1;
				while(pos2 > sb.length())
					sb.append(' ');
			}
			sb.setCharAt(pos2 - 1, c);
		}
		return new Str(sb.toString());
	}

	/**
	 * °alphabet()Int # Return the position of this char in the english alphabet. If the character is not a letter in the alphabet, -1 is returned.
	 */
	private Int mAlphabet(final CallRuntime cr) {
		cr.argsNone();
		final int ord = this.value;
		if(ord >= 65 && ord <= 90)
			return new Int(ord - 64);
		if(ord >= 97 && ord <= 122)
			return new Int(ord - 96);
		return new Int(-1);
	}

	/**
	 * °++ ^ inc
	 * °inc()Char # Increment char by 1
	 * °inc(IntNumber value)Char # Increment char by value
	 * °-- ^ dec
	 * °dec()Char # Decrement char by 1
	 * °dec(IntNumber value)Char # Decrement char by value
	 */
	private Char mIncDec(final CallRuntime cr, final boolean inc) {
		final I_Object[] oa = cr.argsFlex(this, 0, 1);
		// MOut.dev(this.value, (Object)oa);
		if(oa.length == 0)
			return new Char((char)(inc
				? this.value + 1
				: this.value - 1));

		final I_Atomic o = (I_Atomic)cr.argType(oa[0], I_Atomic.class);
		// int arg=(Integer)((S_Atomic)o).convertToInt(cr).gValue();

		final int arg = Lib_Convert.getIntValue(cr, o);
		final int val = this.value;
		final int res = inc
			? val + arg
			: val - arg;
		return new Char((char)res);
	}

	/**
	 * °ord()Int # Return the Unicode integer value
	 */
	private Int mOrd(final CallRuntime cr) {
		cr.argsNone();
		final int ord = this.value;
		return new Int(ord);
	}

	/**
	 * °unicode ^ toUnicode
	 * °toUnicode()Str # Generates a String with the representing Unicode-Value
	 */
	private I_Object mUnicode(final CallRuntime cr) {
		cr.argsNone();
		return new Str(ConvertChar.getUnicode(this.value));
	}

}
