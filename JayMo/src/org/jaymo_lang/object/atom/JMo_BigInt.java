/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomicMath;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.data.bigcalc.Lib_BigCalc;
import de.mn77.base.data.util.Lib_BigMath;
import de.mn77.base.data.util.Lib_E_Notation;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2021-10-10
 */
public class JMo_BigInt extends A_IntNumber implements I_Integer, I_Atomic, I_BigNumber {

	private final BigInteger value;


	/**
	 * ! Object of a immutable big integer number
	 * + 1234a
	 * + -1234a
	 */
	public JMo_BigInt(final BigInteger val) {
		this.value = val;
	}

	public ATOMIC getEnum() {
		return ATOMIC.BIGINT;
	}

	public int getIntValue(final CallRuntime cr) {
		return Lib_BigMath.convertToInt(this.value);
	}

	public BigInteger getValue() {
		return this.value;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		String result = "" + this.value;
		final boolean useNotationE = type != STYPE.DESCRIBE;
		final boolean addSuffix = type == STYPE.IDENT || type == STYPE.DESCRIBE;

		if(useNotationE && result.length() > JayMo.NESTED_MAX)
			result = Lib_E_Notation.toNotationE(result, 16);

		if(addSuffix)
			result += 'a';

		return result;
	}

	@Override
	protected ObjectCallResult call5(final CallRuntime cr, final String method) {
		return null;
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		throw this.newErrorMinMaxValue(cr);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		throw this.newErrorMinMaxValue(cr);
	}

	/**
	 * Override for BigInt-Type
	 */
	@Override
	protected final ObjectCallResult mTimes(final CallRuntime crOld, final boolean reverse) {
		crOld.argsNone();
		Lib_Exec.checkLoopWithout(crOld);

		final BigInteger value = this.getValue();

		final LoopHandle handle = new LoopHandle(this, reverse ? "timesDown" : "timesUp");
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		if(reverse)
			for(BigInteger i = value; i.compareTo(BigInteger.valueOf(1)) >= 0; i = i.subtract(BigInteger.valueOf(1))) {
				handle.startLap();
				result = Lib_Exec.execBlockStream(crNew, new JMo_BigInt(i));

				if((result = Lib_Exec.loopResult(result)) instanceof Return)
					return ((Return)result).getLoopResult();
			}
		else
			for(BigInteger i = BigInteger.valueOf(1); i.compareTo(value) <= 0; i = i.add(BigInteger.valueOf(1))) {
				handle.startLap();
				result = Lib_Exec.execBlockStream(crNew, new JMo_BigInt(i));

				if((result = Lib_Exec.loopResult(result)) instanceof Return)
					return ((Return)result).getLoopResult();
			}
		return new ObjectCallResult(result, true);
	}

	@Override
	protected A_Number numberCalc0(final CallRuntime cr, final NOP0 op) {

		switch(op) {
			case NEG:
				return new JMo_BigInt(this.value.negate());
			case ABS:
				return new JMo_BigInt(this.value.abs());
			case EXP:
				return new JMo_BigDec(Lib_BigCalc.exp(new BigDecimal(this.value), JMo_BigDec.CONTEXT));
			case INC:
				return new JMo_BigInt(this.value.add(BigInteger.ONE));
			case DEC:
				return new JMo_BigInt(this.value.subtract(BigInteger.ONE));
			case POW:
				return new JMo_BigInt(this.value.multiply(this.value));
			case ROOT:
				return new JMo_BigDec(new BigDecimal(this.value).sqrt(JMo_BigDec.CONTEXT));
			case LOG10:
				return new JMo_BigDec(Lib_BigMath.log(new BigDecimal(this.value), BigDecimal.valueOf(10), JMo_BigDec.CONTEXT));
			case LOGN:
				return new JMo_BigDec(Lib_BigMath.log(new BigDecimal(this.value), Lib_BigMath.EULER_256, JMo_BigDec.CONTEXT));
		}
		throw Err.invalid(op);
	}

	@Override
	protected A_Number numberCalc1(final CallRuntime cr, final NOP1 op, final A_Number arg) {
		if(arg instanceof I_Integer)
			return Lib_AtomicMath.calcBigInt(cr, op, this.value, Lib_Convert.getBigIntegerValue(cr, arg));
		else
			return Lib_AtomicMath.calcBigDec(cr, op, new BigDecimal(this.value), Lib_Convert.getBigDecimalValue(cr, arg));
	}

	@Override
	protected A_Number numberCalcSame(final CallRuntime cr, final A_Number num, final boolean inc) {
		final BigInteger arg = Lib_Convert.getBigIntegerValue(cr, num);
		return new JMo_BigInt(inc ? this.value.add(arg) : this.value.subtract(arg));
	}

	@Override
	protected JMo_ByteArray toByteArray(final CallRuntime cr) {
		final byte[] bytes = this.value.toByteArray();
		return new JMo_ByteArray(bytes);
	}

}
