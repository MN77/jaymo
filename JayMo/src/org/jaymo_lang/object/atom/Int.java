/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomicMath;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Int extends A_IntNumber implements I_Integer, I_Atomic {

	public static final long TOP_VALUE = 4294967295l;
	private final int        value;


	/**
	 * ! Object of a immutable 32 bit integer number between -2.147.483.648 and 2.147.483.647
	 * + 123
	 * + -123
	 * + 123i
	 * + -123i
	 */
	public Int(final int val) {
		this.value = val;
	}

	public ATOMIC getEnum() {
		return ATOMIC.INT;
	}

	public int getIntValue(final CallRuntime cr) {
		return this.value;
	}

	public Integer getValue() {
		return this.value;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return type == STYPE.DESCRIBE
			? this.value + "i"
			: "" + this.value;
	}

	@Override
	protected ObjectCallResult call5(final CallRuntime cr, final String method) {
//		switch(method) {
//			default:
		return null;
//		}
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		return new Int(Integer.MAX_VALUE);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return new Int(Integer.MIN_VALUE);
	}

	@Override
	protected A_Number numberCalc0(final CallRuntime cr, final NOP0 op) {

		switch(op) {
			case NEG:
				return new Int(-this.value);
			case ABS:
				return new Int(Math.abs(this.value));
			case EXP:
				return JMo_Dec.valueOf(cr, Math.exp(this.value));
			case INC:
				return new Int(this.value + 1);
			case DEC:
				return new Int(this.value - 1);
			case POW:
				return new Int(this.value * this.value);
//				return new Int(Lib_ConvertExact.longToInt(cr, ((long)this.value) * this.value));	// This is with overflow-check
//				return new JMo_Long(((long)this.value) * this.value);
//				return new JMo_BigInt( BigInteger.valueOf(this.value).multiply(BigInteger.valueOf(this.value)));
			case ROOT:
				return JMo_Dec.valueOf(cr, Math.sqrt(this.value));
			case LOG10:
				return JMo_Dec.valueOf(cr, Math.log10(this.value));
//			case LOG2:
//				return new Dec(Math.log2(this.value));
			case LOGN:
				return JMo_Dec.valueOf(cr, Math.log(this.value));
		}
		throw Err.invalid(cr, op);
	}

	@Override
	protected A_Number numberCalc1(final CallRuntime cr, final NOP1 op, final A_Number paro) {

		if(paro instanceof I_Integer) {
			if(paro instanceof JMo_BigInt)
				return Lib_AtomicMath.calcBigInt(cr, op, BigInteger.valueOf(this.value), ((JMo_BigInt)paro).getValue());
			else if(paro instanceof JMo_Long)
				return Lib_AtomicMath.calcLong(cr, op, this.value, ((JMo_Long)paro).getValue());
			else
				return Lib_AtomicMath.calcInt(cr, op, this.value, Lib_Convert.getIntValue(cr, paro));
		}
		else if(paro instanceof JMo_BigDec)
			return Lib_AtomicMath.calcBigDec(cr, op, BigDecimal.valueOf(this.value), ((JMo_BigDec)paro).getValue());
		else if(paro instanceof JMo_Dec)
			return Lib_AtomicMath.calcDec(cr, op, Dec.valueOf(this.value), Lib_Convert.getDecValue(cr, paro));
		else
			return Lib_AtomicMath.calcDouble(cr, op, this.value, Lib_Convert.getDoubleValue(cr, paro));
	}

	@Override
	protected A_Number numberCalcSame(final CallRuntime cr, final A_Number num, final boolean inc) {
		final int arg = Lib_Convert.getIntValue(cr, num);
		return new Int(inc ? this.value + arg : this.value - arg);
	}

	@Override
	protected JMo_ByteArray toByteArray(final CallRuntime cr) {
		final byte[] bytes = ByteBuffer.allocate(4).putInt(this.value).array();
		return new JMo_ByteArray(bytes);
	}

}
