/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 21.03.2020
 *
 * @apiNote
 *          Abstract class for types with one or more chars
 */
public abstract class A_Chars extends A_Atomic {

	@Override
	protected final ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "+":
			case "append":
			case "add":
				return A_Object.stdResult(this.mAdd(cr));
			case "*":
			case "mul":
				return A_Object.stdResult(this.mMultiply(cr));

			case "get": // Needed for []
//			case "pos":
			case "char":
			case "charAt":
				return A_Object.stdResult(this.mCharAt(cr, false));
			case "pull":
				return A_Object.stdResult(this.mCharAt(cr, true));

//			case "set": result = setChar(c); break;		// String/Char are immutable
			case "change":
				return A_Object.stdResult(this.mChangeChar(cr));

			case "upper":
			case "caseUp":
				return A_Object.stdResult(this.mCaseUp(cr));
			case "lower":
			case "caseDown":
				return A_Object.stdResult(this.mCaseDown(cr));

			case "select":
				return A_Object.stdResult(this.mSelect(cr, false));
			case "selectLazy":
				return A_Object.stdResult(this.mSelect(cr, true));

			case "update":
				return A_Object.stdResult(this.mUpdate(cr, false));
			case "updateLazy":
				return A_Object.stdResult(this.mUpdate(cr, true));

			/**
			 * °isNumber()Bool # Returns true, if this char is a Number (0-9)
			 * °isCaseUp()Bool # Returns true, if the char is a upper case letter (A-Z)
			 * °isCaseDown()Bool # Returns true, if the char is a lower case letter (a-z)
			 * °isBlank()Bool # Returns true, if the Char/Str contains only whitespaces or is empty
			 */
			case "isNumber":
				cr.argsNone();
				return A_Object.stdResult(Bool.getObject(this.charsIsNumber()));
			case "isCaseUp":
				cr.argsNone();
				return A_Object.stdResult(Bool.getObject(this.charsIsCaseUp()));
			case "isCaseDown":
				cr.argsNone();
				return A_Object.stdResult(Bool.getObject(this.charsIsCaseDown()));
			case "isBlank":
				cr.argsNone();
				return A_Object.stdResult(Bool.getObject(this.charsIsBlank()));

			default:
				return this.call4(cr, method);
		}
	}

	protected abstract ObjectCallResult call4(CallRuntime cr, String method);

	protected abstract boolean charsIsBlank();

	protected abstract boolean charsIsCaseDown();

	protected abstract boolean charsIsCaseUp();

	protected abstract boolean charsIsNumber();

	@Override
	protected Int getLength(final CallRuntime cr) {
		if(this instanceof Char)
			return new Int(1);
		return new Int(((Str)this).getValue().length());
	}

	/**
	 * °+ ^ add
	 * °append^add
	 * °add(Object... o)Str # Add Objects and return the new created string
	 */
	protected abstract Str mAdd(final CallRuntime cr);

	/**
	 * °lower ^ caseDown
	 * °caseDown()Chars # Change all chars to lower case
	 */
	protected abstract A_Chars mCaseDown(CallRuntime cr);

	/**
	 * °upper ^ caseUp
	 * °caseUp()Chars # Change all chars to upper case
	 */
	protected abstract A_Chars mCaseUp(CallRuntime cr);

	/**
	 * °change(IntNumber pos, Char c)Chars # Replaces char at position pos with c
	 */
	protected abstract A_Chars mChangeChar(CallRuntime cr);

	/**
	 * °get ^ charAt
	 * °char ^ charAt
	 * °charAt(IntNumber pos)Char # Returns the char at position pos
	 * °pull(IntNumber pos)Char? # Returns the char at position pos. If pos is unreachable, Nil will be returned.
	 */
	protected abstract I_Object mCharAt(CallRuntime cr, boolean lazy);

	/**
	 * °* ^ mul
	 * °mul(IntNumber i)Str # Multiplys the string or char
	 */
	protected abstract Str mMultiply(final CallRuntime cr);

	/**
	 * °select(IntNumber pos...)List # Returns a list with all items from 'pos'.
	 * °selectLazy(IntNumber pos...)List # Returns a list with all items from 'pos'. If one position is out of bounds, fill it with Nil.
	 */
	protected abstract JMo_List mSelect(CallRuntime cr, boolean lazy);

	/**
	 * °update(IntNumber pos...)Str # Returns a list with all items from 'pos'.
	 * °updateLazy(IntNumber pos...)Str # Returns a list with all items from 'pos'. If one position is out of bounds, fill it with Nil.
	 */
	protected abstract Str mUpdate(CallRuntime cr, boolean lazy);

}
