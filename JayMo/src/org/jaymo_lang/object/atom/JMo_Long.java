/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomicMath;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.data.bigcalc.Lib_BigCalc;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_Long extends A_IntNumber implements I_Integer, I_Atomic {

//	public static final BigInteger TOP_VALUE = new BigInteger("18446744073709551615");
	public static final BigDecimal TOP_VALUE = new BigDecimal("18446744073709551615");
	private final long             value;


	/**
	 * ! Object of a immutable 64 bit integer number between -2^63 and 2^63-1
	 * + 1234l
	 * + -1234l
	 */
	public JMo_Long(final long val) {
		this.value = val;
	}

	public ATOMIC getEnum() {
		return ATOMIC.LONG;
	}

	public int getIntValue(final CallRuntime cr) {
		if(this.value < Integer.MIN_VALUE || this.value > Integer.MAX_VALUE)
			throw new RuntimeError(cr, "Value out of Integer-Range", "" + this.value);
		return (int)this.value;
	}

	public Long getValue() {
		return this.value;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
			case NESTED:
				return "" + this.value;
			default:
				return this.value + "l";
		}
	}

	@Override
	protected ObjectCallResult call5(final CallRuntime cr, final String method) {
		return null;
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		return new JMo_Long(Long.MAX_VALUE);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return new JMo_Long(Long.MIN_VALUE);
	}

	/**
	 * Override for Long-Type
	 */
	@Override
	protected final ObjectCallResult mTimes(final CallRuntime crOld, final boolean reverse) {
		crOld.argsNone();
		Lib_Exec.checkLoopWithout(crOld);

		final long value = ((Number)this.getValue()).longValue();

		final LoopHandle handle = new LoopHandle(this, reverse ? "timesDown" : "timesUp");
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		if(reverse)
			for(long i = value; i >= 1; i--) {
				handle.startLap();
				result = Lib_Exec.execBlockStream(crNew, new JMo_Long(i));

				if((result = Lib_Exec.loopResult(result)) instanceof Return)
					return ((Return)result).getLoopResult();
			}
		else
			for(long i = 1; i <= value; i++) {
				handle.startLap();
				result = Lib_Exec.execBlockStream(crNew, new JMo_Long(i));

				if((result = Lib_Exec.loopResult(result)) instanceof Return)
					return ((Return)result).getLoopResult();
			}
		return new ObjectCallResult(result, true);
	}

	@Override
	protected A_Number numberCalc0(final CallRuntime cr, final NOP0 op) {

		switch(op) {
			case NEG:
				return new JMo_Long(-this.value);
			case ABS:
				return new JMo_Long(Math.abs(this.value));
			case EXP:
				return new JMo_BigDec(Lib_BigCalc.exp(new BigDecimal(this.value), JMo_BigDec.CONTEXT));
			case INC:
				return new JMo_Long(this.value + 1);
			case DEC:
				return new JMo_Long(this.value - 1);
			case POW:
				return new JMo_Long(this.value * this.value);
//				return new JMo_BigInt( BigInteger.valueOf(this.value).pow(2) );
			case ROOT:
				return new JMo_Double(Math.sqrt(this.value));
			case LOG10:
				return new JMo_Double(Math.log10(this.value));
			case LOGN:
				return new JMo_Double(Math.log(this.value));
		}
		throw Err.impossible(op);
	}

	@Override
	protected A_Number numberCalc1(final CallRuntime cr, final NOP1 op, final A_Number paro) {

		if(paro instanceof I_Integer) {
			if(paro instanceof JMo_BigInt)
				return Lib_AtomicMath.calcBigInt(cr, op, BigInteger.valueOf(this.value), ((JMo_BigInt)paro).getValue());
			else
				return Lib_AtomicMath.calcLong(cr, op, this.value, Lib_Convert.getLongValue(cr, paro));
		}
		else if(paro instanceof JMo_BigDec)
			return Lib_AtomicMath.calcBigDec(cr, op, BigDecimal.valueOf(this.value), ((JMo_BigDec)paro).getValue());
		else
			return Lib_AtomicMath.calcDouble(cr, op, this.value, Lib_Convert.getDoubleValue(cr, paro));
	}

	@Override
	protected A_Number numberCalcSame(final CallRuntime cr, final A_Number num, final boolean inc) {
		final long arg = Lib_Convert.getLongValue(cr, num);
		return new JMo_Long(inc ? this.value + arg : this.value - arg);
	}

	@Override
	protected JMo_ByteArray toByteArray(final CallRuntime cr) {
		final byte[] bytes = ByteBuffer.allocate(8).putLong(this.value).array();
		return new JMo_ByteArray(bytes);
	}

}
