/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomicMath;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.bigcalc.Lib_BigCalc;
import de.mn77.base.data.util.Lib_BigMath;
import de.mn77.base.data.util.Lib_E_Notation;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 2021-10-10
 */
public class JMo_BigDec extends A_DecNumber implements I_Decimal, I_Atomic, I_BigNumber {

	public static final MathContext CONTEXT      = Lib_BigMath.CONTEXT256;
	public static final MathContext CONTEXT_UP   = new MathContext(JMo_BigDec.CONTEXT.getPrecision(), RoundingMode.UP);
	public static final MathContext CONTEXT_DOWN = new MathContext(JMo_BigDec.CONTEXT.getPrecision(), RoundingMode.DOWN);

	public static final MathContext CONTEXT1 = JMo_BigDec.CONTEXT_UP;
	public static final MathContext CONTEXT2 = JMo_BigDec.CONTEXT_UP;
	public static final MathContext CONTEXT3 = JMo_BigDec.CONTEXT;

	private final BigDecimal value;


	/**
	 * ! Object of a immutable big decimal number.
	 * + 12.34z
	 * + -12.34z
	 * + 12z
	 * + -12z
	 */
	public JMo_BigDec(final BigDecimal val) {
//		this.value = (float)Lib_Math.normalize(val,6);	// 10	or 6, see also Dec
		this.value = val; // Not limited by default.
	}

	public ATOMIC getEnum() {
		return ATOMIC.BIGDEC;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final boolean useNotationE = type != STYPE.DESCRIBE;
		final boolean suffix = type == STYPE.IDENT || type == STYPE.DESCRIBE;

		if(Double.isNaN(this.value.doubleValue()))
			return ATOMIC.NOT_A_NUMBER;
		if(this.value.doubleValue() == Float.POSITIVE_INFINITY)
			return ATOMIC.INFINITY_POSITIVE;
		if(this.value.doubleValue() == Float.NEGATIVE_INFINITY)
			return ATOMIC.INFINITY_NEGATIVE;

//		String result = this.value.toString();	// With exponent
		String result = this.value.toPlainString(); // Without exponent

		if(result.indexOf('.') == -1)
			result += ".";
		else {
			int end = result.length();
			while(result.charAt(end - 1) == '0')
				end--;
			if(end != result.length())
				result = result.substring(0, end);
		}
		if(result.charAt(result.length() - 1) == '.')
			result += "0";

		if(useNotationE && result.length() > JayMo.NESTED_MAX)
			result = Lib_E_Notation.toNotationE(result, 16);

		return suffix
			? result + 'z' // 'z' 'c'
			: result;
	}

	@Override
	protected ObjectCallResult call5(final CallRuntime cr, final String method) {
		return null;
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		throw this.newErrorMinMaxValue(cr);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		throw this.newErrorMinMaxValue(cr);
	}

	@Override
	protected A_Number numberCalc0(final CallRuntime cr, final NOP0 op) {

		switch(op) {
			case NEG:
//				return new JMo_BigDec(-this.value - 1);
				return new JMo_BigDec(this.value.negate());
			case ABS:
				return new JMo_BigDec(this.value.abs());
			case EXP:
				return new JMo_BigDec(Lib_BigCalc.exp(this.value, JMo_BigDec.CONTEXT));
			case INC:
				return new JMo_BigDec(this.value.add(BigDecimal.ONE, JMo_BigDec.CONTEXT));
			case DEC:
				return new JMo_BigDec(this.value.subtract(BigDecimal.ONE, JMo_BigDec.CONTEXT));
			case POW:
				return new JMo_BigDec(this.value.multiply(this.value, JMo_BigDec.CONTEXT));
			case ROOT:
				return new JMo_BigDec(this.value.sqrt(JMo_BigDec.CONTEXT));
//				return new JMo_BigDec(BigDecimalMath.sqrt(this.value, MathContext.UNLIMITED));	// TODO Testen!
			case LOG10:
				return new JMo_BigDec(Lib_BigMath.log(this.value, BigDecimal.valueOf(10), JMo_BigDec.CONTEXT));
			case LOGN:
				return new JMo_BigDec(Lib_BigMath.log(this.value, Lib_BigMath.EULER_256, JMo_BigDec.CONTEXT));
		}
		throw Err.impossible(op);
	}

	@Override
	protected A_Number numberCalc1(final CallRuntime cr, final NOP1 op, final A_Number arg) {
		return Lib_AtomicMath.calcBigDec(cr, op, this.value, Lib_Convert.getBigDecimalValue(cr, arg));
	}

	@Override
	protected A_Number numberCalcSame(final CallRuntime cr, final A_Number num, final boolean inc) {
		final BigDecimal arg = Lib_Convert.getBigDecimalValue(cr, num);
		return new JMo_BigDec(inc ? this.value.add(arg, JMo_BigDec.CONTEXT) : this.value.subtract(arg, JMo_BigDec.CONTEXT));
	}

}
