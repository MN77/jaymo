/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.util.ATOMIC;

import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 13.12.2022
 */
public class Dec extends Number { // TODO Move this to ../object/atom/raw

	private static final long serialVersionUID = 1416433428694095730L;

	public static final long BASE         = 1000000000l;
	public static final Dec  MAX_VALUE    = new Dec(Long.MAX_VALUE);
	public static final Dec  MIN_VALUE    = new Dec(Long.MIN_VALUE);
	public static final Dec  ZERO         = new Dec(0l);
	public static final Dec  ONE          = new Dec(Dec.BASE);
	public static final Dec  NEGATIVE_ONE = new Dec(-Dec.BASE);


	private long value = 0l;


	public static Dec parseDec(final String s) {
		final double d = Double.parseDouble(s);
		return Dec.valueOf(d);
	}

	public static Dec valueOf(final byte b) {
		return new Dec(b * Dec.BASE);
	}

	/**
	 * Please use: JMo_Dec.valueOf(cr, double)
	 */
	public static Dec valueOf(final double d) {
		if(!Double.isFinite(d))
			if(Double.isNaN(d))
//				throw new Err_Runtime("Invalid conversion", "Got: " + ATOMIC.NOT_A_NUMBER);
				return Dec.ZERO;
			else {
				final String inf = d == Double.POSITIVE_INFINITY
					? ATOMIC.INFINITY
					: ATOMIC.INFINITY_NEGATIVE;
				throw new Err_Runtime("Invalid value", "Got: " + inf);
			}

//		return new Dec((long)(d*Dec.BASE));
		final double mul = d * Dec.BASE;
		if(mul > Long.MAX_VALUE || mul < Long.MIN_VALUE)
			throw new Err_Runtime("Value out of bounds", "Got: " + d);

		return new Dec(Math.round(mul));
	}

	public static Dec valueOf(final float f) {
		return Dec.valueOf((double)f);
	}

	public static Dec valueOf(final int i) {
		return new Dec(i * Dec.BASE);
	}

	public static Dec valueOf(final long l) {
		return new Dec(l * Dec.BASE);
	}

	public static Dec valueOf(final short s) {
		return new Dec(s * Dec.BASE);
	}

	private Dec(final long l) {
		this.value = l;
	}

	public Dec abs() {
		return this.value < 0
			? Dec.valueOf(Math.abs(this.doubleValue()))
			: this;
	}

	public Dec add(final Dec arg) {
		return new Dec(this.value + arg.value);
	}

	public BigDecimal bigDecimalValue() {
		return new BigDecimal(this.toString());
	}

	public BigInteger bigIntegerValue() {
		return BigInteger.valueOf(this.longValue());
	}

	@Override
	public byte byteValue() {
		return (byte)(this.value / Dec.BASE);
	}

	public Dec dec() {
		return new Dec(this.value - Dec.BASE);
	}

	public Dec div(final Dec arg) {
//		return new Dec(this.value / arg.value);
		final double d = this.doubleValue() / arg.doubleValue();
		return this.iCheckResult(d);
	}

	@Override
	public double doubleValue() {
//		return this.value / Dec.BASE;
		return (double)this.value / Dec.BASE;
	}

	@Override
	public boolean equals(final Object other) {
		if(other instanceof Dec)
			return this.value == ((Dec)other).value;
		return false;
	}


//	public boolean isGreater0() {
//		return this.value >= Dec.BASE;
//	}

	public Dec exp() {
		return Dec.valueOf(Math.exp(this.doubleValue()));
	}

	@Override
	public float floatValue() {
		return (float)((double)this.value / Dec.BASE);
	}

	public Dec inc() {
		return new Dec(this.value + Dec.BASE);
	}

	@Override
	public int intValue() {
		return (int)(this.value / Dec.BASE);
	}

	public boolean is0() {
		return this.value == 0l;
	}

	public boolean isEqual(final Dec other) {
		return this.value == other.value;
	}

	public boolean isGreater(final Dec other) {
		return this.value > other.value;
	}

	public boolean isGreaterOrEqual(final Dec other) {
		return this.value >= other.value;
	}

	public boolean isGreaterZero() {
		return this.value > 0;
	}

	public boolean isLess(final Dec other) {
		return this.value < other.value;
	}

	public boolean isLessOrEqual(final Dec other) {
		return this.value <= other.value;
	}

	public boolean isLessZero() {
		return this.value < 0;
	}

	public Dec log(final Dec arg) {
		final double d = Math.log(this.doubleValue()) / Math.log(arg.doubleValue());  // benötigt die Basis und gibt die Potenz zurück
		return this.iCheckResult(d);
	}

	public Dec log10() {
		return Dec.valueOf(Math.log10(this.doubleValue()));
	}

	public Dec logN() {
		return Dec.valueOf(Math.log(this.doubleValue()));
	}

	@Override
	public long longValue() {
		return this.value / Dec.BASE;
	}

	public Dec mod(final Dec arg) {
		return new Dec(this.value % arg.value);
	}

	public Dec mul(final Dec arg) {
//		return new Dec(this.value * arg.value);
		final double d = this.doubleValue() * arg.doubleValue();
		return this.iCheckResult(d);
	}

	public Dec neg() {
		return new Dec(-this.value);
	}

	public Dec pow() {
		return Dec.valueOf(this.doubleValue() * this.doubleValue());
	}

	public Dec pow(final Dec arg) {
		final double d = Math.pow(this.doubleValue(), arg.doubleValue());
		return this.iCheckResult(d);
	}

	public Dec root() {
		return Dec.valueOf(Math.sqrt(this.doubleValue()));
	}

	public Dec root(final Dec arg) {
		final double d = Math.exp(Math.log(this.doubleValue()) / arg.doubleValue());
		return this.iCheckResult(d);
	}

	@Override
	public short shortValue() {
		return (short)(this.value / Dec.BASE);
	}

	public Dec sub(final Dec arg) {
		return new Dec(this.value - arg.value);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		String s = "" + this.value;

		if(this.value < 0) {
			s = s.substring(1);
			sb.append('-');
		}
		if(s.length() < 10)
			sb.append('0');
		else
			sb.append(s.substring(0, s.length() - 9));

		sb.append('.');

		if(s.length() < 9) {
			for(int i = 0; i < 9 - s.length(); i++)
				sb.append('0');
			sb.append(s);
		}
		else
			sb.append(s.substring(s.length() - 9, s.length()));
		while(sb.charAt(sb.length() - 1) == '0')
			sb.setLength(sb.length() - 1);

		if(sb.charAt(sb.length() - 1) == '.')
			sb.append('0');

		return sb.toString();
	}

	private Dec iCheckResult(final double d) {
		if(Double.isFinite(d))
			return Dec.valueOf(d);
		if(Double.isNaN(d))
			return Dec.ZERO;

		throw new Err_Runtime("Result out of bounds", "Got: " + (d == Double.POSITIVE_INFINITY ? ATOMIC.INFINITY : ATOMIC.INFINITY_NEGATIVE));
	}

}
