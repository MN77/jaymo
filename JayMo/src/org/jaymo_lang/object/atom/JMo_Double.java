/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomicMath;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 *
 *          https://www.baeldung.com/java-separate-double-into-integer-decimal-parts
 *          https://stackoverflow.com/questions/322749/retain-precision-with-double-in-java
 *          https://www.geeksforgeeks.org/rounding-off-errors-java/
 *          https://dzone.com/articles/never-use-float-and-double-for-monetary-calculatio
 *          http://java-performance.info/bigdecimal-vs-double-in-financial-calculations/
 *          https://medium.com/@vaibhav0109/choosing-data-type-for-monetary-calculation-in-java-float-double-or-bigdecimal-ac6f52e57740
 */
public class JMo_Double extends A_DecNumber implements I_Decimal, I_Atomic {

	private final double value;


	/**
	 * ! Object of a immutable 64 bit decimal floating number between +/-4,9E-324 and +/-1,7E+308.
	 * + 123.456d
	 * + -123.456d
	 * + 123d
	 * + -123d
	 */
	public JMo_Double(final double val) {
//		if(StrictManager.limitDec())
//			this.value = Lib_Math.normalize(val, 12); // 10  // 12 is maybe the best to use.
////			this.value = Lib_Math.limit12Decimals(val);
//		else
		this.value = val;
	}

//	public Dec(final float val) {
////		this.value = Lib_Math.normalize(val,5);	// , 6
//		this.value = val;
//	}

	public ATOMIC getEnum() {
		return ATOMIC.DOUBLE;
	}

	public Double getValue() {
		return this.value;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(Double.isNaN(this.value))
			return ATOMIC.NOT_A_NUMBER;
		if(this.value == Double.POSITIVE_INFINITY)
			return ATOMIC.INFINITY_POSITIVE;
		if(this.value == Double.NEGATIVE_INFINITY)
			return ATOMIC.INFINITY_NEGATIVE;

		if(type == STYPE.DESCRIBE) {
			final String s = "" + this.value;
			if(s.indexOf('E') < 0)
				return s + "d";

//			DecimalFormat df = new DecimalFormat("#");
//			df.setMaximumFractionDigits(8);

//			String.format("%.12f", myvalue)
			final String sVal = new BigDecimal(this.value).toPlainString();

			final StringBuilder sb = new StringBuilder();
			sb.append(sVal);

			if(sVal.indexOf('.') == -1)
				sb.append(".0");

			sb.append('d');
			return sb.toString();
		}
		return type == STYPE.IDENT
			? this.value + "d"
			: "" + this.value;
	}

	@Override
	protected ObjectCallResult call5(final CallRuntime cr, final String method) {
		return null;
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		return new JMo_Double(Double.POSITIVE_INFINITY);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return new JMo_Double(Double.NEGATIVE_INFINITY);
	}

	@Override
	protected A_Number numberCalc0(final CallRuntime cr, final NOP0 op) {

		switch(op) {
			case NEG:
				return new JMo_Double(-this.value);
			case ABS:
				return new JMo_Double(Math.abs(this.value));
			case EXP:
				return new JMo_Double(Math.exp(this.value));
			case INC:
				return new JMo_Double(this.value + 1);
			case DEC:
				return new JMo_Double(this.value - 1);
			case POW:
				return new JMo_Double(this.value * this.value);
			case ROOT:
				return new JMo_Double(Math.sqrt(this.value));
			case LOG10:
				return new JMo_Double(Math.log10(this.value));
			case LOGN:
				return new JMo_Double(Math.log(this.value));
		}
		throw Err.impossible(op);
	}

	@Override
	protected A_Number numberCalc1(final CallRuntime cr, final NOP1 op, final A_Number arg) {
		if(arg instanceof I_BigNumber)
			return Lib_AtomicMath.calcBigDec(cr, op, BigDecimal.valueOf(this.value), Lib_Convert.getBigDecimalValue(cr, arg));
		else
			return Lib_AtomicMath.calcDouble(cr, op, this.value, Lib_Convert.getDoubleValue(cr, arg));
	}

	@Override
	protected A_Number numberCalcSame(final CallRuntime cr, final A_Number num, final boolean inc) {
		final double arg = Lib_Convert.getDoubleValue(cr, num);
		return new JMo_Double(inc ? this.value + arg : this.value - arg);
	}

}
