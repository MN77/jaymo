/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.COMPARE;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_MagicVar;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Bool extends A_Atomic implements I_Integer, I_Atomic, I_AutoBlockDo {

	enum COMPARE2 {
		AND,
		OR,
		XOR,
		NAND,
		NOR,
		XNOR
	}


	public static final Bool TRUE  = new Bool(true);
	public static final Bool FALSE = new Bool(false);


	private final boolean value;


	public static final Bool getObject(final boolean b) {
		return b
			? Bool.TRUE
			: Bool.FALSE;
	}

	/**
	 * ! Object of an immutable boolean value
	 * + true
	 * #+ True
	 * + TRUE
	 * + false
	 * #+ False
	 * + FALSE
	 * %.if
	 */
	private Bool(final boolean val) {
		this.value = val;
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mDoIf(cr, false, false, false).obj;
	}

	public ATOMIC getEnum() {
		return ATOMIC.BOOL;
	}

	public int getIntValue(final CallRuntime cr) {
		return this.value ? 1 : 0;
	}

	public Boolean getValue() {
		return this.value;
	}

	/**
	 * @Override
	 */
	@Override
	public Boolean isGreater3(final I_Atomic o) {
		if(o instanceof Bool)
//			return Boolean.valueOf(this.value).compareTo(((Bool)o).getValue());
			return this.value && !((Bool)o).getValue();
		return null;
	}

	// Methoden

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return "" + this.value;
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {
		// S_Object result=null;

		switch(method) {
			case "else":
				return this.mDoElse(cr);

			case "+":
			case "add":
				return A_Object.stdResult(this.mAdd(cr));

			case "!&&":
			case "nand":
				return A_Object.stdResult(this.mComparsion2(cr, COMPARE2.NAND));
			case "!||":
			case "nor":
				return A_Object.stdResult(this.mComparsion2(cr, COMPARE2.NOR));
			case "!^^":
			case "xnor":
				return A_Object.stdResult(this.mComparsion2(cr, COMPARE2.XNOR));

			case "!":
			case "not":
				return A_Object.stdResult(this.mNot(cr));

			case "&&":
			case "and":
				return A_Object.stdResult(this.mComparsion2(cr, COMPARE2.AND));
			case "||":
			case "or":
				return A_Object.stdResult(this.mComparsion2(cr, COMPARE2.OR));
			case "^^":
			case "xor":
				return A_Object.stdResult(this.mComparsion2(cr, COMPARE2.XOR));

			default:
				Lib_MagicVar.checkForbiddenFuncs(cr, method);
				return null;
		}
		// return stdResult(result, false);
	}

	@Override
	protected Int getLength(final CallRuntime cr) {
		return new Int(this.value ? 4 : 5);
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		return Bool.TRUE;
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return Bool.FALSE;
	}

	@Override
	protected Bool mComparsion(final CallRuntime cr, final COMPARE m) {
		final I_Object raw = cr.argsExt(this, new Class<?>[]{Bool.class, A_Number.class})[0]; // Theoretisch möglich: I_Atomic // Früher: Char.class, Int.class, Dec.class
		final int arg = Lib_Convert.getIntValue(cr, raw);
		final int val = this.value
			? 1
			: 0;

		switch(m) {
			case G:
				return Bool.getObject(val > arg);
			case L:
				return Bool.getObject(val < arg);
			case GE:
				return Bool.getObject(val >= arg);
			case LE:
				return Bool.getObject(val <= arg);
			default:
				throw Err.impossible(m);
		}
	}

	/**
	 * °+^add
	 * °add(Atomic a)Atomic
	 */
	private I_Object mAdd(final CallRuntime cr) {
		final I_Object para = cr.args(this, A_Atomic.class)[0];

		final String vb = this.value ? "true" : "false";

		if(para instanceof Str)
			return new Str(vb + ((Str)para).getValue());
		if(para instanceof Char)
			return new Str(vb + ((Char)para).getValue());

		final int vn = this.value
			? 1
			: 0;

		if(para instanceof Bool)
			return this.mComparsion2(cr, COMPARE2.AND);
		if(para instanceof JMo_Long)
			return new JMo_Long(vn + ((JMo_Long)para).getValue());
		if(para instanceof JMo_BigInt)
			return new JMo_BigInt(BigInteger.valueOf(vn).add(Lib_Convert.getBigIntegerValue(cr, para)));
		if(para instanceof JMo_BigDec)
			return new JMo_BigDec(BigDecimal.valueOf(vn).add(Lib_Convert.getBigDecimalValue(cr, para)));
		if(para instanceof JMo_Dec)
			return new JMo_Dec(Dec.valueOf(vn).add(Lib_Convert.getDecValue(cr, para)));
		if(para instanceof I_Decimal)
			return new JMo_Double(vn + Lib_Convert.getDoubleValue(cr, para));
		if(para instanceof I_Integer)
			return new Int(vn + Lib_Convert.getIntValue(cr, para));

		throw Err.invalid(para);
	}

	/**
	 * °|| ^ or
	 * °or(Bool b)Bool # Logical "Or"
	 * °&& ^ and
	 * °and(Bool b)Bool # Logical "And"
	 * °&#94;&#94; ^ xor
	 * °xor(Bool b)Bool # Logical "Xor"
	 *
	 * °!|| ^ nor
	 * °nor(Bool b)Bool # Logical "Not Or" (NOR)
	 * °!&& ^ nand
	 * °nand(Bool b)Bool # Logical "Not And" (NAND
	 * °!&#94;&#94; ^ xnor
	 * °xnor(Bool b)Bool # Logical "Not Xor" (XNOR)
	 */
	private Bool mComparsion2(final CallRuntime cr, final COMPARE2 m) {
		final boolean val = this.value;

		// Shortcuts // TODO args will not be checked here!
		if(val)
			switch(m) {
				case OR:
					return Bool.TRUE;
				case NOR:
					return Bool.FALSE;
				default:
			}
		else
			switch(m) {
				case AND:
					return Bool.FALSE;
				case NAND:
					return Bool.TRUE;
				default:
			}

		final I_Object raw = cr.args(this, Bool.class)[0];
		final boolean arg = Lib_Convert.getBoolValue(cr, raw);

		switch(m) {
			case AND:
				return Bool.getObject(val && arg);
			case OR:
				return Bool.getObject(val || arg);
			case XOR:
				return Bool.getObject(val ^ arg);

			case NAND:
				return Bool.getObject(!(val && arg));
			case NOR:
				return Bool.getObject(!(val || arg));
			case XNOR:
				return Bool.getObject(!(val ^ arg));

			default:
				throw Err.todo(cr, m);
		}
	}

	/**
	 * °else()%Bool # If the current object is 'false', execute the block and send a 'true' to the stream. Otherwise the result will be 'false'.
	 * °else(Bool b)%Bool # Execute the block, if the current value is false and 'b' is true. Returns 'true' if the block was executed.
	 */
	private ObjectCallResult mDoElse(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);

		if(this.value)
			return new ObjectCallResult(this, true);


		final Call stream = cr.getStream();
		final Block block = cr.getCallBlock();

		final boolean withPar = args != null && args.length == 1;

		if(!withPar && stream == null && block == null)
			return new ObjectCallResult(this, true);

		boolean check = this.value;

		if(withPar) {
			final I_Object base = cr.argType(args[0], Bool.class);
			check = ((Bool)base).getValue();
		}
		else
			check = !this.value;
		I_Object res = Bool.getObject(check);
		res = Lib_Exec.execIf(cr, check, res, false);
		return new ObjectCallResult(res, true);
	}

	/**
	 * °!^not
	 * °not()Bool#Inverts the current bool value
	 */
	private Bool mNot(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(!this.value);
	}

}
