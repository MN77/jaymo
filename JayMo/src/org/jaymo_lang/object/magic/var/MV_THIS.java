/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.var;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.I_MagicReplace;


/**
 * @author Michael Nitsche
 * @apiNote
 *          Reference of an Instance to itself = this
 */
public class MV_THIS extends A_MagicVar implements I_Object, I_MagicReplace {

	public static final String ID = "this";


	public MV_THIS(final boolean allowWithoutDot) {
		super(allowWithoutDot);
	}

	@Override
	public String getID() {
		return MV_THIS.ID;
	}

}
