/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.var;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.I_MagicGet;
import org.jaymo_lang.object.magic.I_MagicReplace;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @since 17.11.2020
 */
public class MagicVar extends A_MagicVar implements I_MagicReplace, I_MagicGet {

	public enum MAGICVAR {
		FUNC,	 //TODO MC_FUNC?
		LOOP, 	 //TODO MC_LOOP?
	}


	private final MAGICVAR type;


	public MagicVar(final MAGICVAR type) {
		super(false);
		this.type = type;
	}

	public I_Object get(final CallRuntime cr) {

		switch(this.type) {
			case FUNC:
				final Function f = cr.getSurrBlock().getFunction(cr);
				if(f != null)
					return f;
				else
					throw new CodeError(cr, "Invalid access to 'func'", "Can't call 'func' outside a function definition!");

			case LOOP:
				return cr.vce.loopGet(cr);

			default:
				throw Err.impossible(this.type);
		}
	}

	@Override
	public String getID() {

		switch(this.type) {
			case LOOP:
				return "loop";
			case FUNC:
				return "func";

			default:
				throw Err.impossible(this.type);
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.getID();
	}

}
