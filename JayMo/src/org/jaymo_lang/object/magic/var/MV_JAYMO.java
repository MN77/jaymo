/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.var;

import java.io.File;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 09.10.2019
 */
public class MV_JAYMO extends A_MagicVar {

	public static final String ID = "jaymo";


	public MV_JAYMO() {
		super(false);
	}

	@Override
	public String getID() {
		return MV_JAYMO.ID;
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "version":
				final String version = JayMo.version().toStringShort(); // Without build-number, cause its maybe not available
				return A_Object.stdResult(new Str(version));

			case "exec":
				cr.getStrict().checkSandbox(cr, "jaymo.exec");
				return A_Object.stdResult(this.mExec(cr));
//			case "script":
//			case "file":
			case "run":
				cr.getStrict().checkSandbox(cr, "jaymo.run");
				return A_Object.stdResult(this.mExecFile(cr));
			case "allTypes":
				cr.getStrict().checkSandbox(cr, "jaymo.allTypes");
				return A_Object.stdResult(this.mAllTypes(cr));

			default:
				return null;
		}
	}

	private JMo_List mAllTypes(final CallRuntime cr) {
		final SimpleList<String> names = ObjectManager.allTypes(cr);

		final SimpleList<I_Object> result = new SimpleList<>(names.size());
		for(final String name : names)
			result.add(new Str(name));
		return new JMo_List(result);
	}

	/**
	 * @return The return can be a Str or Nil
	 */
	private A_Immutable mExec(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String value = Lib_Convert.getStringValue(cr, arg);
		return Lib_Exec.execJayMo(cr, value, "jaymo.exec", null);
	}

	/**
	 * @return The return can be a Str or Nil
	 */
	private A_Immutable mExecFile(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 1, 2);
		final I_Object target = cr.argTypeExt(args[0], new Class<?>[]{Str.class, JMo_File.class});
		final File file = Lib_Convert.getFile(cr, target);

		String[] argsArray = null;

		if(args.length == 2) {
			final JMo_List targetArgs = (JMo_List)cr.argType(args[1], JMo_List.class);
			argsArray = Lib_Convert.toStringArray(targetArgs);
		}
		String script = null;

		try {
			script = Lib_TextFile.read(file);
		}
		catch(final Err_FileSys e) {
			throw new ExternalError(cr, "File access error", "File: " + file.getAbsolutePath());
		}
		return Lib_Exec.execJayMo(cr, script, "jaymo.run", argsArray);
	}

}
