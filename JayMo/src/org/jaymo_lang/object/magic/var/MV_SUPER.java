/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.var;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.I_MagicReplace;


/**
 * @author Michael Nitsche
 * @created 2022-01-13
 * @apiNote
 *          Reference of an Instance to the parent = super
 */
public class MV_SUPER extends A_MagicVar implements I_Object, I_MagicReplace {

	public static final String ID = "super";


	public MV_SUPER(final boolean allowWithoutDot) {
		super(allowWithoutDot);
	}

	@Override
	public String getID() {
		return MV_SUPER.ID;
	}

}
