/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.var;

import java.io.File;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.App;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.filesys.JMo_Dir;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.magic.I_MagicReplace;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.sys.file.MDir;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 * @apiNote
 *          Reference to the current App
 */
public class MV_APP extends A_MagicVar implements I_Object, I_MagicReplace {

	public static final String ID = "app";


	public MV_APP() {
		super(false);
	}

	@Override
	public String getID() {
		return MV_APP.ID;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return type == STYPE.IDENT
			? MV_APP.ID
			: App.name;
	}

	/**
	 * °args()List # Returns the given arguments of this application
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		cr.getApp().strict.checkSandbox(cr, MV_APP.ID);

		if(method.startsWith("::@"))
			return cr.getApp().call(cr);

		switch(method) {
			case "setOutput":
				return A_Object.stdResult(this.mSetOutput(cr));
			case "resetOutput":
				return A_Object.stdResult(this.mResetOutput(cr));
			case "setLogDir":
				return A_Object.stdResult(this.mSetLogDir(cr));
			case "setLogFile":
				return A_Object.stdResult(this.mSetLogFile(cr));
			case "args":
				cr.argsNone();
				return A_Object.stdResult(cr.getApp().getArgs());

			case "=":
				return this.mExit(cr, 1, 1);
//			case "<<":
//				return this.iExit(cr, 0, 0);
//			case "end": 	// 'exit' has a better distinction
			case "exit":
				return this.mExit(cr, 0, 1);
			case "keep":
				return this.mKeep(cr);

			case "exec":
				return A_Object.stdResult(this.mExec(cr));

			default:
				return null;
		}
	}

	/**
	 * @return: Str or Nil
	 */
	private A_Immutable mExec(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String value = Lib_Convert.getStringValue(cr, arg);
		cr.getStrict().checkUnsafeHotCode(cr, MV_APP.ID + ".exec");
		return Lib_Exec.execJayMoHot(cr, value, MV_APP.ID + ".exec");
	}

	/**
	 * °exit()Int # Exit this application.
	 * °exit(Int result)Int # Exit this application with result value.
	 * °=(Int result)Int # Exit this application with result value.
	 */
	private ObjectCallResult mExit(final CallRuntime cr, final int parsMin, final int parsMax) {
		final I_Object[] args = cr.argsFlex(this, parsMin, parsMax);
		int exitState = 0;

		if(args.length == 1) {
			final I_Object arg = cr.argType(args[0], Int.class);
			exitState = ((Int)arg).getValue();
		}
		final App app = cr.getApp();
		app.exit(cr, exitState);
		return new ObjectCallResult(app, true); // true, because this is the end ;-)
	}

	private ObjectCallResult mKeep(final CallRuntime cr) {
		cr.argsNone();
		cr.getApp().setKeep(cr);
		return new ObjectCallResult(this, false);
	}

	/**
	 * °resetOutput()Same # Reset output stream to default = StdOut
	 */
	private I_Object mResetOutput(final CallRuntime cr) {
		cr.argsNone();
		cr.getApp().setOutputFile(null);
		return this;
	}

	/**
	 * °setLogDir(Dir|Str dir)Same # Set the log directory for this application.
	 */
	private I_Object mSetLogDir(final CallRuntime cr) {
		final I_Object arg = cr.argsExt(this, new Class[]{JMo_Dir.class, Str.class})[0];

		try {
			MDir dir = null;

			if(arg instanceof JMo_Dir) {
				final File f = ((JMo_Dir)arg).getInternalFile();
				dir = new MDir(f);
			}
			else {
				final String s = Lib_Convert.getStringValue(cr, arg);
				dir = new MDir(s);
			}
			MOut.setLogDir(dir);
		}
		catch(final Err_FileSys e) {
			throw new ExternalError(cr, "Directory error", e.getMessage());
		}
		return this;
	}

	/**
	 * °setLogFile(File|Str target)Same # Set the log file for this application
	 */
	private I_Object mSetLogFile(final CallRuntime cr) {
		final I_Object arg = cr.argsExt(this, new Class[]{JMo_File.class, Str.class})[0];

		final String filePath = arg instanceof Str
			? Lib_Convert.getStringValue(cr, arg)
			: ((JMo_File)arg).getInternalFile().getAbsolutePath();
		final File file = new File(filePath);
		MOut.setLogFile(file);
		return this;
	}

	/**
	 * °setOutput(File#Str file, Bool clearBefore)Same # Redirect output to a file
	 */
	private I_Object mSetOutput(final CallRuntime cr) {
		final I_Object[] args = cr.argsExt(this, new Class[]{JMo_File.class, Str.class}, new Class[]{Bool.class});

		final String file = args[0] instanceof Str
			? Lib_Convert.getStringValue(cr, args[0])
			: ((JMo_File)args[0]).getInternalFile().getAbsolutePath();
		final MFile file2 = new MFile(file);
		cr.getApp().setOutputFile(file2);

		final boolean clear = ((Bool)args[1]).getValue();
		if(clear)
			try {
				Lib_TextFile.set(cr.getApp().getOutputFile().getFile(), "");
			}
			catch(final Err_FileSys e) {
				Err.show(e);
			}

		return this;
	}

}
