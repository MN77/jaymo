/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.con;

import org.jaymo_lang.info.Lib_Info;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.I_MagicGet;
import org.jaymo_lang.object.magic.I_MagicReplace;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @since 04.08.2019
 * @see
 *      PHP:
 *      https://www.php.net/manual/de/language.constants.predefined.php
 */
public class MagicConst extends A_MagicConst implements I_MagicReplace, I_MagicGet {

	public enum MAGICCONST {
		LINE,
		FILE,
		ARGS,
//		APP,
		DEVELOPERS,
		SUPPORTERS,
		SPONSORS,
	}


	private final MAGICCONST type;


	public MagicConst(final MAGICCONST type) {
		this.type = type;
	}

	public I_Object get(final CallRuntime cr) {

		switch(this.type) {
			case LINE:
				return cr.getDebugInfo().getLine();
			case FILE:
				return cr.getDebugInfo().getFile();
			case ARGS:
				return cr.getApp().getArgs();
//			case APP:
//				return cr.getApp();
//			case BLOCK:
//				return new Handle_Block();
//			case STREAM:
//				return new Handle_Stream();

			case DEVELOPERS:
				return Lib_Info.developers(cr);
			case SUPPORTERS:
				return Lib_Info.supporters(cr);
			case SPONSORS:
				return Lib_Info.sponsors(cr);

			// case DIR:
			// case FUNCTION:
		}
		throw Err.impossible(this.type);
	}

	@Override
	public String getName() {
//		return "MagicConst(_" + this.type + ")";
		return this.type.toString();
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		return new ObjectCallResult(null, false);
	}

}
