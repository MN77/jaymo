/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.con;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.constant.AXIS;


/**
 * @author Michael Nitsche
 * @since 25.03.2022
 */
public class MagicAxis extends A_MagicConst {

	private final AXIS axis;


	public MagicAxis(final AXIS axis) {
		this.axis = axis;
	}

	public AXIS get() {
		return this.axis;
	}

	@Override
	public String getName() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.axis.toString());
		sb.append('_');
		sb.append("AXIS");
		return sb.toString();
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		throw Lib_Error.notAllowed(cr);
	}

}
