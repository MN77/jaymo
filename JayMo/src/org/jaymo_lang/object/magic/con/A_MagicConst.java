/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.con;

import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 18.10.2019
 */
public abstract class A_MagicConst extends A_Object {

	public abstract String getName();

	@Override
	public final void init(final CallRuntime cr) {
		cr.getStrict().checkSandbox(cr, this.toString());
	}

	@Override
	public final String toString() {
		return "__" + this.getName();
	}

	@Override
	public final String toString(final CallRuntime cr, final STYPE type) {
		return this.toString();
	}

}
