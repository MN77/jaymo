/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.Return.LEVEL;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_MagicVar;


/**
 * @author Michael Nitsche
 * @created 03.11.2019
 */
public class LoopHandle extends A_Object {

	private final I_Object loop;
	private final String   method;
	private int            counter = 0;


	public LoopHandle(final I_Object loop) {
		this(loop, "each");
	}

	public LoopHandle(final I_Object loop, final String method) {
		this.loop = loop;
		this.method = method;
	}

	public int getLap() {
		return this.counter;
	}

	@Override
	public void init(final CallRuntime cr) {}

	public void startLap() {
		this.counter++;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.loop.toString(cr, type.getNested()) + '.' + this.method; // This is helping information
//		return this.getTypeName() + '(' + this.loop.toStringIdent(null) + '.' + this.method + ")";
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		/**
		 * #°>> ^ next
		 * #°continue ^ next
		 * °next()Nil
		 * °next(Object value)Object
		 *
		 * #°|| ^ break
		 * #°end ^ break
		 * °break()Nil
		 * °break(Object value)Object
		 *
		 * °=(Object value)Object # Break the loop with this exit value.
		 *
		 * °lap()Int # Returns the current lap number
		 */
		switch(method) {
//			case ">>":
			case "next":
//			case "continue":	// too long ... 'next' is good!
				final Return rc = new Return(LEVEL.NEXT, Lib_Exec.mSingleArgument(cr, this, 0, 1, null)); //TODO 1 Argument?
				return A_Object.stdResult(rc);

			case "=":
				final Return re = new Return(LEVEL.BREAK, Lib_Exec.mSingleArgument(cr, this, 1, 1, null));
				return A_Object.stdResult(re);
//			case "||":
//			case "end": 	// 'break' has a better distinction
			case "break":
				final Return rb = new Return(LEVEL.BREAK, Lib_Exec.mSingleArgument(cr, this, 0, 1, null));
				return A_Object.stdResult(rb);

			case "lap":
				cr.argsNone();
				return A_Object.stdResult(new Int(this.counter));

			default:
				Lib_MagicVar.checkForbiddenFuncs(cr, method);
				return null;
		}
	}

}
