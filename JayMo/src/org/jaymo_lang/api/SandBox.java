/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.api;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.App;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Java;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Exception;


/**
 * @author Michael Nitsche
 * @created 03.02.2020
 */
public final class SandBox extends A_Object {

	private final JMo_Object obj;
	private final Call[]     args;
	private final ToolBox    toolbox;


	public SandBox(final App app, final Class<? extends JMo_Object> type, final Call[] args) {
		this.args = args;

		try {
			this.obj = type.newInstance();
		}
		catch(InstantiationException | IllegalAccessException e) {
			final Err_Exception e2 = Err.wrap(e, "Maybe the object has no blank constructor");
			throw Err.exit(e2);
		}
		this.toolbox = new ToolboxImpl(app);
	}

	@Override
	public void init(final CallRuntime cr) {
		final int len = this.args == null ? 0 : this.args.length;
		final Object[] pars2 = new Object[len];

		for(int p = 0; p < len; p++) {
			final I_Object o = Lib_Convert.getValue(cr, cr.execInit(this.args[p], this));
			pars2[p] = Lib_Java.jmoToJava(cr, o, null);
		}

		try {
			this.obj.init(pars2);
		}
		catch(final Throwable t) {
			throw new CodeError(cr, "Can't instantiate Object", t.getMessage());
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
//		return this.obj.toString();
		return Lib_Type.getName(this.obj.getClass(), null);
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		final Object[] pars2 = new Object[args.length];
		for(int p = 0; p < args.length; p++)
			pars2[p] = Lib_Java.jmoToJava(cr, args[p], null);

		Object o = null;

		try {
			o = this.obj.call(method, pars2, this.toolbox);
		}
		catch(final Throwable t) {
			throw new CodeError(cr, "Execution error", t.getMessage());
		}
		if(o == null)
			return null;
		final I_Object o2 = Lib_Java.javaToJmo(o);
		return A_Object.stdResult(o2);
	}

}
