/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.parser.obj.I_ParseObject;
import org.jaymo_lang.parser.obj.ParseObj_Bin;
import org.jaymo_lang.parser.obj.ParseObj_Char;
import org.jaymo_lang.parser.obj.ParseObj_Const;
import org.jaymo_lang.parser.obj.ParseObj_Help;
import org.jaymo_lang.parser.obj.ParseObj_Hex;
import org.jaymo_lang.parser.obj.ParseObj_MagicConst;
import org.jaymo_lang.parser.obj.ParseObj_MagicVar;
import org.jaymo_lang.parser.obj.ParseObj_MagicVar_Shortcut;
import org.jaymo_lang.parser.obj.ParseObj_MemLet;
import org.jaymo_lang.parser.obj.ParseObj_NewWithArg;
import org.jaymo_lang.parser.obj.ParseObj_NewWithOpenArg;
import org.jaymo_lang.parser.obj.ParseObj_NewWithoutArg;
import org.jaymo_lang.parser.obj.ParseObj_Number;
import org.jaymo_lang.parser.obj.ParseObj_Oct;
import org.jaymo_lang.parser.obj.ParseObj_ShortCuts;
import org.jaymo_lang.parser.obj.ParseObj_Stream;
import org.jaymo_lang.parser.obj.ParseObj_String;
import org.jaymo_lang.parser.obj.ParseObj_SysCom;
import org.jaymo_lang.parser.obj.ParseObj_Table;
import org.jaymo_lang.parser.obj.ParseObj_Tree;
import org.jaymo_lang.parser.obj.ParseObj_Type;
import org.jaymo_lang.parser.obj.ParseObj_Var;
import org.jaymo_lang.parser.obj.ParseObj_VarConstType;


/**
 * @author Michael Nitsche
 * @created 28.12.2019
 */
public class ParseManagerObj {

	private static final ParseObj_VarConstType      parser_varConstType     = new ParseObj_VarConstType();
	private static final ParseObj_MagicVar          parser_magicVar         = new ParseObj_MagicVar();
	private static final ParseObj_Bin               parser_bin              = new ParseObj_Bin();
	private static final ParseObj_Hex               parser_hex              = new ParseObj_Hex();
	private static final ParseObj_Oct               parser_oct              = new ParseObj_Oct();
	private static final ParseObj_Number            parser_number           = new ParseObj_Number();
	private static final ParseObj_Char              parser_char             = new ParseObj_Char();
	private static final ParseObj_String            parser_string           = new ParseObj_String();
	private static final ParseObj_Type              parser_type             = new ParseObj_Type();
	private static final ParseObj_SysCom            parser_syscom           = new ParseObj_SysCom();
	private static final ParseObj_Stream            parser_stream           = new ParseObj_Stream();
	private static final ParseObj_MagicVar_Shortcut parser_magicVarShortcut = new ParseObj_MagicVar_Shortcut();
	private static final ParseObj_Var               parser_var              = new ParseObj_Var();
	private static final ParseObj_MemLet            parser_memlet           = new ParseObj_MemLet();
	private static final ParseObj_MagicConst        parser_magicConst       = new ParseObj_MagicConst();
	private static final ParseObj_Const             parser_const            = new ParseObj_Const();
	private static final ParseObj_NewWithArg        parser_newWithArg       = new ParseObj_NewWithArg(); // TODO Combine all 3 NewWith...
	private static final ParseObj_NewWithOpenArg    parser_newWithOpenArg   = new ParseObj_NewWithOpenArg();
	private static final ParseObj_NewWithoutArg     parser_newWithoutArg    = new ParseObj_NewWithoutArg();
	private static final ParseObj_Table             parser_table            = new ParseObj_Table();
	private static final ParseObj_Tree              parser_tree             = new ParseObj_Tree();
	private static final ParseObj_Help              parser_help             = new ParseObj_Help();
	private final ParseObj_ShortCuts                parser_shortcuts        = new ParseObj_ShortCuts();

	private boolean shortCuts      = true;
	private boolean sysCom         = true;
	private boolean magicShortCuts = true;


	public ParseManagerObj() {}

	public I_ParseObject getParser(final Block current, final String s, final Parser_Script parser, final boolean isArgument) {
		final char c0 = s.charAt(0);
		final I_ParseObject po = this.iGetParser(c0, current, s);

		if(po != null) {
//			MOut.temp(po, c0, s);

			if(isArgument) {
				if(po == ParseManagerObj.parser_stream)
					throw new CodeError("Unknown object as argument for function", "Arguments can't start with a dot, but got: " + s, parser);
				if(po == ParseManagerObj.parser_newWithOpenArg)
					throw new CodeError("Invalid use of open argument", "Please use brackets for nested argument: " + s, parser);
			}
			return po;
		}
		else
			throw new CodeError("Unknown object", s, parser);
	}

	public void strictLevel(final PARSER_LEVEL level) {
		this.parser_shortcuts.strict(level.isAllowed(PARSER_LEVEL.INSANE), level.isAllowed(PARSER_LEVEL.HIGH), level.isAllowed(PARSER_LEVEL.LOW));
//		this.parser_shortcuts_qm.strict(level.isAllowed(PARSER_LEVEL.HIGH), level.isAllowed(PARSER_LEVEL.LOW), level.isAllowed(PARSER_LEVEL.HIGH));

		this.shortCuts = true;
		this.sysCom = true;
		this.magicShortCuts = true;

		switch(level) { // No breaks, should run through!
			case INSANE:
				this.shortCuts = false;
			case HIGH:
				this.sysCom = false;
			case MEDIUM:
				this.magicShortCuts = false;
			case LOW:
//					if(po instanceof ParseObj_PathList || po instanceof ParseObj_Path)
//						continue;
			case OPEN:
				//				po instanceof ParseObj_Search
				//				|| po instanceof ParseObj_SearchPars
		}
	}

	public void strictSandbox() {
		this.sysCom = false;
	}

	private I_ParseObject iGetParser(final char c0, final Block current, final String s) {
		final I_ParseObject po = this.iGetParser1(c0, current, s);
		if(po != null)
			return po;
//			return po.hits(c0, current, s)	// Not necessary, every parser here checks only for the first char.
//				? po
//				: null;

		return this.iGetParserX(c0, current, s);
	}

	private I_ParseObject iGetParser1(final char c0, final Block current, final String s) {
		// Every parser here must be the only one starting with this char!

		switch(c0) {
			case '.':
				return ParseManagerObj.parser_stream;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				return ParseManagerObj.parser_number;
			case '"':
				return ParseManagerObj.parser_string;
			case '\'':
				return ParseManagerObj.parser_char;
			case '(':
			case '[':
			case '{':
				return this.shortCuts
					? this.parser_shortcuts
					: null;
			case '<':
				return ParseManagerObj.parser_type;
			case '´':
			case '`':
				return this.sysCom
					? ParseManagerObj.parser_syscom
					: null;
			case '°':
//			case '%':	// 'it' is much better and clear
//			case '$':
//			case '€':
			case '§':
				return this.magicShortCuts
					? ParseManagerObj.parser_magicVarShortcut
					: null;
			case ':':
				return ParseManagerObj.parser_memlet;
			case '>':
				return ParseManagerObj.parser_tree;
			case '|':
				return ParseManagerObj.parser_table;
			case '?':
				return ParseManagerObj.parser_help;
		}
		return null;
	}

	private I_ParseObject iGetParserX(final char c0, final Block current, final String s) {

		if(c0 == '0') {
			if(ParseManagerObj.parser_bin.hits(c0, current, s))
				return ParseManagerObj.parser_bin;
			if(ParseManagerObj.parser_oct.hits(c0, current, s))
				return ParseManagerObj.parser_oct;
			if(ParseManagerObj.parser_hex.hits(c0, current, s))
				return ParseManagerObj.parser_hex;
			if(ParseManagerObj.parser_number.hits(c0, current, s))
				return ParseManagerObj.parser_number;
			return null;
		}

		if(c0 == '_') {
			if(ParseManagerObj.parser_varConstType.hits(c0, current, s))
				return ParseManagerObj.parser_varConstType;
			if(ParseManagerObj.parser_magicConst.hits(c0, current, s))
				return ParseManagerObj.parser_magicConst;
			if(ParseManagerObj.parser_newWithArg.hits(c0, current, s))
				return ParseManagerObj.parser_newWithArg;
			if(ParseManagerObj.parser_newWithOpenArg.hits(c0, current, s))
				return ParseManagerObj.parser_newWithOpenArg;
			if(ParseManagerObj.parser_newWithoutArg.hits(c0, current, s))
				return ParseManagerObj.parser_newWithoutArg;
			return null;
		}

		if(c0 == '-' || c0 == '+') {
			if(ParseManagerObj.parser_magicVar.hits(c0, current, s))
				return ParseManagerObj.parser_magicVar;
			if(ParseManagerObj.parser_number.hits(c0, current, s))
				return ParseManagerObj.parser_number;
			return null;
		}

		if(c0 >= 'A' && c0 <= 'Z') {
			if(ParseManagerObj.parser_magicVar.hits(c0, current, s))
				return ParseManagerObj.parser_magicVar;
			if(ParseManagerObj.parser_varConstType.hits(c0, current, s))
				return ParseManagerObj.parser_varConstType;
			if(ParseManagerObj.parser_const.hits(c0, current, s))
				return ParseManagerObj.parser_const;
			if(ParseManagerObj.parser_newWithArg.hits(c0, current, s))
				return ParseManagerObj.parser_newWithArg;
			if(ParseManagerObj.parser_newWithOpenArg.hits(c0, current, s))
				return ParseManagerObj.parser_newWithOpenArg;
			if(ParseManagerObj.parser_newWithoutArg.hits(c0, current, s))
				return ParseManagerObj.parser_newWithoutArg;
			return null;
		}

		if(c0 >= 'a' && c0 <= 'z') {
			if(ParseManagerObj.parser_magicVar.hits(c0, current, s))
				return ParseManagerObj.parser_magicVar;
			if(ParseManagerObj.parser_var.hits(c0, current, s))
				return ParseManagerObj.parser_var;
		}
		return null;
	}

}
