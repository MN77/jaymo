/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.magic.con.MagicAxis;
import org.jaymo_lang.object.magic.con.MagicConst;
import org.jaymo_lang.object.magic.con.MagicConst.MAGICCONST;
import org.jaymo_lang.object.magic.con.MagicDebug;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.constant.AXIS;
import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 03.08.2019
 */
public class ParseObj_MagicConst implements I_ParseObject {

	public static final String[] MAGIC_CONSTS = {
		"DEBUG", "LINE", "FILE", "ARGS",
		"TOP_LEFT", "TOP_RIGHT", "BOTTOM_LEFT", "BOTTOM_RIGHT", "CENTER", "TOP", "LEFT", "BOTTOM", "RIGHT",
		"X_AXIS", "HORIZONTAL", "Y_AXIS", "VERTICAL", "Z_AXIS",
		"PI", "E",
		"DEVELOPERS", "SUPPORTERS", "SPONSORS"
	};


	public boolean hits(final char c0, final Block current, final String s) {
		if(c0 != '_')
			return false;
		return s.length() > 2 && s.charAt(1) == '_';
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);

		final String var = FilterString.matchingLeft('_' + CHARS.ABC_UPPER, s).substring(2);
//		final String var = s.replaceFirst(ParseObj_MagicConst.regex, "$1");
		final String rem = s.substring(var.length() + 2);

		// !!! At changes --> update BLOCKED.MAGIC_CONSTS !!!
		switch(var) {
//			case "_THIS":
//				return this.res(new MC_THIS(false), rem);
//			case "_JMO":
//				return this.res(new MC_JMO(), rem);
//			case "_CONTROL":
//				return this.res(new MC_CONTROL(false), rem);
			case "DEBUG":
				return this.res(new MagicDebug(), rem);

//			case "_CUR":
//			case "_CURRENT":	// Is okay, but not documented
//				return this.res(new MC_IT_STREAM(false), rem);
//			case "_EACH":
//				return this.res(new MC_IT_EACH(false), rem);
//			case "_IT":
//				return this.res(new MC_IT_BLOCK(false), rem);

//			case "_FUNC":
//				return this.res(new MagicConst(MAGICCONST.FUNC), rem);
//			case "_LOOP":
//				return this.res(new MagicConst(MAGICCONST.LOOP), rem);

//			case "_BLOCK":
//				return this.res(new MagicConst(MAGICCONST.BLOCK), rem);
//			case "_STREAM":
//				return this.res(new MagicConst(MAGICCONST.STREAM), rem);

			case "LINE": // TODO zu _DEBUG.line?
				return this.res(new MagicConst(MAGICCONST.LINE), rem);
			case "FILE": // TODO zu _DEBUG.file?
				return this.res(new MagicConst(MAGICCONST.FILE), rem);
			case "ARGS":
				return this.res(new MagicConst(MAGICCONST.ARGS), rem);

			case "TOP_LEFT":
				return this.res(new MagicPosition(POSITION.TOP_LEFT), rem);
			case "TOP_RIGHT":
				return this.res(new MagicPosition(POSITION.TOP_RIGHT), rem);
			case "BOTTOM_LEFT":
				return this.res(new MagicPosition(POSITION.BOTTOM_LEFT), rem);
			case "BOTTOM_RIGHT":
				return this.res(new MagicPosition(POSITION.BOTTOM_RIGHT), rem);
			case "CENTER":
				return this.res(new MagicPosition(POSITION.CENTER), rem);
			case "TOP":
				return this.res(new MagicPosition(POSITION.TOP), rem);
			case "LEFT":
				return this.res(new MagicPosition(POSITION.LEFT), rem);
			case "BOTTOM":
				return this.res(new MagicPosition(POSITION.BOTTOM), rem);
			case "RIGHT":
				return this.res(new MagicPosition(POSITION.RIGHT), rem);

//			case "X": // TODO Experimental
			case "X_AXIS":
			case "HORIZONTAL":
				return this.res(new MagicAxis(AXIS.X), rem);
//			case "Y": // TODO Experimental
			case "Y_AXIS":
			case "VERTICAL":
				return this.res(new MagicAxis(AXIS.Y), rem);
//			case "Z": // TODO Experimental
			case "Z_AXIS":
				return this.res(new MagicAxis(AXIS.Z), rem);

//			case "_APP":
//				return this.res(new MC_APP(), rem);

			case "PI":
				return this.res(new JMo_Double(Math.PI), rem);
			case "E":
				return this.res(new JMo_Double(Math.E), rem);

			case "DEVELOPERS":
				return this.res(new MagicConst(MAGICCONST.DEVELOPERS), rem);
			case "SUPPORTERS":
				return this.res(new MagicConst(MAGICCONST.SUPPORTERS), rem);
			case "SPONSORS":
				return this.res(new MagicConst(MAGICCONST.SPONSORS), rem);

//			case "_QUEST":
//				return this.res(new MC_QUEST(), rem);

			default:
				final I_Object c = parser.app.getMagicConst(var, parser.getDebugInfo());
				if(c != null)
					return this.res(c, rem);
				else
					throw new CodeError("Unknown MagicConst", "Got: __" + var, parser);
		}
	}

	private Group2<I_Object, String> res(final I_Object result, final String rem) {
		return new Group2<>(result, rem);
	}

}
