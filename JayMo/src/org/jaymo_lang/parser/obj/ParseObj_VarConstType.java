/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Prefix;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 03.07.2020
 *
 *          Matching: Var/Const with predefined Type: "Type var"
 */
public class ParseObj_VarConstType implements I_ParseObject {

	private enum STATE {
		TYPE,
		SPACE1,
		VARCONST,
		SPACE2,
		LET
	}


	private static final String matching = CHARS.ABC_LOWER + CHARS.ABC_UPPER + '_' + CHARS.NUMBERS + '?' + ' ' + '\t';


	public static final String NOT = "=+-*/!<>"; // ==, =++, =--, ...


	public boolean hits(final char c0, final Block current, final String s) {
		if(c0 != '_' && (c0 < 'A' || c0 > 'Z'))
			return false;

		STATE state = STATE.TYPE;
		final int len = s.length();
		boolean openSub = false;

		for(int i = 1; i < len; i++) {
			final char c = s.charAt(i);

			switch(state) {
				case TYPE:
					if(c == '{') {
						openSub = true;
						continue;
					}
					if(c == '}') {
						openSub = false;
						continue;
					}
					if(c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_')
						continue;
					if(openSub && (c == '.' || c == '$'))
						continue;

					if(c == ' ' || c == '\t' || c == '?') {
						state = STATE.SPACE1;
						continue;
					}
					return false;
				case SPACE1:
					if(c == ' ' || c == '\t')
						continue;
					if(c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z') {
						state = STATE.VARCONST;
						continue;
					}
					return false;
				case VARCONST:
					if(c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_')
						continue;
					if(c == '?' || c == ' ' || c == '\t') {
						state = STATE.SPACE2;
						continue;
					}
					if(c == '=') {
						state = STATE.LET;
						continue;
					}
					if(c == '~') // ~=
						continue;
					return false;
				case SPACE2:
					if(c == ' ' || c == '\t')
						continue;
					if(c == '=') {
						state = STATE.LET;
						continue;
					}
					if(c == '~') // ~=
						continue;
					return false;
				case LET:
					return len > i && ParseObj_VarConstType.NOT.indexOf(s.charAt(i)) == -1;
			}
		}
		return false;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);
		int is = s.indexOf(' ');
		if(is == -1)
			is = s.indexOf('\t');
		String type = s.substring(0, is);

//		boolean typeNilable = type.endsWith("?");
		final boolean typeNilable = type.charAt(type.length() - 1) == '?';
		if(typeNilable)
			type = type.substring(0, type.length() - 1);

		Lib_Type.checkValidity(type, parser);
		if(type.charAt(0) == '_')
			type = Lib_Prefix.addPrefix(type, parser);

		String vcName = "";
		int pointer = is;
		char c0 = s.charAt(pointer);

		while(ParseObj_VarConstType.matching.indexOf(c0) >= 0) {
			pointer++;
			if(c0 != ' ' && c0 != '\t')
				vcName += c0;
			if(pointer >= s.length())
				break;
			c0 = s.charAt(pointer);
		}
		final String rem = s.substring(pointer); //+1 == '='

		if(typeNilable && !vcName.endsWith("?"))
			vcName += '?';

		final char vcn0 = vcName.charAt(0);
		final I_Mem obj = vcn0 >= 'A' && vcn0 <= 'Z'
			? current.getConstManager().use_ParseTime(parser, vcName, true)
			: current.getVarManager().use_ParseTime(parser, vcName, true);

		obj.setType(type, parser.getDebugInfo());
		return new Group2<>(obj, rem);
	}

}
