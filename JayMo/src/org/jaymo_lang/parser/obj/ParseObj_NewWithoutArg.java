/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.Lib_Prefix;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_NewWithoutArg implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {

		if(c0 == '_' && s.length() > 1) {
			final char c1 = s.charAt(1);
			if(c1 >= 'A' && c1 <= 'Z' || c1 == '{')
				return true;
		}
		if(c0 >= 'A' && c0 <= 'Z')
			return true;
		return false;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		String m = Lib_Parser.getTypeName(s, parser);
		final String rem = s.substring(m.length()).trim();

		m = Lib_Prefix.addPrefix(m, parser);
		Lib_Type.checkValidity(m, parser);
		final I_Object o = ObjectManager.createNew(parser.app, current, m, null, parser.getDebugInfo());
		return new Group2<>(o, rem);
	}

}
