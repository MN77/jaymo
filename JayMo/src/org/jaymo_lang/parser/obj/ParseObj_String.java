/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_RegEx;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.pseudo.NonAtomic;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Group3;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_String implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {
//		return c0 == '"' && Lib_StringParser.checkString(s, false, null) >= 0;
		return c0 == '"'; // Unclosed String will be checked at "iParseModifier"
		// Following '?' will not be checked here
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		final Group3<String, Integer, Boolean> g = this.iParseModifier(s, parser.getDebugInfo());
		final String rem = s.substring(g.o2 + 1);

		if(g.o3) { // Regex
			final Call arg = new Call(current, new Str(g.o1), parser.getDebugInfo());
			return new Group2<>(new NonAtomic(JMo_RegEx.class, new Call[]{arg}), rem);
		}
		else // String / Literal
			return new Group2<>(new Str(g.o1), rem);
	}

	/**
	 * @return Returns a Group with: String, endIndex, isRegEx
	 */
	private Group3<String, Integer, Boolean> iParseModifier(final String s, final DebugInfo debugInfo) {
		int end = 1;
		final int sLen = s.length();
		boolean reworkNeeded = false; // When it contains escaped chars.

		for(end = 1; end < sLen; end++) {
			final char c = s.charAt(end);

			if(c == '\\') {
				end++;
				reworkNeeded = true;
			}
			else if(c == '"')
				break;
		}
		if(end == sLen)
			throw new CodeError("Unclosed string", s, debugInfo);

		final String str = s.substring(1, end);

		if(end + 1 < sLen) {
			final char cx = s.charAt(end + 1);

			if(cx == 'l')
				return new Group3<>(str, end + 1, false); // --- Literal ---
			if(cx == 'r')
				return new Group3<>(str, end + 1, true); // --- RegEx ---
		}
		// --- Str ---
		if(!reworkNeeded)
			return new Group3<>(str, end, false);

		// Rework string
		final int strLen = str.length();
		final StringBuilder sb = new StringBuilder(strLen);

		for(int idx = 0; idx < strLen; idx++) {
			char c = str.charAt(idx);

			if(c == '\\') {
				idx++;
				c = str.charAt(idx);

				if(c == 'u') {
					final char utf = Lib_Parser.replaceUtfChar(str, idx, debugInfo);
					sb.append(utf);
					idx += 4;
				}
				else
					sb.append(Lib_Parser.replaceEscapeChar(c, debugInfo));
			}
			else
				sb.append(c);
		}
		return new Group3<>(sb.toString(), end, false);
	}

}
