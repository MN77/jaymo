/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.Const;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.object.pseudo.ConstLet;
import org.jaymo_lang.object.pseudo.FuncLet;
import org.jaymo_lang.object.pseudo.VarLet;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Comply;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 14.04.2021
 *
 *          TODO: Combine parsing of VarLet, ConstLet, FuncLet
 *          TODO: Abstract Type for VarLet & ConstLet
 */
public class ParseObj_MemLet implements I_ParseObject {

	private static final String matchingConst = CHARS.ABC_UPPER + '_' + CHARS.NUMBERS; // + '?' + '¿';
	private static final String matchingVar   = CHARS.ABC_LOWER + CHARS.ABC_UPPER + '_' + CHARS.NUMBERS + '?';


	public boolean hits(final char c0, final Block current, final String s) {
		return c0 == ':';
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		if(s.length() == 1)
			throw new CodeError("Invalid MemLet definition", "Lonely ':' found, missing Const,Var,Brackets", parser);

		final char c1 = s.charAt(1);
		if(c1 >= 'A' && c1 <= 'Z')
			return this.parseConstLet(parser, current, s);
		if(c1 >= 'a' && c1 <= 'z')
			return this.parseVarLet(parser, current, s);
		if(c1 == '{')
			return this.parseFuncLet(parser, current, s);

		throw new CodeError("Invalid MemLet definition", "Got: " + s, parser);
	}

	public Group2<I_Object, String> parseConstLet(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);
		final String con = FilterString.matchingLeft(ParseObj_MemLet.matchingConst, s.substring(1));
		if(!Lib_Comply.checkConstName(con))
			throw new CodeError("Invalid constant name", "This constant name can't be used: " + con, parser);
		final String rem = s.substring(con.length() + 1);
		final Const co = current.getConstManager().use_ParseTime(parser, con, false);
		final ConstLet vl = new ConstLet(co);
		return new Group2<>(vl, rem);
	}

	public Group2<I_Object, String> parseFuncLet(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);
		final String func = Lib_Parser.group('{', '}', s, parser.getDebugInfo());

		final String rem = s.substring(3 + func.length());

		final Call[] ca = Parser_Call.parseArguments(parser, current, func);
		Lib_Error.ifArgs(ca.length, 1, 1, "FuncLet", parser.getDebugInfo());

//		if(ca[0].getInternPars() != null && ca[0].getInternPars().length > 0)
//			throw new CodeError("No arguments allowed for FuncLet!", func, debugInfo);

		final FuncLet fl = new FuncLet(ca[0]);
		return new Group2<>(fl, rem);
	}

	public Group2<I_Object, String> parseVarLet(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);
		final String var = FilterString.matchingLeft(ParseObj_MemLet.matchingVar, s.substring(1));
		if(!Lib_Comply.checkVarName(var))
			throw new CodeError("Invalid variable name", "This variable name can't be used: " + var, parser);
		final String rem = s.substring(var.length() + 1);
		final Var vo = current.getVarManager().use_ParseTime(parser, var, false);
		final VarLet vl = new VarLet(vo);
		return new Group2<>(vl, rem);
	}

}
