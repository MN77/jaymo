/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_E_Notation;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_Number implements I_ParseObject {

	private static final String MODIFIERS = "bsilcfdaz";

//	private static final String regex = "^([-+]?[0-9]+)[bsilfd]?.*$";


	public boolean hits(final char c0, final Block current, final String s) {
		return c0 >= '0' && c0 <= '9' || c0 == '+' || c0 == '-';
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		final ScanResult sr = this.iScan(parser, s);
		String rem = sr.rem;
		final boolean emptyRem = rem.length() == 0;
		final char typeChar = emptyRem ? ' ' : rem.charAt(0);
		I_Object obj = null;

		try {

			if(emptyRem || ParseObj_Number.MODIFIERS.indexOf(typeChar) < 0)
				obj = sr.hasE
					? new JMo_Double(Double.parseDouble(sr.nr)) // Default with 'E' notation is Double
					: sr.hasDot
						? new JMo_Dec(Dec.parseDec(sr.nr))
						: new Int(Integer.parseInt(sr.nr)); // Default // Don't use double!
			else {
				String nr2 = sr.nr;

				if(typeChar == 'b' || typeChar == 's' || typeChar == 'i' || typeChar == 'l') {
					if(sr.hasDot && !sr.hasE)
						throw this.iDecToIntError(parser, nr2);
					if(sr.hasE)
						nr2 = Lib_E_Notation.unfold(nr2);
				}

				switch(typeChar) {
					case 'b':
						obj = new JMo_Byte(Byte.parseByte(nr2));
						rem = rem.substring(1);
						break;
					case 's':
						obj = new JMo_Short(Short.parseShort(nr2));
						rem = rem.substring(1);
						break;
					case 'i':
						obj = new Int(Integer.parseInt(nr2));
						rem = rem.substring(1);
						break;
					case 'l':
						obj = new JMo_Long(Long.parseLong(nr2));
						rem = rem.substring(1);
						break;
					case 'c':
						obj = new JMo_Dec(Dec.parseDec(nr2));
						rem = rem.substring(1);
						break;
					case 'f':
						obj = new JMo_Float(Float.parseFloat(nr2));
						rem = rem.substring(1);
						break;
					case 'd':
						obj = new JMo_Double(Double.parseDouble(nr2));
						rem = rem.substring(1);
						break;
					//				case 'e':
					//					nr = nr.substring(0, len - 1);
					//					final double e = Double.parseDouble(nr);
					//					return new Group2<>(new JMo_Double(e), rem);
					case 'a':
						final BigInteger bigInt = sr.hasE
							? new BigDecimal(nr2).toBigInteger()
							: new BigInteger(nr2);
						obj = new JMo_BigInt(bigInt);
						rem = rem.substring(1);
						break;
					case 'z':
						obj = new JMo_BigDec(new BigDecimal(nr2));
						rem = rem.substring(1);
						break;
					default:
						throw Err.impossible(typeChar);
				}
			}
		}
		catch(final NumberFormatException e) {
			final int typeIdx = ParseObj_Number.MODIFIERS.indexOf(typeChar);
			final String type = typeIdx == -1 ? "Int" : new String[]{"Byte", "Short", "Int", "Long", "Dec", "Float", "Double", "BigInt", "BigDec"}[typeIdx];
			if(sr.nr.toUpperCase().endsWith("E"))
				throw new CodeError("Parsing of number failed", "Missing exponent: " + sr.nr, parser);
			else
				throw new CodeError("Parsing of number failed", "Invalid or out of range for type <" + type + ">: " + sr.nr, parser);
		}

		return new Group2<>(obj, rem);
	}

	private CodeError iDecToIntError(final Parser_Script parser, final String nr) {
		throw new CodeError("Invalid number", "A integer number has no dot, but got: " + nr, parser);
	}

	private ScanResult iScan(final Parser_Script parser, final String nr) {
		String s = nr;
		int len = s.length();
		char c = ' ';
		boolean hasUnderline = false;
		boolean hasDot = false;
		boolean hasE = false;
		boolean afterExp = false;
		boolean afterDot = false;

		for(int i = 1; i < len; i++) {
			c = s.charAt(i);

			if(c == '.') {

				if(hasDot) {
					s = s.substring(0, i);
					len = s.length();
					break;
				}
				hasDot = true;
				afterDot = true;
				continue;
			}

			if(c == '_') {
//				if(hasDot)
//					throw new CodeError("Invalid decimal number", "...: " + s, parser.gDebugInfo());	// TODO Really allow underline for decimals?
				hasUnderline = true;
				continue;
			}

			if(!afterDot && (c == 'e' || c == 'E')) {
				afterExp = true;
				hasE = true;
				continue;
			}

			if(afterExp && (c == '-' || c == '+')) {
				afterExp = false;
				continue;
			}
			afterExp = false;
			afterDot = false;

			if(c < '0' || c > '9') {
				s = s.substring(0, i);
				len = s.length();
				break;
			}
		}

		if(hasDot && s.charAt(s.length() - 1) == '.') {
			hasDot = false;
			s = s.substring(0, s.length() - 1);
			len--;
		}
		if(hasUnderline)
			s = Lib_Parser.removeUnderlines(s, parser.getDebugInfo());

		return new ScanResult(s, nr.substring(len), hasDot, hasE);
	}

}


class ScanResult {

	public final String  nr;
	public final String  rem;
	public final boolean hasDot;
	public final boolean hasE;


	public ScanResult(final String nr, final String rem, final boolean hasDot, final boolean hasE) {
		this.nr = nr;
		this.rem = rem;
		this.hasDot = hasDot;
		this.hasE = hasE;
	}

}
