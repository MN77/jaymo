/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Comply;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_Var implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {
		return c0 >= 'a' && c0 <= 'z';
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);
		final int gap = this.iSearchGap(s);
		String var = s.substring(0, gap);
		if(!Lib_Comply.checkVarName(var)) // Not necessary, but senseful
			throw new CodeError("Invalid variable name", "This variable name can't be used: " + var, parser);

		final String rem = s.substring(gap);

		if(var.equals(Var.IT))
			var = Var.IT_FULL;

		final Var vo = current.getVarManager().use_ParseTime(parser, var, false);
		return new Group2<>(vo, rem);
	}

	private int iSearchGap(final String s) {
		int end = 0;

		for(final char c : s.toCharArray())
			if(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_')
				end++;
			else {
				if(c == '?')
					end++;

				break;
			}

		return end;
	}

}
