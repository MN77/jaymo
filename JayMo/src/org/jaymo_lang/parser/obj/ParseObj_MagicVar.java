/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.magic.var.MV_APP;
import org.jaymo_lang.object.magic.var.MV_CUR;
import org.jaymo_lang.object.magic.var.MV_EACH;
import org.jaymo_lang.object.magic.var.MV_JAYMO;
import org.jaymo_lang.object.magic.var.MV_SUPER;
import org.jaymo_lang.object.magic.var.MV_THIS;
import org.jaymo_lang.object.magic.var.MagicVar;
import org.jaymo_lang.object.magic.var.MagicVar.MAGICVAR;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.ATOMIC;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 17.11.2020
 */
public class ParseObj_MagicVar implements I_ParseObject {

	// !!!!! This Strings must be also blocked in: A_VarConstManager
//	private static final String[] literals = new String[] {"cur", "CUR", "each", "EACH", "func", "it", "loop", "this", "THIS", "true", "True", "TRUE", "false", "False", "FALSE", "nil", "Nil", "NIL", "app", "jmo", "JMO"};
//	private static final String[] literals = new String[] {"cur", "CUR", "each", "EACH", "func", "loop", "this", "THIS", "true", "True", "TRUE", "false", "False", "FALSE", "nil", "Nil", "NIL", "app", "jmo", "JMO"};	// it
	private static final String[] literals = {
		"cur", "each", "func", "loop", "this", "super",
		"true", "TRUE", "false", "FALSE",
		"nil", "NIL", "null", "NULL",
		"app", "jmo", "jaymo",
		ATOMIC.NOT_A_NUMBER,
		ATOMIC.INFINITY, ATOMIC.INFINITY_POSITIVE, ATOMIC.INFINITY_NEGATIVE};
	// "it" is parsed as variable
	// "True", "False", "Nil", "Null" are reserved but no valid Types
	// MagicVar "event" is reserved (BLOCKED) but not used!
	private static final String literals0 = "acefijlnstFNT+-";


	public boolean hits(final char c0, final Block current, final String s) {
		if(ParseObj_MagicVar.literals0.indexOf(c0) == -1)
			return false;

		int l = s.length();

		for(final String literal : ParseObj_MagicVar.literals)
			if(s.startsWith(literal)) {
				l = literal.length();

				if(s.length() > l) {
					final char c = s.charAt(l);
					if(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_')
						return false;
				}
				return true;
			}

		return false;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);

		switch(s.charAt(0)) {
			case 'a': //app
				return new Group2<>(new MV_APP(), s.substring(3));
			case 'c': //cur
//			case 'C': //CUR
				return new Group2<>(new MV_CUR(false), s.substring(3));
			case 'e': //each
//			case 'E': //EACH
				return new Group2<>(new MV_EACH(false), s.substring(4));
			case 'f': //false, func
				if(s.charAt(1) == 'a')
					return new Group2<>(Bool.FALSE, s.substring(5));
				else
					return new Group2<>(new MagicVar(MAGICVAR.FUNC), s.substring(4));
//			case 'i': //it
//				return new Group2<>(new MC_IT_BLOCK(false), s.substring(2));
			case 'i': //infinity
				return new Group2<>(new JMo_Double(Double.POSITIVE_INFINITY), s.substring(8));
			case 'j': //jmo, jaymo
//			case 'J': //JMO, JAYMO
				if(s.charAt(1) == 'a')
					return new Group2<>(new MV_JAYMO(), s.substring(5));
				else
					return new Group2<>(new MV_JAYMO(), s.substring(3));
			case 'l': //loop
				return new Group2<>(new MagicVar(MAGICVAR.LOOP), s.substring(4));
			case 't': //true, this
				if(s.charAt(1) == 'r')
					return new Group2<>(Bool.TRUE, s.substring(4));
				else
					return new Group2<>(new MV_THIS(false), s.substring(4));
			case 'F': // False, FALSE
				return new Group2<>(Bool.FALSE, s.substring(5));
			case 'T': // True, TRUE, THIS
//				if(s.charAt(1) == 'H')
//					return new Group2<>(new MC_THIS(false), s.substring(4));
//				else
				return new Group2<>(Bool.TRUE, s.substring(4));
			case 'n': // nil, null, not_a_number (nan)
				if(s.charAt(1) == 'o')
					return new Group2<>(new JMo_Double(Double.NaN), s.substring(12));
				// ... run through ...
			case 'N': // Nil, NIL, Null, NULL
				final char c1 = s.charAt(1);
				return new Group2<>(Nil.NIL, s.substring(c1 == 'i' || c1 == 'I' ? 3 : 4));
			case 's': //super
				return new Group2<>(new MV_SUPER(false), s.substring(5));
			case '+': //+inf
				return new Group2<>(new JMo_Double(Double.POSITIVE_INFINITY), s.substring(9));
			case '-': //-inf
				return new Group2<>(new JMo_Double(Double.NEGATIVE_INFINITY), s.substring(9));
		}
		throw Err.impossible(s);
	}

}
