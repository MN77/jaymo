/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.Lib_Prefix;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_NewWithArg implements I_ParseObject {

//	private static final String regex_NewObjWith = "^(" + REGEX.TYPE + ")\\s*\\((.*?)\\)(.*)$";

	public boolean hits(final char c0, final Block current, final String s) { //TODO dies hier stark vereinfachen!
//		return c0 >= 'A' && c0 <= 'Z' && s.matches(ParseObj_NewWithPar.regex_NewObjWith);
		if(c0 != '_' && (c0 < 'A' || c0 > 'Z'))
			return false;

//		int low = 0;
		final byte state = 0; // 0 = type, 1 = spaces, (2 = open)
		for(final char c : s.toCharArray())
			switch(state) {
				case 0:
					if(c == '(')
//						state = 2;
						return true;
					else if(c == ' ')
//						state = 1;
						return false;
					else {
						final boolean hit = c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_';
						if(!hit)
							return false;
//						if(c >= 'a' && c <= 'z')
//							low++;
					}
					break;
//				case 1:
//					if(c == '(')
////						state = 2;
//						return true;
//					else if(c != ' ')
//						return false;
//					break;
//				case 2:
//					if(c == ')')
//						return low > 0;
//					break;
			}
		return false;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
//		String m = s.replaceFirst("(.*?)\\(.*", "$1");
		String m = Lib_Parser.getTypeName(s, parser);
		final int spaces = this.spaces(s, m.length());
		final String sp = s.substring(spaces + m.length());

		final String ps = Lib_Parser.group('(', ')', sp, parser.getDebugInfo());
		String rem = sp.substring(1 + ps.length() + 1);
//		String ps = Lib_Parser.group('(', ')', s.substring(m.length()), parser.getDebugInfo());
//		String rem = s.substring(m.length()+1 + ps.length() + 1);

//		m = m.trim();
//		ps = ps.trim();
		rem = rem.trim();
		m = Lib_Prefix.addPrefix(m, parser);
		Lib_Type.checkValidity(m, parser);

		final Call[] args = Parser_Call.parseArguments(parser, current, ps);
		final I_Object o = ObjectManager.createNew(parser.app, current, m, args, parser.getDebugInfo());

		return new Group2<>(o, rem);
	}

	private int spaces(final String s, int offset) {
		int count = 0;

		for(; offset < s.length(); offset++) {
			final char c = s.charAt(offset);
			if(c == ' ' || c == '\t')
				count++;
			else
				return count;
		}
		return count;
	}

}
