/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.parser.tdef.ParseTDef;
import org.jaymo_lang.util.LineBuffer;
import org.jaymo_lang.util.TYPENAMES;

import de.mn77.base.data.search.SearchArray;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 */
public class Parser_Type {

	private static ParseTDef pt = new ParseTDef();


	public static void parse(final Parser_Script parser, final Type currentType, final String s) {
		final LineBuffer buffer = parser.buffer;

		// Check for illegal Prefix
		if(parser.getCurrentPrefix() != null && parser.getCurrentPrefix().equals("Java"))
			throw new CodeError("Illegal type definition", "It is not possible to define a new type for the prefix 'Java'.", parser);

		// Parse type
		final Type t = Parser_Type.pt.parse(parser, currentType, s);

		// Check validity
		final String name = t.getName();
		if(SearchArray.knows(TYPENAMES.ATOMIC, name) || SearchArray.knows(TYPENAMES.ABSTRACT, name) || SearchArray.knows(TYPENAMES.BLOCKED, name)
			|| ObjectManager.searchJavaJMoClass(name) != null)
			throw new CodeError("Reserved Type-Name", name, buffer);

		// Integrieren
		currentType.addType(t, buffer.getDebugInfo());

		if(buffer.nextLevelDiff() > 0)
			new Parser_Block().parse(parser, t, null, t.getBlock(), false);
	}

}
