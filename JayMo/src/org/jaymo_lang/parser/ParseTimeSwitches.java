/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.util.ClassFinder;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.Lib_Prefix;
import org.jaymo_lang.util.Lib_StringParser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 * @created 25.10.2019
 */
public class ParseTimeSwitches {

	public void parse(final Parser_Script parser, String s, final DebugInfo debug) {
		final App app = parser.app;
		s = s.substring(1).trim();
		app.strict.checkSandbox(debug, "> " + s);
		final char c0 = s.charAt(0);

		// Imports
		switch(c0) {
			case '*': // >*
				app.strict.checkShortImports(debug);
				final String jclass = this.iIncludeFree(s, "java-package", debug);
				ClassFinder.getInstance().addJavaImportPackage(jclass, debug);
				return;
			case '?': // >?    // '+' ?!?
				app.strict.checkShortImports(debug);
				final String jpack = this.iIncludeFree(s, "java-class", debug);
				ClassFinder.getInstance().addJavaImportClass(jpack, debug);
				return;
			case '>': // >>
				app.strict.checkShortImports(debug);
				final String include = this.iIncludeFree(s, "file", debug);
				app.importsAdd(parser.getFileBase(), include);
				return;
		}
		final Group2<String, String[]> g = this.iSplitLine(s, parser);

		// --- Switches that can used in every script-file ---

		switch(g.o1) {
//			case "lib":
//			case "import":
			case "include": // Like PHP, Ruby, C, ...
				for(String si : g.o2) {
					si = this.iCheckParseArgument(si, "include", debug);
					app.importsAdd(parser.getFileBase(), si);
				}
				return;
//			case "java":
			case "class":
				for(String si : g.o2) {
					si = this.iCheckParseArgument(si, "java-class", debug);
					ClassFinder.getInstance().addJavaImportClass(si, debug);
				}
				return;
			case "package":
				for(String si : g.o2) {
					si = this.iCheckParseArgument(si, "java-package", debug);
					ClassFinder.getInstance().addJavaImportPackage(si, debug);
				}
				return;
			case "prefix":
				this.argCheckCount(0, 1, g.o2, debug);
				String si = g.o2.length == 0 ? null : g.o2[0];
				if(si != null) {
					si = this.iCheckParseArgument(si, "prefix", debug);
					Lib_Prefix.checkPrefix(si, debug);
				}
				parser.setCurrentPrefix(si);
				return;
		}
		// ---Switches that can only used in the first script-file ---

		app.strict.checkParseTimeSwitch(g.o1, debug);

		switch(g.o1) {
//			case "exit":
//				MOut.dev("Abort parsing");
//				return true;

			case "debug":
				app.setDebug();
//				MOut.debugDetail();	// Toooo much informations
//				MOut.debugMin();
				MOut.setDebug(DEBUG_MODE.MINIMAL);
				MOut.setJavaErrors(true);
				return;
			case "setOutputFile":
				this.argCheckCount(0, 1, g.o2, debug);
//				final String opf = s.replaceFirst("^.*\"(.*)\".*$", "$1");
				final String opf = g.o2.length == 0 ? null : this.parString(g.o2[0], debug);
//				MOut.dev("OutputFile: " + opf);
				final MFile opf2 = opf == null ? null : new MFile(opf);
				app.setOutputFile(opf2);
				return;

//			case "setShortcuts":
//				Lib_Error.ifPars(g.o2.length, 1, 1, debug);
//				String si = g.o2[0].trim();
//				final Group2<String, Integer> sg = Lib_Parser.stringGet(si, debug);
//				final String toSet = sg.o1;
//				final int needed = REGEX.getShortcuts().length();
//				if(sg.o2 != si.length() - 1 || toSet.length() != needed)
//					throw new CodeError("Invalid argument for '"+g.o1+"'!", "String with "+needed+" chars needed. Got: "+s, debug);
//				for(char x : toSet.toCharArray())
//					if(x >='a' && x <='z' || x >='A' && x <='Z' || x >='0' && x <='9' || "?¿*()[]{}<>=-\\@¶^:;/".indexOf(x) >=0)
//						throw new CodeError("Invalid char for '"+g.o1+"'!", "Got: "+x, debug);
//
//				REGEX.setShortcuts(toSet);
//				return;

			// ----- Strict -----

//			case "strictBlocks":
//				app.strictAutoBlock();
//				return;
//			case "strictConsts":
//				Parser_Call.strictMagicConsts();
//				return;
//			case "strictObjects":
//				Parser_Call.strictObjectShortcuts();
//				return;
//			case "strictFunctions":
//				Parser_Call.strictFunctions();
//				return;
//			case "strictSafe":
			case "sandbox":
			case "sandBox":
				this.argCheckNone(g.o2, debug);
//				Quarantine
				app.strict.setSandbox();
				return;

			case "nojava":
			case "noJava":
				this.argCheckNone(g.o2, debug);
				app.strict.setNoJava();
				return;

			case "strictWebstart":
				this.argCheckNone(g.o2, debug);
				app.strict.setWebstart();
				return;

			case "lazyErrors":
			case "errorsLazy":
				this.argCheckNone(g.o2, debug);
				app.strict.setLazyErrors();
				return;

			case "noWarnings":
				this.argCheckNone(g.o2, debug);
				app.strict.setNoWarnings();
				return;

			case "unsafeHotCode":
				this.argCheckNone(g.o2, debug);
				app.strict.setUnsafeHotCode();
				return;

			case "cleanFlow":
				this.argCheckNone(g.o2, debug);
				app.strict.setClearFlow();
				return;

			case "open": // normal?!?
				this.argCheckNone(g.o2, debug);
//			case "level0":
				return; // This is standard
//				throw new CodeError("Invalid use of parsetime-switch!", "'open' is reserved but currently not used.", debug);

			case "low":
				this.argCheckNone(g.o2, debug);
//			case "level1":
				app.strict.set(PARSER_LEVEL.LOW, debug);
				return;

			case "strict":
			case "medium":
				this.argCheckNone(g.o2, debug);
//			case "level2":
				app.strict.set(PARSER_LEVEL.MEDIUM, debug);
				return;

			case "high":
				this.argCheckNone(g.o2, debug);
//			case "level3":
				app.strict.set(PARSER_LEVEL.HIGH, debug);
				return;

			case "insane":
				this.argCheckNone(g.o2, debug);
//			case "level4":
				app.strict.set(PARSER_LEVEL.INSANE, debug);
				return;


			// ----- Test -----

//			case "testAutoBlockAfterFunction":
//				app.strict.setTestAutoBlockAfterFunction();
//				return;
		}
		throw new CodeError("Unknown Parsetime-Command", s, debug);
	}

	private void argCheckCount(final int min, final int max, final String[] pa, final DebugInfo debug) {
		Lib_Error.ifNotBetween(min, max, pa.length, "arguments", debug);
	}

	private void argCheckNone(final String[] pa, final DebugInfo debug) {
		Lib_Error.ifNotBetween(0, 0, pa.length, "arguments", debug);
	}

	private String iCheckParseArgument(String s, final String info, final DebugInfo debug) {
		s = s.trim();
		final Group2<String, Integer> sg = Lib_Parser.stringGet(s, debug);
		if(sg == null || sg.o2 != s.length() - 1)
			throw new CodeError("Invalid argument for " + info + "!", s, debug);
		return sg.o1;
	}

	private String iIncludeFree(final String s, final String title, final DebugInfo debug) {
		final String s2 = s.substring(1).trim();
		if(s2.length() == 0)
			throw new CodeError("Invalid include!", "Which " + title + " should I include?", debug);

		if(s2.charAt(0) == '"') {
			final Group2<String, Integer> g = Lib_Parser.stringGet(s2, debug);
			if(g.o2 != s2.length() - 1) //Prevent some Text after the import
				throw new CodeError("Invalid argument for include!", s, debug);
			return g.o1;
		}
		else
			return s2;
	}

	private Group2<String, String> iSplitCommand(final String s, final Parser_Script parser) {
		for(int i = 0; i < s.length(); i++)
			switch(s.charAt(i)) {
				case '(':
					final String command = s.substring(0, i).trim();
					String rem = s.substring(i).trim();
					if(rem.charAt(rem.length() - 1) != ')')
						throw new CodeError("Closing bracket is missing!", s, parser.getDebugInfo());

					rem = rem.substring(1, rem.length() - 1);
					return new Group2<>(command, rem);
				case ' ':
				case '=':
					final String command2 = s.substring(0, i).trim();
					String rem2 = s.substring(i).trim();

					if(rem2.startsWith("=")) {
						if(!parser.app.strict.isValid_AutoProperty())
							throw new CodeError("Strict! Auto-Property is forbidden!", "Please use brackets for arguments.", parser.getDebugInfo());
						rem2 = rem2.substring(1).trim();
					}
					else
						parser.app.strict.checkOpenArg(parser.getDebugInfo());

					return new Group2<>(command2, rem2);
				default:
			}

		return new Group2<>(s, null);
	}

	private Group2<String, String[]> iSplitLine(final String s, final Parser_Script parser) {
		final Group2<String, String> command_args = this.iSplitCommand(s, parser);
		final String command = command_args.o1;
		String rem = command_args.o2;
		if(rem == null)
			return new Group2<>(command, new String[0]);

		final SimpleList<String> pa = new SimpleList<>();

		while(rem.length() > 0) {
			final Group2<String, String> result = Lib_StringParser.getNextPar(rem, parser.getDebugInfo());
			pa.add(result.o1);
			if(result.o2.equals(rem))
				throw new CodeError("Invalid ParseTime-Command!", s, parser.getDebugInfo());
			rem = result.o2;
		}
		final String[] sa = pa.toArray(new String[pa.size()]);
		return new Group2<>(command, sa);
	}

	private String parString(String s, final DebugInfo debug) {
		s = s.trim();
		if(s.charAt(0) != '"' || s.charAt(s.length() - 1) != '"')
			throw new CodeError("Invalid string!", s, debug);
		return s.substring(1, s.length() - 1);
	}

}
