/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.LineBuffer;
import org.jaymo_lang.util.ScriptFilter;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 */
public class Parser_Script implements I_DebugInfoSource {

	public final LineBuffer buffer;
	public final App        app;
	private String          currentPrefix = null;
	private final String    fileBase;


	public Parser_Script(final App app, final String fileBase, final String fileInfo, final String text) {
		final Group2<String[], int[]> g = new ScriptFilter().filter(fileInfo, text);
		final String[] lines = g.o1;
//		final String[] lines = text.split(System.getProperty("line.separator"));
		this.iCombineLines(lines);
		this.buffer = new LineBuffer(fileInfo, lines, g.o2);
		this.app = app;
		this.fileBase = fileBase;
	}

	public String getCurrentPrefix() {
		return this.currentPrefix;
	}

	@Override
	public DebugInfo getDebugInfo() {
		return this.buffer.getDebugInfo();
	}

	public String getFileBase() {
		return this.fileBase;
	}

	public void parse() {
		final Parser_Block parserBlock = new Parser_Block();
		parserBlock.parse(this, this.app.getRootType(), null, this.app.getRootType().getBlock(), true);
	}

	public void setCurrentPrefix(final String prefix) {
		this.currentPrefix = prefix == null || prefix.length() == 0 ? null : prefix;
	}

	private void iCombineLines(final String[] lines) {
		final int max = lines.length - 1;

		for(int bottom = max; bottom >= 1; bottom--) {
			// --- Filter empty lines ---

			final int top = bottom - 1;

			// --- Combine ---

			// Maybe append next line to this line  # Depth will be ignored!
			if(Lib_Parser.combineEnd(lines[top])) {
				lines[top] += lines[bottom].trim();
				lines[bottom] = null;
			}
			// If COMBINE-Chars at the beginning of the line, add it to the prior line #! Depth is important
//			if(i != 0 && s.matches("^\\s*[" + Lib_Parser.COMBINE_CHARS + "].*$")) {

			final int combineBegin = lines[bottom] == null ? 0 : Lib_Parser.combineBegin(lines[bottom]);

			if(combineBegin > 0) {
				final int depth_bottom = Lib_Parser.getDepth(lines[bottom]);
				final int depth_top = Lib_Parser.getDepth(lines[top]);

				if(depth_bottom >= depth_top || combineBegin == 2) {
					lines[top] += lines[bottom].trim();
					lines[bottom] = null;
				}
			}
		}
	}

}
