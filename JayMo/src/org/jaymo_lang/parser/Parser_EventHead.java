/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.Event;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.parser.event.ParseEvDef_Direct;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 */
public class Parser_EventHead {

	private static final ParseEvDef_Direct funcparser = new ParseEvDef_Direct();
//	private static final I_ParseEvDef[] funcparser = new I_ParseEvDef[]{
//		new ParseEvDef_Direct(),
//		new ParseEvDef_ParMulti()
//	};


	public static Event parse(final Parser_Script parser, final Type type, final String s, final DebugInfo debug) {
//		MOut.dev("Parse Event-Head", type);

//		for(final I_ParseEvDef pf : Parser_EventHead.funcparser)
//			if(pf.hits(s))
//				return pf.parse(parser, type, s);
//		if(Parser_EventHead.funcparser.hits(s))
		return Parser_EventHead.funcparser.parse(parser, type, s);

//		final String detail = s.indexOf('.') > -1 ? "No attached stream allowed, but got" : "Got";
//		throw new CodeError("Unknown Event-Type", detail + ": " + s, debug);
	}

}
