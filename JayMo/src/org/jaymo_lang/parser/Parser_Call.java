/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.var.A_MagicVar;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.pseudo.A_MemLet;
import org.jaymo_lang.object.pseudo.MultiCall;
import org.jaymo_lang.parser.event.I_ParserEvStart;
import org.jaymo_lang.parser.event.ParserEvStart_Args;
import org.jaymo_lang.parser.event.ParserEvStart_Direct;
import org.jaymo_lang.parser.func.I_ParseFunc;
import org.jaymo_lang.parser.obj.I_ParseObject;
import org.jaymo_lang.util.Lib_StringParser;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Parser_Call {

	private static int multiCallCounter = 0;

	private static final I_ParserEvStart[] parsers_EvStart = {
		new ParserEvStart_Direct(),
		new ParserEvStart_Args()
	};


	/**
	 * @return Argument-Calls
	 * @implNote
	 *           - is there one Par without grouping? (for setting last block)
	 */
	public static Call[] parseArguments(final Parser_Script parser, final Block current, String s) {
//		MOut.dev("ParseArgument: " + s);
		if(s.length() == 0)
			return new Call[0];

		s = s.trim();

		// Kommentare
//		if(s.matches("^\\s*#.*") || s.matches("^\\s*//.*")) // Mittlerweile vmtl. hinfällig // ggf. sonst zuerst charAt!
//			s="";

		final SimpleList<Call> args = new SimpleList<>();

		while(s.length() > 0) {
			final Group2<Call, String> g = Parser_Call.parseArgumentItem(parser, current, s.trim());
			args.add(g.o1);
			s = g.o2;
		}
		return args.toArray(new Call[args.size()]);
	}

	public static Call parseCall(final Parser_Script parser, final Block current, final String s, final boolean isArgument) {
//		MOut.dev("parseCall " + s);

		final Group2<I_Object, String> go = Parser_Call.parseObject(parser, current, s, isArgument);
		return Parser_Call.parseFunction(parser, current, go.o1, go.o2, isArgument);
	}

	public static Call parseCallMulti(final Parser_Script parser, final Block current, final String s, final int key) {
//		MOut.dev("parseCallLoop " + s);
		final Call c = Parser_Call.parseCall(parser, current, s, true);
		final MultiCall mc = new MultiCall(key, c);
		return new Call(current, mc, parser.getDebugInfo());
	}

	public static Call parseFunction(final Parser_Script parser, final Block current, I_Object obj, String s, final boolean isArgument) {
		// Kommentare
		s = Parser_Call.comment(s);

		Call first = null;
		Call last = null;

		while(s != null && s.length() > 0) {
			Call c = null;
			char c0 = s.charAt(0);
			s = s.trim();
			boolean dot = false;

			if(c0 == '.' && !s.startsWith(".", 1)) {
				s = s.substring(1);
				c0 = s.charAt(0);
				if(c0 == ' ' || c0 == '\t')
					throw new CodeError("Space or tab after a dot", "Got: ." + s, parser.getDebugInfo());
				dot = true;
			}

			// Start Event
			if(c0 == '@') {
//				Err.ifNull(obj);	// Object can be null, necessary for stream
				c = Parser_Call.parseEventStart(parser, current, obj, s);
				s = "";
			}
			else if(c0 == '\\') {
				if(s.length() < 2 || s.charAt(1) != '\\')
					throw new CodeError("Invalid block definition", "Missing second '\\', got: " + s, parser);

				s = s.substring(2).trim();

				if(s.length() == 0)
					throw new CodeError("Invalid block definition", "Missing variable or constant after \\\\", parser);

				final I_ParseObject arg = new ParseManagerObj().getParser(current, s, parser, true);
				final Group2<I_Object, String> args = arg.parse(parser, current, s);

				if(first == null)
					first = new Call(current, obj, parser.getDebugInfo());
				if(last == null)
					last = first;

				final I_Object argObj = args.o1;
				if(argObj instanceof I_Mem)
					last.setBlockStore((I_Mem)argObj);
				else if(argObj instanceof A_MemLet)
					last.setBlockStore(((A_MemLet)argObj).getMem());
				else
					throw new CodeError("Invalid argument", "Need Variable, Constant, VarLet or ConstLet, but got: " + Lib_Type.getTypeString(argObj), parser);

				if(args.o2.length() > 0)	//args.o2 != null &&
					throw new CodeError("Invalid function call", "No stream allowed here! Got: " + args.o2, parser);

				return first;
			}
			else {

				if(!dot) {
					// Normal Function
					if(c0 >= 'a' && c0 <= 'z' || c0 == '_')
						if(obj == null || !(obj instanceof A_MagicVar) || !((A_MagicVar)obj).withoutDot())
							throw new CodeError("Missing dot or mathematical sign.", s, parser);
					// Shell-Command
					if(c0 == '´' || c0 == '`')
						throw new CodeError("Missing dot or mathematical sign.", s, parser);
				}
				final I_ParseFunc pf = parser.app.parsemanager_func.getParser(obj, s, parser);
				final Group2<Call, String> res = pf.parse(parser, current, obj, s, isArgument);
				c = res.o1;
				s = res.o2;
			}
			if(first == null)
				first = c;
			if(last != null)
				last.setStream(c);
			last = c;
			obj = null;

			if(s == null || s.length() == 0) {
				final Call last2 = c.searchLastCall();
				if(c.searchLastCall() != null)
					last = last2;
			}
			// Kommentare
			if(s != null)
				s = Parser_Call.comment(s);
		}
		if(first == null)
			//			MOut.dev("First ist null, setze: " + obj);
			first = new Call(current, obj, parser.getDebugInfo());

		return first;
	}

	public static Group2<I_Object, String> parseObject(final Parser_Script parser, final Block current, String s, final boolean isArgument) {
//		MOut.dev("ParseObject: " + s);
		s = s.trim();
		if(s.length() == 0)
			throw new CodeError("Empty substring", "Object or argument is missing!", parser);
		final I_ParseObject po = parser.app.parsemanager_obj.getParser(current, s, parser, isArgument);
		return po.parse(parser, current, s);
	}

	private static String comment(String s) {
		s = s.trim();
		return s.length() == 0 ? "" : s.charAt(0) == '#' ? "" : s;
	}

//	/**
//	 * @return Argument-Calls
//	 */
//	public static Group2<Call[],String> parseArgumentsOpenEnd(final Parser_Script parser, final Block current, String s) {
//		if(s.length() == 0)
//			return new Group2<>(new Call[0], "");
//
//		final Group2<Call, String> g = Parser_Call.parseArgumentItem(parser, current, s);
//		Call[] args = new Call[] {g.o1};
//		return new Group2<>(args, g.o2);
//	}

	private static Group2<Call, String> parseArgumentItem(final Parser_Script parser, final Block current, final String s) {
//		MOut.dev("ParseParameterItem: " + s);

		final Group2<String, String> result = Lib_StringParser.getNextPar(s, parser.getDebugInfo());
		String arg = result.o1;
		final String rem = result.o2;

		final int key = ++Parser_Call.multiCallCounter;

//		// MultiCall
		final int aLen = arg.length();

		if(aLen > 0 && arg.charAt(aLen - 1) == '.') {
			if(!arg.endsWith("..."))
				throw new CodeError("Open end of argument", "Argument ends with a dot: " + s, parser);
			arg = arg.substring(0, arg.length() - 3);
			arg += "¶" + key + ""; // ¢·–º¶←↓→¬¤
			return new Group2<>(Parser_Call.parseCallMulti(parser, current, arg, key), rem);
		}
		return new Group2<>(Parser_Call.parseCall(parser, current, arg, true), rem);
	}

	private static Call parseEventStart(final Parser_Script parser, final Block current, final I_Object obj, final String s) {
		for(final I_ParserEvStart pes : Parser_Call.parsers_EvStart)
			if(pes.hits(s))
				return pes.parse(parser, current, obj, s);
		final String detail = s.indexOf('.') > -1
			? "No attached stream allowed, but got"
			: s.indexOf(' ') > -1
				? "Open argument is currently not allowed"
				: "Got";

		throw new CodeError("Invalid Event-Start", detail + ": " + s, parser);
	}

	private Parser_Call() {}

}
