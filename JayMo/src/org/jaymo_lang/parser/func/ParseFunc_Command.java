/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Modifier;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 14.09.2021
 */
public class ParseFunc_Command implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
		return c0 == '´' || c0 == '`';
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		final Group2<String, String> g = Lib_Parser.scanCommand(s, parser.getDebugInfo());
		final String cmd = g.o1;
		final String[] method_rem = Lib_Modifier.sysCmd(g.o2, "pipeLive", "pipeOutput", "pipeError", "pipeBuffer");

		Call arg0 = null;

		if(cmd.startsWith("(")) {
			final Call[] ca = Parser_Call.parseArguments(parser, current, cmd + ".toStr");
			Lib_Error.ifArgs(ca.length, 1, 1, "Cmd", parser.getDebugInfo());
			arg0 = ca[0];
		}
		else
			arg0 = new Call(current, new Str(cmd), parser.getDebugInfo());
		final Call[] args = {arg0};
		final Call c = new Call(current, obj, method_rem[0], args, parser.getDebugInfo());
		return new Group2<>(c, method_rem[1]);
	}

}
