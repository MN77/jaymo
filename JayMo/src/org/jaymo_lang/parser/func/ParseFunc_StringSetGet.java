/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 02.09.2020
 */
public class ParseFunc_StringSetGet implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
//		return c0 == ':' && s.length() >= 2 && s.charAt(1) != ':';
		return c0 == '$';
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		String rem = s.substring(1);
		final String str = this.iString(rem);
		rem = rem.substring(str.length()).trim();

		if(str.length() == 0)
			throw new CodeError("Invalid key for set/get", "Got empty string for get/set with ':', valid are: A-Z,a-z,0-9,_", parser);

//		boolean lazy = rem.length()>=1 && rem.charAt(0)=='?';
//		if(lazy)
//			rem = rem.substring(1).trim();

		final boolean isSet = rem.length() >= 2 && rem.charAt(0) == '=' && rem.charAt(1) != '=';

		return isSet
			? this.iSet(parser, current, obj, str, rem)
			: this.iGet(parser, current, obj, str, rem);
	}

	private Group2<Call, String> iGet(final Parser_Script parser, final Block current, final I_Object obj, final String str, final String rest) {
		final Call[] aPos = {new Call(current, new Str(str), parser.getDebugInfo())};
		Lib_Error.ifArgs(aPos.length, 1, null, "get", parser.getDebugInfo()); // 1 for List, 2 for Table, 3 for self defined ...
		final Call c = new Call(current, obj, "get", aPos, parser.getDebugInfo());
		return new Group2<>(c, rest);
	}

	private Group2<Call, String> iSet(final Parser_Script parser, final Block current, final I_Object obj, final String str, final String rest) {
		final String arg = rest.substring(rest.indexOf('=') + 1).trim();
		final Call[] aPar = Parser_Call.parseArguments(parser, current, arg);
		Err.ifNot(1, aPar.length);
		final Call cPar = aPar[0];

		final Call[] aPos = {new Call(current, new Str(str), parser.getDebugInfo())};
		// TODO Ist es wirklich möglich, dass aPos.size > 1 ist ?!?!?!
		Lib_Error.ifArgs(aPos.length, 1, null, "set", parser.getDebugInfo());
		final int len = aPos.length;
		final Call[] ca = new Call[len + 1];
		System.arraycopy(aPos, 0, ca, 1, len);
		ca[0] = cPar;

//		final Call c = new Call(current, obj, "set", new Call[]{cPos, cPar}, debugInfo);
		final Call c = new Call(current, obj, "set", ca, parser.getDebugInfo());
		return new Group2<>(c, null);
	}

	private String iString(final String s) {

		for(int p = 0; p <= s.length() - 1; p++) {
			final char c = s.charAt(p);
			if((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9') && c != '_')
				return s.substring(0, p);
		}
		return s;
	}

}
