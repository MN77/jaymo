/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.fdef;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.model.FunctionPar;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Comply;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group3;


/**
 * @author Michael Nitsche
 * @created 27.01.2021
 */
public class ParseFDef {

//	private static final String regex = "^(" + REGEX.FUNCTION_DEF + "(\\s*\\|\\s*"+REGEX.FUNCTION_DEF+")*)\\s*[({]"
//		+ "\\s*" + REGEX.PARDEF_PARS + "\\s*" + "(\\.\\.\\.\\s*)?" + "[)}]\\s*"
//		+ Lib_FDef.FDEF_RETURN
//		+ Lib_FDef.FDEF_RESULT + "$";

//	private static final String regex = "^(" + REGEX.FUNCTION_DEF + "(\\s*\\|\\s*"+REGEX.FUNCTION_DEF+")*)" + "(\\s+|\\s*[({]\\s*[)}]\\s*)?"
//		+ Lib_FDef.FDEF_RETURN + Lib_FDef.FDEF_RESULT + "$";

	private enum PHASE {
		NAMES,
		PARS,
		MODIF,
		RETURNTYPE,
		RESULT
	}


	public Function parse(final Parser_Script parser, final Type type, final String s) {
		final FunctionInfo info = this.scan(parser, s);

		final Group3<FunctionPar[], Integer, Boolean> pars_rem_varargs = Lib_Parser.getFunctionPars(parser, type.getBlock(), info.args, '(', ')'); // TODO Ließe sich noch integrieren

		parser.app.strict.checkFuncResultType(info.returnType, info.returnNil, info.names[0], parser.getDebugInfo());

		final Function f = new Function(type, info.names, pars_rem_varargs.o1, info.control, info.isPrivate, info.returnType, info.returnNil, parser.getDebugInfo());
		if(info.returnDef != null)
			Lib_FDef.setReturnCall(info.returnDef, parser, f);
		if(pars_rem_varargs.o3)
			f.setVarArgs();

		return f;
	}

	private FunctionInfo scan(final Parser_Script parser, final String s) {
		boolean isControl = false;
		boolean returnNil = false;
		boolean brackets = false;
		String[] names = null;
		String args = null;
		String returnType = null;
		String returnDef = null;

		PHASE phase = PHASE.NAMES;
		int start = 0;
		boolean nameLastIsSpace = false;

		for(int i = 0; i < s.length(); i++) {
			final char c = s.charAt(i);

			switch(phase) {
				case NAMES:
					if(c == '(') {
						if(nameLastIsSpace)
							throw new CodeError("Invalid function definition", "Please remove whitespace(s) between function name and '('", parser);
						brackets = true;
						names = Lib_FDef.toNames(parser, s.substring(start, i));
						phase = PHASE.PARS;
						start = i;
						continue;
					}
					if(c == '=') {
						names = Lib_FDef.toNames(parser, s.substring(start, i));
						phase = PHASE.RESULT;
						start = i;
						continue;
					}
					if(nameLastIsSpace && c >= 'A' && c <= 'Z') {
						names = Lib_FDef.toNames(parser, s.substring(start, i));
						phase = PHASE.RETURNTYPE;
						start = i;
						continue;
					}
					if(c == ' ') {
						nameLastIsSpace = true;
						continue;
					}
					if(c == '!') {
						Lib_Parser.checkControl(s, i, parser);
						names = Lib_FDef.toNames(parser, s.substring(start, i));
						isControl = true;
						phase = PHASE.RETURNTYPE;
						i++;
						start = i + 1;
						continue;
					}
					if(c == '?') {
						names = Lib_FDef.toNames(parser, s.substring(start, i));
						returnNil = true;
						phase = PHASE.RESULT;
						start = i + 1;
						continue;
					}
					if(c == '\t')
						throw new CodeError("Invalid function definition", "Please remove tabulator after: " + s.substring(0, i), parser);
					// This will be checked later:
//					if((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9') && c != '_' && c != '|')
//						throw new CodeError("Invalid function name", "Invalid char in function name: " + s, parser.gDebugInfo());

					nameLastIsSpace = false;
					break;

				case PARS:
					if(c == '(') {
						final String group = Lib_Parser.group('(', ')', s.substring(i), parser.getDebugInfo());
						i += group.length() + 1;
						continue;
					}

					if(c == ')') {
						args = s.substring(start, i + 1);
						phase = PHASE.MODIF;
						start = i + 1;
					}
					break;

				case MODIF:
					if(c == '=') {
						phase = PHASE.RESULT;
						start = i;
						break;
					}
					if(c == '?') {
						phase = PHASE.RESULT;
						returnNil = true;
						start = i + 1;
						break;
					}
					if(c == '!') {
						Lib_Parser.checkControl(s, i, parser);
						i++;
						isControl = true;
						start = i + 1;
					}
					phase = PHASE.RETURNTYPE;
					break;

				case RETURNTYPE:
					if(c == ' ')
						continue;

					if(c == '=') {
						phase = PHASE.RESULT;
						start = i;
						break;
					}

					if(c == '?') {
						returnNil = true;
						returnType = s.substring(start, i);
						if(returnType.endsWith(" "))
							throw new CodeError("Invalid function definition", "Return type has spaces between name and modifier: " + returnType + '?', parser);
						returnType = returnType.trim();
						phase = PHASE.RESULT;
						start = i + 1;
						continue;
					}
					break;

				case RESULT:
//					returnDef = s.substring(start).trim();
//					return new FunctionInfo(names, Lib_FDef.isPrivate(names[0]), isControl, args, /* isVarArgs, */ returnType, returnNil, returnDef);
					break;
			}
		}
		if(phase == PHASE.NAMES)
			names = Lib_FDef.toNames(parser, s.substring(start));
		if(phase == PHASE.PARS)
			//			args = s.substring(start);
			throw new CodeError("Invalid function definition", "Missing closing bracket: " + s, parser);

		if(phase == PHASE.RETURNTYPE) {
			returnType = s.substring(start).trim();

			if(returnType.endsWith("?")) {
				returnNil = true;
				returnType = returnType.substring(0, returnType.length() - 1);
			}
		}
		if(phase == PHASE.RESULT)
			returnDef = s.substring(start).trim();

		if(returnType != null && returnType.length() > 0) {
			if(!brackets)
				throw new CodeError("Invalid function definition", "Brackets missing before return type: " + returnType, parser);
			if(!Lib_Comply.checkTypeName(returnType))
				throw new CodeError("Invalid function definition", "Invalid return type: " + returnType, parser);
		}
//		MOut.temp(s, phase, names, Lib_FDef.isPrivate(names[0]), isControl, returnType, returnNil, returnDef);
		return new FunctionInfo(names, Lib_FDef.isPrivate(names[0]), isControl, args, /* isVarArgs, */ returnType, returnNil, returnDef);
	}

}
