/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang._test;

import org.jaymo_lang._test.lib.AutoTest;
import org.jaymo_lang._test.lib.FAIL_ACTION;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.error.Err;
import de.mn77.base.sys.file.SysDir;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class TestAll_Local {

	private static final FAIL_ACTION FAIL       = FAIL_ACTION.SHOW;
	private static final DEBUG_MODE  OUTPUT     = DEBUG_MODE.NO;
	private static final boolean     PARSE_ONLY = false;


	public static void main(final String[] args) {

		try {
			final String testdir = SysDir.current().dirMust("test").dirMust("local").getPathAbsolute();
			AutoTest.testAll(TestAll_Local.FAIL, TestAll_Local.OUTPUT, TestAll_Local.PARSE_ONLY, testdir);
		}
		catch(final Throwable t) {
			Err.exit(Lib_Error.wrap(t));
		}
	}

}
