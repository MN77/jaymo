/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang._test.lib;

import org.jaymo_lang.error.ErrorBase;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;
import org.jaymo_lang.util.ClassFinder;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.cmd.SYSCMD_IO;
import de.mn77.base.sys.cmd.SysCmd;
import de.mn77.base.sys.cmd.SysCmdData;
import de.mn77.base.sys.cmd.SysCmdResult;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.sys.file.MDir;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class AutoTest {

	private final FAIL_ACTION ACTION_AT_FAIL;
//	private final boolean HINTS;
	private final boolean            PARSE_ONLY;
	private final SimpleList<String> slowTests = new SimpleList<>();


	public static void testAll(final FAIL_ACTION fail, final DEBUG_MODE debug, final boolean parseOnly, final String testdir) {
		MOut.setDebug(debug);
//		MOut.setJavaErrors(false);

		try {
			final AutoTest tester = new AutoTest(fail, debug, parseOnly);
			tester.testDir(testdir);
		}
		catch(final Throwable t) {
			Err.exit(Lib_Error.wrap(t));
		}
	}

	public static void testOne(final FAIL_ACTION fail, final DEBUG_MODE debug, final boolean parseOnly, final String test, final I_Directory dir) {
		MOut.setDebug(debug);

		if(test.length() == 0)
			return;
		final String testFile = test.endsWith(".jmo")
			? test
			: test + ".jmo";

		final String testAutoDir = dir.getPathAbsolute();
		final AutoTest tester = new AutoTest(fail, debug, parseOnly);
		tester.testOne(testAutoDir, testFile);
	}

	public AutoTest(final FAIL_ACTION atFail, final DEBUG_MODE debug, final boolean parseOnly) { //, final boolean hints2
		this.ACTION_AT_FAIL = atFail;
//		this.HINTS = hints2;
		this.PARSE_ONLY = parseOnly;
		MOut.setDebug(debug);
//		MOut.onlyStdOut();
	}

	public void testDir(final String s) {

		try {
			final Stopwatch clock = new Stopwatch();
//			MOut.debugMin();
			this.iTestDir(s);
			clock.print();

			if(this.slowTests.size() > 0) {
				MOut.print("\n--- Slow tests ---");
				for(final String st : this.slowTests)
					MOut.print(st);
			}
		}
		catch(final Throwable t) {
			Err.exit(t);
		}
	}

	public void testOne(final String testdir, final String filename) {

		try {
			final Stopwatch timer = new Stopwatch();
			final boolean tr = this.iTest(timer, testdir + Sys.getSeperatorDir() + filename, true);
			if(!tr)
				MOut.print("\n--- Failed ---");
			timer.print();
		}
		catch(final Throwable t) {
			Err.exit(t);
		}
	}

	/**
	 * @return true if the test was successfull
	 */
	private boolean iTest(final Stopwatch timer, final String s, final boolean versionUpdate) throws Exception {
		final I_File testFile = new MFile(s);

		final I_File outFile = new MFile("/tmp/jmo-test.txt");
		if(outFile.exists())
			outFile.delete();

		MOut.print("===== " + s);

		// Clear static
		ClassFinder.getInstance().reset();

		// Prepare
		final Parser_App parser = new Parser_App(versionUpdate);
		parser.setDebug(); // Tests are always with "debug"
//		final App app = parser.parseText("_JMO.setOutputFile(\"/tmp/jmo-test.txt\")");
//		final App app = parser.parseText("> setOutputFile(\"/tmp/jmo-test.txt\")");
		final App app = parser.parseText("> setOutputFile(\"/tmp/jmo-test.txt\")\n> lazyErrors");
		boolean parseError = false;
		timer.resetDiff();

		try {
			parser.parseFile(app, testFile.getFile());
		}
		catch(final ErrorBaseDebug err) {
			parseError = true;
			Lib_TextFile.append(outFile.getFile(), err.toInfo() + '\n');
		}

		if(this.PARSE_ONLY) {
			MOut.print("Parsing: " + (parseError ? "FAIL" : "OK"));
			return !parseError;
		}
		String result = null;
		if(!parseError)
			// Hints
//			if(this.HINTS) {
//				app.setGenHints("/tmp/jmo-types.csv", "/tmp/jmo-hints.csv");
//				app.exec(null);
//				app.hintsSave();
//				app.hintsShow();
//			}
//			else
			try {
				result = app.execToDescribe(null);
			}
			catch(final ErrorBaseDebug eb) {
				Lib_TextFile.append(outFile.getFile(), eb.toInfo() + '\n');
			}
			catch(final ErrorBase eb) {
				if(this.ACTION_AT_FAIL == FAIL_ACTION.STOP)
					Err.exit(eb);
				else
					Err.show(eb);
				return false;
			}

		Lib_TextFile.append(outFile.getFile(), "===== RESULT =====\n" + result + '\n');

		// Vergleich
		final I_File output_saved = testFile.getDirectory().dirFlex("output").fileMay(testFile.getName(), "txt");

		if(!outFile.exists())
			outFile.create(); // Create empty file

		if(!output_saved.exists() && outFile.exists())
			outFile.copy(output_saved);

//		if(!output_test.exists())
//			return true;

		final String[] cmdDiffArgs = {output_saved.getPathAbsolute(), outFile.getPathAbsolute()};
		final SysCmdData sysInfo = new SysCmdData(true, true, SYSCMD_IO.DISCARD, "diff", cmdDiffArgs);
		sysInfo.setWorkingDir(outFile.getDirectory());

		boolean ok = true;

		try {
			final SysCmdResult res = new SysCmd().exec(sysInfo);
			ok = res.result == 0;
		}
		catch(final Exception f) {
			ok = false;
		}
		final long msDiff = timer.getDiffMillisec();
		if(msDiff > 100)
			this.slowTests.add(msDiff + " ms -> " + testFile.getName());

		if(ok) {
			MOut.print("Check: OK    ( " + msDiff + " ms )");
			return true;
		}
		else {
			MOut.print("Check: FAIL");

			if(this.ACTION_AT_FAIL != FAIL_ACTION.TOTAL) {
				// KDIFF3 starten:
				final String[] cmdKDiffArgs = {output_saved.getPathAbsolute(), outFile.getPathAbsolute()};
				final SysCmdData sysInfo2 = new SysCmdData(true, false, SYSCMD_IO.DISCARD, "kdiff3", cmdKDiffArgs);
				sysInfo2.setWorkingDir(outFile.getDirectory());

				try {
					new SysCmd().exec(sysInfo2);
				}
				catch(final Exception f) {}
				// Err.show(f, true);

				if(this.ACTION_AT_FAIL == FAIL_ACTION.EDIT)
//					SysCmd.start(datei.getDirectory(), "mate-terminal -x mcedit '" + datei.getPathAbsolute() + "'", "", true);
//					SysCmd.start(datei.getDirectory(), "xfce4-terminal -x mcedit '" + datei.getPathAbsolute() + "'", "", true);
					SysCmd.exec(true, true, testFile.getDirectory(), "xfce4-terminal", "-x", "mcedit", "'" + testFile.getPathAbsolute() + "'");
//					SysCmd.start(datei.getDirectory(), "sh", true, "-c", "xfce4-terminal", "-x", "mcedit", "'" + datei.getPathAbsolute() + "'");

				if(this.ACTION_AT_FAIL == FAIL_ACTION.SHOW)
					Sys.sleepSeconds(3); // 3	10	15
				else
					System.exit(0);
			}
			return false;
		}
	}

	private void iTestDir(final String s) throws Exception {
		final I_Directory o = new MDir(s);
		final I_List<I_File> list = o.contentFilesWithSuffix("jmo");

		list.sort();

//		MOut.dev(list);
		int fails = 0;
		final SimpleList<String> failed = new SimpleList<>();
		final Stopwatch timer = new Stopwatch();
		boolean first = true;

		for(final I_File d : list) {
			final boolean tr = this.iTest(timer, d.getPathAbsolute(), first);
			first = false;

			if(!tr) {
				fails++;
				failed.add(d.getName());
			}
		}
		final String stop = timer.gStringMillisec();

		if(fails > 0) {
			MOut.print("\n--- Failed ---");
			for(final String f : failed)
				MOut.print(f);
		}
		final String result = "\n"
			+ "Testtime: " + stop + "\n"
			+ "Result  : " + (fails == 0 ? "Passed" : "FAILED (" + fails + ")");
		MOut.print(result);
	}

}
