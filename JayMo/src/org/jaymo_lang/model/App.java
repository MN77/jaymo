/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.util.HashMap;
import java.util.function.Consumer;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.ReturnException;
import org.jaymo_lang.error.RuntimeWarning;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.magic.con.MagicConstSelf;
import org.jaymo_lang.object.pseudo.JMo_Warning;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.parser.ParseManagerFunc;
import org.jaymo_lang.parser.ParseManagerObj;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ErrorComposer;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.event.Procedure;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.I_File;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 */
public class App extends Instance {

	public static final String name     = "App";
	public static final String typeName = "Root";

	private final ImportManager             imports;
	private HashMap<String, MagicConstSelf> magicConsts = null; // Key is without "__"
	private I_File                          outputFile  = null;
	public boolean                          genHints    = false;
//	private final HintManager hintManager = new HintManager();
	private String[]              args              = null;
	private Consumer<String>      outputConsumer    = null;
	public final ParseManagerObj  parsemanager_obj  = new ParseManagerObj();
	public final ParseManagerFunc parsemanager_func = new ParseManagerFunc();
	public final StrictManager    strict            = new StrictManager(this.parsemanager_obj, this.parsemanager_func);
	public final boolean          terminalRawMode;
	private boolean               debug             = false;
	private byte                  terminate         = 0; // 0 = active, 1 = terminate, 2 = shutdown hook, 3 = exit
	private boolean               keep              = false;
	private boolean               noHardExit        = false;
	private SimpleList<Procedure> doAtCalledExit    = null;
	private int                   activeForks       = 0;


	public App(final boolean terminalRawMode, final boolean debug) {
		super(null, null, new Type(App.typeName, null, null, false, false, null, null), null);
		Sys.correctCharset();
		this.terminalRawMode = terminalRawMode;
		this.debug = debug;
		this.imports = new ImportManager();
	}

	public void addMagicConst(final String key, final MagicConstSelf obj, final DebugInfo debug) {
		if(this.magicConsts == null)
			this.magicConsts = new HashMap<>();
		if(this.magicConsts.containsKey(key))
			throw new CodeError("Invalid magic constant", "Constant is already defined: __" + key, debug);
		this.magicConsts.put(key, obj);
	}

	/**
	 * @apiNote This function must always be executed after changes at:
	 *          - a forking Thread ends
	 *          - the value of this.terminate changes
	 */
	public void checkExit(final boolean fromFork) {
		if(fromFork)
			this.activeForks--;

		synchronized(this) {
			this.notify();
		}
	}

	public void describe() {
		MOut.print("Unknown App");
		final CallRuntime cr = new CallRuntime(null, this, null);
		final String s = this.getType().toString(cr, STYPE.DESCRIBE);
		MOut.print(s); // TODO Lib_Output.print?!?
	}

	// --- Events ---

	public final void doAtCalledExit(final Procedure p) {
		if(this.doAtCalledExit == null)
			this.doAtCalledExit = new SimpleList<>();
		this.doAtCalledExit.add(p);
	}

	// --- Functions ---

	public String exec(final CallRuntime cr, final App app) {
		final I_Object result = this.iExec(cr, app);
//		return result == null ? null : result.toString(cr, STYPE.REGULAR);
		return result == null ? null : result.toString();
	}

	public String exec(final String[] args) {
		final CallRuntime cr = new CallRuntime(null, this, null);
		this.args = args;
		final I_Object result = this.iExec(cr, this);
//		return result == null ? null : result.toString(cr, STYPE.REGULAR);
		return result == null ? null : result.toString();
	}

	public String execToDescribe(final String[] args) {
		final CallRuntime cr = new CallRuntime(null, this, null);
		this.args = args;
		final I_Object result = this.iExec(cr, this);
		return result == null ? null : result.toString(cr, STYPE.DESCRIBE);
	}

	/**
	 * °= ^ exit
	 * °exit()App
	 * °exit(Int status)App
	 */
	public void exit(final CallRuntime cr, final int exitState) {
		if(this.doAtCalledExit != null)
			for(final Procedure p : this.doAtCalledExit)
				p.execute();

		this.terminate = 1; // Abort all calls --> normal end of main-function

		if(exitState == 0)
			// Soft exit
			this.checkExit(false);
		else {
			// Hard exit!
			this.iRunShutdownHooks(cr, this);
			if(!this.noHardExit) // TODO: Is otherwise something to do?
				System.exit(exitState);
		}
	}

	public JMo_List getArgs() {
		final int size = this.args == null ? 0 : this.args.length;
		final SimpleList<I_Object> al = new SimpleList<>(size);
		if(size > 0)
			for(final String arg : this.args)
				al.add(new Str(arg));
		final JMo_List list = new JMo_List(al);
		list.setReadOnly();
		return list;
	}

	public I_Object getMagicConst(final String key, final DebugInfo debug) {
		if(this.magicConsts == null)
			throw new CodeError("Invalid magic constant", "Unknown constant: __" + key, debug);
		final I_Object obj = this.magicConsts.get(key);
		if(obj == null)
			throw new CodeError("Invalid magic constant", "Unknown constant: __" + key, debug);
		return obj;
	}

	// --- get ---

	public I_File getOutputFile() {
		return this.outputFile;
	}

//	public HintManager getHintManager() {
//		return this.hintManager;
//	}
//
//	public HintManager getHints() {
//		return this.hintManager;
//	}

	public Type getRootType() {
		return this.getType();
	}

	public void importsAdd(final String basePath, final String file) {
		this.imports.add(basePath, file);
	}

	// --- Hints ---

//	public void hintsSave() {
//		try {
//			this.hintManager.save();
//		}
//		catch(final Err_FileSys e) {
//			Err.show(e);
//		}
//	}
//
//	public void hintsShow() {
//		this.hintManager.show();
//	}


	// --- Imports ---

	public IncludeInfo importsReadNext(final DebugInfo debug) {
		return this.imports.readNext(debug);
	}

	@Override
	public void init(final CallRuntime cr) {
//		Err.forbidden(cr);
	}

	// --- Settings ---

//	public void setGenHints(final String fileTypes, final String fileCalls) {
//		this.genHints = true;
//		this.hintManager.setFiles(fileTypes, fileCalls);
//		try {
//			this.hintManager.load();
//		}
//		catch(final Err_FileSys e) {
//			Err.show(e);
//		}
//	}

	public boolean isDebug() {
		return this.debug;
	}

	public void registerFork() {
		this.activeForks++;
	}

	public void setDebug() {
		this.debug = true;
	}

	public void setKeep(final CallRuntime cr) {
		this.keep = true;
	}

	public void setNoHardExit() {
		this.noHardExit = true;
	}

	/**
	 * @param opf
	 *            = OutputFile, this can be null
	 */
	public void setOutputFile(final I_File opf) {
		this.outputFile = opf;
	}

	public void setOutputRedirection(final Consumer<String> c) {
		this.outputConsumer = c;
	}

	public void terminate() {
		this.terminate = 1;
		this.checkExit(false);
	}

	public boolean toBeTerminated() {
		// Allow "active" and "shutdown hook"
		return this.terminate > 0 && this.terminate != 2;
	}

	@Override
	public boolean validateEvent(final CallRuntime cr, final String event) {

		switch(event) {
			case "@exit":
			case "@warning":
			case "@error":
				return true;
			default:
				return super.validateEvent(cr, event);
		}
	}

	public void warning(final CallRuntime cr, final RuntimeWarning t) {
		final JMo_Warning warning = new JMo_Warning(t);
		this.eventRunRaw(cr, "@warning", warning);

		if(this.strict.getNoWarnings())
			return;

		final ErrorComposer composer = new ErrorComposer(t, cr);
		Lib_Output.err(this, composer.compose(), true);
	}

//	public void unregisterFork() {
//		this.activeForks--;
//	}

	/**
	 * @return Returns true, if the output was already done
	 */
	public boolean writeOutput(final String s, final boolean newline) {

		if(this.outputConsumer != null) {
			this.outputConsumer.accept(newline ? s + '\n' : s);
			return true;
		}
		return false;
	}

	@Override
	protected ObjectCallResult callMethod(final CallRuntime cr, final String method) {
//		this.strict.checkSave(cr, "_APP");
//		switch(method) {
//			case "setOutput":
//				return A_Object.stdResult(this.setOutput(cr));
//			case "args":
//				return A_Object.stdResult(this.getArgs());
//			case "exit":
//				return A_Object.stdResult(this.exit(cr));
//
//			default:
		return super.callMethod(cr, method);

//				// --- Execute self created function ---
//				final Function f = this.getRootType().getFunctions().get(cr, method);
//				I_Object result = null;
//				try {
//
//					result = f.getBlock().execFunction(cr, cr, f);
//				}
//				catch(final ReturnException e) {
//					if(e.get().getLevel() != LEVEL.RETURN) // Ist etwas kleinlich, aber so vmtl. besser und vor allem klarer!
//						throw new CodeError(cr, "Invalid exit for function", "Got " + e.get().getLevel() + ", need " + LEVEL.RETURN);
//					result = e.get().getResult();
//				}
//
//				if(result == null)
//					throw Err.todo(method, cr.getDebugInfo());
//				return new ObjectCallResult(result, f.hasControl());
//		}
	}

	/**
	 * @return Result object or Nil
	 */
	private I_Object iExec(final CallRuntime cr, final App app) {
		app.setMainEnv(cr.vce);

		// ShutdownHook and app.@exit: this don't work for Tests and embeded JayMo.
//		Runtime.getRuntime().addShutdownHook( new Thread(){

		I_Object result = null;

		try {
			result = app.getType().getBlock().execAppRoot(cr);
		}
		catch(final ReturnException e) {
			final Return temp = e.get();

			switch(temp.getLevel()) {
				case RETURN:
					return temp.getResult(); // TODO check this!
				case NEXT:
					throw new CodeError(cr, "Invalid loop control", "Got 'Next' without a loop.");
				case BREAK:
					throw new CodeError(cr, "Invalid loop control", "Got 'Break' without a loop.");
			}
		}
		catch(final Throwable t) {
			Lib_Error.handleThreadErrorEnd(cr, t);
		}
		if(app == this) // so "keep" and "forks" doesn't work for HotExecution!
			this.iWaitForOtherThreads();
		this.iRunShutdownHooks(cr, app);

		return result;
	}

	private void iRunShutdownHooks(final CallRuntime cr, final App app) {
		if(app.terminate > 1)
			return;
		app.terminate = 2; // Allow executing the event
		app.eventRunRaw(cr, "@exit", this);
		app.terminate = 3;
	}


	// Private

	private void iWaitForOtherThreads() {
		while(this.terminate == 0 && (this.activeForks != 0 || this.keep))
			synchronized(this) {

				try {
					this.wait(); // With this, notify in checkExit is needed
//					Thread.sleep(100);	// This way is also possible, then remove notify from checkExit
				}
				catch(final InterruptedException ex) {}
			}
	}

}
