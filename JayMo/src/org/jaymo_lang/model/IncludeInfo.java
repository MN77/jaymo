/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

/**
 * @author Michael Nitsche
 * @created 03.04.2022
 */
public class IncludeInfo {

	public final String fileBase;
	public final String fileInfo;
	public final String code;


	public IncludeInfo(final String fileBase, final String fileInfo, final String code) {
		this.fileBase = fileBase;
		this.fileInfo = fileInfo;
		this.code = code;
	}

}
