/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.error.ReturnException;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.Return.LEVEL;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_MagicVar;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.data.group.Group4;


/**
 * @author Michael Nitsche
 * @created 22.02.2018
 */
public class Function extends A_Object {

	private enum CONTROL_PART {
		BLOCK,
		STREAM
	}


	private final String[]      names;
	private final Block         block;
	private final boolean       control;
	private final FunctionPar[] args;
	private final String        returnType;
	private final boolean       returnNil;
	private final boolean       isPrivate;
	private final Type          typ;
	private boolean             oneLine = false;
	private boolean             varArgs;
	private final int           defaultArgs;


	public Function(final Type typ, final String[] names, final FunctionPar[] args, final boolean control, final boolean isPrivate, final String returnType, final boolean returnNil,
		final DebugInfo debugInfo) {
		this.typ = typ;
		this.names = names;
		this.isPrivate = isPrivate;
		this.block = new Block(typ, this, typ.getBlock());
		this.args = args;

		int defArgCounter = 0;
		if(args != null)
			for(final FunctionPar arg : args) {
				if(arg.isConst)
					this.block.getConstManager().define(arg.name, debugInfo);
				else
					this.block.getVarManager().define(arg.name, debugInfo);

				if(arg.defaultValue != null)
					defArgCounter++;
			}
		this.defaultArgs = defArgCounter;

		this.control = control;
		this.returnType = returnType;
		this.returnNil = returnNil;
	}

	public Block getBlock() {
		return this.block;
	}

	public int getDefaultArgs() {
		return this.defaultArgs;
	}

	/**
	 * @return String or String[] with all names
	 */
	public String[] getNames() {
		return this.names;
	}

	public boolean getReturnNil() {
		return this.returnNil;
	}

	public String getReturnType() {
		return this.returnType;
	}

	public Type getType() {
		return this.typ;
	}

	public FunctionPar[] getVars() {
		return this.args;
	}

	public boolean hasControl() {
		return this.control;
	}

	@Override
	public void init(final CallRuntime cr) {}

	public boolean isOneLine() {
		return this.oneLine;
	}

	public boolean isPrivate() {
		return this.isPrivate;
	}

	public boolean isVarArgs() {
		return this.varArgs;
	}

	public void setOneLine() {
		this.oneLine = true;
	}

	public void setVarArgs() {
		this.varArgs = true;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.typ.getName() + "." + this.names[0]; // + "\n" + this.block.toString();
			default:
				return Lib_Output.appShowPart("::" + this.names[0], this.block.toString(cr, type));
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) { // Sync with MC_CONTROL
//			case "let":
			case "=":
				return this.mResult(cr, 1, 1);
//			case "<<":
//				return this.iResult(cr, 0, 0);
//			case "end":	// 'return' has a better distinction
			case "return":
				return this.mResult(cr, 0, 1);

			case "_init": // To use it the same way as _THIS._init
			case "init":
				return this.iInit(cr);

			case "exportLoop":
				return this.iExportLoop(cr);

			case "hasStream":
				this.iCheckControl(cr, CONTROL_PART.STREAM);
				final Bool res = Bool.getObject(cr.funcOutside.hasStream());
				return A_Object.stdResult(res);
			case "hasBlock":
				this.iCheckControl(cr, CONTROL_PART.BLOCK);
				final Bool resb = Bool.getObject(cr.funcOutside.hasBlock());
				return A_Object.stdResult(resb);

			case "pushStream":
			case "execStream":
				this.iCheckControl(cr, CONTROL_PART.STREAM);
				return A_Object.stdResult(this.mExecStream(cr));

			case "pushBlock":
			case "execBlock":
				// !!! A block should ALWAYS executed INCLUDING the stream --> .push
				this.iCheckControl(cr, CONTROL_PART.BLOCK);
				return this.mExecBlock(cr);

			case "push":
				this.iCheckControl(cr, null);
				return this.mExecBlockStream(cr);

//			// --- Loop ---
//			case ">":
//			case "next":
//				final Return rc = new Return(LEVEL.NEXT, Lib_Exec.onePar(cr, this, 0, 1));
//				return A_Object.stdResult(rc);
//			case ">>":
//			case "break":
//				final Return rb = new Return(LEVEL.BREAK, Lib_Exec.onePar(cr, this, 0, 1));
//				return A_Object.stdResult(rb);

			default:
				Lib_MagicVar.checkForbiddenFuncs(cr, method);
				return null;
		}
	}

	private void iCheckControl(final CallRuntime cr, final CONTROL_PART bs) {
		if(!this.control || cr.funcOutside == null)
			throw new CodeError(cr, "Invalid " + (bs == null ? "block/stream" : bs.name().toLowerCase()) + " access", "This function has no control functionality!");
	}

	private ObjectCallResult iExportLoop(final CallRuntime cr) {
		if(!this.control)
			throw new CodeError(cr, "Function has no control-functionality.", "Function: " + this.names[0]);

//		LoopHandle handle = new LoopHandle(this);	// Methoden-Name ".each"?
		final LoopHandle handle = cr.vce.loopGet(cr);
		if(handle == null)
			throw new CodeError(cr, "No loop found.", "Please place this function inside a loop, to export it");

		cr.funcOutside.vce.setChildLoop(handle);

		return new ObjectCallResult(this, false);
	}

	private ObjectCallResult iInit(final CallRuntime cr) {
		if(!this.control)
			throw new CodeError(cr, "Function has no control-functionality.", "Function: " + this.names[0]);

		final I_Object[] args = cr.argsFlex(null, 0, 2);

		final Group4<Block, CallRuntime, CallRuntime, FunctionPar[]> g = cr.vce.getFuncInit(cr);
		if(g == null)
			throw new JayMoError(cr, "Initializing parameters failed", "Function: " + this.names[0]);

		if(args.length == 0)
			g.o1.initPars(g.o2, g.o3, g.o4, g.o2.getArgs(null, false));
		else {
			final int parNr = Lib_Convert.getIntValue(cr, cr.argType(args[0], A_IntNumber.class));
			Lib_Error.ifNotBetween(cr, 1, g.o4.length, parNr, "Argument position");

			final I_Object argArg = args.length == 1
				? null
				: args[1];
			final I_Object arg = g.o2.getArgAdvance(cr, Nil.NIL, parNr - 1, g.o4, false, argArg);

			final int funcParCount = g.o4.length;
			final I_Object[] initPars = new I_Object[funcParCount];
			initPars[parNr - 1] = arg;
			g.o1.initPars(g.o2, g.o3, g.o4, initPars);
		}
		return A_Object.stdResult(Nil.NIL);
	}

	private ObjectCallResult mExecBlock(final CallRuntime cr) {
		final I_Object result = cr.args(this, I_Object.class)[0];

		if(cr.funcOutside.getCallBlock() == null)
			return A_Object.stdResult(result);

		try {
			final I_Object it2 = cr.funcOutside.getCallBlock().exec(cr, result);
			return A_Object.stdResult(it2);
		}
		catch(final ReturnException e) {
			return new ObjectCallResult(e.get(), true); // Send to upper level
		}
	}

	private ObjectCallResult mExecBlockStream(final CallRuntime cr) {
		I_Object result = cr.args(this, I_Object.class)[0];

		if(cr.funcOutside.getCallBlock() != null)
			try {
				final Group4<Block, CallRuntime, CallRuntime, FunctionPar[]> g = cr.vce.getFuncInit(cr);
				if(g == null)
					throw new JayMoError(null, cr, "Missing initialization of the parameters.", "Function: " + this.names[0]);

				result = cr.funcOutside.getCallBlock().exec(g.o2, result);
			}
			catch(final ReturnException e) {
				return new ObjectCallResult(e.get(), true); // Send to upper level
			}

		if(cr.funcOutside.getStream() != null)
			//					MOut.temp(cr, cr.funcOutside);
			result = cr.funcOutside.execInit(cr.funcOutside.getStream(), result); //!!!!!!!!!!!!!!!!!!!!!!

		return A_Object.stdResult(result);
	}

	private I_Object mExecStream(final CallRuntime cr) {
		final I_Object it = cr.args(this, I_Object.class)[0];
		final CallRuntime crOutside = cr.funcOutside;
		if(crOutside.getStream() == null)
			return it;
		return crOutside.execInit(crOutside.getStream(), it);
	}

	private ObjectCallResult mResult(final CallRuntime cr, final int min, final int max) {
		final I_Object o = Lib_Exec.mSingleArgument(cr, this, min, max, Nil.NIL);
		return A_Object.stdResult(new Return(LEVEL.RETURN, o));
	}

}
