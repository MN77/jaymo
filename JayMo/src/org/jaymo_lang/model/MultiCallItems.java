/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.util.HashMap;

import org.jaymo_lang.object.I_Object;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 05.11.2019
 */
public class MultiCallItems {

	private HashMap<Integer, SimpleList<I_Object>> multicall = null;


	public void add(final int key, final I_Object o) {
		this.iInit(key);
		this.multicall.get(key).add(o);
	}

	public SimpleList<I_Object> get(final int key) {
		this.iInit(key);
		return this.multicall.get(key);
	}

	private void iInit(final int key) {
		if(this.multicall == null)
			this.multicall = new HashMap<>();
		if(!this.multicall.containsKey(key))
			this.multicall.put(key, new SimpleList<>());
	}

}
