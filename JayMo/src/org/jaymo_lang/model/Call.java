/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.object.A_ObjectToString;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Function;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Prio;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Call extends A_ObjectToString {

	public final DebugInfo debugInfo;
	public final I_Object  object; // This could be null ( at Stream )
	public final String    method; // methode | Can be null
	public final byte      prio;
	public final Call[]    argCalls; // arguments
	public final Block     surrounding;
	private Block          ownBlock   = null;
	private Call           stream     = null;
	private I_Mem          blockStore = null; // Raw object needed, so no Call
//	private String[] varsToCopy = null; // Vars to copy for Event handler


	public Call(final Block surrBlock, final I_Object obj, final DebugInfo debugInfo) {
		this(surrBlock, obj, null, null, debugInfo);
	}

	public Call(final Block surrBlock, final I_Object obj, final String met, final Call[] args, final DebugInfo debugInfo) {
		if(met != null && met.length() == 0)
			Err.invalid(met);

		Err.ifNull(surrBlock);
		this.surrounding = surrBlock;
		this.object = obj;
		this.method = met;
		this.argCalls = args; // Kann null sein!
		this.prio = met == null || args != null && args.length != 1 ? 99 : Lib_Prio.streamPrio(met, debugInfo);
		this.debugInfo = debugInfo;
	}

	public Call(final CallRuntime cr, final I_Object obj) {
		this(cr.getSurrBlock(), obj, null, null, cr.getDebugInfo());
	}

	public Call copy(final I_Object obj, final String method, final Call[] args) {
		final Call c2 = new Call(this.surrounding, obj, method, args, this.debugInfo);

		if(this.hasBlock())
			c2.setBlock(this.getBlock());
		if(this.hasStream())
			c2.setStream(this.getStream());
		if(this.blockStore != null)
			c2.setBlockStore(this.blockStore);

		return c2;
	}

	public int getArgCount() {
		return this.argCalls == null
			? 0
			: this.argCalls.length;
	}

	public Block getBlock() {
		return this.ownBlock;
	}

	public I_Mem getBlockStore() {
		return this.blockStore;
	}

	public Call getStream() {
		return this.stream;
	}

	public boolean hasBlock() {
		return this.ownBlock != null;
	}

//	public boolean isStream() {
//		return this.object == null;
//	}

	public boolean hasObject() {
		return this.object != null;
	}

	public boolean hasStream() {
		return this.stream != null;
	}

	/**
	 * Search last Call to add a Block
	 */
	public Call searchLastCall() {
		// TODO nur für Zuweisungen nötig?
		if(this.method != null && Lib_Function.isVarFunction(this.method) > -1 && this.argCalls != null && this.argCalls.length > 0)
			return this.argCalls[0].searchLastCall();

		if(this.stream == null)
			return this;
		return this.stream.searchLastCall();
	}

//	public void setEventCopyVars(final String[] varsToCopy) {
//		this.varsToCopy = varsToCopy;
//	}

//	public String[] getEventCopyVars() {
//		return this.varsToCopy;
//	}

	public void setBlock(final Block b) {
		Err.ifNull(b);

		if(this.ownBlock != null)
			Err.invalid(this.ownBlock, b);
		this.ownBlock = b;
	}

	public void setBlockStore(final I_Mem vc) {
		this.blockStore = vc;
	}

	public void setStream(final Call c) {
		Err.ifNull(c);

		if(this.stream == null)
			this.stream = c;
		else
			throw Err.invalid(this.toString(), this.stream, c);
	}

	@Override
	public String toString() {
		// Object could be null! At Stream for example
		final Group2<String, Boolean> g = Lib_Output.toObjectWithFunction(null, this.object, this.method, false);	//, STYPE.IDENT
		String p = Lib_Output.toString(this.argCalls); //, true
		if(p.length() != 0)
			p = g.o2 ? " " + p : "(" + p + ")";

		final String a = this.ownBlock != null || this.stream != null
			? "…"
			: "";

		return g.o1 + p + a;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.toString(cr, type, false);
	}

	public String toString(final CallRuntime cr, final STYPE type, final boolean replaceMem) {

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.toString();
			case IDENT:
				return this.toStringIdent(cr, true, replaceMem);
			case DESCRIBE:
				return this.toStringDescribe(cr, replaceMem);
			default:
				throw Err.impossible(type);
		}
	}

	private StringBuilder iToString(final CallRuntime cr, final boolean replaceMem) {
		final StringBuilder sb = new StringBuilder();
		I_Object obj = this.object;

		final boolean isAssign = this.method != null && this.method.indexOf('=') >= 0;

		if(replaceMem && obj instanceof I_Mem && ((I_Mem)obj).isInitialized(cr) && !isAssign)
			obj = Lib_Convert.getValue(cr, obj);

		// Very critical:
		final Group2<String, Boolean> g = Lib_Output.toObjectWithFunction(cr, obj, this.method, false);	//, STYPE.DESCRIBE / .IDENT

		sb.append(g.o1);

//		String p = Lib_Output.toString(this.argCalls, true);
		if(this.argCalls != null && this.argCalls.length > 0) {
			sb.append(g.o2 ? ' ' : '(');

			for(int i = 0; i < this.argCalls.length; i++) {
				if(i > 0)
					sb.append(',');
				sb.append(this.argCalls[i].toStringIdent(cr, true, isAssign ? false : replaceMem));
			}
			if(!g.o2)
				sb.append(')');
		}
		return sb;
	}

	private String toStringDescribe(final CallRuntime cr, final boolean replaceMem) {
		final StringBuilder sb = this.iToString(cr, replaceMem);

		if(this.ownBlock != null)
			sb.append("{<Block>}");

		if(this.stream != null)
			sb.append(this.stream.toStringDescribe(cr, replaceMem));

		Lib_Output.removeEnd(sb, '\n');
		return sb.toString();
	}

	private String toStringIdent(final CallRuntime cr, final boolean addBlockStreamChar, final boolean replaceMem) {
		final StringBuilder sb = this.iToString(cr, replaceMem);

		final String bs = addBlockStreamChar && (this.ownBlock != null || this.stream != null)
			? "…"
			: "";

		sb.append(bs);
		return sb.toString();
	}

}
