/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

/**
 * @author Michael Nitsche
 * @created 09.05.2018
 */
public class FunctionPar {

	public final String  typeName;
	public final String  name;
	public final Call    defaultValue;
	public final boolean isConst;


	public FunctionPar(final String typeName, final boolean fix, final String name, final Call defaultValue) {
		this.typeName = typeName;
		this.isConst = fix;
		this.name = name;
		this.defaultValue = defaultValue;
	}

	@Override
	public String toString() {
		String result = "";
		if(this.typeName != null)
			result += this.typeName + " ";
		result += this.name;
		if(this.defaultValue != null)
			result += "=" + this.defaultValue;
		return result;
	}

}
