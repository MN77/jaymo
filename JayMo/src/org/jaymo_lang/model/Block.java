/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.util.Collection;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.error.ReturnException;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.magic.var.MV_THIS;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.object.pseudo.A_MemLet;
import org.jaymo_lang.object.pseudo.NonAtomic;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.Return.LEVEL;
import org.jaymo_lang.object.struct.A_Sequence;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_MagicVar;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Block extends A_Object {

	private static int counter = 0;

	private final int          nr;
	private SimpleList<Call>   items = null;
	private final Block        parent;
	private final Function     func;
	private final Type         typ;
	private final VarManager   vars;
	private final ConstManager consts;


	public Block(final Type typ) {
		this(typ, null, null);
	}

	public Block(final Type typ, final Function func, final Block parent) {
//		super();
		this.typ = typ;
		this.func = func;
		this.parent = parent;

		this.nr = ++Block.counter;
		this.vars = new VarManager(parent == null ? null : parent.getVarManager());
		this.consts = new ConstManager(parent == null ? null : parent.getConstManager());
	}

	public final void add(final Call m) {
		Err.ifNull(m);
		if(this.items == null)
			this.items = new SimpleList<>();
		this.items.add(m);
	}

	public final I_Object exec(CallRuntime cr, final I_Object blockIT) throws ReturnException {
		cr = cr.copyCall(cr.call, true);
		return this.iExec(cr, blockIT, false, true);
	}

	public final I_Object execAppRoot(final CallRuntime cr) throws ReturnException {
		return this.iExec(cr, Nil.NIL, true, false); // root 'it' init
	}

	public I_Object execFunction(final CallRuntime crOutside, final CallRuntime crInside, final Function f) throws ReturnException {
		if(!f.hasControl() && crInside.hasBlock())
			throw new CodeError(crInside, "No Block allowed", "Function has no control functionality: " + crInside.call.toString());

		// Clear all variables
		final CallRuntime crNew = crInside.copyFunction(crOutside);

		// Init parameter
		if(!f.hasControl())
			this.initPars(crOutside, crNew, f.getVars(), crOutside.getArgs(crNew.instance)); // 'cur' init
		else
			crNew.vce.setFuncInit(this, crOutside, crNew, f.getVars());

		// Execute
		I_Object returnValue = null; // The Object that will be returned
		I_Object checkValue = null; // The real Object, maybe within a Return-Object
		boolean throw_return = false;
		final String fReturnType = f.getReturnType();

		// Execute Block
		try {
			returnValue = this.iExec(crNew, Nil.NIL, false, true); // Block 'it' init
			checkValue = returnValue;
		}
		catch(final ReturnException e) { // Abfangen, um den Typ zu testen!
			final Return r = e.get();
			returnValue = r;
			checkValue = r.getResult();
			throw_return = true;
			if(r.getLevel() != LEVEL.RETURN)
				Err.todo(r);
		}

		if(checkValue == null) {
//			returnValue = Lib_AtomConv.getDefaultValue(f.getReturnType());
			returnValue = Nil.NIL;
			checkValue = Nil.NIL;
		}
		// Check the Return-Type
		if(fReturnType != null)
			if(fReturnType.equals("Same")) {
				if(throw_return && !(checkValue == Nil.NIL) && !(checkValue instanceof MV_THIS) && !(checkValue instanceof Instance))
					throw new CodeError(crNew, "Invalid value for return type 'Same'", "Got " + Lib_Type.getTypeString(checkValue.getClass(), checkValue) + ", need '" + MV_THIS.ID + "'");

				final I_Object newValue = crNew.instance;
				returnValue = throw_return
					? new Return(((Return)returnValue).getLevel(), newValue)
					: newValue;
			}
			else {
				final String resultType = Lib_Type.getTypeString(checkValue.getClass(), checkValue);
				final String wantedType = fReturnType;
//				if(checkValue instanceof Return)
//					resultType = ((Return)checkValue).getResult().getTypeName();

				if(!Lib_Type.isInstanceOf(checkValue, wantedType))
					if(resultType.equals("<Nil>")) { //Nil.NIL.toString()
						if(!f.getReturnNil())
							throw new CodeError(crNew, "Invalid return value", "Return value of function is nil");
					}
					else
						throw new CodeError(crNew, "Invalid return type", "Got " + resultType + ", need <" + wantedType + ">");
			}

		// This is only for >high, and will be checked with ResultType
//		if(checkValue == Nil.NIL && !f.getReturnNil() && (fReturnType == null || !fReturnType.equals("Nil")) && crOutside.getStrict().isStrict_FunctionReturnNil())
//			throw new CodeError(crNew, "Invalid return value", "Return value of function is nil");

		if(throw_return)
			throw new ReturnException((Return)returnValue);

		return returnValue;
	}

	/**
	 * @implNote
	 *           Catch Returns
	 */
	public I_Object execOverwrite(final CallRuntime cr, final Instance instance, final Function f) {
		final CallRuntime crInside = cr.copyVCE(instance, instance.getMainEnv(), true);

		if(f.hasControl())
			throw new CodeError(crInside, "Invalid function overwrite", "Function has control-functionality: " + f.getNames()[0]);

		try {
			return f.getBlock().execFunction(cr, crInside, f);
		}
		catch(final ReturnException e) {
			if(e.get().getLevel() != LEVEL.RETURN)
				throw new JayMoError(crInside, "Invalid return from overwritten function!", "Function " + f.toString(crInside, STYPE.IDENT) + " returns " + e.get().getLevel());
			return e.get().getResult();
		}
	}

	/**
	 * Type Constructor
	 */
	public void execTypeBlock(final CallRuntime cr) {

		try {
			this.iExec(cr, Nil.NIL, false, false); // type-block 'it' init
			// Return-Value will be discarded, no use for
		}
		catch(final ReturnException e) {
//			Lib_Exec.checkIsReturn(cpNew2, e);
			throw new CodeError(cr, "Invalid condition", "'" + e.get().toString() + "' in Type-Body is forbidden."); //Return, Break & Next
		}
	}

	public ConstManager getConstManager() {
		return this.consts;
	}

	/**
	 * @return Returns the surrounding function-definition. This can also be null!
	 */
	public Function getFunction(final CallRuntime cr) {
//		return this.func != null ? this.func : cr.getType().getMain(cr);
		return this.func;
	}

	// Only for JayMo-Converter
	public Collection<Call> getItems() {
		return this.items;
	}

	public Block getParent() {
		return this.parent;
	}

	public Type getType() {
		return this.typ;
	}

	public VarManager getVarManager() {
		return this.vars;
	}

	@Override
	public void init(final CallRuntime cr) {}

	/**
	 * Prepare parameters
	 *
	 * @apiNote
	 *          Items of 'args' can be 'null' and would be ignored
	 **/
	public void initPars(final CallRuntime crOld, final CallRuntime crNew, final FunctionPar[] pars, final I_Object[] args) {
		final int argsLen = args == null ? 0 : args.length;
		final int parsLen = pars == null ? 0 : pars.length;
		final boolean hasVarArgs = this.func != null && this.func.isVarArgs() || this.func == null && this.typ != null && this.typ.isVarArgs();
		final int fDefaults = this.func != null ? this.func.getDefaultArgs() : this.typ != null ? this.typ.getDefaultArgs() : 0;

		if(argsLen > parsLen && !hasVarArgs)
			throw new CodeError(crOld, "Too much arguments", "Got " + argsLen + ", need " + parsLen + " argument(s).");

		final int parsNeeded = parsLen - fDefaults - (hasVarArgs ? 1 : 0);
		if(argsLen < parsNeeded)
			throw new CodeError(crOld, "Missing argument or default value", "Got " + argsLen + ", need " + parsNeeded + " argument(s).");

		if(pars != null) {
			Err.ifNull((Object)args);

			int offset = parsLen - argsLen;

			for(int i = parsLen - 1; i >= 0; i--) {
				final FunctionPar par = pars[i];
				final boolean useDefault = offset > 0 && par.defaultValue != null;
				if(useDefault)
					offset--;
				final boolean useVarArgs = hasVarArgs && i == parsLen - 1;
				if(useVarArgs)
					if(argsLen >= parsLen - 1)
						offset = 0;
					else
						offset--;
				final int parIdx = i - offset;
				if(parIdx < 0)
					throw new CodeError(crOld, "Missing argument or default value", "Got " + argsLen + ", need " + parsLen + " argument(s).");

				I_Object arg = null;

				if(useDefault) {
					final I_Object defaultObj = crOld.execInit(par.defaultValue, crNew.instance);
					arg = Lib_MagicVar.replace(defaultObj, crOld, crNew.instance, null);
				}
				else if(useVarArgs) {

					// If argument for VarArg's == MemLet with a Collection, then use that Collection!
					if(argsLen >= 1 && parsLen == argsLen && parIdx == argsLen - 1 && args[parIdx] instanceof A_MemLet) {
						final I_Object paro = ((A_MemLet)args[parIdx]).getMem().get(crOld);
						if(!Lib_Type.typeIs(crOld, paro, A_Sequence.class))
							throw new RuntimeError(crOld, "Invalid type of variable for varargs", "To use Var-Args with a VarLet, the variable must be a <Collection>");
						arg = paro;
					}
					else {
						final SimpleList<I_Object> al = new SimpleList<>(argsLen);

						for(int v = parsLen - 1; v < argsLen; v++) {
							final I_Object p = args[v];

							if(p != null) { // Don't add empty placeholder
								if(par.typeName != null && arg != Nil.NIL && !Lib_Type.isInstanceOf(p, par.typeName)) // Nil will be checked at assigning the value to the var
									throw new CodeError(crOld, "Invalid type of argument in VarArgs", "Got " + p.getType(crNew).toString() + ", need <" + par.typeName + '>');
								al.add(p);
							}
						}
						arg = new JMo_List(al);
					}
				}
				else {
					if(parIdx > argsLen - 1)
						Err.invalid(parIdx, argsLen, parsLen);
					final I_Object par1 = args[parIdx];
					if(par1 == null) // Because of init one single arg
						continue;

					arg = Lib_Convert.getValue(crOld, par1);

//					if(par.typeName != null && !(arg instanceof VarLet) && par1 instanceof Var && crOld.getStrict().isValid_AutoVarLet() && par.typeName.equals(Lib_Type.getName(VarLet.class, null)))
//						arg = new VarLet((Var)par1); // Auto-VarLet wrapping
				}

				if(!useVarArgs && par.typeName != null && arg != Nil.NIL) { // Nil will be checked at assigning the value to the var
					boolean valid = Lib_Type.isInstanceOf(arg, par.typeName);

					if(!valid && arg instanceof A_Atomic && crOld.getStrict().isValid_ArgConvert()) {
						arg = Lib_Convert.toSafeType(crOld, (I_Atomic)arg, par.typeName);
						valid = Lib_Type.isInstanceOf(arg, par.typeName); // Another check
					}
					if(!valid)
						throw new CodeError(crOld, "Invalid type of argument", "Got " + arg.getType(crNew).toString() + ", need <" + par.typeName + '>');
				}
				// Prevent using of Vars/Consts, that are already used in the surrounding Block/Type
				if(this.vars.parentKnowsRaw(par.name))
					throw new CodeError(crOld, "Can't create Var/Const with this name!", "Name is already used/known in this block: " + par.name);

				final I_Mem mem = par.isConst
					? this.consts.use_RunTime(crOld, par.name)
					: this.vars.use_RunTime(crOld, par.name);

				// This is necessary because var.let calls initOnce
				if(arg instanceof NonAtomic)
					arg = ((NonAtomic)arg).create(crNew);

				if(arg instanceof A_MemLet)
					((A_MemLet)arg).checkRegistered(crNew);

				mem.let(crOld, crNew, arg); // TODO Only 'new' here?
//				mem.let(crNew, crNew, arg);
			}
			Err.ifNot(offset, 0);
		}
	}

	public void initTypeBlock(final CallRuntime cpOutside, final CallRuntime cpInside, final I_Object[] args) {
		// Alle bestehenden Variablen löschen! ---> Offensichtlich nicht mehr nötig
//		final CallRuntime cpNew2 = cpInside.copyCall(cpInside.call, !true);// .copy("execTypeBlock - "+this.toString());		// Wozu dies hier überhaupt?!?
		this.initPars(cpOutside, cpInside, cpInside.instance.getType().getVars(), args);
	}

	public boolean isEmpty() {
		return this.items == null || this.items.size() == 0;
	}

	public void reInit(final CallRuntime crOut, final CallRuntime crIn, final I_Object[] args) {
		this.initPars(crOut, crIn, crIn.instance.getType().getVars(), args);
	}

	public void reInit(final CallRuntime crOut, final CallRuntime crIn, final int parsGiven, final int parToInit, final I_Object parValue) {
		final I_Object[] args = new I_Object[parsGiven];
		args[parToInit - 1] = parValue;
		this.initPars(crOut, crIn, crIn.instance.getType().getVars(), args);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(type != STYPE.DESCRIBE)
			return "Block_" + this.nr;

		final StringBuilder sb = new StringBuilder();

		if(this.vars != null) {
			sb.append(Lib_Output.appShowPart("Variables:", this.vars.toString(cr, STYPE.DESCRIBE)));
			sb.append('\n');
		}

		if(this.consts != null && this.consts.getCount() > 0) {
			sb.append(Lib_Output.appShowPart("Constants:", this.consts.toString(cr, STYPE.DESCRIBE)));
			sb.append('\n');
		}

		if(this.items != null) {
			final StringBuilder sbi = new StringBuilder();

			for(final Call c : this.items) {
				final String s = c.toString(cr, STYPE.DESCRIBE);
				sbi.append(s + '\n');
			}
			sb.append(Lib_Output.appShowPart("Calls:", sbi.toString())); // + '\n'	no newline, cause it's the last
		}
		Lib_Output.removeEnd(sb, '\n');
		return Lib_Output.appShowPart(this.toString(cr, STYPE.IDENT), sb.toString());
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		throw Err.forbidden(cr);
//		return null;
	}

	/**
	 * @return [BlockObject,LastObject]
	 * @implNote
	 *           LastObject is only necessary for App and Root-Block --> Exit-Value
	 */
	private final I_Object iExec(final CallRuntime cr, final I_Object blockIT, final boolean returnLast, final boolean setAlternate) throws ReturnException {
		Err.ifNull(blockIT);

		// Create and assign value to "it"
		final Var var = new Var(Var.IT_FULL);
		cr.vce.vars.set(cr, var, blockIT, true, false);

		// Second variable / Alternate for it
		if(setAlternate && cr.call != null && cr.call.getBlockStore() != null)
			cr.call.getBlockStore().let(cr, cr, blockIT);

		// Attached Calls:
		if(this.items == null)
			return null;

		I_Object last_result = null;

		for(final Call c : this.items) {
			final CallRuntime crCall = cr.copyBlockItem(c);

//			Stopwatch sw = new Stopwatch();	// Profile

			final I_Object result = crCall.exec(cr.instance, false);

//			try {
//				long ms = sw.getMillisec();
//				if(ms > 1)
//					Lib_TextFile.append(new File("/tmp/profile.txt"), crCall.getDebugInfo().toString()+"  "+sw.getMillisec()+" ms  ."+ c.method +"\n", true);
//			}
//			catch(Err_FileSys e) {
//				Err.exit(e);
//			}

			last_result = result;

			// Block exited with Return
			if(result instanceof Return)
				throw new ReturnException((Return)result);
		}
		return returnLast ? last_result : var.get(cr);
	}

}
