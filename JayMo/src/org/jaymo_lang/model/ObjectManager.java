/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.util.Collections;
import java.util.ServiceLoader;

import org.jaymo_lang.CoreTypes;
import org.jaymo_lang.api.JMo_Object;
import org.jaymo_lang.api.SandBox;
import org.jaymo_lang.api.TypeDictionary;
import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.pseudo.NonAtomic;
import org.jaymo_lang.object.pseudo.NonAtomicJava;
import org.jaymo_lang.object.pseudo.NonAtomicSelf;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.ClassFinder;
import org.jaymo_lang.util.Lib_Java;
import org.jaymo_lang.util.Lib_Prefix;
import org.jaymo_lang.util.TYPENAMES;

import de.mn77.base.data.search.SearchArray;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.sort.Sort;


/**
 * @author Michael Nitsche
 * @created 26.05.2018
 * @apiNote
 *          Combines Type-String with Object-Type, to create a new Object
 */
public class ObjectManager {

	private static final String         JAVA_TYPE = "Java";
	private static final TypeDictionary coreTypes = new CoreTypes();


	public static SimpleList<String> allTypes(final CallRuntime cr) {
		final SimpleList<String> result = new SimpleList<>();

		// Atomic
		Collections.addAll(result, TYPENAMES.ATOMIC);

		// Abstract
//		for(String s : ABSTRACT.TYPES)
//			result.add(s);

		// Self defined
		final SimpleList<String> self = cr.getApp().getType().getAllTypeNames();
		result.addAll(self);

		// Java Core-Types
		String[] allTypes = ObjectManager.coreTypes.allTypes();
		Collections.addAll(result, allTypes);

		// Java Plugin-Types
		final ServiceLoader<TypeDictionary> services = ServiceLoader.load(TypeDictionary.class);

		for(final TypeDictionary service : services) {
			allTypes = service.allTypes();
			Collections.addAll(result, allTypes);
		}
		// ClassFinder JMo-Objects
		for(final String s : ClassFinder.getInstance().allJMoClasses())
			result.add(s);

		// Clean up
		Sort.sortable(result, false);

		for(int i = result.size() - 1; i >= 1; i--)
			if(result.get(i).equals(result.get(i - 1)))
				result.remove(i);

		return result;
	}

	public static I_Object createNew(final App app, final Block current, final String name, final Call[] args, final DebugInfo debugInfo) {
		// --- Java ---

		if(name.charAt(0) == 'J' && name.startsWith(ObjectManager.JAVA_TYPE)) {
			final int len = name.length();

			if(len == 4) // Java
				return ObjectManager.iJava(app, args, debugInfo);
			else if(len >= 5 && name.charAt(4) == '_') { // Java_
				String javaclass = null;
				if(len >= 6 && name.charAt(5) == '{')
					javaclass = Lib_Java.packConv_FullToClass(name);
				else
					javaclass = Lib_Prefix.cutJava(name);

				final Call[] args2 = {new Call(current, new Str(javaclass), debugInfo), new Call(current, args == null ? new JMo_List() : new JMo_List(args), debugInfo)}; // TODO null statt JMo_List!
				return ObjectManager.iJava(app, args2, debugInfo);
			}
		}
		final boolean nameWithPrefix = Lib_Prefix.isNameWithPrefix(name);
		return ObjectManager.createNew(app, name, nameWithPrefix, args, debugInfo);
	}

	public static boolean isTypeKnown(final CallRuntime cr, final String type) {
		// Atomic
		// Abstract
		if(type.equals("Object") || SearchArray.knows(TYPENAMES.ATOMIC, type, true) || SearchArray.knows(TYPENAMES.ABSTRACT, type, true) || SearchArray.knows(TYPENAMES.KNOWN, type, true))
			return true;

		// Self defined
		final Type t = cr.getApp().getType().getType(type); // cr.getType() ?
		if(t != null)
			return true;

		// Search Java-Type
		if(type.startsWith(ObjectManager.JAVA_TYPE)) {
			final int len = type.length();

			if(len >= 5 && type.charAt(4) == '_') { // Java_
				Class<?> c3 = null;

				if(len >= 6 && type.charAt(5) == '{') { // Java_{
					final String javaType = Lib_Java.packConv_FullToClass(type);
					c3 = ClassFinder.getInstance().searchJavaClassPath(javaType);
				}
				else {
					final String javaType = Lib_Prefix.cutJava(type);
					c3 = ClassFinder.getInstance().searchJavaClass(javaType);
				}
				if(c3 != null)
					return true;
			}
		}
		// JMo-Object
		if(ObjectManager.searchJavaJMoClass(type) != null)
			return true;

		// Nothing found
		return false;
	}

	/**
	 * @return null, if nothing found
	 **/
	public static Class<?> searchJavaJMoClass(final String search) {
		Class<?> result = ObjectManager.coreTypes.lookup(search);
		if(result != null)
			return result;

		final ServiceLoader<TypeDictionary> services = ServiceLoader.load(TypeDictionary.class);

		for(final TypeDictionary service : services) {
			result = service.lookup(search);
			if(result != null)
				return result;
		}
		return ClassFinder.getInstance().searchJMoClass("JMo_" + search);
	}

	@SuppressWarnings("unchecked")
	private static I_Object createNew(final App app, final String name, final boolean withPrefix, final Call[] args, final DebugInfo debugInfo) {

		if(!withPrefix) {
			for(final String r : TYPENAMES.ATOMIC)
				if(name.equals(r))
					throw ObjectManager.errorType(name, debugInfo);
			for(final String r : TYPENAMES.ABSTRACT)
				if(name.equals(r))
					throw ObjectManager.errorType(name, debugInfo);
			for(final String r : TYPENAMES.BLOCKED)
				if(name.equals(r))
					throw ObjectManager.errorType(name, debugInfo);
		}
		// --- Static assigned types ---

		final Class<? extends A_Object> c = ObjectManager.coreTypes.lookup(name);
		if(c != null)
			return new NonAtomic(c, args); // , debugInfo

		// --- Block forbidden types ---

		for(final String block : TYPENAMES.BLOCKED)
			if(block.equals(name))
				throw new CodeError("Invalid type", "Type is private or forbidden: " + name, debugInfo);

		// --- Java ---

//		if(name.charAt(0) == 'J' && name.equals("Java")) {
//			return ObjectManager.iJava(app, args, debugInfo);
//		}

		// --- Search Java-JMo-Type ---

		// Java-JMo-Objects has a higher priority as self defined Types
		final Class<?> c2 = ObjectManager.searchJavaJMoClass(name);
		if(c2 != null)
			if(I_Object.class.isAssignableFrom(c2))
				return new NonAtomic((Class<? extends I_Object>)c2, args);
			else if(JMo_Object.class.isAssignableFrom(c2))
				return new NonAtomic(SandBox.class, c2, args);
			else
				throw new CodeError("Invalid type hierarchy", "JayMo-Types must implements I_Object.class of JMo_Object.class: " + name, debugInfo);

		// --- Script defined Type ---

		// Lower priority as Java-JMo-Objects, because of security and clarity
		final Type t = app.getType().getType(name);
		if(t != null)
			return new NonAtomicSelf(t, args);

		// --- Search Java-Type ---

		// For clarity disabled. If this will be reactivated in future, rewrite it and use Class "NonAtomicJava"
//		final Class<?> c3 = ClassFinder.getInstance(app.isDebug()).getJavaClass(name);
//		if(c3 != null) {
//			app.strict.checkJavaDirect(debugInfo);
//			return new JMo_Java(c3, args, debugInfo);
//		}

		// --- Nothing found ---
		throw new CodeError("Unknown type", name, debugInfo);
	}

	private static ErrorBaseDebug errorType(final String type, final DebugInfo debugInfo) {
		return new CodeError("This type cannot be created via typename!", type, debugInfo);
	}

	private static I_Object iJava(final App app, final Call[] args, final DebugInfo debugInfo) {
		app.strict.checkSandbox(debugInfo, "Java");

//		if(name.length() > 5 && name.charAt(4) == '_')
//			return new JMo_Java(name.substring(5), args, debugInfo);

//		if(args.length < 1)
//			throw new CodeError("Missing argument for Java-Class", "Par-Length: 0", debugInfo);
//		final Call[] parscopy = new Call[args.length - 1];
//		if(args.length > 1)
//			System.arraycopy(args, 1, parscopy, 0, args.length - 1);
//		return new JMo_Java(args[0], parscopy, debugInfo);
//		return new JMo_Java(args, debugInfo);
		return new NonAtomicJava(args);
	}

}
