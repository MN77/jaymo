/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.util.HashMap;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.object.A_ObjectToString;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_Enum;
import org.jaymo_lang.object.pseudo.NonAtomic;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Prefix;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 * @apiNote
 *          Self created Type
 * @implNote
 *           TODO private final zu public final und ohne Funktionen ?!?
 */
public class Type extends A_ObjectToString {

	private final String                 name;
	private final Block                  mainblock;
	private final FuncManager            functions;
	private final EventManager           events;
	private final FunctionPar[]          args;
	private final String                 autoBlockFunction;
	private final boolean                control;
	private final boolean                varargs;
	private final int                    defaultArgs;
	private HashMap<String, Type>        selfTypes = null;
	private HashMap<String, A_Immutable> selfConst = null;
	private final I_Object               parent; // This is NonAtomic or A_Immutable


	public Type(final String name, final FunctionPar[] args, final String abf, final boolean control, final boolean varargs, final Parser_Script parser, final DebugInfo debug) {
		this(name, args, abf, control, varargs, parser, null, debug);
	}

	public Type(final String name, final FunctionPar[] args, final String abf, final boolean control, final boolean varargs, final Parser_Script parser, final I_Object parent, final DebugInfo debug) {
		this.name = Lib_Prefix.addPrefix(name, parser);
		if(parser != null)
			Lib_Type.checkValidity(this.name, parser);
		this.autoBlockFunction = abf;
		this.mainblock = new Block(this);

		if(!(parent == null || parent instanceof NonAtomic || parent instanceof A_Immutable))
			throw new JayMoError("Invalid parent object", "Parent object is not immutable: " + parent, debug);
		this.parent = parent;

		this.functions = new FuncManager();
		this.events = new EventManager();
		this.control = control;
		this.varargs = varargs;
		this.args = args;

		int defArgCounter = 0;
		if(this.args != null)
			for(final FunctionPar arg : args) {
				if(arg.isConst)
					this.mainblock.getConstManager().define(arg.name, debug);
				else
					this.mainblock.getVarManager().define(arg.name, debug);

				if(arg.defaultValue != null)
					defArgCounter++;
			}
		this.defaultArgs = defArgCounter;
	}

	public void addConstant(final String name, final A_Immutable value, final DebugInfo debug) {
		if(this.selfConst == null)
			this.selfConst = new HashMap<>();

		if(this.selfConst.containsKey(name))
			throw new CodeError("Enum-Name already used!", "Enum is already set: " + name, debug);

		this.selfConst.put(name, value);

		if(value instanceof JMo_Enum)
			((JMo_Enum)value).setOrdinal(this.selfConst.size());
	}

	public void addType(final Type typ, final DebugInfo debug) {
		final String newName = typ.getName();
		final String newNameLower = newName.toLowerCase();

		if(this.selfTypes == null)
			this.selfTypes = new HashMap<>();

		for(final String s : this.selfTypes.keySet())
			if(newNameLower.equals(s.toLowerCase())) {
				final String m2 = newName.equals(s) ? "You cannot redefine: " : "You cannot redefine '" + s + "' with: ";
				throw new CodeError("Type-Name already used!", m2 + newName, debug);
			}
		this.selfTypes.put(newName, typ);
	}

	public SimpleList<String> getAllTypeNames() {
		final SimpleList<String> result = new SimpleList<>();
		if(this.selfTypes != null)
			result.addAll(this.selfTypes.keySet());
		return result;
	}

	public String getAutoBlockFunction() {
		return this.autoBlockFunction;
	}

	public Block getBlock() {
		return this.mainblock;
	}

	public int getDefaultArgs() {
		return this.defaultArgs;
	}

	public A_Immutable getEnum(final String name, final DebugInfo debug) {
		final A_Immutable result = this.selfConst == null ? null : this.selfConst.getOrDefault(name, null);
		if(result == null)
			throw new CodeError("Unknown Enum!", "Enum is not known: " + name, debug);
		return result;
	}

	public EventManager getEvents() {
		return this.events;
	}

	public FuncManager getFunctions() {
		return this.functions;
	}

	public String getName() {
		return this.name;
	}

	public I_Object getParent() {
		return this.parent;
	}

	public Type getType(final String type) {
		return this.selfTypes == null ? null : this.selfTypes.getOrDefault(type, null);
	}

	public FunctionPar[] getVars() {
		return this.args;
	}

	public boolean isControl() {
		return this.control;
	}

	public boolean isVarArgs() {
		return this.varargs;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.name;
			case IDENT:
				return "::" + this.name;
			default:
				final StringBuilder sb = new StringBuilder();

				if(this.functions != null) {
					final String sf = this.functions.toString(cr, STYPE.DESCRIBE);
					sb.append(sf);
					sb.append('\n');
				}

				if(this.events != null) {
					final String se = this.events.toString(cr, STYPE.DESCRIBE);
					sb.append(se);
					sb.append('\n');
				}

				if(this.mainblock != null) {
					final String sm = this.mainblock.toString(cr, STYPE.DESCRIBE);
					sb.append(sm);
					sb.append('\n');
				}

				if(this.selfTypes != null)
					for(final Type t : this.selfTypes.values()) {
						final String st = t.toString(cr, STYPE.DESCRIBE);
						sb.append(st);
						sb.append('\n');
					}

				Lib_Output.removeEnd(sb, '\n');
				return Lib_Output.appShowPart(this.toString(cr, STYPE.IDENT), sb.toString());
		}
	}

//	public Function getMain(final CallRuntime cr) {
//		if(this.mainFunction == null)
//			this.mainFunction = new Function(this, "main", null, false, null, false, cr.getDebugInfo());
//		return this.mainFunction;
//	}

}
