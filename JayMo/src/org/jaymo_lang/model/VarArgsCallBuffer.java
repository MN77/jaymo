/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 17.10.2019
 */
public class VarArgsCallBuffer {

	private final Call[] calls;
	private I_Object[]   objects = null;
	private boolean      init    = false; // Only to prevent coding errors, but maybe remove this later!


	public VarArgsCallBuffer(final Call[] ca) {
		Err.ifNull((Object)ca);
		this.calls = ca;
	}

	public I_Object[] get() {
		if(!this.init)
			Err.invalid("VarArgCallBuffer not initialized!", this.calls);
		return this.objects;
	}

	public I_Object[] init(final CallRuntime cr, final I_Object streamIt) {
		if(this.init)
			Err.todo("Already init!", this.objects);
		final I_Object[] result = new I_Object[this.calls.length];
		for(int i = 0; i < this.calls.length; i++)
			result[i] = Lib_Convert.getValue(cr, cr.execInit(this.calls[i], streamIt)); // Init and fetch value from variables
		this.init = true;
		this.objects = result;
		return result;
	}

//	public void replace(final Call c) {
//		this.call = c;
//	}

//	public String toStringIdent(final CallRuntime cr) {
//		return Lib_Output.toStringNested(cr, this.calls);
//	}

	@Override
	public String toString() {
		return Lib_Output.toString(this.calls);
//		return this.objects != null
//			? Lib_Output.toString(this.objects, true)
//			: Lib_Output.toString(this.calls, false);
	}

}
