/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map.Entry;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.ExternalError;

import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.Lib_Jar;
import de.mn77.base.sys.file.Lib_TextFile;


/**
 * @author Michael Nitsche
 * @created 02.04.2022
 */
public class ImportManager {

	private final HashMap<String, Boolean> imports = new HashMap<>(); // Key: Absolute Filepath, Value: isParsed


	public void add(final String basePath, final String file) {
		String filePath = basePath != null && !file.startsWith("/")
			? filePath = basePath + Sys.getSeperatorDir() + file
			: file;

		this.imports.putIfAbsent(filePath, false);
//		MOut.print(this.imports);
	}

	public IncludeInfo readNext(final DebugInfo debug) {
		for(final Entry<String, Boolean> g : this.imports.entrySet())
			if(!g.getValue()) {
				final String filePath = g.getKey();
				this.imports.put(filePath, true);

				final int indexJar = filePath.indexOf("!/");
//				MOut.print(this.baseDirectory, g.getKey(), filePath, indexJar);

				if(indexJar >= 0) {
					final String jarPath = filePath.substring(indexJar + 1);

//					MOut.print(jarPath);
					try {
						final InputStream is = Lib_Jar.getStream(jarPath);
						final String code = Lib_Stream.readUTF8(is);
						final String jarAbsolutePath = Lib_Jar.getAbsolutePath(jarPath);
						final File file = new File(jarAbsolutePath);
						return new IncludeInfo(file.getParent(), jarPath, code);
					}
					catch(final Exception e) {
						throw new ExternalError("Include Jar-File read error", jarPath, debug);
					}
				}
				else {
					final File file = new File(filePath);
					if(!file.exists())
						throw new ExternalError("Include file not found", filePath, debug);

					try {
						final String code = Lib_TextFile.read(file);
						return new IncludeInfo(new File(filePath).getParent(), filePath, code);
					}
					catch(final Exception e) {
						throw new ExternalError("Include file read error", filePath, debug);
					}
				}
			}

		return null;
	}

}
