/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.BLOCKED;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 *
 * @apiNote
 *          This class holds all the var-objects of a Block. This is not the runtime-version!
 */
public abstract class A_VarConstManager<T extends I_Object> {

	private final A_VarConstManager<T> parent;
	private final SimpleList<MODIFIER> varsModifier = new SimpleList<>(); // TODO Why not using the modifiers from varsObject?
	private final SimpleList<String>   varsRaw      = new SimpleList<>();
	private final SimpleList<T>        varsObject   = new SimpleList<>();


	public A_VarConstManager(final A_VarConstManager<T> parent) {
		this.parent = parent;
	}

	/**
	 * At Parse-Time
	 */
	public T define(final String name, final DebugInfo debug) {
		// Check, if real name already used
		final Group2<String, MODIFIER> g = this.iSplitName(name, debug);

		final char c0 = name.charAt(0);

		if(c0 >= 'a' && c0 <= 'z' && Lib_Array.contains(BLOCKED.VAR_CONST_LOWER, g.o1))
			throw new CodeError("Invalid variable!", "This name can't be used: " + g.o1, debug);
		if(c0 >= 'A' && c0 <= 'Z' && Lib_Array.contains(BLOCKED.VAR_CONST_UPPER, g.o1))
			throw new CodeError("Invalid constant!", "This name can't be used: " + g.o1, debug);

		// Create and store new Item
		this.varsRaw.add(g.o1);
		this.varsModifier.add(g.o2);
		final T vo = this.pCreateNew(name);
		this.varsObject.add(vo);
		return vo;
	}

	public int getCount() {
		return this.varsRaw.size();
	}

	public boolean knowsRaw(final String rawname) {
		if(this.varsRaw.contains(rawname) || this.parent != null && this.parent.knowsRaw(rawname))
			return true;
		return false;
	}

	public boolean parentKnowsRaw(final String rawname) {
		if(this.parent != null && this.parent.knowsRaw(rawname))
			return true;
		return false;
	}

	public String toString(final CallRuntime cr, final STYPE type) {
		if(type != STYPE.DESCRIBE)
			return this.toString();

		final StringBuilder sb = new StringBuilder();

		for(final T t : this.varsObject) {
			final String s = t.toString(cr, STYPE.DESCRIBE);
			sb.append(s + '\n');
		}

		if(this.parent != null) {
			final String s = this.parent.toString(cr, STYPE.DESCRIBE);
			sb.append(s);
		}
		Lib_Output.removeEnd(sb, '\n');
		return sb.toString();
	}

	/**
	 * At Parse-Time
	 */
	public T use_ParseTime(final Parser_Script parser, final String name, final boolean onlyDefine) {
		final T vo = this.iUse(parser.app, name, false, parser.getDebugInfo());

		if(vo != null) {

			if(onlyDefine) {
				final char c0 = name.charAt(0);
				final String message = c0 >= 'A' && c0 <= 'Z'
					? "Constant already defined"
					: "Variable already defined";
				throw new CodeError(message, "Name: " + name, parser);
			}
			return vo;
		}
		return this.define(name, parser.getDebugInfo());
	}

	/**
	 * At Run-Time
	 */
	public T use_RunTime(final CallRuntime cr, final String name) {
		return this.iUse(cr.getApp(), name, true, cr.getDebugInfo());
	}


	// Internal //

	/**
	 * Check existing Var, check parent
	 */
	protected T iUse(final App app, final String name, final boolean isRuntime, final DebugInfo debug) {
		if(this.parent == this)
			Err.invalid(name);

		final Group2<String, MODIFIER> g = this.iSplitName(name, debug);

		final int idx = this.varsRaw.indexOf(g.o1);

		if(idx >= 0) {

			if(!isRuntime) {
				final MODIFIER modifOriginal = this.varsModifier.get(idx);

				if(modifOriginal != g.o2) {

					if(g.o2 != MODIFIER.SAFE) {
						final char c0 = g.o1.charAt(0);
						final String vc = c0 >= 'A' && c0 <= 'Z' ? "Const" : "Var";
						throw new CodeError("Invalid modifier for Var/Const", vc + " '" + g.o1 + "' is '" + modifOriginal.title + "' but the query is: '" + g.o2.title + "'", debug);
					}

					if(g.o2 == MODIFIER.SAFE) {
						final String full = g.o1 + (modifOriginal == MODIFIER.NILABLE ? "?" : "??");
						app.strict.checkVarConstShortcut(debug, name, full);
					}
				}
			}
			return this.varsObject.get(idx);
		}
		if(this.parent != null && this.parent.knowsRaw(g.o1))
			return this.parent.iUse(app, name, false, debug);

		if(isRuntime)
			throw Err.forbidden("Access to unknown variable: " + name);
		else
			return null;
	}

	protected abstract T pCreateNew(String name);

	private Group2<String, MODIFIER> iSplitName(final String name, final DebugInfo debug) {
		int len = name.length();
		boolean nilable = false;

		if(len == 0)
			throw new CodeError("Invalid variable/constant", "Missing name of variable/constant", debug);

		final char c_l1 = name.charAt(len - 1);

		if(c_l1 == '?') {
			len--;
			nilable = true;
		}
		final String rawname = name.substring(0, len);
		final MODIFIER modifier = nilable ? MODIFIER.NILABLE : MODIFIER.SAFE;
		return new Group2<>(rawname, modifier);
	}

}
