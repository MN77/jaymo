# JayMo
JayMo is a scripting language for the Java VM.
It is kept lean and simple, is strongly object-oriented and functional.

JayMo is especially suitable for small applications,
with or without a graphical user interface.

Idea, realization & development: Michael Nitsche (DE, Übersee)


## Hello world
Classic version:
	Print "Hello world!"
Flow version:
	"Hello world!".print


## Website
For more information, downloads, documentation, support and contact,
please take a look at:
[www.jaymo-lang.org](https://www.jaymo-lang.org)

Thank you!

