
# CONTRIBUTING
Thank you for your interest and for using JayMo.

JayMo is currently a very time-consuming one-man project.
The development so far took many many hundreds of hours.
So help, also financial, is really welcome.


## What you can do:
* Use it
* Tell your friends
* Report bugs/issues at [Gitlab](https://gitlab.com/MN77/jmo/-/issues)
* Participate in the [Forum](https://jaymo-lang.org/en/board)
* Donate via PayPal to: info@jmo-lang.org
* Share your code or participate in development, please send a mail to: info@jaymo-lang.org

